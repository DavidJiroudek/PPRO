﻿using System.Configuration;


namespace Blivnik.Caching.Configuration
{
    public class CacheOptions 
    {
        public RedisConfiguration RedisConfiguration { get; set; }

        public DualCacheConfiguration DualCacheConfiguration { get; set; }


        public string ServerPrefix { get; set; }
    }




}

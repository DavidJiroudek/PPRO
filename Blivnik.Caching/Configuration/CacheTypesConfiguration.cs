﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Blivnik.Caching.Configuration
{

    /// <summary>
    /// abstract cache element, these are used for cache definition,
    /// every specified element has to be also defined in enum for outer initialization without expicit configuration definition
    /// 
    /// </summary>
    public class Cache
    {
        public CacheType CacheType { get; set; }
        public bool Enabled { get; set; }
        public string CacheManager { get; set; }
        public int Duration { get; set; }
        public int Randomness { get; set; }
        /// <summary>
        /// works only with DUAL cache, sets new time for ASPcache if value is restored for redis
        /// 0 means that data from redis are not stored in ASP cache
        /// </summary>
        public int RestoreTTL { get; set; }


        public string Prefix { get; set; }

    }
}

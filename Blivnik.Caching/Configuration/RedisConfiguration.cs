﻿using System.Configuration;

namespace Blivnik.Caching.Configuration
{
    public class RedisConfiguration
    {
        public string ConnectionString { get; set; }
        public int DatabaseId { get; set; }
        public string ClientName { get; set; }
        public string RedisPassword { get; set; }
        public int ConnectionTimeoutInMs { get; set; }
        public int ConnectionAttempts { get; set; }
    }
}

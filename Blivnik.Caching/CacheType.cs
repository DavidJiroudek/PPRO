﻿namespace Blivnik.Caching
{
    /// <summary>
    /// enum storing used cache types in project
    /// </summary>
    public enum CacheType
    {
        SongCache = 1,
        PageCache = 2,
        ListCache = 3,
        TestCache = 4
    }
}

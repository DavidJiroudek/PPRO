﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Caching
{
    /// <summary>
    /// Internal cache interface, while implementing new cache only this needs to be implemented
    /// </summary>
    public interface ICache
    {
        string Prefix { get; } 
        void SetValue<T>(string key, T value);
        void SetMultipleValues<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class;
        Task SetHashedAsync<T>(string key, IEnumerable<T> items) where T : class;

        Task SetValueAsync<T>(string key, T value);
        Task SetMultipleValuesAsync<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class;


        T GetValue<T>(string key) where T : class;

        Task<T> GetValueAsync<T>(string key) where T : class;

        Task<Dictionary<string, T>> GetMultipleValuesAsync<T>(IEnumerable<string> keys) where T : class;
        /// <summary>
        /// returns set of objects by given keys
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        Dictionary<string, T> GetMultipleValues<T>(IEnumerable<string> keys) where T : class;


        Task<List<T>> GetMultipleValuesAsListAsync<T>(IEnumerable<string> keys) where T : class;
        /// <summary>
        /// faster as dictionary, but you have to pair key with value manually.
        ///  uses same count and order in input and output
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keys"></param>
        /// <returns>null if item doesnt exist in cache</returns>
        List<T> GetMultipleValuesAsList<T>(IEnumerable<string> keys) where T : class;

        Task<IEnumerable<T>> GetHashedAsync<T>(string key) where T : class;

        void DeleteValue(string key);

        Task DeleteValueAsync(string key);

    }
}

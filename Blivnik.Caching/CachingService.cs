﻿using Blivnik.Caching.CacheStores;
using Blivnik.Caching.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Caching
{
    public class CachingService : IKeyValueStore
    {
        private ICache _innerCache;
        public string Prefix { get; }
        public bool _enabled;
        //public IConfiguration Configuration;
        public Cache CacheConfiguration { get; set; }

        public string Separator => ":";



        public CachingService(IOptions<CacheOptions> options, Cache cacheConfiguration)
        {
            var _options = options.Value;
            CacheConfiguration = cacheConfiguration;
            _enabled = cacheConfiguration.Enabled;
            if (_enabled)
            {
                var duration = cacheConfiguration.Duration;
                var randomness = cacheConfiguration.Randomness;
                var restoreTTL = cacheConfiguration.RestoreTTL;
                if (duration + randomness == 0)
                {
                    _enabled = false;
                }
                string serPrefix = _options.ServerPrefix;
                if (!String.IsNullOrEmpty(Environment.GetEnvironmentVariable("ASPNETCORE_INSTANCE_ID")))
                {
                    serPrefix = _options.ServerPrefix + "_" + Environment.GetEnvironmentVariable("ASPNETCORE_INSTANCE_ID");
                }
                Prefix = serPrefix + Separator + cacheConfiguration.Prefix + Separator;

                switch (cacheConfiguration.CacheManager)
                {
                    case "http": _innerCache = new HttpCacheStore(duration, randomness, Prefix); break;
                    case "redis": _innerCache = new RedisCache(duration, randomness, true, _options.RedisConfiguration, Prefix); break;
                    case "dual": _innerCache = new DualCache(duration, randomness, restoreTTL, _options, Prefix); break;
                    //case "fabric": _innerCache = new FabricCache(duration, randomness); break;
                    default: throw new InvalidOperationException("Only available managers are http and redis.");
                }
            }

        }



        /// <summary>
        /// sets value to cache, duration is specified in config
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetValue<T>(string key, T value)
        {
            if (_enabled)
            {
                _innerCache.SetValue(Prefix + key, value);
            }
        }
        /// <summary>
        /// Sets value in new task, doesnt wait for result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetValueAbandonTask<T>(string key, T value)
        {
            if (_enabled)
            {
                Task.Run(() => SetValue<T>(key, value)).ConfigureAwait(false);
            }
        }
        /// <summary>
        /// gets value from cache, null is provided if not found
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetValue<T>(string key) where T : class
        {
            if (_enabled)
            {
                return _innerCache.GetValue<T>(Prefix + key);
            }
            return null;
        }
        /// <summary>
        /// deletes value in cache
        /// </summary>
        /// <param name="key"></param>
        public void DeleteValue(string key)
        {
            if (_enabled)
            {
                _innerCache.DeleteValue(Prefix + key);
            }
        }
        /// <summary>
        /// deletes value in new task, doesnt wait for result
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void DeleteValueAbandonTask(string key)
        {
            if (_enabled)
            {
                Task.Run(() => DeleteValue(key)).ConfigureAwait(false);
            }
        }
        /// <summary>
        /// return dictionary of keys and values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public Dictionary<string, T> GetMultipleValues<T>(IEnumerable<string> keys) where T : class
        {
            if (_enabled)
            {
                keys = keys.Distinct();
                //TODO: try how fast is this backwards key prefix removing, otherwise use inside function storing keys without prefixes to regain original keys
                return _innerCache.GetMultipleValues<T>(keys.Select(i => Prefix + i).ToArray()).Select(i => new KeyValuePair<string, T>(i.Key.Remove(0, Prefix.Length), i.Value)).ToDictionary(x => x.Key, x => x.Value);
            }
            return new Dictionary<string, T>();
        }
        /// <summary>
        /// faster as dictionary, but you have to pair key with value manually.
        ///  uses same count and order in input and output
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keys"></param>
        /// <returns>null if item doesnt exist in cache</returns>
        public List<T> GetMultipleValuesAsList<T>(IEnumerable<string> keys) where T : class
        {
            if (_enabled)
            {
                keys = keys.Distinct();
                return _innerCache.GetMultipleValuesAsList<T>(keys.Select(i => Prefix + i));
            }
            return new List<T>();
        }

        /// <summary>
        /// sets multiple values at once
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="saveItems"></param>
        public void SetMultipleValues<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class
        {
            if (_enabled)
            {
                saveItems = saveItems.Select(i => new Tuple<string, T>(Prefix + i.Item1, i.Item2));
                _innerCache.SetMultipleValues<T>(saveItems);
            }
        }

        /// <summary>
        /// sets values to cache, duration is specified in config
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetMultipleValuesAbandonTask<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class
        {
            if (_enabled)
            {
                Task.Run(() => SetMultipleValues(saveItems)).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// tries to get data, calls function if data are not in cache
        /// caching its results on its own
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="realCall"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public T TryGetCachedData<T>(Func<T> realCall, string key) where T : class
        {
            if (_enabled)
            {
                var val = GetValue<T>(Prefix + key);
                if (val != null)
                {
                    return val;
                }
                var result = realCall();
                _innerCache.SetValue(Prefix + key, result);
                return result;
            }
            return null;
        }

        /// <summary>
        /// sets values to cache, duration is specified in config
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetHashedAbandonTask<T>(string key, IEnumerable<T> items) where T : class
        {
            if (_enabled)
            {
                SetHashedAsync(key, items).ConfigureAwait(false);
            }
        }
        public async Task SetHashedAsync<T>(string key, IEnumerable<T> items) where T : class
        {
            if (_enabled) {
                await _innerCache.SetHashedAsync<T>(Prefix + key, items);
            }

        }
        public async Task<IEnumerable<T>> GetHashedAsync<T>(string key) where T : class
        {
            if (_enabled)
            {
                return await _innerCache.GetHashedAsync<T>(Prefix + key);
            }
            return new List<T>();
        }

        public async Task SetValueAsync<T>(string key, T value)
        {
            if (_enabled)
            {
                await _innerCache.SetValueAsync<T>(Prefix + key, value); // was "not implemented" check if it is implemented everywhere
            }
        }

        public async Task SetMultipleValuesAsync<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class
        {
            if (_enabled) {
                saveItems = saveItems.Select(i => new Tuple<string, T>(Prefix + i.Item1, i.Item2));
                await _innerCache.SetMultipleValuesAsync(saveItems);
            }
        }

        public async Task<T> GetValueAsync<T>(string key) where T : class
        {
            if (_enabled)
            {
                return await _innerCache.GetValueAsync<T>(Prefix + key);
            }
            return null;
        }

        public async Task<Dictionary<string, T>> GetMultipleValuesAsync<T>(IEnumerable<string> keys) where T : class
        {
            if (_enabled)
            {
                return (await _innerCache.GetMultipleValuesAsync<T>(keys.Select(i => Prefix + i).ToArray())).Select(i => new KeyValuePair<string, T>(i.Key.Remove(0, Prefix.Length), i.Value)).ToDictionary(x => x.Key, x => x.Value);
            }
            return null;
        }

        public async Task<List<T>> GetMultipleValuesAsListAsync<T>(IEnumerable<string> keys) where T : class
        {
            if (_enabled)
            {
                return await _innerCache.GetMultipleValuesAsListAsync<T>(keys.Select(i => Prefix + i));
            }
            return null;
        }

        public async Task DeleteValueAsync(string key)
        {
            if (_enabled)
            {
                await _innerCache.DeleteValueAsync(Prefix + key);
            }
        }
    }
}

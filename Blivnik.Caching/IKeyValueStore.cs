using Blivnik.Caching.Configuration;
using System;
using System.Collections.Generic;

namespace Blivnik.Caching
{
    /// <summary>
    /// universal caching interface made for CacheManager
    /// </summary>
    public interface IKeyValueStore : ICache
    {
        Cache CacheConfiguration { get; }

        string Separator { get; }
        /// <summary>
        /// deletes value in another task, doesnt wait for confirmation
        /// </summary>
        /// <param name="key"></param>
        void DeleteValueAbandonTask(string key);
        /// <summary>
        /// sets multiple values in another task, doesnt wait for confirmation
        /// </summary>
        void SetMultipleValuesAbandonTask<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class;
        /// <summary>
        /// sets value in another task, doesnt wait for confirmation
        /// </summary>
        void SetValueAbandonTask<T>(string key, T value);
        /// <summary>
        /// tries to get data from cache, if not found, runs given method
        /// </summary>
        T TryGetCachedData<T>(Func<T> realCall, string cacheKey) where T : class;

        void SetHashedAbandonTask<T>(string key, IEnumerable<T> items) where T : class;
    }
}

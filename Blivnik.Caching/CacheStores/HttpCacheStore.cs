using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using YYProject.XXHash;

namespace Blivnik.Caching.CacheStores
{
    internal class HttpCacheStore : ICache
    {
        private readonly int _duration;
        private readonly int _randomness;
        private readonly Random  random;
        private Microsoft.Extensions.Caching.Memory.IMemoryCache _cache;
        public string Prefix { get; }
        private JsonSerializerSettings _selializerSettings = new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
            ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
            PreserveReferencesHandling = PreserveReferencesHandling.None,
            Formatting = Formatting.None
        };

        public HttpCacheStore(int duration, int randomness, string prefix) {
            this._duration = duration;
            this._randomness = randomness;
            random = new Random();
            var options = new MemoryCacheOptions();
            _cache = new MemoryCache(options);
            Prefix = prefix;
        }

        public void DeleteValue(string key) {
                _cache.Remove(key);
        }


        public T GetValue<T>(string key) where T : class {
            object obj;
            if (_cache.TryGetValue(key, out obj))
            {
                return (T)obj;
            }
            return null;
        }

        public void SetValue<T>(string key, T value) {
            SetValue(key, value, _duration + random.Next(_randomness));
        }
        internal void SetValue<T>(string key, T value, int cacheDuration)
        {
            var expirationTime = DateTime.Now.AddSeconds(cacheDuration);

            var cacheEntryOptions = new MemoryCacheEntryOptions();
            cacheEntryOptions.AbsoluteExpiration = expirationTime;
            _cache.Set(key, value, cacheEntryOptions);
        }

        internal async Task SetValueAsync<T>(string key, T value, int cacheDuration)
        {
            var expirationTime = DateTime.Now.AddSeconds(cacheDuration);

            var cacheEntryOptions = new MemoryCacheEntryOptions();
            cacheEntryOptions.AbsoluteExpiration = expirationTime;
            _cache.Set(key, value, cacheEntryOptions);
        }

        public void SetMultipleValues<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class {
            foreach (var item in saveItems)
            {
                SetValue<T>(item.Item1, item.Item2);
            }
        }

        public Dictionary<string, T> GetMultipleValues<T>(IEnumerable<string> keys) where T : class {
            var retVal = new Dictionary<string, T>();
            foreach (var key in keys)
            {
                object obj;
                if (_cache.TryGetValue(key, out obj) && !retVal.ContainsKey(key))
                {
                    retVal.Add(key, obj as T);
                }
                else
                {
                    if (!retVal.ContainsKey(key))
                    {
                        retVal.Add(key, null);
                    }
                }
            }
            return retVal;
        }

        /// <summary>
        /// faster as dictionary, but you have to pair key with value manually.
        ///  uses same count and order in input and output
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keys"></param>
        /// <returns>null if item doesnt exist in cache</returns>
        public List<T> GetMultipleValuesAsList<T>(IEnumerable<string> keys) where T : class {
            var retVal = new List<T>();
            foreach (var key in keys)
            {
                object obj;
                _cache.TryGetValue(key, out obj);
                retVal.Add(obj as T);
            }
            return retVal;
        }



        public async Task SetValueAsync<T>(string key, T value)
        {
            SetValue(key, value);
        }

        public async Task SetMultipleValuesAsync<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class
        {
             SetMultipleValues(saveItems);
        }

        public async Task<T> GetValueAsync<T>(string key) where T : class
        {
            return GetValue<T>(key);
        }

        public async Task<Dictionary<string, T>> GetMultipleValuesAsync<T>(IEnumerable<string> keys) where T : class
        {
            return GetMultipleValues<T>(keys);
        }

        public async Task<List<T>> GetMultipleValuesAsListAsync<T>(IEnumerable<string> keys) where T : class
        {
            return GetMultipleValuesAsList<T>(keys);
        }

        public async Task DeleteValueAsync(string key)
        {
            DeleteValue(key);
        }

        public async Task SetHashedAsync<T>(string key, IEnumerable<T> items) where T : class
        {
            var algorithm = XXHash64.Create();
            var expirationTime = DateTime.Now.AddSeconds(_duration + _randomness + 1);

            var cacheEntryOptions = new MemoryCacheEntryOptions();
            cacheEntryOptions.AbsoluteExpiration = expirationTime;

            var hashKeys = new List<string>();
            foreach (var item in items)
            {
                var strVal = JsonConvert.SerializeObject(item, _selializerSettings);
                algorithm.ComputeHash(Encoding.UTF8.GetBytes(strVal)); //compute hash, do not use output, use long using bit and convert.tohexstring is faster, but exists in .net 5 only
                var itemKey = Prefix + typeof(T).Name + ":" + algorithm.HashUInt64.ToString();
                _cache.Set(itemKey, strVal); //object is stored serialized, while getting object fastest way to make copy is serialization/deserialization
                hashKeys.Add(itemKey);
            }
            //set keys
            SetValue(key, hashKeys);
        }

        public async Task<IEnumerable<T>> GetHashedAsync<T>(string key) where T : class
        {
            var hashes = GetValue<List<string>>(key);
            if (hashes != null)
            {
                var serialized = GetMultipleValuesAsList<string>(hashes);
                return serialized.Select(i => JsonConvert.DeserializeObject<T>(i));
            }
            return null;
        }
    }
}
﻿using Blivnik.Caching.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Caching.CacheStores
{
    internal class DualCache : ICache
    {
        private readonly int duration;
        private readonly int randomness;
        private readonly int restoredFromRedisTime;
        private readonly Random random;
        private HttpCacheStore aspCache;
        private RedisCache redisCache;
        private static CacheOptions _configuration;

        public string Prefix { get; }


        public DualCache(int duration, int randomness, int restoreTTL, CacheOptions cacheServerConfiguration, string prefix)
        {
            this.duration = duration;
            this.randomness = randomness;
            _configuration = cacheServerConfiguration;
            var redisAdditionalTime = _configuration.DualCacheConfiguration.RedisAdditionInSeconds;
            aspCache = new HttpCacheStore(duration, randomness, prefix);
            redisCache = new RedisCache(duration + redisAdditionalTime, randomness, true, cacheServerConfiguration.RedisConfiguration, prefix);
            restoredFromRedisTime = restoreTTL;
            Prefix = prefix;
        }
        
        public void DeleteValue(string key)
        {
            aspCache.DeleteValue(key);
            redisCache.DeleteValue(key);
        }

        public Dictionary<string, T> GetMultipleValues<T>(IEnumerable<string> keys) where T : class
        {
            var aspVal = aspCache.GetMultipleValues<T>(keys);

            var retDict = new Dictionary<string, T>();
            var toRedis = new List<string>();

            foreach (string key in keys)
            {
                if (aspVal.ContainsKey(key) && aspVal[key] != null)
                {
                    retDict.Add(key, aspVal[key]);
                }
                else
                {
                    toRedis.Add(key);
                    retDict.Add(key, null);
                }
            }
            var redVal = redisCache.GetMultipleValues<T>(toRedis.ToArray());
            foreach (var key in toRedis)
            {
                retDict[key] = redVal[key];
                if (redVal[key] != null && restoredFromRedisTime > 0) {
                    aspCache.SetValue(key, redVal[key], restoredFromRedisTime);
                }
            }

            return retDict;
        }

        public List<T> GetMultipleValuesAsList<T>(IEnumerable<string> keys) where T : class
        {
            //get ASP cache items
            var aspVal = aspCache.GetMultipleValuesAsList<T>(keys);

            //get missing values
            var keyIter = keys.GetEnumerator();
            var missingKeys = new List<string>();
            foreach(var val in aspVal) { 
                keyIter.MoveNext();
                if (val == null)
                {
                    missingKeys.Add(keyIter.Current);
                }
            }

            var retList = new List<T>();
            //search for redis cache items
            if (missingKeys.Any()) {
                var redVal = redisCache.GetMultipleValuesAsList<T>(missingKeys.ToArray());

                //iterate over keys
                var keyIterSec = keys.GetEnumerator();
                var missingKeysIter = missingKeys.GetEnumerator();
                var redIter = redVal.GetEnumerator();

                missingKeysIter.MoveNext(); //select first redis key
                foreach (var val in aspVal) {
                    //here is result for every key, so iterator is in sync
                    keyIterSec.MoveNext();
                    if (keyIterSec.Current == missingKeysIter.Current)
                    {
                        //value was in missing
                        retList.Add(redIter.Current);
                        //saveToAspCache
                        if (restoredFromRedisTime > 0 && redIter.Current != null) {
                            aspCache.SetValue(missingKeysIter.Current, redIter.Current, restoredFromRedisTime);
                        }
                        //iterate
                        missingKeysIter.MoveNext();
                        redIter.MoveNext();

                    }
                    else {
                        //value found in ASP cache
                        retList.Add(val);

                    }
                }


            }
            return retList;
        }

        public T GetValue<T>(string key) where T : class
        {
            var retVal = aspCache.GetValue<T>(key);
            if (retVal == null) {
                if (retVal == null)
                {
                    retVal = redisCache.GetValue<T>(key);
                    if (restoredFromRedisTime > 0 && retVal != null)
                    {
                        aspCache.SetValue(key, retVal, restoredFromRedisTime);
                    }
                }
            }
            return retVal;
        }

        public void SetMultipleValues<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class
        {
            aspCache.SetMultipleValues(saveItems);
            redisCache.SetMultipleValues(saveItems);
        }


        public void SetValue<T>(string key, T value)
        {
            aspCache.SetValue<T>(key, value);
            redisCache.SetValue<T>(key, value);
        }

        public async Task SetValueAsync<T>(string key, T value)
        {
            await aspCache.SetValueAsync(key, value);
            await redisCache.SetValueAsync(key, value);
        }

        public async Task SetMultipleValuesAsync<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class
        {
            await aspCache.SetMultipleValuesAsync(saveItems);
            await redisCache.SetMultipleValuesAsync(saveItems);
        }

        public async Task<T> GetValueAsync<T>(string key) where T : class
        {
            var retVal = await aspCache.GetValueAsync<T>(key);
            if (retVal == null)
            {
                if (retVal == null)
                {
                    retVal = await redisCache.GetValueAsync<T>(key);
                    if (restoredFromRedisTime > 0 && retVal != null)
                    {
                        await aspCache.SetValueAsync(key, retVal, restoredFromRedisTime);
                    }
                }
            }
            return retVal;
        }

        public async Task<Dictionary<string, T>> GetMultipleValuesAsync<T>(IEnumerable<string> keys) where T : class
        {
            var aspVal = await aspCache.GetMultipleValuesAsync<T>(keys);

            var retDict = new Dictionary<string, T>();
            var toRedis = new List<string>();

            foreach (string key in keys)
            {
                if (aspVal.ContainsKey(key) && aspVal[key] != null)
                {
                    retDict.Add(key, aspVal[key]);
                }
                else
                {
                    toRedis.Add(key);
                    retDict.Add(key, null);
                }
            }
            var redVal = await redisCache.GetMultipleValuesAsync<T>(toRedis.ToArray());
            foreach (var key in toRedis)
            {
                retDict[key] = redVal[key];
                if (redVal[key] != null && restoredFromRedisTime > 0)
                {
                    await aspCache.SetValueAsync(key, redVal[key], restoredFromRedisTime);
                }
            }

            return retDict;
        }

        public async Task<List<T>> GetMultipleValuesAsListAsync<T>(IEnumerable<string> keys) where T : class
        {
            //get ASP cache items
            var aspVal = await aspCache.GetMultipleValuesAsListAsync<T>(keys);

            //get missing values
            var keyIter = keys.GetEnumerator();
            var missingKeys = new List<string>();
            foreach (var val in aspVal)
            {
                keyIter.MoveNext();
                if (val == null)
                {
                    missingKeys.Add(keyIter.Current);
                }
            }

            var retList = new List<T>();
            //search for redis cache items
            if (missingKeys.Any())
            {
                var redVal = await redisCache.GetMultipleValuesAsListAsync<T>(missingKeys.ToArray());

                //iterate over keys
                var keyIterSec = keys.GetEnumerator();
                var missingKeysIter = missingKeys.GetEnumerator();
                var redIter = redVal.GetEnumerator();

                missingKeysIter.MoveNext(); //select first redis key
                foreach (var val in aspVal)
                {
                    //here is result for every key, so iterator is in sync
                    keyIterSec.MoveNext();
                    if (keyIterSec.Current == missingKeysIter.Current)
                    {
                        //value was in missing
                        retList.Add(redIter.Current);
                        //saveToAspCache
                        if (restoredFromRedisTime > 0 && redIter.Current != null)
                        {
                            await aspCache.SetValueAsync(missingKeysIter.Current, redIter.Current, restoredFromRedisTime);
                        }
                        //iterate
                        missingKeysIter.MoveNext();
                        redIter.MoveNext();

                    }
                    else
                    {
                        //value found in ASP cache
                        retList.Add(val);

                    }
                }


            }
            return retList;
        }

        public async Task DeleteValueAsync(string key)
        {
            await aspCache.DeleteValueAsync(key);
            await redisCache.DeleteValueAsync(key);
        }

        public async Task SetHashedAsync<T>(string key, IEnumerable<T> items) where T : class
        {
            await aspCache.SetHashedAsync(key, items);
            await redisCache.SetHashedAsync(key, items);
        }

        public async Task<IEnumerable<T>> GetHashedAsync<T>(string key) where T : class
        {
            //TODO: implement http cache
            var data = await aspCache.GetHashedAsync<T>(key);
            if (data != null) {
                return data;
            }
            return await redisCache.GetHashedAsync<T>(key);
        }
    }
}

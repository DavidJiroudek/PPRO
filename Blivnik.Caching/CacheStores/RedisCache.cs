﻿using Blivnik.Caching.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Text;
using YYProject.XXHash;

namespace Blivnik.Caching.CacheStores
{
    internal class RedisCache : ICache
    {
        private static IDatabase _db;
        private static RedisConfiguration _configuration;
        private int duration;
        private int randomness;
        private Random _random;
        private JsonSerializerSettings _selializerSettings = new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
            ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
            PreserveReferencesHandling = PreserveReferencesHandling.None,
            Formatting = Formatting.None
        };
        public string Prefix { get;}


        public RedisCache(int duration, int randomness, bool forceUpdate, RedisConfiguration redisConf, string prefix)
        {
            _configuration = redisConf;
            this.duration = duration;
            this.randomness = randomness;
            _random = new Random();
            if (forceUpdate)
            {
                Reconfigure();
            }
            Prefix = prefix;
        }
        private void Reconfigure()
        {
            ConfigurationOptions options = ConfigurationOptions.Parse(_configuration.ConnectionString);
            options.ClientName = _configuration.ClientName;
            options.Password = _configuration.RedisPassword;
            options.ConnectTimeout = _configuration.ConnectionTimeoutInMs;
            options.ConnectRetry = _configuration.ConnectionAttempts;
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(options);
            _db = redis.GetDatabase(_configuration.DatabaseId);
        }

        public T GetValue<T>(string key) where T : class
        {
            if (_db == null)
            {
                Reconfigure();
            }
            try
            {
                var item = _db.StringGet(key);
                if (item.IsNull)
                {
                    return null;
                }
                //XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                //using (StringReader textReader = new StringReader(item))
                //{
                //    return (T)xmlSerializer.Deserialize(textReader);
                //}
                return JsonConvert.DeserializeObject<T>(item);
            }

            catch (Exception e)
            {
                return null;
            }

            //ZeroFormatterSerializer.Deserialize<T>(val);

        }

        public void SetValue<T>(string key, T value)
        {
            if (_db == null)
            {
                Reconfigure();
            }

            var strVal = JsonConvert.SerializeObject(value,
                new JsonSerializerSettings() {
                    NullValueHandling = NullValueHandling.Ignore,
                    ReferenceLoopHandling = ReferenceLoopHandling.Serialize,
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                });
            try
            {
                //XmlSerializer xmlSerializer = new XmlSerializer(value.GetType());

                //using (StringWriter textWriter = new StringWriter())
                //{
                //    xmlSerializer.Serialize(textWriter, value);
                //    _db.StringSet(key, textWriter.ToString(), TimeSpan.FromSeconds(_duration + _random.Next(_randomness)));
                //}
                var time = new TimeSpan(0,0,duration + _random.Next(randomness));
                _db.StringSet(key, strVal, time);

            }
            catch (Exception e)
            {
            }
        }


        public void SetMultipleValues<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class
        {
            //var values = saveItems.Select(i => new KeyValuePair<RedisKey, RedisValue>((RedisKey)i.Item1, (RedisValue)JsonConvert.SerializeObject(i.Item2))).ToArray();
            //_db.StringSet(values, When.Always, );
            //_db.KeyExpire();
            //#### redis doesnt allow multiple set with duration, there are two ways how to do it
            //   1) just call set for each item
            //   2) call multiple set and then call KeyExpire for each key
            // TODO: compare speed of methods

            foreach (var item in saveItems)
            {
                try
                {
                    SetValue(item.Item1, item.Item2);
                }
                catch (Exception e)
                {
                }
            }
        }
        public async Task SetHashedAsync<T>(string key, IEnumerable<T> items) where T : class
        {
            if (_db == null)
            {
                Reconfigure();
            }

            var algorithm = XXHash64.Create();
            var result = new List<KeyValuePair<RedisKey, RedisValue>>();

            foreach (var item in items)
            {
                var strVal = JsonConvert.SerializeObject(item, _selializerSettings);
                algorithm.ComputeHash(Encoding.UTF8.GetBytes(strVal)); //compute hash, do not use output, use long using bit and convert.tohexstring is faster, but exists in .net 5 only
                result.Add(new KeyValuePair<RedisKey, RedisValue>(Prefix + typeof(T).Name + ":" + algorithm.HashUInt64.ToString(), strVal)); //separate hash pools for better browsing
            }
            //set keys
            await _db.StringSetAsync(result.ToArray());

            //set expiration
            var hashes = new List<string>();
            foreach (var item in result)
            {
                var time = new TimeSpan(0, 0, duration + randomness + 1); //object life muset be longer than list life
                hashes.Add(item.Key);
                await _db.KeyExpireAsync(item.Key, time);

            }
            SetValue(key, hashes);
        }

        public async Task<IEnumerable<T>> GetHashedAsync<T>(string key) where T : class
        {
            var hashes = GetValue<List<string>>(key);
            if (hashes != null){
                return await GetMultipleValuesAsListAsync<T>(hashes);
            }
            return null;
        }


        public Dictionary<string, T> GetMultipleValues<T>(IEnumerable<string> keys) where T : class
        {
            var redKeys = keys.Select(key => (RedisKey)key).ToArray();
            try
            {
                var ret = _db.StringGet(redKeys);


                var retDict = new Dictionary<string, T>();

                var keyIter = keys.GetEnumerator();
                foreach (var item in ret)
                {
                    keyIter.MoveNext();
                    T returnValue = null;
                    if (!item.IsNull)
                    {
                        //XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                        //using (StringReader textReader = new StringReader(item))
                        //{
                        //    returnValue = (T)xmlSerializer.Deserialize(textReader);
                        //}
                        returnValue = JsonConvert.DeserializeObject<T>(item);
                    }
                    if (!retDict.ContainsKey(keyIter.Current))
                    {
                        if (!item.IsNull)
                        {
                            retDict.Add(keyIter.Current, returnValue);
                        }
                        else
                        {
                            retDict.Add(keyIter.Current, null);
                        }
                    }
                }
                return retDict;
            }
            catch (Exception e)
            {
                var retVal = new Dictionary<string, T>();
                foreach (var key in keys) {
                    if (!retVal.ContainsKey(key)) {
                        retVal.Add(key, null);
                    }
                }
                return retVal;
            }
        }

        /// <summary>
        /// faster as dictionary, but you have to pair key with value manually.
        ///  uses same count and order in input and output
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keys"></param>
        /// <returns>null if item doesnt exist in cache</returns>
        public List<T> GetMultipleValuesAsList<T>(IEnumerable<string> keys) where T : class
        {
            var redKeys = keys.Select(key => (RedisKey)key).ToArray();
            try
            {
                var ret = _db.StringGet(redKeys);
                List<T> retVal = new List<T>();
                foreach (var item in ret)
                {
                    T returnValue = null;
                    if (!item.IsNull)
                    {
                        //XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                        //using (StringReader textReader = new StringReader(item))
                        //{
                        //    returnValue = (T)xmlSerializer.Deserialize(textReader);
                        //}
                        returnValue = JsonConvert.DeserializeObject<T>(item);
                    }
                    retVal.Add(returnValue);
                }
                return retVal;
            }
            catch (Exception e)
            {
                var retVal = new List<T>();
                foreach (var key in keys)
                {
                    retVal.Add(null);
                }
                return retVal;
            }
        }

        public void DeleteValue(string key)
        {
            try
            {
                _db.KeyDelete(key);
            }
            catch (Exception e)
            {

            }
        }

        public T TryGetCachedData<T>(Func<T> realCall, string cacheKey) where T : class
        {
            try
            {
                var val = GetValue<T>(cacheKey);
                if (val != null) {
                    return val;
                }
                var result = realCall();
                SetValue(cacheKey, result);
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public async Task SetValueAsync<T>(string key, T value)
        {
            if (_db == null)
            {
                Reconfigure();
            }

            var strVal = JsonConvert.SerializeObject(value,
                _selializerSettings);
            try
            {

                var time = new TimeSpan(0, 0, duration + _random.Next(randomness));
                await _db.StringSetAsync(key, strVal, time);

            }
            catch (Exception e)
            {
            }
        }

        public async Task SetMultipleValuesAsync<T>(IEnumerable<Tuple<string, T>> saveItems) where T : class
        {

            foreach (var item in saveItems)
            {
                try
                {
                    await SetValueAsync(item.Item1, item.Item2);
                }
                catch (Exception e)
                {
                }
            }
        }

        public async Task<T> GetValueAsync<T>(string key) where T : class
        {
            if (_db == null)
            {
                Reconfigure();
            }
            try
            {
                var item = await _db.StringGetAsync(key);
                if (item.IsNull)
                {
                    return null;
                }
                //XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                //using (StringReader textReader = new StringReader(item))
                //{
                //    return (T)xmlSerializer.Deserialize(textReader);
                //}
                return JsonConvert.DeserializeObject<T>(item);
            }

            catch (Exception e)
            {
                return null;
            }

        }

        public async Task<Dictionary<string, T>> GetMultipleValuesAsync<T>(IEnumerable<string> keys) where T : class
        {
            var redKeys = keys.Select(key => (RedisKey)key).ToArray();
            try
            {
                var ret = await _db.StringGetAsync(redKeys);


                var retDict = new Dictionary<string, T>();

                var keyIter = keys.GetEnumerator();
                foreach (var item in ret)
                {
                    keyIter.MoveNext();
                    T returnValue = null;
                    if (!item.IsNull)
                    {
                        returnValue = JsonConvert.DeserializeObject<T>(item);
                    }
                    if (!retDict.ContainsKey(keyIter.Current))
                    {
                        if (!item.IsNull)
                        {
                            retDict.Add(keyIter.Current, returnValue);
                        }
                        else
                        {
                            retDict.Add(keyIter.Current, null);
                        }
                    }
                }
                return retDict;
            }
            catch (Exception e)
            {
                var retVal = new Dictionary<string, T>();
                foreach (var key in keys)
                {
                    if (!retVal.ContainsKey(key))
                    {
                        retVal.Add(key, null);
                    }
                }
                return retVal;
            }
        }

        public async Task<List<T>> GetMultipleValuesAsListAsync<T>(IEnumerable<string> keys) where T : class
        {
            var redKeys = keys.Select(key => (RedisKey)key).ToArray();
            try
            {
                var ret = await _db.StringGetAsync(redKeys);
                List<T> retVal = new List<T>();
                foreach (var item in ret)
                {
                    T returnValue = null;
                    if (!item.IsNull)
                    {
                        returnValue = JsonConvert.DeserializeObject<T>(item);
                    }
                    retVal.Add(returnValue);
                }
                return retVal;
            }
            catch (Exception e)
            {
                var retVal = new List<T>();
                foreach (var key in keys)
                {
                    retVal.Add(null);
                }
                return retVal;
            }
        }

        public async Task DeleteValueAsync(string key)
        {
            try
            {
               await _db.KeyDeleteAsync(key);
            }
            catch (Exception e)
            {

            }
        }
    }
}


﻿using Autofac;
using Blivnik.Caching.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Blivnik.Caching
{
    public class CachingModule : Autofac.Module
    {
        private IConfiguration _config;

        public CachingModule() {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var cb = new ConfigurationBuilder()
             .AddJsonFile("cacheSettings.json")
             .AddJsonFile($"cacheSettings.{env}.json", optional: true)
             .AddJsonFile("cacheSettings.Test.json", true)
             .AddEnvironmentVariables("BLIVNIK_");
            _config = cb.Build();
             
        }

        protected override void Load(ContainerBuilder builder)
        {
            var cacheOptions = new CacheOptions();
            _config.GetSection("CacheOptions").Bind(cacheOptions);
            //default cache
            var defaultCache = new CachingService(Options.Create(cacheOptions), new Configuration.Cache() {
                Enabled = false,
                CacheType = CacheType.SongCache,
                CacheManager = "http",
                Duration = 0
            });
            
            builder.RegisterInstance(defaultCache).As<IKeyValueStore>().SingleInstance();

            var cacheConfigItems = _config.GetSection("CacheTypes").GetChildren();
            foreach (var item in cacheConfigItems) {
                var cache = new Configuration.Cache();
                item.Bind(cache);
                var cacheInstance = new CachingService(Options.Create(cacheOptions), cache);
                builder.RegisterInstance(cacheInstance).Keyed<IKeyValueStore>(cache.CacheType).SingleInstance();
            }

        }
    }
}

﻿using Blinvik.Search.Models;
using System;
using System.Globalization;

namespace Blivnik.Database.Models
{
    public class BookFilter
    {
        public int Priority { get; set; }
        public DateTime Date { get; set; }
        public PublicableType Publicable { get; set; }

        public BookFilter() { }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Database.Models
{
    public class Song
    {
        public int? SongId { get; set; }
        public DateTime? Date { get; set; }
        public string? SongName { get; set; }
        public int? Publicable { get; set; }
        public int? Priority { get; set; }
        public int? Capo { get; set; }
        public bool? Validated { get; set; }
        public string? Url { get; set; }
        public bool? Reprint { get; set; }
        public int? Creator { get; set; }
        public int? Editor { get; set; }
        public string? Text { get; set; }
        public IEnumerable<(string name, int type)>? Authors { get; set; }
        public IEnumerable<int>? Rythms { get; set; }
    }
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Blivnik.Database.SQL
{
    public class NullableSqlDataReader : IDisposable
    {
        protected SqlDataReader m_SqlDataReader;

        public NullableSqlDataReader(SqlDataReader reader)
        {
            m_SqlDataReader = reader ?? throw new ArgumentNullException("reader");
        }

        public bool HasRows() {
            return m_SqlDataReader.HasRows;
        }

        public bool Read()
        {
            return m_SqlDataReader.Read();
        }

        public bool ReadNextSet()
        {
            return m_SqlDataReader.NextResult();
        }

        public DataTable GetSchemaTable()
        {

            return m_SqlDataReader.GetSchemaTable();
        }

        public bool ContainsColumn(string columnName)
        {
            return this.GetSchemaTable().Rows.Cast<DataRow>().Any(r => r["ColumnName"].ToString().ToLowerInvariant().Equals(columnName.ToLowerInvariant()));
        }

        public byte[] GetNullableBytes(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return (byte[])m_SqlDataReader.GetValue(ordinal);
        }

        public string GetNullableString(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetString(ordinal);
        }

        public string GetNullableString(int ordinal)
        {
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetString(ordinal);
        }

        public object GetNullableValue(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetValue(ordinal);
        }

        public bool? GetNullableBoolean(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetBoolean(ordinal);
        }

        public int? GetNullableInt16(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetInt16(ordinal);
        }

        public int? GetNullableInt32(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetInt32(ordinal);
        }

        public long? GetNullableInt64(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetInt64(ordinal);
        }

        public decimal? GetNullableDecimal(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetDecimal(ordinal);
        }

        public decimal? GetNullableDecimalFromNullableFloat(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return Convert.ToDecimal(m_SqlDataReader.GetDouble(ordinal));
        }

        public double? GetNullableDouble(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetDouble(ordinal);
        }

        public DateTime? GetNullableDateTime(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetDateTime(ordinal);
        }

        public Guid? GetNullableGuid(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return null;
            return m_SqlDataReader.GetGuid(ordinal);
        }


        public int GetInt16(string columnName)
        {
            var ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
            {
                return 0;
            }
            return m_SqlDataReader.GetInt16(ordinal);
        }

        public int GetInt32(string columnName)
        {
            //int? i = GetNullableInt32(columnName);
            //if (i == null) {
            //  i = 0;
            //}
            var ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
            {
                return 0;
            }
            return m_SqlDataReader.GetInt32(ordinal);
        }

        public int GetInt32(int ordinal)
        {
            //int? i = GetNullableInt32(columnName);
            //if (i == null) {
            //  i = 0;
            //}

            if (m_SqlDataReader.IsDBNull(ordinal))
            {
                return 0;
            }
            return m_SqlDataReader.GetInt32(ordinal);
        }
        public long GetInt64(int ordinal)
        {

            if (m_SqlDataReader.IsDBNull(ordinal))
            {
                return 0;
            }
            return m_SqlDataReader.GetInt64(ordinal);
        }


        public byte GetByte(string columnName)
        {
            return m_SqlDataReader.GetByte(m_SqlDataReader.GetOrdinal(columnName));
        }

        public string GetString(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return string.Empty;
            return m_SqlDataReader.GetString(ordinal);
        }

        public Guid GetGuid(string columnName)
        {
            return m_SqlDataReader.GetGuid(m_SqlDataReader.GetOrdinal(columnName));
        }

        public bool GetBoolean(string columnName)
        {
            return m_SqlDataReader.GetBoolean(m_SqlDataReader.GetOrdinal(columnName));
        }
        public bool GetBooleanFromBit(string columnName)
        {
            return m_SqlDataReader.GetBoolean(m_SqlDataReader.GetOrdinal(columnName));
        }
        public string GetBinaryAsString(string columnName)
        {
            return Convert.ToBase64String(m_SqlDataReader.GetSqlBinary(m_SqlDataReader.GetOrdinal(columnName)).Value);
        }


        public DateTime GetDateTime(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
                return new DateTime();
            return m_SqlDataReader.GetDateTime(m_SqlDataReader.GetOrdinal(columnName));
        }

        public decimal GetDecimal(string columnName)
        {
            int ordinal = m_SqlDataReader.GetOrdinal(columnName);
            if (m_SqlDataReader.IsDBNull(ordinal))
            {
                return 0;
            }
            return m_SqlDataReader.GetDecimal(m_SqlDataReader.GetOrdinal(columnName));
        }


        #region IDisposable Members

        public void Dispose()
        {
            this.m_SqlDataReader.Dispose();
        }

        #endregion
    }
}

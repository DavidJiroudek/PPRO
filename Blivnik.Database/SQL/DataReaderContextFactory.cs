﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Blivnik.Database.SQL
{
    public class DataReaderContextFactory : IDataReaderContextFactory
    {
        private Dictionary<string, string> _cs { get; set; }
        private static string _defaultConnection { get { return "Mssql"; } }
        public DataReaderContextFactory(IOptions<Dictionary<string, string>> connectionStrings) {
            _cs = connectionStrings.Value;
        }


        public SqlDataReaderContextAsync GetReaderContext(string connectionStringName)
        {
            return GetReaderContext(string.Empty, CommandType.StoredProcedure, connectionStringName);
        
        }

        public SqlDataReaderContextAsync GetReaderContext(string storedProcedureName, string connectionStringName)
        {
            return GetReaderContext(storedProcedureName, CommandType.StoredProcedure, connectionStringName);
        }

        public SqlDataReaderContextAsync GetReaderContext(string commandText, CommandType commandType)
        {
            return new SqlDataReaderContextAsync(commandText, commandType, ResolveConnectionString(null));

        }

        public SqlDataReaderContextAsync GetReaderContext(string commandText, CommandType commandType, string connectionStringName)
        {
            return GetReaderContext(commandText, commandType, connectionStringName, null);
        
        }

        public SqlDataReaderContextAsync GetReaderContext(string commandText, CommandType commandType, params SqlParameter[] commandParameters) {
            return GetReaderContext(commandText, commandType, null, commandParameters);
        }

        public SqlDataReaderContextAsync GetReaderContext(string commandText, CommandType commandType, string connectionStringName, params SqlParameter[] commandParameters)
        {
            return new SqlDataReaderContextAsync(commandText, commandType, ResolveConnectionString(connectionStringName), commandParameters);
        }

        private string ResolveConnectionString(string name) {
            if (name == null) {
                name = _defaultConnection;
            }
            if (_cs.ContainsKey(name)) {
                return _cs[name];
            }
            throw new IndexOutOfRangeException("Connection string named "+ name  + " is not configured");
        }
    }
}

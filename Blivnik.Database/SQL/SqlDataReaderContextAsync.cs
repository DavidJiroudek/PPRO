﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Blivnik.Database.SQL
{
    public class SqlDataReaderContextAsync : SqlDataReaderContext
    {


        #region INIT

        public SqlDataReaderContextAsync(string storedProcedureName, string connectionString)
                : this(storedProcedureName, CommandType.StoredProcedure, connectionString, null)
        {
        }
        public SqlDataReaderContextAsync(string storedProcedureName, CommandType commandType, string connectionString)
            : this(storedProcedureName, commandType, connectionString, null)
        {

        }
        public SqlDataReaderContextAsync(string storedProcedureName, CommandType commandType, string connectionString, params SqlParameter[] commandParameters)
            : base(storedProcedureName, commandType, connectionString, commandParameters)
        {

        }

        #endregion

        /// <summary>
        /// predava rizeni next result set na datalayer tridu, nelze napsat obecne
        /// pouzit pak MoveNextRecordSet, CreateObject, CreateColelction
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethod">metoda kam se preda rizeni vytvareni objektu a cteni z readeru</param>
        /// <returns></returns>
        public async Task<T> CreateCompositeObjectAsync<T>(Func<SqlDataReaderContextAsync, Task<T>> readerContentMethod, int number = 0)
        {
            SqlConnection conn = new SqlConnection(m_ConnectionString);
            T result = default(T);

            var cmd = m_Command;
            cmd.CommandTimeout = 6000;
            m_Command.Connection = conn;
            cmd.Prepare();
            await conn.OpenAsync();
            SqlDataReader reader = await cmd.ExecuteReaderAsync();
            m_ActualReader = new NullableSqlDataReaderAsync(reader);
            result = await readerContentMethod(this);

            reader.Close();
            conn.Close();
            return result;
        }



        /// <summary>
        /// pro vztvoreni kolekce objektu, otevre a zavre connection a provede command
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethod">metoda pro inicializaci objektu</param>
        /// <returns></returns>
        public IEnumerable<T> ProcessCollectionLazy<T>(CreateInstance<T> readerContentMethod, int number = 0)
        {
            using (SqlConnection conn = new SqlConnection(m_ConnectionString))
            {
                using (var cmd = new SqlCommand(m_CommandText, conn))
                {
                    cmd.CommandTimeout = 6000;
                    conn.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        m_ActualReader = new NullableSqlDataReaderAsync(reader);
                        while (m_ActualReader.Read())
                        {
                            yield return readerContentMethod.Invoke(m_ActualReader);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// pro vztvoreni kolekce objektu, otevre a zavre connection a provede command
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethod">metoda pro inicializaci objektu</param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> ProcessCollectionAsync<T>(CreateInstance<T> readerContentMethod, int number = 0)
        {
            SqlConnection conn = new SqlConnection(m_ConnectionString);
            var cmd = m_Command;
            cmd.CommandTimeout = 6000;
            m_Command.Connection = conn;
            List<T> result = new List<T>();
            await conn.OpenAsync();
            cmd.Prepare();
            SqlDataReader reader = await cmd.ExecuteReaderAsync();

            m_ActualReader = new NullableSqlDataReaderAsync(reader);
            while (m_ActualReader.Read())
            {
                result.Add(readerContentMethod.Invoke(m_ActualReader));
            }
            reader.Close();
            conn.Close();
            return result;
        }

        /// <summary>
        /// recte a vztvori kolekci objektu - musi byt naplnen reader a spusten command
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethodMain"></param>
        /// <returns></returns>
        public new IEnumerable<T> CreateCollectionLazy<T>(CreateInstance<T> readerContentMethodMain)
        {
            if (m_ActualReader == null)
            {
                throw new ApplicationException("Run CreateCompositeObject method first");
            }

            while (m_ActualReader.Read())
            {
                yield return readerContentMethodMain.Invoke(m_ActualReader);
            }
        }
        /// <summary>
        /// recte a vztvori kolekci objektu - musi byt naplnen reader a spusten command
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethodMain"></param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> CreateCollectionAsync<T>(CreateInstance<T> readerContentMethodMain)
        {
            if (m_ActualReader == null)
            {
                throw new ApplicationException("Run CreateCompositeObject method first");
            }
            List<T> result = new List<T>();
            while (await m_ActualReader.ReadAsync())
            {
                result.Add(readerContentMethodMain.Invoke(m_ActualReader));
            }
            return result;
        }

        /// <summary>
        /// prejde na dalsi dataset a vrati true pokud dalsi dataset existuje
        /// </summary>
        /// <returns></returns>
        public async Task<bool> MoveToNextRecordSetAsync()
        {
            if (m_ActualReader == null)
            {
                throw new ApplicationException("Run CreateCompositeObject method first");
            }
            return await m_ActualReader.ReadNextSetAsync();
        }

        // TODO: log errors
        //protected void LoggError(Exception ex)
        //{
        //    if (m_Logg == null)
        //        m_Logg = new FischerLogger(this);
        //    m_Logg.Error($"SQL error, cmd:{m_CommandText}", ex);
        //}

        protected SqlParameter CopyParam(SqlParameter originalParam)
        {
            return new SqlParameter(originalParam.ParameterName, originalParam.Value)
            {
                SqlDbType = originalParam.SqlDbType,
                TypeName = originalParam.TypeName,
                UdtTypeName = originalParam.UdtTypeName
            };
        }

    }
}

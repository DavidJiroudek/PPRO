﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Database.SQL
{
    public class NullableSqlDataReaderAsync : NullableSqlDataReader
    {
        public NullableSqlDataReaderAsync(SqlDataReader reader) : base(reader)
        {
        }

        public Task<bool> ReadAsync()
        {
            return m_SqlDataReader.ReadAsync();
        }

        public Task<bool> ReadNextSetAsync()
        {
            return m_SqlDataReader.NextResultAsync();
        }

    }
}

﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Xml.Linq;

namespace Blivnik.Database.SQL
{
    public delegate T CreateInstance<T>(NullableSqlDataReader reader);

    public class SqlDataReaderContext : IDisposable
    {
        #region Fields and Properities
        protected SqlCommand m_Command;
        protected string m_ConnectionString;
        protected string m_CommandText;


        protected NullableSqlDataReaderAsync m_ActualReader;

        public SqlParameterCollection Parameters
        {
            get { return m_Command.Parameters; }
        }

        #endregion

        #region INIT


        public SqlDataReaderContext(string connectionStringName)
            : this(string.Empty, CommandType.StoredProcedure, connectionStringName)
        {
        }

        public SqlDataReaderContext(string storedProcedureName, string connectionStringName)
            : this(storedProcedureName, CommandType.StoredProcedure, connectionStringName)
        {
        }

        public SqlDataReaderContext(string commandText, CommandType commandType, string connectionStringName)
            : this(commandText, commandType, connectionStringName, null)
        {
        }

        public SqlDataReaderContext(string commandText, CommandType commandType, params SqlParameter[] commandParameters) 
           : this(commandText, commandType, "Mssql", commandParameters)
        { }       

        public SqlDataReaderContext(string commandText, CommandType commandType, string connectionStringName, params SqlParameter[] commandParameters)
        {
            m_ConnectionString = connectionStringName;
            if (commandText == null)
                throw new ArgumentNullException("commandText");
            m_Command = new SqlCommand(commandText);
            m_Command.CommandTimeout = 6000;
            m_CommandText = commandText;
            if (commandParameters != null)
            {
                foreach (SqlParameter paremeter in commandParameters)
                {
                    m_Command.Parameters.Add(paremeter);
                }
            }
            m_Command.CommandType = commandType;
        }
        #endregion


        /// <summary>
        /// executes command without return data
        /// </summary>
        public void Execute() {
            using (SqlConnection conn = new SqlConnection(m_ConnectionString))
            {
                m_Command.Connection = conn;
                conn.Open();
                m_Command.ExecuteReader();
            }
        }

        /// <summary>
        /// pro command co vrati jeden radek
        /// vraci prave jeden objekt
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethod"></param>
        /// <returns></returns>
        public T Process<T>(CreateInstance<T> readerContentMethod)
        {
            using (SqlConnection conn = new SqlConnection(m_ConnectionString))
            {
                m_Command.Connection = conn;
                conn.Open();
                T result = default(T);
                using (SqlDataReader reader = m_Command.ExecuteReader())
                {
                    m_ActualReader = new NullableSqlDataReaderAsync(reader);
                    result = CreateObject(readerContentMethod);;
                }
                return result;
            }
        }

        /// <summary>
        /// pro vztvoreni kolekce objektu, otevre a zavre connection a provede command
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethod">metoda pro inicializaci objektu</param>
        /// <returns></returns>
        public IEnumerable<T> ProcessCollection<T>(CreateInstance<T> readerContentMethod)
        {
            using (SqlConnection conn = new SqlConnection(m_ConnectionString))
            {

                m_Command.Connection = conn;
                conn.Open();
                using (SqlDataReader reader = m_Command.ExecuteReader())
                {
                    m_ActualReader = new NullableSqlDataReaderAsync(reader);
                    return CreateCollection(readerContentMethod);
                }
            }
        }

        /// <summary>
        /// predava rizeni next result set na datalayer tridu, nelze napsat obecne
        /// pouzit pak MoveNextRecordSet, CreateObject, CreateColelction
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethod">metoda kam se preda rizeni vytvareni objektu a cteni z readeru</param>
        /// <returns></returns>
        public T CreateCompositeObject<T>(Func<SqlDataReaderContext, T> readerContentMethod)
        {
            using (SqlConnection conn = new SqlConnection(m_ConnectionString))
            {
                m_Command.Connection = conn;
                m_Command.CommandTimeout = 6000;
                T result = default(T);
                conn.Open();
                using (SqlDataReader reader = m_Command.ExecuteReader())
                {
                    m_ActualReader = new NullableSqlDataReaderAsync(reader);
                    result = readerContentMethod.Invoke(this);
                }
                return result;
            }

        }

        /// <summary>
        /// prejde na dalsi dataset a vrati true pokud dalsi dataset existuje
        /// </summary>
        /// <returns></returns>
        public bool MoveToNextRecordSet()
        {
            if (m_ActualReader == null)
            {
                throw new ApplicationException("Run CreateCompositeObject method first");
            }
            return m_ActualReader.ReadNextSet();
        }

        /// <summary>
        /// precte a vytvori objekt - musi byt naplnen reader a spusten command
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethodMain"></param>
        /// <returns></returns>
        public T CreateObject<T>(CreateInstance<T> readerContentMethodMain)
        {
            if (m_ActualReader == null)
            {
                throw new ApplicationException("Run CreateCompositeObject method first");
            }
            T result = default(T);
            if (!m_ActualReader.HasRows()) {
                return result;
            }
            while (m_ActualReader.Read())
            {
                result = readerContentMethodMain.Invoke(m_ActualReader);
            }
            return result;
        }

        /// <summary>
        /// recte a vztvori kolekci objektu - musi byt naplnen reader a spusten command
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethodMain"></param>
        /// <returns></returns>
        public IEnumerable<T> CreateCollectionLazy<T>(CreateInstance<T> readerContentMethodMain)
        {
            if (m_ActualReader == null)
            {
                throw new ApplicationException("Run CreateCompositeObject method first");
            }
            yield return readerContentMethodMain.Invoke(m_ActualReader);
        }
        /// <summary>
        /// recte a vztvori kolekci objektu - musi byt naplnen reader a spusten command
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readerContentMethodMain"></param>
        /// <returns></returns>
        public IEnumerable<T> CreateCollection<T>(CreateInstance<T> readerContentMethodMain)
        {
            if (m_ActualReader == null)
            {
                throw new ApplicationException("Run CreateCompositeObject method first");
            }
            List<T> result = new List<T>();
            while (m_ActualReader.Read())
            {
                result.Add(readerContentMethodMain.Invoke(m_ActualReader));
            }
            return result;
        }

        public XElement ExecuteProcedure(string procedureName, params KeyValuePair<string, object>[] parameters)
        {
            var xml = default(XElement);

            using (var connection = new SqlConnection(m_ConnectionString))
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = procedureName;
                    connection.Open();

                    if (parameters != null)
                    {
                        command.Parameters.AddRange(parameters.Select(parameter => new SqlParameter(parameter.Key, parameter.Value)).ToArray());
                    }

                    using (var reader = command.ExecuteXmlReader())
                    {
                        xml = reader.Read() ? XElement.Load(reader) : default(XElement);
                    }
                }
            }

            return xml;
        }

        public void Dispose()
        {
            m_ActualReader.Dispose();
            m_Command.Dispose();

        }
    }
}

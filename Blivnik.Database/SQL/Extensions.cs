﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;

namespace Blivnik.Database.SQL
{
    public static class Extensions
    {
        public static DataTable GetDataTableNoDistinct(this int[] ids, string columnName)
        {
            DataTable table = new DataTable();
            table.Columns.Add(columnName, typeof(int));
            foreach (int id in ids)
            {
                table.Rows.Add(id);
            }
            return table;
        }
        public static DataTable GetDataTable(this int[] ids, string columnName)
        {
            DataTable table = new DataTable();
            table.Columns.Add(columnName, typeof(int));
            foreach (int id in ids.Distinct())
            {
                table.Rows.Add(id);
            }
            return table;
        }

        public static DataTable GetDataTable(this string[] ids, string columnName)
        {
            DataTable table = new DataTable();
            table.Columns.Add(columnName, typeof(string));
            foreach (string id in ids.Distinct())
            {
                table.Rows.Add(id);
            }
            return table;
        }

        public static SqlParameter CreateParemeter(this int[] ids, string column, string parametr, bool distinct = false)
        {
            SqlParameter p4 = new SqlParameter(parametr, SqlDbType.Structured);
            if (ids != null)
            {
                var table = new DataTable();
                if (distinct)
                {
                    table = ids.GetDataTableNoDistinct(column);
                }
                else
                {
                    table = ids.GetDataTable(column);
                }

                p4.Value = table;
            }
            return p4;
        }

        public static SqlParameter CreateParametr(this string[] codes, string parametr)
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("value", typeof(int));

            foreach (var id in codes)
            {
                var row = table.NewRow();
                row["ID"] = id;
                row["value"] = -1;
                table.Rows.Add(row);
            }
            SqlParameter p4 = new SqlParameter(parametr, SqlDbType.Structured);
            p4.Value = table;
            return p4;
        }

        public static SqlParameter CreateParemeter(this IDictionary<string, int[]> ids, string parametr)
        {
            DataTable table = new DataTable();
            table.Columns.Add("ID", typeof(string));
            table.Columns.Add("value", typeof(int));

            var sel = ids.SelectMany(p => p.Value.Select(i => new { ID = p.Key, Value = i }));

            foreach (var id in sel)
            {
                var row = table.NewRow();
                row["ID"] = id.ID;
                row["value"] = id.Value;
                table.Rows.Add(row);
            }
            SqlParameter p4 = new SqlParameter(parametr, SqlDbType.Structured);
            p4.Value = table;
            return p4;
        }

        public static SqlParameter CreateParameter(this bool val, string parametr)
        {
            SqlParameter p4 = new SqlParameter(parametr, SqlDbType.Bit);
            p4.Value = val;
            return p4;
        }

        public static SqlParameter CreateParameter(this bool? val, string parametr)
        {
            SqlParameter p4 = new SqlParameter(parametr, SqlDbType.Bit);
            p4.Value = val;
            return p4;
        }

        public static SqlParameter CreateParameter(this int? val, string parametr)
        {
            SqlParameter p4 = new SqlParameter(parametr, SqlDbType.Int);
            p4.Value = val;
            return p4;
        }

        public static SqlParameter CreateParameter(this int val, string parametr)
        {
            SqlParameter p4 = new SqlParameter(parametr, SqlDbType.Int);
            p4.Value = val;
            return p4;
        }
        public static SqlParameter CreateParameter(this DateTime val, string parameter)
        {
            SqlParameter p4 = new SqlParameter(parameter, SqlDbType.DateTime);
            p4.Value = val;
            return p4;
        }
        public static SqlParameter CreateParameter(this DateTime? val, string parameter)
        {
            SqlParameter p4 = new SqlParameter(parameter, SqlDbType.DateTime);
            p4.Value = val;
            return p4;
        }

        public static SqlParameter CreateParameter(this string val, string parameter)
        {
            SqlParameter p4 = new SqlParameter(parameter, SqlDbType.NVarChar);
            p4.Value = val;
            return p4;
        }
       
        public static void AddTableParameter(this SqlParameterCollection cmd, string paramName, IEnumerable<int> data)
        {
            var newPar = cmd.Add(paramName, SqlDbType.Structured);
            newPar.TypeName = "frontend.IntField";
            newPar.Value = data.ToArray().GetDataTable("id");
        }

        public static void AddTableParameter(this SqlCommand cmd, string paramName, IEnumerable<int> data)
        {
            cmd.Parameters.AddTableParameter(paramName, data);
        }
        //checks if conversion to SqlDateTime would be valid
        public static bool ValidSqlDateTime(this DateTime date) {
            if (date < SqlDateTime.MinValue.Value || date > SqlDateTime.MaxValue.Value) {
                return false;
            }
            return true;
        }

        public static DataTable CreateIdTable(this IEnumerable<int> ids, string columnName)
        {
            DataTable table = new DataTable();
            table.Columns.Add(columnName, typeof(int));
            foreach (int id in ids)
            {
                table.Rows.Add(id);
            }
            return table;
        }
        public static DataTable CreateIdTable(this IEnumerable<string> ids, string columnName)
        {
            DataTable table = new DataTable();
            table.Columns.Add(columnName, typeof(string));
            foreach (var id in ids)
            {
                table.Rows.Add(id);
            }
            return table;
        }
        //public static SqlParameter CreateUserDefinedTableTypeParam<T>(this List<T> values, string columnName, string tableTypeName, string parameterName )
        //{
        //    var musicAuthors = new SqlParameter(parameterName, System.Data.SqlDbType.Structured);

        //    DataTable table = new DataTable();
        //    table.Columns.Add("stringValue", typeof(T));

        //    foreach (var name in values)
        //    {
        //        var row = table.NewRow();

        //        row["stringValue"] = name;
        //        table.Rows.Add(row);
        //    }
        //    musicAuthors.Value = table;
        //    musicAuthors.TypeName = "dbo.nvarchar_Table";
        //    return musicAuthors;
        //}

    }
}

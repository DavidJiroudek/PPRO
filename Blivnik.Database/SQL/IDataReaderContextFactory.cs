﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Blivnik.Database.SQL
{
    public interface IDataReaderContextFactory
    {
        SqlDataReaderContextAsync GetReaderContext(string connectionStringName);
        SqlDataReaderContextAsync GetReaderContext(string storedProcedureName, string connectionStringName);

        SqlDataReaderContextAsync GetReaderContext(string commandText, CommandType commandType);
        SqlDataReaderContextAsync GetReaderContext(string commandText, CommandType commandType, string connectionStringName);
        SqlDataReaderContextAsync GetReaderContext(string commandText, CommandType commandType, params SqlParameter[] commandParameters);
        SqlDataReaderContextAsync GetReaderContext(string commandText, CommandType commandType, string connectionStringName, params SqlParameter[] commandParameters);
    }
}

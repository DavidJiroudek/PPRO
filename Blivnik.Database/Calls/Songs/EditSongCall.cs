﻿using Blivnik.Database.Models;
using Blivnik.Database.SQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Database.Calls.Songs
{
    public class EditSongCall
    {

        private IDataReaderContextFactory _dataReader;
        public EditSongCall(IDataReaderContextFactory dataReader)
        {
            _dataReader = dataReader;
        }
        public void EditSong(Song data)
        {


            var songId = data.SongId.CreateParameter("@songId");
            var songName = data.SongName.CreateParameter("@name");
            var publicable = data.Publicable.CreateParameter("@publicable");
            var priority = data.Priority.CreateParameter("@priority");
            var ytURL = data.Url.CreateParameter("@youtubeURL");
            var creator = data.Creator.CreateParameter("@creator");
            var editor = data.Editor.CreateParameter("@editor");
            var songText = data.Text.CreateParameter("@value");
            var reprint = data.Reprint.CreateParameter("@reprint");
            var capo = data.Capo.CreateParameter("@capo");
            var authors = new SqlParameter("@authors", SqlDbType.Structured);



            DataTable table = new DataTable();
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Type", typeof(int));

            foreach (var author in data.Authors)
            {
                var row = table.NewRow();

                row["Name"] = author.name;
                row["Type"] = author.type;
                table.Rows.Add(row);
            }
            authors.Value = table;
            authors.TypeName = "dbo.AuthorsData_Table";


            var rythms = new SqlParameter("@rythms", SqlDbType.Structured);
            table = new DataTable();
            table.Columns.Add("Id", typeof(int));

            foreach (var author in data.Rythms)
            {
                var row = table.NewRow();

                row["Id"] = author;
                table.Rows.Add(row);
            }
            rythms.Value = table;
            rythms.TypeName = "dbo.Int_Table";


            var reader = _dataReader.GetReaderContext("dbo.EditSong", CommandType.StoredProcedure, songId, reprint, songName, publicable, priority, capo, ytURL, creator, editor, authors, rythms, songText);
            reader.Execute();



        }

    }
}

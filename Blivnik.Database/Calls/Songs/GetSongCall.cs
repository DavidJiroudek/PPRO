﻿using Blivnik.Database.Models;
using Blivnik.Database.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Database.Calls.Songs
{
    public class GetSongCall
    {
        private IDataReaderContextFactory _dataReader;
        public GetSongCall(IDataReaderContextFactory dataReader)
        {
            _dataReader = dataReader;
        }



        public Song GetSong(int songId)
        {
            var songIdParam = songId.CreateParameter("@songId");

            var reader = _dataReader.GetReaderContext("dbo.GetSong", System.Data.CommandType.StoredProcedure, songIdParam);

            return reader.CreateCompositeObject(CreateSongElastic);

        }

        private Song CreateSongElastic(SqlDataReaderContext arg)
        {

            var result = arg.CreateObject(reader => new Song()
            {
                SongId = reader.GetInt32("SongId"),
                SongName = reader.GetString("Name"),
                Date = reader.GetDateTime("Date"),
                Priority = reader.GetInt32("Priority"),
                Publicable = reader.GetInt32("Publicable"),
                Capo = reader.GetInt32("Capo"),
                Url = reader.GetString("YoutubeURL"),
                Validated = reader.GetBoolean("Validated"),
                Creator = reader.GetInt32("Creator"),
                Editor = reader.GetNullableInt32("Editor"),
                Text = reader.GetString("Value")
            });
            if (result == null)
            {
                return null;
            }
            arg.MoveToNextRecordSet();
            var authors = arg.CreateCollection(reader => new
            {
                SongId = reader.GetInt32("SongId"),
                AuthorName = reader.GetString("Name"),
                Type = reader.GetInt32("Type")


            });
            arg.MoveToNextRecordSet();
            var rythms = arg.CreateCollection(reader => new
            {
                SongId = reader.GetInt32("SongId"),
                RythmId = reader.GetInt32("Rythm_Id"),
            });


            result.Authors = authors.Select(i => (i.AuthorName, i.Type));
            result.Rythms = rythms.Select(i => i.RythmId);






            return result;
        }
    }
}

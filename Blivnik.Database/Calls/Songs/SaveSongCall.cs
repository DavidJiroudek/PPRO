﻿using Blivnik.Database.Models;
using Blivnik.Database.SQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Database.Calls.Songs
{
    public class SaveSongCall
    {
        private IDataReaderContextFactory _dataReader;
        public SaveSongCall(IDataReaderContextFactory dataReader)
        {
            _dataReader = dataReader;
        }
        public int SaveSong(Song data)
        {


            var songName = data.SongName.CreateParameter("@name");
            var publicable = data.Publicable.CreateParameter("@publicable");
            var priority = data.Priority.CreateParameter("@priority");
            var capo = data.Capo.CreateParameter("@capo");
            var ytURL = data.Url.CreateParameter("@youtubeURL");
            var creator = data.Creator.CreateParameter("@creator");
            var songText = data.Text.CreateParameter("@value");

            //musicAuthors
            var authors = new SqlParameter("@authors", SqlDbType.Structured);

            //later send as one array with roles


            DataTable table = new DataTable();
            table.Columns.Add("Name", typeof(string));
            table.Columns.Add("Type", typeof(int));

            foreach (var author in data.Authors)
            {
                var row = table.NewRow();

                row["Name"] = author.name;
                row["Type"] = author.type;
                table.Rows.Add(row);
            }
            authors.Value = table;
            authors.TypeName = "dbo.AuthorsData_Table";


            var rythms = new SqlParameter("@rythms", SqlDbType.Structured);
            table = new DataTable();
            table.Columns.Add("Id", typeof(int));

            foreach (var author in data.Rythms)
            {
                var row = table.NewRow();

                row["Id"] = author;
                table.Rows.Add(row);
            }
            rythms.Value = table;
            rythms.TypeName = "dbo.Int_Table";


            var reader = _dataReader.GetReaderContext("dbo.SaveSong", CommandType.StoredProcedure, songName, publicable, priority, capo, ytURL, creator, authors, rythms, songText);
            return reader.CreateCompositeObject(obj =>
            {
                return
                    obj.CreateObject(data =>

                    data.GetInt32("SongId")
                    )
                ;
            });

        }
    }
}

﻿using Blinvik.Search.ElasticSearch.Models;
using Blivnik.Database.SQL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Blivnik.Database.Calls.Songs
{
    public class GetElasticSong
    {
        private IDataReaderContextFactory _dataReader;
        public GetElasticSong(IDataReaderContextFactory dataReader)
        {
            _dataReader = dataReader;
        }


        public ElasticSong GetSong(int songId)
        {
            var authors = new SqlParameter("@songId", SqlDbType.Structured);

            //later send as one array with roles


            DataTable table = new DataTable();
            table.Columns.Add("Id", typeof(int));

            var row = table.NewRow();

            row["Id"] = songId;
            table.Rows.Add(row);

            authors.Value = table;
            authors.TypeName = "dbo.Int_Table";


            var reader = _dataReader.GetReaderContext("dbo.GetElasticSong", CommandType.StoredProcedure, authors);
            var result = reader.CreateCompositeObject(CreateSongElastic);


            return result;
        }
        private ElasticSong CreateSongElastic(SqlDataReaderContext arg)
        {

            var result = arg.CreateObject(reader => new ElasticSong()
            {
                SongId = reader.GetInt32("SongId"),
                Index = reader.GetInt32("Id"),
                CreatedDate = reader.GetDateTime("Date"),
                SongName = reader.GetString("Name"),
                Publicable = reader.GetInt32("Publicable"),
                Priority = reader.GetInt32("Priority"),
                Validated = reader.GetBoolean("Validated"),
                Creator = reader.GetInt32("Creator"),
                Editor = reader.GetNullableInt32("Editor"),
                Text = Regex.Replace(reader.GetString("Value"), "<.*?>", string.Empty)
            });

            arg.MoveToNextRecordSet();
            var authors = arg.CreateCollection(reader => new
            {
                Id = reader.GetInt32("Id"),
                Author = new ElasticAuthor()
                {
                    Index = reader.GetInt32("Author_Id"),
                    AuthorType = reader.GetString("Type"),
                    Name = reader.GetString("Name")
                }
            }).GroupBy(i => i.Id).ToDictionary(i => i.Key, i => i.Select(j => j.Author).ToList());
            arg.MoveToNextRecordSet();
            var rythms = arg.CreateCollection(reader => new
            {
                Id = reader.GetInt32("Id"),
                RythmId = reader.GetInt32("Rythm_Id"),
            }).GroupBy(i => i.Id).ToDictionary(i => i.Key, i => i.Select(j => j.RythmId).ToList());


            if (authors.ContainsKey(result.Index))
            {
                result.Authors = authors[result.Index];
            }
            if (rythms.ContainsKey(result.Index))
            {
                result.Rythms = rythms[result.Index];
            }

            return result;
        }
    }
}

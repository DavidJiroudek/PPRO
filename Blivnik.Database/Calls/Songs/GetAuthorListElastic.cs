﻿

using Blinvik.Search.ElasticSearch.Models;
using Blinvik.Search.Models;
using Blivnik.Database.SQL;

namespace Blivnik.Database.Calls.Songs
{
    public class GetAuthorListElastic
    {
        private IDataReaderContextFactory _dataReader;
        public GetAuthorListElastic(IDataReaderContextFactory dataReader)
        {
            _dataReader = dataReader;
        }

        public IEnumerable<ElasticAuthor> GetAuthorList()
        {
            var reader = _dataReader.GetReaderContext("dbo.[GetAllAuthors]", System.Data.CommandType.StoredProcedure);

            var result = reader.CreateCompositeObject(CreateAuthorElastic);


            return result;
        }

        private IEnumerable<ElasticAuthor> CreateAuthorElastic(SqlDataReaderContext arg)
        {

            var result = arg.CreateCollection(reader => new ElasticAuthor()
            {
                Name = reader.GetString("Name"),
                Index = reader.GetInt32("Id")
            });

            arg.MoveToNextRecordSet();
            var songs = arg.CreateCollection(reader => new
            {
                AuthorId = reader.GetInt32("Author_Id"),
                Publicable = reader.GetInt32("Publicable"),
                Type = reader.GetInt32("Type"),
            }).GroupBy(i => i.AuthorId).ToDictionary(i => i.Key, i => i.ToList());
            arg.MoveToNextRecordSet();


            foreach (var author in result)
            {
                if (songs.ContainsKey(author.Index))
                {
                    author.MinimalPublicable = songs[author.Index].Min(i => i.Publicable);
                    author.AuthorTypesUsed = songs[author.Index].Select(i => (AuthorType)i.Type).Distinct().ToArray();
                }

            }
            return result;
        }
    }
}
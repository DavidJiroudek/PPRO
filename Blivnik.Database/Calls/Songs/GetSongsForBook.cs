﻿using Autofac;
using Blinvik.Search.ElasticSearch.Models;
using Blivnik.Database.Models;
using Blivnik.Database.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Blivnik.Database.Calls.Songs
{
    public class GetSongsForBook
    {
        private IDataReaderContextFactory _dataReader;
        public GetSongsForBook(IDataReaderContextFactory dataReader)
        {
            _dataReader = dataReader;
        }


        public IEnumerable<Song> GetSongList(BookFilter filter)
        {
            var priorityParam =  filter.Priority.CreateParameter("@priority");
            var dateParam = filter.Date.CreateParameter("@date");
            var publicableParam = ((int)filter.Publicable).CreateParameter("publicable");

            var reader = _dataReader.GetReaderContext("dbo.GetSongsForBook", System.Data.CommandType.StoredProcedure, priorityParam, dateParam, publicableParam);

            var result = reader.CreateCompositeObject(CreateSongList);


            return result;
        }
        private IEnumerable<Song> CreateSongList(SqlDataReaderContext arg)
        {
            try
            {
                var result = arg.CreateCollection(reader => new Song()
                {
                    SongId = reader.GetInt32("SongId"),
                    Date = reader.GetDateTime("Date"),
                    SongName = reader.GetString("Name"),
                    Capo = reader.GetInt32("Capo"),
                    Publicable = reader.GetInt32("Publicable"),
                    Priority = reader.GetInt32("Priority"),
                    Validated = reader.GetBoolean("Validated"),
                    Creator = reader.GetInt32("Creator"),
                    Editor = reader.GetNullableInt32("Editor"),
                    Text = reader.GetString("Value")
                });

                arg.MoveToNextRecordSet();
                var authors = arg.CreateCollection(reader => new
                {
                    SongId = reader.GetInt32("SongId"),
                    AuthorName = reader.GetString("Name"),
                    Type = reader.GetInt32("Id")

                }).GroupBy(i => i.SongId).ToDictionary(i => i.Key, i => i.Select(j => (j.AuthorName, j.Type)).ToList());
                arg.MoveToNextRecordSet();
                var rythms = arg.CreateCollection(reader => new
                {
                    SongId = reader.GetInt32("SongId"),
                    RythmId = reader.GetInt32("Rythm_Id"),
                }).GroupBy(i => i.SongId).ToDictionary(i => i.Key, i => i.Select(j => j.RythmId).ToList());

                foreach (var song in result)
                {
                    if (authors.ContainsKey(song.SongId.Value))
                    {
                        song.Authors = authors[song.SongId.Value];
                    }
                    if (rythms.ContainsKey(song.SongId.Value))
                    {
                        song.Rythms = rythms[song.SongId.Value];
                    }

                }
                return result;
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

    }
}

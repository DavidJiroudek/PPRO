﻿using Autofac;
using Blinvik.Search.ElasticSearch.Models;
using Blivnik.Database.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Blivnik.Database.Calls.Songs
{
    public class GetSongListElastic
    {
        private IDataReaderContextFactory _dataReader;
        public GetSongListElastic(IDataReaderContextFactory dataReader)
        {
            _dataReader = dataReader;
        }


        public IEnumerable<ElasticSong> GetSongList()
        {
            var reader = _dataReader.GetReaderContext("dbo.GetAllSongs", System.Data.CommandType.StoredProcedure);

            var result = reader.CreateCompositeObject(CreateSongElastic);


            return result;
        }
        private IEnumerable<ElasticSong> CreateSongElastic(SqlDataReaderContext arg)
        {

            var result = arg.CreateCollection(reader => new ElasticSong()
            {
                SongId = reader.GetInt32("SongId"),
                Index = reader.GetInt32("Id"),
                CreatedDate = reader.GetDateTime("Date"),
                SongName = reader.GetString("Name"),
                Publicable = reader.GetInt32("Publicable"),
                Priority = reader.GetInt32("Priority"),
                Validated = reader.GetBoolean("Validated"),
                Creator = reader.GetInt32("Creator"),
                Editor = reader.GetNullableInt32("Editor"),
                Text = Regex.Replace(reader.GetString("Value"), "<.*?>", string.Empty)
            });

            arg.MoveToNextRecordSet();
            var authors = arg.CreateCollection(reader => new
            {
                Id = reader.GetInt32("Id"),
                Author = new ElasticAuthor()
                {
                    Index = reader.GetInt32("Author_Id"),
                    AuthorType = reader.GetString("Type"),
                    Name = reader.GetString("Name")
                }
            }).GroupBy(i => i.Id).ToDictionary(i => i.Key, i => i.Select(j => j.Author).ToList());
            arg.MoveToNextRecordSet();
            var rythms = arg.CreateCollection(reader => new
            {
                Id = reader.GetInt32("Id"),
                RythmId = reader.GetInt32("Rythm_Id"),
            }).GroupBy(i => i.Id).ToDictionary(i => i.Key, i => i.Select(j => j.RythmId).ToList());

            foreach (var song in result)
            {
                if (authors.ContainsKey(song.Index))
                {
                    song.Authors = authors[song.Index];
                }
                if (rythms.ContainsKey(song.Index))
                {
                    song.Rythms = rythms[song.Index];
                }
            }
            return result;
        }

    }
}

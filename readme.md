*This is Blivnik Web page project*

**Minimal Requirements**

Project uses .net core 5.0 and kubernetes

Visual studio 2019 is needed

 [ ] Module Kubernetes bridge for visual studio
 [ ] Visual studio Azure SDK components - docker integration


You also need installed Docker https://docs.docker.com/desktop/windows/install/
 [ ] Docker needs to have virtualization allowed in UEFI/BIOS 
 [ ] It also needs Linux kernel installed in windows https://docs.microsoft.com/cs-cz/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package



 set .docker/daemon.json buildklit to false

 **Deploy**
 Run "docker build -t blivniknetapi ." in project root folder
 Tag blivniknetapi to match artifact repository
 docker tag blivniknetapi:latest europe-west3-docker.pkg.dev/uplifted-scout-359513/blivniknetapi/blivniknetapi:dev7
 docker push europe-west3-docker.pkg.dev/uplifted-scout-359513/blivniknetapi/blivniknetapi:dev7

 kubectl apply -f blivniknetapi-deployment.yaml


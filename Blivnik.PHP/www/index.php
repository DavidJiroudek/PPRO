<?php
/**
 * index file process all entries (mod_rewrite convertes everything to get)
 */
 
/**
 * bootloader
 */
require_once('core/bootloader.php');


//require pages
require_once(ROOT_PATH . 'templates/pages/main.php');
require_once(ROOT_PATH . 'templates/pages/about.php');
require_once(ROOT_PATH . 'templates/pages/notFound.php');
require_once(ROOT_PATH . 'templates/pages/artists.php');
require_once(ROOT_PATH . 'templates/pages/artist.php');
require_once(ROOT_PATH . 'templates/pages/songs.php');
require_once(ROOT_PATH . 'templates/pages/song.php');
require_once(ROOT_PATH . 'templates/pages/add.php');
require_once(ROOT_PATH . 'templates/pages/editUser.php');
require_once(ROOT_PATH . 'templates/pages/search.php');
require_once(ROOT_PATH . 'templates/pages/print.php');
require_once(ROOT_PATH . 'templates/pages/printBook.php');
require_once(ROOT_PATH . 'templates/pages/book.php');
require_once(ROOT_PATH . 'templates/pages/wholeBook.php');

//print_r($_GET);
//GET transformation
if(!isset($_GET['page'])){
    $_GET['page'] = 'main';
}
switch($_GET['page']){
    case 'about':
        (new aboutPage)->init();
        break; 
    case 'add':
        (new addPage)->init();
        break;
    case 'artist':
        (new artistPage)->init();
        break; 
    case 'artists':
        (new artistsPage)->init();
        break;
    case 'book':
        (new bookPage)->init();
        break;
    case 'wholeBook':
        (new WholeBookPage)->init();
        break;  
    case 'delete':
        Old::songDelete($_GET["id"]);
        (new songsPage)->init();
        break; 
    case 'editUser':
        (new editUserPage)->init();
        break; 
    case 'logout':
        User::logout();
        header("Location:" . OUTER_PATH);
        break; 
    case 'main':
        (new mainPage)->init();
        break;
    case 'print':
        (new printPage)->init();
        break; 
    case 'printBook':
        (new printBookPage)->init();
        break; 
    case 'search':
        (new searchPage)->init();
        break; 
    case 'song':
        (new songPage)->init();
        break; 
    case 'songs':
        (new songsPage)->init();
        break;
    case '404':
    default:
        (new notFoundPage)->init();
        break;    
}



?>
<?php
/**
 * bootloader
 */
require_once('core/bootloader.php');

if(isset($_GET["act"]) && isset($_GET["ID"])){
    $_GET["act"] = Con::$con->real_escape_string($_GET["act"]);
    $_GET["ID"] = Con::$con->real_escape_string($_GET["ID"]);
    $query = Con::$con->query("SELECT * FROM Users WHERE id='".$_GET["ID"]."'");
    if($query->num_rows == 1){
        $data = $query->fetch_array();
        if($_GET["act"] == sha1($data["nick"].$data["id"]) && $data["permission"] == 0){
            Con::$con->query("UPDATE Users SET permission=1 WHERE id='".$data["id"]."'");
            header("Location: index.php?action=loginForm&detail=activated");
            return;
        }
    }
}
header("Location: index.php?action=loginForm&detail=notActivated");
?>
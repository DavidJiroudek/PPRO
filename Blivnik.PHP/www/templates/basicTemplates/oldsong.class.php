<?php
/**
 * file providing song class and it!s operations
 */
/**
 * song
 * 
 * this class provides all operations with song, creating, edit, parsing
 * @package old
 */
class OldSong{
    public $ID;
    public $name;
    public $author;
    public $date;
    public $appr;
    public $importance;
    public $text;
    public $publishable;
    public $capo;
    public $taby= Array();
    public $authorList = Array();
    private $verses;
    private $plainText;
    
    function __construct($ID){
        $this->ID = $ID;
    }
    
    public function parseLoad($data){
        $this->author = self::switchName($data["jmeno"]);
        $this->name = $data["nazev"];
        $this->appr = $data["overena"];
        $this->publishable = $data["publikovatelna"];
        $this->date = date('d. m. Y', $data["utime"]);
        $this->importance = $data["dulezitost"];
        $this->text = urldecode($data["text"]) . PHP_EOL . PHP_EOL;
        $this->capo = 0;
        if(preg_match_all('/\[CAPO:(\d{1,2})\]/', $this->text, $rawCapo)){
            $this->capo = $rawCapo[1][0];
        }
    }
    
    public function load(){
        $sql = "SELECT *, UNIX_TIMESTAMP(pridano) AS utime, GROUP_CONCAT(DISTINCT jmeno SEPARATOR ';') as autor, oldSong.id as IDs FROM oldSong 
                LEFT JOIN `oldAutSong` ON oldSong.id = `oldAutSong`.song 
                LEFT JOIN oldAuthors ON `oldAutSong`.autor = oldAuthors.ID
                WHERE (oldSong.id = '".$this->ID."' AND `oldAutSong`.vztah='autorText' )
                ";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        if($ret->num_rows == 0){
            return false;
        }
        $data = $ret->fetch_array();
        $authSQL = Con::$con->query('SELECT autor, vztah FROM oldAutSong WHERE song = ' . $this->ID) or die(Con::$con->error);
        while($art = $authSQL->fetch_array()){
            $this->authorList[] = $art;
        }
        $this->parseLoad($data);
        return true;
    }
    
    private function getPlainText(){
        $plainText = explode("[TEXT:]", $this->text);
        if(isset($plainText[1])){
            $this->plainText = "\n".$plainText[1];
        }
        else{
            $this->plainText="";
        }
    }
    
    private function getVerses(){
            $this->plainText .= "\n\n";
            preg_match_all('/(\s*(\(|\[)[\s\S]*?\n\s*?\n)/', $this->plainText, $plainVerses);
            $this->verses = $plainVerses[1];
    }
    public function getDesc(){
        $this->getPlainText();
        $this->getVerses();
        $returnDesc = "";
        foreach($this->verses as $verse){
            $verse = preg_replace("/\[[^:]{1,7}\]/", "", $verse);
            $verse = preg_replace("/^(\s*\n)/m", "", $verse);
            $verse = preg_replace("/\|/", "", $verse);
            $verse = preg_replace("/[ ]{1,}/", " ", $verse);
            $returnDesc .= $verse."\n";
        }
        return $returnDesc;
    }
    function getTextPart($t){
            $t = preg_replace("/\d{0,2}x/", "", $t);
            $match = array("(solo)", "(Solo)", "Solo", "(info:)","(Rec)", " (Rec)", "(intro:)", "(intro)", "(INTRO:)", "(PŘEDEHRA:)", "[", "]", "(original:) 1 step down tuning", "(Po každé sloce R)", ":");
            $t = str_replace($match,"",$t);
             

            $t = preg_replace("/\([I]{0,1}[R]{0,1}\d{0,2}\)\s{0,10}/", "", $t);
            $t = preg_replace("/\s{1,}/", " ", $t);
            $t = trim($t);
            $t = iconv("UTF-8", "CP1250//IGNORE",  $t);
            $l = 50;
            $len = strlen($t);
            while($t[$l] != " " && $l < $len){
                $l++;
            }
            
            //echo $t."<br>";
            $t = substr(" ".$t, 0, $l+1);
            $t = str_replace(" el",chr(0x8A)."el",$t);
            //echo $t;
            return $t;
    }
    function getTextStart(){
        return $this->getTextPart($this->getDesc());
    }
    
    function getTextR(){
        $t =  $this->getDesc();
        $r = explode("(R", $t);
        if(isset($r[1])){
            $r = str_replace("1)","",$r[1]);
            $r = str_replace(")","",$r);
        return $this->getTextPart($r);
        }
        
        return $this->getTextPart($t);
    }
    
    public function getTabs(){
        //unset($this->taby);
        $this->taby = array();
        $tabyCount = preg_match_all("/\[(.{1,50}:)\][\s]*\n(([\s]*\S{0,3}\|[\S]*\|[\s]*\n){1,12})/", $this->text, $rawTaby);
        if($tabyCount){
            for($i = 0;$i <$tabyCount;$i++){
                $tabulatura = new Tab($rawTaby[1][$i]); //jmeno
                
                preg_match_all("/([\s]*\S{0,3}\|[\S]*\|)[\s]*\n/", $rawTaby[2][$i], $tab);
                foreach($tab[1] as $data){
                    $tabulatura->addLine($data);
                }
                $this->taby[]=$tabulatura;
            }
        }
    }
    
    public function getChords(){
            $strChords = "";
            $count = preg_match_all('/\[(\S{1,10})\]\(([\d,X]*)\)/', $this->text, $rawChords);
            for($i = 0;$i <$count;$i++){
                $strChords .= $rawChords[1][$i]." ".$rawChords[2][$i]."\n";
            }
            return $strChords;
    }
    
    private function multiPass($string, $pdf){
        $string = preg_replace("/\n\s*\n/","", $string);
        $string.="\n";
        $lineCount = substr_count( $string, "\n");
        $lines = explode("\n", $string);
        if(($pdf->GetY()+(4*$lineCount)) >= 276){    //4.5 is size of line, 297 size of A4
            $pdf->AddPage();
            $pdf->SetY(13);
        }
        foreach($lines as &$line){
            //$line = substr($line,0, strlen($line)-1);
            $pdf->SetX(11);
            if(preg_match("/(\([SRI\d]{1,3}\))/", $line, $rawNumber)){
                $line = preg_replace("/(\([SRI\d]{1,3}\))/","", $line, 1);
                $pdf->SetFont('Courier','B',12);
                $pdf->Write(4,iconv("UTF-8", "WINDOWS-1250", $rawNumber[0]));
                $pdf->SetFont('Courier','',12);
            }
            $parsedChordsSplit = preg_split("/(\[[^:]{1,10}?\])/", $line, -1,PREG_SPLIT_DELIM_CAPTURE);
            foreach($parsedChordsSplit as $parseChord){
                $pdf->SetFont('Courier','',12);
                $lineBetweenChords = preg_split("/(\[[^:]{1,10}?\])/", $parseChord, -1);
                //echo $lineBetweenChords[0];
                $pdf->Write(4,iconv("UTF-8", "WINDOWS-1250//IGNORE", $lineBetweenChords[0]));
                if(preg_match("/(\[[^:]{1,10}?\])/", $parseChord , $parsedChords)){
                    $parsedChords[0] = str_replace("["," ",$parsedChords[0]);
                    $parsedChords[0] = str_replace("]"," ",$parsedChords[0]);
                    $pdf->SetFont('Courier','B',12);
                    $pdf->Write(4,iconv("UTF-8", "WINDOWS-1250", $parsedChords[0]));
                    $pdf->SetFont('Courier','',12);
                }
            }
            $pdf->Ln(4);
        }
        //$pdf->Ln(2);
    }
    public function writePDFHeader(&$pdf){
        //header
        $pdf->AddPage();
        $pdf->SetFont('Courier','B',12);
        $pdf->Cell(10,8,iconv("UTF-8", "WINDOWS-1250", $this->name),0, 0, 'L');
        $pdf->Cell(180,8,iconv("UTF-8", "WINDOWS-1250", $this->author), 0,0, 'R');
        $pdf->Line(10, 17, 200, 17);
        $pdf->Ln(10);
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(40,1,iconv("UTF-8", "WINDOWS-1250", "Přidáno: ".$this->date),0, 0, 'L');
        $importWidth = 150;
        if($this->capo != 0){
          $pdf->Cell(110,1,iconv("UTF-8", "WINDOWS-1250", "Capo: ".$this->capo),0, 0, 'C');
          $importWidth = 40;
        }
        $pdf->Cell($importWidth,1,iconv("UTF-8", "WINDOWS-1250", "Důležitost: ".$this->importance),0, 0, 'R');
        $pdf->Ln(2);
        //chords
        $pdf->Ln();
        $pdf->SetFont('Courier','B',10);
        $pdf->SetX(19);
        $pdf->Multicell(180,4.5,iconv("UTF-8", "WINDOWS-1250", $this->getChords()));
        //tabs
        $this->getTabs();
        $pdf->SetFont('Courier','',10);
        foreach($this->taby as $tab){ 
            $pdf->Cell(40,1,iconv("UTF-8", "WINDOWS-1250", $tab->name),0, 0, 'L');
            $pdf->Ln(3);
            $pdf->Multicell(180,4.5,iconv("UTF-8", "WINDOWS-1250", $tab->writeParsed()));
            $pdf->Ln(1);
        }
        //verses
        $pdf->SetX(11);
        $this->getPlainText();
        $this->getVerses();
        foreach($this->verses as $verse){
             $this->multiPass($verse, $pdf);
        }
    }
    
    public function writePDFBookHeader(&$pdf){
        //header
        $pdf->AddPage();
        $pdf->_numberingFooter = true;
        $pdf->startPageNums();
        $pdf->TOC_Entry(iconv("UTF-8", "WINDOWS-1250", $this->name), 0);
        $pdf->TOI_Entry($this->getTextStart(), 0);
        $pdf->TOR_Entry($this->getTextR(), 0);
        $importWidth = 110;
        if($this->capo != 0){
          
          $importWidth = 80;
        }
        if($pdf->numPageNo() % 2 == 0){
            $pdf->SetFont('Courier','B',12);
            $pdf->Cell($importWidth,8,iconv("UTF-8", "WINDOWS-1250", $this->name),0, 0, 'L');
            $pdf->SetFont('Courier','',12);
            if($this->capo != 0){
                $pdf->Cell(30,8,iconv("UTF-8", "WINDOWS-1250", "Capo: ".$this->capo),0, 0, 'C');
            }
            $pdf->SetFont('Courier','B',12);
            $pdf->Cell(80,8,iconv("UTF-8", "WINDOWS-1250", $this->author), 0,0, 'R');
            $pdf->Line(10, 17, 200, 17);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',8);
        } else{
            $pdf->SetFont('Courier','',12);
            $pdf->SetFont('Courier','B',12);
            $pdf->Cell(80,8,iconv("UTF-8", "WINDOWS-1250", $this->author), 0,0, 'L');
            if($this->capo != 0){
                $pdf->Cell(30,8,iconv("UTF-8", "WINDOWS-1250", "Capo: ".$this->capo),0, 0, 'C');
            }
            $pdf->SetFont('Courier','B',12);
            $pdf->Cell($importWidth,8,iconv("UTF-8", "WINDOWS-1250", $this->name),0, 0, 'R');
            $pdf->Line(10, 17, 200, 17);
            $pdf->Ln(10);
            $pdf->SetFont('Arial','',8);
        }
        //$pdf->Cell(40,1,iconv("UTF-8", "WINDOWS-1250", "Přidáno: ".$this->date),0, 0, 'L');
    
        $pdf->Ln(0);
        //$pdf->Cell($importWidth,1,iconv("UTF-8", "WINDOWS-1250", "Důležitost: ".$this->importance),0, 0, 'R');
        //$pdf->Ln(2);
        //chords
        
        $chords = $this->getChords();
        if($chords != ""){
            
            $pdf->SetFont('Courier','B',10);
            $pdf->SetX(15);
            $pdf->Multicell(180,4.5,iconv("UTF-8", "WINDOWS-1250", $chords));
        }
        //tabs
        $this->getTabs();
        $pdf->SetFont('Courier','',10);
        foreach($this->taby as $tab){ 
            $pdf->Ln(1);
            $pdf->Cell(40,1,iconv("UTF-8", "WINDOWS-1250", $tab->name),0, 0, 'L');
            $pdf->Ln(3);
            $pdf->Multicell(180,4.5,iconv("UTF-8", "WINDOWS-1250", $tab->writeParsed()));
            $pdf->Ln(1);
        }
        $pdf->Ln(2);
        //verses
        $pdf->SetX(1);
        $this->getPlainText();
        $this->getVerses();
        foreach($this->verses as $verse){
             $this->multiPass($verse, $pdf);
        }

    }
    
    static function switchName($nameString){
        $returnString = "";
        $names = explode(";", $nameString);
        $i = 0;
        foreach($names as $name){
            if($i != 0){
                $returnString.= "; ";  
            }
            $i++;
            if(strstr($name, ",")){
                $partName = explode(",", $name);
                $surname = $partName[0];
                foreach ($partName as $first){
                    if($first != $surname){
                        if($first[0] = " "){
                            $first = substr($first, 1);
                        }
                        $returnString .= $first." "; 
                    }
                }
                $returnString .= $surname;   
            }
            else{
                $returnString .= $name;
            }
        }
        return $returnString;
    }
    
    /**
     * approve song
     */
    public function appr(){
        if(isset($_SESSION['user']) && $_SESSION['user']->permission('song_appr')){
            $data = Con::$con->query("SELECT folder FROM oldSong WHERE (ID = '".$this->ID."')"); 
            $value = $data->fetch_array();
            if(isset($value["folder"]) && $value["folder"] != ""){
                Con::$con->query("UPDATE oldSong SET pridano=pridano, folder='' WHERE (ID = '".$this->ID."')"); 
            }else {
                Con::$con->query("UPDATE oldSong SET pridano=pridano, overena=1, schvaleno=CURRENT_TIMESTAMP WHERE (ID = '".$this->ID."')"); 
            }        
        }
    }
    
    public static function getSong($id){
        $sql = "SELECT *, UNIX_TIMESTAMP(pridano) AS utime, oldSong.ID as IDs FROM oldSong 
                LEFT JOIN `oldAutSong` ON oldSong.ID = `oldAutSong`.song 
                LEFT JOIN oldAuthors ON `oldAutSong`.autor=oldAuthors.ID
                WHERE (oldSong.ID = '".$id."' AND `oldAutSong`.vztah='autorText')";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        if($ret->num_rows){
            $data = $ret->fetch_array();
            return $data;
        }
        return null;     
    }
    
     public static function parseSong($data){
        $autor = $data["jmeno"];
        $skladba = $data["nazev"];
        $pridano = date('d. m. Y', $data["utime"]);
        $dulezitost = $data["dulezitost"];
        $text = $data["text"];
        //akordy
        $akordyCount = preg_match_all('/\[(\S{1,6})\]\((\w{6})\)/', urldecode($text), $rawAkordy);
        $akordy;
        $akordy["set"] = false;
        for($i = 0;$i<$akordyCount;$i++){
            $akordy["set"] = true;
            $akordy["line"][$i]=$rawAkordy[0][$i];
        } 
        //kapodastr
        $capo = 0;
        if(preg_match_all('/\[CAPO:(\d{1,2})\]/', urldecode($text), $rawCapo)){
             $capo = $rawCapo[1][0];
        }
        //taby
        $tabySet = false;
        $taby = Array();
        $tabyCount = preg_match_all("/\[((.){1,50}):\][\s]{0,200}\n[\s]{0,200}(\S)(\|[\S]{1,3000}\|)[\s]{0,200}\n[\s]{0,200}(\S)(\|[\S]{1,3000}\|)[\s]{0,200}\n[\s]{0,200}(\S)(\|[\S]{1,3000}\|)[\s]{0,200}\n[\s]{0,200}(\S)(\|[\S]{1,3000}\|)[\s]{0,200}\n[\s]{0,200}(\S)(\|[\S]{1,3000}\|)[\s]{0,200}\n[\s]{0,200}(\S)(\|[\S]{1,3000}\|)[\s]{0,200}/", urldecode($text), $rawTaby);
        if($tabyCount){
            $tabySet = true;
            for($i = 0;$i <$tabyCount;$i++){
                $taby[$rawTaby[1][$i]]["name"] = $rawTaby[1][$i];
                 $taby[$rawTaby[1][$i]][0] = $rawTaby[3][$i].$rawTaby[4][$i];
                 $taby[$rawTaby[1][$i]][1] = $rawTaby[5][$i].$rawTaby[6][$i];
                 $taby[$rawTaby[1][$i]][2] = $rawTaby[7][$i].$rawTaby[8][$i];
                 $taby[$rawTaby[1][$i]][3] = $rawTaby[9][$i].$rawTaby[10][$i];
                 $taby[$rawTaby[1][$i]][4] = $rawTaby[11][$i].$rawTaby[12][$i];
                 $taby[$rawTaby[1][$i]][5] = $rawTaby[13][$i].$rawTaby[14][$i];
            }
        }
        
        //text
        $textSet = false;
        if(preg_match("/\[TEXT:\]/", urldecode($text))){ 
            $textSet = true;
            $text = explode("[TEXT:]", urldecode($text));
            $text=$text[1];
            $text = explode("\n", $text);
        }
        $output["autor"]=$autor;
        $output["skladba"]=$skladba;
        $output["pridano"]=$pridano;
        $output["dulezitost"]=$dulezitost;
        $output["capo"]=$capo;
        $output["akordy"]=$akordy;
        $output["taby"]=$taby;
        $output["tabySet"]=$tabySet;
        $output["text"]=$text;
        $output["textSet"]=$textSet;
        return $output;
    } 
}

?>
<?php
/**
 * basic page template
 */

/**
 * require needed modules
 * @todo automatic load
 */
require_once(ROOT_PATH . 'modules/head/head.php');
require_once(ROOT_PATH . 'modules/watchLine/watchLine.php');
require_once(ROOT_PATH . 'modules/addBox/addBox.php');
require_once(ROOT_PATH . 'modules/title/title.php');
require_once(ROOT_PATH . 'modules/footer/footer.php');
require_once(ROOT_PATH . 'modules/ajax/ajax.php');
require_once(ROOT_PATH . 'modules/old/old.php');
require_once(ROOT_PATH . 'modules/alternatives/alternatives.php');
/**
 * basic page template
 * @param initialized page
 */
function showBasicPage($page){
    //basicPage module init
    $page->modules['ajax'] = new Ajax(); 
    $page->modules['head'] = new Head(); 
    $page->modules['addBox'] = new AddBox();
    $page->modules['title'] = new Title();
    $page->modules['footer'] = new Footer();   
    echo '
        <!DOCTYPE html>
        <html>
          <head>
              <meta charset="utf-8" />
              <META name="keywords" content="Kytarový Blivník, Kytara, Akordy, Noty, Taby, tabs, chords" >
              <link rel="stylesheet" type="text/css" href="' . OUTER_PATH . 'css/core/core.css">
              <link rel="stylesheet" type="text/css" href="' . OUTER_PATH . 'css/core/basicPage.css">
              <meta name="msvalidate.01" content="67F81B575276ADB740F3B9ED1141CF5A" />
              <META name="distribution" content="web"> 
         ';?>
              <script>
                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                
                  ga('create', 'UA-58277164-1', 'auto');
                  ga('send', 'pageview');
        
              </script>
    <?php
    foreach($page->modules as $module){
        echo $module->initHead().PHP_EOL;
    }
    echo '
          </head>
          <body>
              <div class="basicWrapper">
         ';
    $page->modules['head']->show();
    $page->modules['addBox']->show();
    $page->showContent();
    
    echo '
            </div>
         ';
    $page->modules['footer']->show();
    echo '
        </body>
      </html>
     ';
}
<?php
require_once(ROOT_PATH . 'modules/oldpdf/createPage.php');
require_once(ROOT_PATH . 'modules/oldpdf/fpdf/fpdf.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/mrizka.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/mainPage.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/endPage.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/songs.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/bookKvint.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/bookStupnice.php');

//require_once(ROOT_PATH . 'modules/oldPDF/createBook.php');
function showBookPDF($page){
    $pdf = new PdfGenerator();
    $pdf->AddFont('Arial', '', 'arial.php');
    $pdf->AddFont('Arial', 'B', 'arialbd.php');
    $pdf->AddFont('Courier', '', 'cour.php');
    $pdf->AddFont('Courier', 'B', 'courbd.php');
    //$pdf = $page->showContent($pdf);
    $pdf->AddPage();
        
    mainPage($pdf);
    
    $pdf->AddPage();

    //endPage($pdf);
    //$pdf->AddPage();
    
    $pdf->_numberingFooter = false;
    $pdf->AddPage();
    $pdf->startPageNums();
    $pdf->_numberingFooter = false;
    songsBook($pdf);
    $pdf->AddPage();
    $pdf->_numberingFooter = true;
    $pdf->insertTOC(4);
    $pdf->insertTOR($pdf->numPageNo() + 20);
    $pdf->insertTOA();
    mrizka($pdf);
        kvint($pdf);
    stupnice($pdf);

    $pdf->Output();

    
}

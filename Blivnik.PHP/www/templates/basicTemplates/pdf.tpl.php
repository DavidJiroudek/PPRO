<?php
require_once(ROOT_PATH . 'modules/oldpdf/createPage.php');
require_once(ROOT_PATH . 'modules/oldpdf/fpdf/fpdf.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/mrizka.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/mainPage.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/songs.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/bookKvint.php');
require_once(ROOT_PATH . 'modules/oldpdf/pdfPages/bookStupnice.php');

//require_once(ROOT_PATH . 'modules/oldPDF/createBook.php');
function showPDF($page){
    $pdf = new FPDF();
    //$pdf->AddPage();
    $pdf->AddFont('Arial', '', 'arial.php');
    $pdf->AddFont('Arial', 'B', 'arialbd.php');
    $pdf->AddFont('Courier', '', 'cour.php');
    $pdf->AddFont('Courier', 'B', 'courbd.php');
    $pdf = $page->showContent($pdf);
    $pdf->Output();
    
}

<?php
/**
 * artists list file
 *
 */
/**
 * load template
 */
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');

/**
 * artists list page class
 * 
 */
class artistsPage{
    /**
     * list of modules
     */
    public $modules;
    /**
     * page limit
     */
    private $limit = 1;
    /**
     * configure and init page modules
     */
    function init(){
        //configure and init page modules
        Title::setTitle(Lang::t('artistList'));
        $this->modules['addBox'] = new AddBox();
        $this->modules['old'] = new Old();
        
        if(isset($_GET['limit'])){
            $this->limit = $lim = Con::$con->real_escape_string($_GET['limit']);    
        }
        
        showBasicPage($this);
    }
    /**
     * shows body content
     */
    function showContent(){
          
          echo $this->modules['old']->authList($this->limit);
          $this->modules['addBox']->showResponsiveBox();
    }

}
?>
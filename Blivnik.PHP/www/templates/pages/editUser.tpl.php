<?php
/**
 * file ocntains editUser template
 */
/**
 * edit user body template
 * @param initialized modules
 * @todo write form generator
 * @todo write for database language
 */
function showEditUser($modules){
    echo '<div class="basicContent center">';
    echo '<h1> ' . Lang::t('settings') . '</h1>';
    ?>
    <div class="narrow">
        <form method="POST">
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="settingsFormName"><?php echo Lang::t('name');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="hidden" name="action" value="user_change_settings">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('settingsFormName') ? 'red' : '';?>" name="settingsFormName" id="settingsFormName" value="<?php echo RetVal::ifSet('settingsFormName') ?  RetVal::r('settingsFormName') : $_SESSION['user']->name; ?>" required>
                </div>
                <div class="itemApproved" id="settingsFormNameIMG">
                </div>         
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="settingsFormSurname"><?php echo Lang::t('surname');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('settingsFormSurname') ? 'red' : '';?>" name="settingsFormSurname" id="settingsFormSurname" value="<?php echo RetVal::ifSet('settingsFormSurname') ?  RetVal::r('settingsFormSurname') : $_SESSION['user']->surname; ?>" required>
                </div>
                <div class="itemApproved" id="settingsFormSurnameIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="settingsFormNick"><?php echo Lang::t('nick');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('settingsFormNick') ? 'red' : '';?>" name="settingsFormNick" id="settingsFormNick" value="<?php echo RetVal::ifSet('settingsFormNick') ?  RetVal::r('settingsFormNick') : $_SESSION['user']->nick; ?>" required>
                </div>
                <div class="itemApproved" id="settingsFormNickIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="settingsFormEmail"><?php echo Lang::t('email');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('settingsFormEmail') ? 'red' : '';?>" name="settingsFormEmail" id="settingsFormEmail" value="<?php echo RetVal::ifSet('settingsFormEmail') ?  RetVal::r('settingsFormEmail') : $_SESSION['user']->email; ?>" required>
                </div>
                <div class="itemApproved" id="settingsFormEmailIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="settingsFormPassword"><?php echo Lang::t('password');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="password" class="itemInput <?php echo  RetVal::wrong('settingsFormPassword') ? 'red' : '';?>" name="settingsFormPassword" id="settingsFormPassword" required>
                </div>
                <div class="itemApproved" id="settingsFormPasswordIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="settingsFormAgainPassword"><?php echo Lang::t('againPassword');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="password" class="itemInput <?php echo  RetVal::wrong('settingsFormAgainPassword') ? 'red' : '';?>" name="settingsFormAgainPassword" id="settingsFormAgainPassword" required>
                </div>
                <div class="itemApproved" id="settingsFormAgainPasswordIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="settingsFormLanguage"><?php echo Lang::t('language');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <select class="itemInput <?php echo  RetVal::wrong('settingsFormLanguage') ? 'red' : '';?>" name="settingsFormLanguage" id="settingsFormLanguage" required>
                      <?php
                      if(RetVal::wrong('settingsFormLanguage')){
                      ?>
                          <option value="cs" <?php echo $_SESSION['user']->lang == 'cs' ? 'selected' : '';?> >Čeština</option>
                          <option value="en" <?php echo $_SESSION['user']->lang == 'en' ? 'selected' : '';?> >English</option>

                      <?php
                      } else{
                      ?>
                          <option value="cs" <?php echo RetVal::r('settingsFormLanguage') == 'cs' ? 'selected' : '';?> >Čeština</option>
                          <option value="en" <?php echo RetVal::r('settingsFormLanguage') == 'en' ? 'selected' : '';?> >English</option>
                      <?php
                      }
                      ?>
                    </select>
                </div>
                <div class="itemApproved" id="settingsFormAgainPasswordIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="registerFormLicence">
                    <input type="submit" class="registerFormSubmit" value="<?php echo Lang::t('edit');?>">
                </div>
            </div>
        </form>
    </div>
    <?php
    echo '</div>';
}
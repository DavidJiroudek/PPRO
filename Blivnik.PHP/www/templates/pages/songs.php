<?php
/**
 * file including page with list of songs
 * @package old
 */
//load template
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');

/**
 * main page completition 
 * @package old
 */
class songsPage{
    /**
     * list of initialized modules
     */
    public $modules;
    /**
     * showed songs limit
     */
    private $limit = 1;
    /**
     * song list type
     */
    private $type = 'published';
    /**
     * unpublished folder name
     */
    private $folder = '';  
    /**
     * configure and init page modules
     */
    function init(){
        //configure and init page modules
        Title::setTitle(Lang::t('songList'));
        $this->modules['addBox'] = new AddBox();
        $this->modules['old'] = new Old();
        
        if(isset($_GET['limit'])){
            $this->limit = Con::$con->real_escape_string($_GET['limit']);    
        }
        if(isset($_GET['type'])){
            $this->type = Con::$con->real_escape_string($_GET['type']);    
        }
        if(isset($_GET['folder'])){
            $this->folder = Con::$con->real_escape_string($_GET['folder']);    
        }
        showBasicPage($this);
    }
    /**
     * shows body content
     */
    function showContent(){
          
          echo $this->modules['old']->songList($this->type, $this->limit, $this->folder);
          $this->modules['addBox']->showResponsiveBox();
    }

}
?>
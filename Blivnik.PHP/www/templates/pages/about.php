<?php
/**
 * file contains obout web class
 */
/**
 * import templates
 */
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');
require_once(ROOT_PATH . 'templates/pages/about.tpl.php');

/**
 * about page class
 * @package old
 */
class aboutPage{
    /**
     * initialized modules list
     */
    public $modules;
    /**
     * configure and init page modules
     */
    function init(){
        Title::setTitle(Lang::t('menuAbout'));
        $this->modules['addBox'] = new AddBox();
        
        
        showBasicPage($this);
    }

    /**
     * shows body content
     */
    function showContent(){   
          showAbout($this->modules);
          $this->modules['addBox']->showResponsiveBox();
    }

}
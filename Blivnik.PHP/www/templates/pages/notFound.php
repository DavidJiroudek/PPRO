<?php
/**
 * file contains not found page class
 */
/**
 * import templates
 */
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');
require_once(ROOT_PATH . 'templates/pages/notFound.tpl.php');
/**
 * not found page class
 */
class notFoundPage{
    /**
     * initialized modules list
     */
    public $modules;
    /**
     * configure and init page modules
     */
    function init(){
        Title::setTitle(Lang::t('pageNotFound'));
        $this->modules['addBox'] = new AddBox();
        
        
        showBasicPage($this);
    }

    /**
     * shows body content
     */
    function showContent(){   
          showNotFound($this->modules);
          $this->modules['addBox']->showResponsiveBox();
    }

}
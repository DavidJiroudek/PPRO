<?php
/**
 * file ocntains edit user page class
 */
/**
 *load template
 */
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');
require_once(ROOT_PATH . 'templates/pages/editUser.tpl.php');
/**
 * edit user Page
 */
class editUserPage{
    /**
     * initialized modules list
     */
    public $modules;
    /**
     * configure and init page modules
     */
    function init(){
        if(isset($_POST["action"]) && $_POST["action"] == 'user_change_settings' && User::isLogged()){
            User::changeSettings();
        }
        Title::setTitle(Lang::t('settings'));
        $this->modules['addBox'] = new AddBox();
        
        
        showBasicPage($this);
    }

    /**
     * shows body content
     */
    function showContent(){   
          showEditUser($this->modules);
          $this->modules['addBox']->showResponsiveBox();
    }

}
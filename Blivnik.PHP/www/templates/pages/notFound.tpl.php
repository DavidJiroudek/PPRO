<?php
/**
 * file contains not found page template
 */
/**
 * not found body template
 * @param list of modules
 */
function showNotFound($modules){
    echo '<div class="basicContent">';
    echo '<h1> ' . Lang::t('pageNotFound') . '</h1>';
    echo '<div class="basicSpacerSmall"></div>';
    echo '<h2> ' . Lang::t('pageNotFoundDesc') . '</h2>';
    echo '</div>';


}
?>
<?php
/**
 * file with book print
 * @package old
 */
/**
 * load template
 */
require_once(ROOT_PATH . 'templates/basicTemplates/bookpdf.tpl.php');

/**
 * add page class
 * 
 * adds and edits song
 * @package old
 */
class wholeBookPage{

    /**
     * song id
     */
    public $id;
    /**
     * edit errors
     */
    public $errors = '';

    /**
     * configure and init page modules
     */
    function init(){
        showBookPDF($this);
    }
    /**
     * shows body content
     */
    function showContent($pdf){
        //if(RetVal::ifSet("mainPage") && RetVal::r("mainPage") == 0){
            //mainPage($pdf);

        return $pdf;
    }

}
<?php
/**
 * file with song print
 * @package old
 */
/**
 * load template
 */
require_once(ROOT_PATH . 'templates/basicTemplates/pdf.tpl.php');
/**
 * add page class
 * 
 * adds and edits song
 * @package old
 */
class printPage{

    /**
     * song id
     */
    public $id;
    /**
     * edit errors
     */
    public $errors = '';
    /**
     * song data
     */
    private $song = null;
    /**
     * configure and init page modules
     */
    function init(){
      
        $this->id = $_GET["id"];
        
        
        $this->song = new OldSong($this->id);
        if(!$this->song->load()){
            header('Location:' . OUTER_PATH . '404/');
            return;
        }

        if($this->song != null){
            showPDF($this);
        }else{
            header('Location:' . OUTER_PATH . '404/');
        }
        
    }
    /**
     * shows body content
     */
    function showContent($pdf){
        $this->song->writePDFHeader($pdf);
        return $pdf;
    }

}
?>
<?php
/**
 * file contains artist page class
 */
/**
 * import templates
 */
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');
/**
 * artist page class
 * @package old
 */
class artistPage{
    /**
     * initialized modules list
     */
    public $modules;
    /**
     * artist id
     */
    private $id;
    /**
     * configure and init page modules
     */
    function init(){
        if(isset($_GET['id'])){
            $this->id = Con::$con->real_escape_string($_GET['id']);    
        }else{
            header('Location:' . OUTER_PATH . 'artists/');
            return;
        }
        //configure and init page modules
        $this->modules['addBox'] = new AddBox();
        $this->modules['old'] = new Old();
        Title::setTitle($this->modules['old']->authorName($this->id));
                 
        showBasicPage($this);
    }
    /**
     * shows body content
     */
    function showContent(){
          
          echo $this->modules['old']->authorDetail($this->id);
          $this->modules['addBox']->showResponsiveBox();
    }

}
?>
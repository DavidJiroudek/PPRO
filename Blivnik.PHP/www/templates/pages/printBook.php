<?php
/**
 * file contains print book class
 */
/**
 *load template
 */
require_once(ROOT_PATH . 'templates/basicTemplates/pdf.tpl.php');
require_once(ROOT_PATH . 'templates/pages/printBook.tpl.php');
/**
 * edit user Page
 */
class printBookPage{
    /**
     * initialized modules list
     */
    public $modules;
    /**
     * last generation date
     */
    public $lastDate = "1970-01-01";
    /**
     * configure and init page modules
     */
    function init(){
        if(User::isLogged()){
            $lDate = $_SESSION['user']->getParam("lastBookDate");
            if($lDate != null){
                $this->lastDate = $lDate;
            }
        }
        Title::setTitle(Lang::t('printBook'));
        $this->modules['addBox'] = new AddBox();
        
        
        showBasicPage($this);
    }

    /**
     * shows body content
     */
    function showContent(){   
          showPrintBook($this->modules, $this->lastDate);
          $this->modules['addBox']->showResponsiveBox();
    }

}
<?php
/**
 * main page file 
 * 
 *  
 */
 
/**
 * load template
 */
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');
require_once(ROOT_PATH . 'templates/pages/main.tpl.php');
/**
 * main page completition 
 */ 
class mainPage{
    /**
     * list of modules
     */
    public $modules;
    /**
     * configure and init page modules
     */
    function init(){
        //configure and init page modules
        $this->modules['watchLine'] = new watchLine();
        $this->modules['addBox'] = new AddBox();
        
        $exampleDetail = new WatchLineDetail();
        $exampleDetail->name = "Nejnavštěvovanější skladby";
        
        $result = Con::$con->query('SELECT id, nazev FROM oldSong ORDER BY seen desc LIMIT 14') OR die(Con::$con->error);
        while($line = $result->fetch_array()){
            $href =  OUTER_PATH  . 'song/' . $line['id'] . '/';
            array_push($exampleDetail->parts, Array('key' => '', 'value' => $line['nazev'], 'href' => $href));
        }

        array_push($this->modules['watchLine']->details, $exampleDetail);
        
        showBasicPage($this);
    }

    /**
     * shows body content
     */
    function showContent(){
          $this->modules['watchLine']->show();
          showMain($this->modules);
          $this->modules['addBox']->showResponsiveBox();
    }

}
?>
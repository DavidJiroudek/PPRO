<?php
/**
 * file including page with song
 * @package old
 */
//load template
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');
/**
 * main page completition 
 */
 
class songPage{
    /**
     * list of initialized modules
     */
    public $modules;
    /**
     * song id
     */
    private $id;
    /**
     * song class
     */
    private $song;
    /**
     * configure and init page modules
     */
    function init(){
        if(isset($_GET['id'])){
            $this->id = $lim = Con::$con->real_escape_string($_GET['id']);    
        }else{
            header('Location:' . OUTER_PATH . 'song/');
            return;
        }
  
        $this->song = new OldSong($this->id);
        if(isset($_POST['action']) && $_POST['action'] == 'song_appr'){
            $this->song->appr();    
        }  
        if(!$this->song->load()){
            header('Location:' . OUTER_PATH . '404/');
            return;
        }
        //configure and init page modules
        $this->modules['addBox'] = new AddBox();
        $this->modules['old'] = new Old();
        $this->modules['alternatives'] = new Alternatives();
        $this->modules['alternatives']->id = $this->id;
        Title::setTitle($this->modules['old']->songName($this->id));    
        showBasicPage($this);
    }
    /**
     * shows body content
     */
    function showContent(){
          echo '<div class="basicContent">';
          echo $this->modules['old']->songDetail($this->song);
          
          $this->modules['alternatives']->show();
          echo '</div>';
          $this->modules['addBox']->showResponsiveBox();
    }

}
?>
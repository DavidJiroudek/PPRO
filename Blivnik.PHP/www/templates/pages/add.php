<?php
/**
 * file with song adding page logic
 * @package old
 */
/**
 * load template
 */
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');
/**
 * add page class
 * 
 * adds and edits song
 * @package old
 */
class addPage{
    /**
     * initialized modules list
     */
    public $modules;
    /**
     * song id
     */
    public $id;
    /**
     * edit errors
     */
    public $errors = '';
    /**
     * configure and init page modules
     */
    function init(){
        //configure and init page modules
        $this->modules['addBox'] = new AddBox();
        $this->modules['old'] = new Old();
        
        $this->id = 0;
        if(isset($_POST["editID"])){
            $this->id = $_POST["editID"]; 
        }
        if(!isset($_SESSION['user'])){
            header('Location:' . OUTER_PATH . '404/');
        }
        //add new song
        if($_SESSION['user']->permission("song_create") && $this->id == 0 && isset($_POST['action'])){
            if($this->modules['old']->postCheck()){
                $myid = $this->modules['old']->addSong($this->id);
                header('Location:' . OUTER_PATH . 'song/' . $myid);
            }else{
                $this->errors .= 'Missing values'; 
            }       
        } else{
            $this->errors .= 'Unable to add song.'; 
        }
        
        if( $_SESSION['user']->permission("song_edit") && $this->id != 0 && isset($_POST['action'])){ 

            if($this->modules['old']->postCheck()){
                $myid = $this->modules['old']->addSong($this->id);
                header('Location:' . OUTER_PATH . 'song/' . $myid);
            }else{
                $this->errors .= 'Missing values'; 
            }       
        } else{
            $this->errors .= 'Unable to edit song.'; 
        }
        
        if(isset($_GET["id"]) && $_SESSION['user']->permission("song_edit")){
            
            $this->id = $_GET["id"]; 
        }

            

        //check user and permission
        Title::setTitle(Lang::t('menuAdd'));    
        showBasicPage($this);
    }
    /**
     * shows body content
     */
    function showContent(){
          echo $this->modules['old']->addDetail($this->id, '');
          $this->modules['addBox']->showResponsiveBox();
    }

}
?>
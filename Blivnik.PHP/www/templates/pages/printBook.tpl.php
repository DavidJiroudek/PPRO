<?php
/**
 * file contains print book form template
 */
/**
 * print book form template
 * @param initialized modules
 * @todo write form generator
 * @todo write for database language
 */
function showPrintBook($modules, $lastDate){
    echo '<div class="basicContent center">';
    echo '<h1> ' . Lang::t('printBook') . '</h1>';
    ?>
        <form action="<?php echo OUTER_PATH."book/";?>" method="POST">
        <p>Zde můžete do PDF vyexportovat více písniček najednou, export slouží primárně k tisku celého zpěvníku, případně dotisku nových písní.</p>
        Hlavní strana: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ano<input class="yesNo" type="range" name="mainPage" min="0" max="1"  value="0">Ne<br>
        <h4>Hudební teorie:</h4>
        Stupnice:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ano<input class="yesNo" type="range" name="stupnice" min="0" max="1" >Ne<br>
        Akordy:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ano<input class="yesNo" type="range" name="chords" min="0" max="1" >Ne<br>
        Kadence:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ano<input class="yesNo" type="range" name="kadence" min="0" max="1" >Ne<br>
        Eulerova Tónová mřížka:&nbsp;&nbsp; Ano<input class="yesNo" type="range" name="euler" min="0" max="1"  value="0">Ne<br>
        
        <h4>Skladby</h4>
        Tisknout Skladby:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ano<input class="yesNo" type="range" name="songs" min="0" max="1"  value="0">Ne<br>
        Nepublikovené skladby:&nbsp;&nbsp;&nbsp; Ano<input class="yesNo" type="range" name="notPublishable" min="0" max="1"  value="0">Ne<br>
        Neschválené skladby:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ano<input class="yesNo" type="range" name="notApproved" min="0" max="1"  value="1">Ne<br>
        Důležitost:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1<input type="range" name="importance" min="1" max="5" value="3">5 <br>
        Odkdy:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="date" name="myDate" value="<?php echo $lastDate;?>"><br>
        <br>
        <input type="submit" value="<?php echo Lang::t('generate');?>">
        </form>    
    <?php
    echo '</div>';
}                                   
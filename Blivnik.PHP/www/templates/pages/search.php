<?php
/**
 * file contains obout web class
 */
/**
 * import templates
 */
require_once(ROOT_PATH . 'templates/basicTemplates/basicPage.tpl.php');
require_once(ROOT_PATH . 'templates/pages/search.tpl.php');

/**
 * about page class
 * @package old
 */
class searchPage{
    /**
     * initialized modules list
     */
    public $modules;
    /**
     * configure and init page modules
     */
    function init(){
        Title::setTitle(Lang::t('menuSearch'));
        $this->modules['addBox'] = new AddBox();
        
        
        showBasicPage($this);
    }

    /**
     * shows body content
     */
    function showContent(){   
          showSearch($this->modules);
          $this->modules['addBox']->showResponsiveBox();
    }

}
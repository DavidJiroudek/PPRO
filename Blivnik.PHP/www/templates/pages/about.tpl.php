<?php
/**
 * template file - page About
 */
 
/**
 * about body template
 * @package old
 * @param modules aray of initialized modules
 */
function showAbout($modules){
    echo '<div class="basicContent">';
    echo 
      "<h1>O zpěvníku</h1>
      <h3> Přidávání skladeb </h3>
      <p>
      Přidat skladbu může každý, kdo se zaregistruje, upravit existující mají možnost pouze administrátoři.
Pokud naleznete chybu, oznamte ji, prosím, na mail - <a href=\"mailto:admin@blivnik.cz\">admin@blivnik.cz</a>, nebo na <a href=\"https://www.facebook.com/blivnik\">facebook Blivníku</a>.
      </p>
      <h3> Export zpěvníku </h3>
      <p>
Hlavní věc, v čem se Blivník liší od ostatních webových zpěvníků. Je zde možnost exportovat si písničky podle Vaší potřeby.
Základní nástroje jsou datum přidání a důležitost skladby. Datum přidání zaručuje, že si vytisknete pouze nově přidané skladby,
 přičemž si server pamatuje, kdy jste si tiskli zpěvník naposledy. Důležitost je u skladby označení, jak je skladba známá. 1-3 jsou vyhrazené pro skladby,
 které zná každý, nebo téměř každý, 4 je pro téměř neznámé a 5 pro neznámé (sem patří např. vlastní tvorba).
Dále si jde vyexportovat zpěvník bez neslušné části, takže může být propůjčen i mládeži.
      </p>
      <h3> Historie </h3>
      <p>
Plán vytvoření vlastního zpěvníku sahá do roku 2010. Na kytarovém soustředění jsme se pravidelně scházeli 
a opakovaně řešili problém, že skladbu, kterou chceme hrát máme jen v jedné kopii, což pro 20 lidí nebylo zrovna optimální.
Přišel nápad vytvořit 2 zpěvníky, které by měl každý - Kytarový Zpěvník a Kytarový Blivník. Obsahem Zpěvníku měly být běžné skladby,
 obsahem Blivníku skladby nepublikovatelné. Základní nástroj měl být LaTEX a zpěvník si měl každý uživatel zkompilovat sám pomocí přidaných scriptů.
Bohužel takovéto řešení nebylo dobré pro uživatele, kteří se v TEXování vůbec nevyznají. Okolo roku 2012 vzniknul plán,
 ve kterém již měl být zpěvník online s možností exportu do PDF. Kvůli nedostatku času jsem ale plán začal realizovat až začátkem léta 2014.
Vznikl tak první Kytarový Blivník. Vše bylo minimalistické, ale fungovalo to. Poté, co bylo přidáno prvních 50 písní jsem ale shledal,
 že to stále ještě není ono. Některé věci byly řešeny značně nešťastně (například s aktualizací písně se měnilo její ID, byla možnost zapsání maximálně 3 interpetů a hosting na moxo.cz měl časté výpadky).
Koncem léta 2014 psát druhou verzi Blivníku(a snad už finální). Přestože není web ještě zcela funční, je již použitelný a proto jsem se rozhodl jej publikovat.
      </p>
      <h3> Budoucnost </h3>
      <p>
V dohledné době přibude vyhledávání, transpozice, možnost přidání not do speciální notové části a možnost přehrání skladby z Youtube.
</p>   
      ";
    echo '</div>';


}
?>
<?php
/**
 * file with book print
 * @package old
 */
/**
 * load template
 */
require_once(ROOT_PATH . 'templates/basicTemplates/pdf.tpl.php');

/**
 * add page class
 * 
 * adds and edits song
 * @package old
 */
class bookPage{

    /**
     * song id
     */
    public $id;
    /**
     * edit errors
     */
    public $errors = '';

    /**
     * configure and init page modules
     */
    function init(){
        showPDF($this);
    }
    /**
     * shows body content
     */
    function showContent($pdf){
        if(RetVal::ifSet("mainPage") && RetVal::r("mainPage") == 0){
            mainPage($pdf);
        }
        if(RetVal::ifSet("euler") && RetVal::r("euler") == 0){
            mrizka($pdf);
        }
        if(RetVal::ifSet("stupnice") && RetVal::r("stupnice") == 0){
            stupnice($pdf);
        }
        if(RetVal::ifSet("kadence") && RetVal::r("kadence") == 0){
            kvint($pdf);
        }
        if(RetVal::ifSet("songs") && RetVal::r("songs") == 0){
            songs($pdf);
        }
        return $pdf;
    }

}
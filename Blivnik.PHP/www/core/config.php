<?php
/**
 * configuration file including configuration class
 */
 /**
  * outer path definition
  * @package core
  */
define('OUTER_PATH', '/'); // outer path to root folder
define('FPDF_FONTPATH', getcwd().'/modules/oldpdf/fpdf/font/'); //FPDF font path
//define('FONTPATH', 'modules/oldpdf/fpdf/font/');
//echo FPDF_FONTPATH;
/**
 * configuration class
 * configured values are in init
 * 
 */
class Settings{
    /**
     * static array of settings
     */
    static $setting;
    
    /**
     * get config var
     * @param part1
     * @param part2
     * @param part3
     * @return NULL if not found
     */
    static function get($a, $b, $c = null){
        if(isset(self::$setting[$a][$b]) && $c == null){
            return self::$setting[$a][$b];
        }
        if(isset(self::$setting[$a][$b][$c])){
            return self::$setting[$a][$b][$c];
        }
        else return NULL;
    }
    
    /**
     * set config var
     * @param part1
     * @param part2
     * @param part3
     * @param value
     */
    static function setVal($a, $b, $c, $value){
        if($c == null){
            self::$setting[$a][$b] = $value;
            return;
        }
 
        self::$setting[$a][$b][$c] = $value;    
    }

}

//configuration file
Settings::setVal('mysqli', 'server', null, 'mysql');
Settings::setVal('mysqli', 'login', null, getenv("MYSQL_USER"));
Settings::setVal('mysqli', 'password', null, getenv("MYSQL_PASSWORD"));
Settings::setVal('mysqli', 'db', null, getenv("MYSQL_DATABASE"));
Settings::setVal('action', 'user_login', 'amount', 2);
Settings::setVal('action', 'user_register', 'amount', 3);
Settings::setVal('action', 'song_edit', 'permission', 4);
Settings::setVal('action', 'song_delete', 'permission', 6);
Settings::setVal('action', 'song_hidden_folders', 'permission', 5);
Settings::setVal('action', 'song_create', 'permission', 1);
Settings::setVal('action', 'song_import', 'permission', 4);
Settings::setVal('action', 'song_appr', 'permission', 3);
Settings::setVal('watchLine', 'strlen', 'amount', 28);
Settings::setVal('alternatives', 'strlen', 'amount', 28);
Settings::setVal('alternatives', 'count', 'amount', 22);
Settings::setVal('alternatives', 'strlen', 'author', 23);
Settings::setVal('user', 'nick', 'strlen', 5);
Settings::setVal('user', 'password', 'strlen', 6);
Settings::setVal('pdf', 'tabDelimiter', null, 83);


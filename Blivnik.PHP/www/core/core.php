<?php
/**
 * core file is for small objects needed in page core
 * @package core
 */

/**
 * defines executed action for log
 * @package core
 */
class Action{
    /**
     * action user id
     */
    public $id;
    /**
     * action text
     */
    public $text;
    /**
     * action details - string describing action
     */
    public $details;
}

/**
 * provides coin transaction
 * @package core
 */
class Points{
    /**
     * adds points to user
     * @param user id
     * @param action name
     * @todo move to user
     */
    static function Add($id, $actionName){
        $amount = Settings::get('action', $actionName, 'amount');
        if($amount != NULL && is_numeric($id)){
            Con::$con->query('UPDATE Users SET points = points + '.$amount.', pointsTotal= pointsTotal + '.$amount.' WHERE id = '.$id) OR die(Con::$con->error);
        }
    }
}



/**
 * sends mails such as registration
 * @package core
 * @todo write class
 */
class Email{
    /**
     * sends registration mail
     * @param user id
     * @param email
     * @param name + surname
     * @param login
     * @todo rewrite email
     */
    static function registerMail($id, $email, $name, $login){
        $headers = "From: admin@blivnik.cz\n";
        $text="
        Dekujeme V�m za registraci v Kytarov�m Blivn�ku. Pro kontrolu va�e �daje jsou: 
        jm�no: ".$name."
        email: ".$email."
        V�mi zvolen� heslo z bezpecnostn�ch duvodu na mail nepos�l�me, pokud heslo ztrat�te, napi�te n�m na email.
        V� �cet je potreba aktivovat, link pro aktivaci: http://www.blivnik.cz/activate.php?act=".sha1($login.$id)."&ID=".$id."
        
        Pokud jste se neregistrovali, napi�te mi, pros�m, zpr�vu, pravdepodobne se nekdo sna�il zaregistrovat na V� email.
        
        S pozdravem
        Humblee
        Administr�tor Kytarov�ho Blivn�ku
        ";
        mail($email, "Registrace - Kytarov� Blivn�k", $text, $headers);
    }
}

?>
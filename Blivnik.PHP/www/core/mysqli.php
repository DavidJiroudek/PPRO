<?php
/**
 * file contains mysqli connection class
 */
/**
 * mysqli loading class
 * 
 * singleton for access to DB
 */
class Con{
    /**
     * mysqli connection
     */
    static $con;
    /**
     * initialize mysqli connection
     * @param db server
     * @param db login
     * @param db password
     * @param database
     */
    static function init($server, $login, $password, $db){
        self::$con = new mysqli($server, $login, $password, $db);
        
        if (self::$con->connect_error) {
            header($_SERVER["SERVER_PROTOCOL"] . ' 503 Service Temporarily Unavailable');
            header('Status: 503 Service Temporarily Unavailable');
            header('Retry-After: 3600');
            die('Server db not available');
        }
        self::$con->query('SET NAMES "utf8"');
    } 
}


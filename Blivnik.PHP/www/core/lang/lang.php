<?php
/**
 * this file is containing language class
 */
/**
 * language manager class
 *  @package core
 *  @todo switch language selecting and variable in User into lang id
 */
class Lang{
    /**
     * defualt language
     */
    static $def;
    /**
     * chosen language
     */
    static $code;
    /**
     * wordlist
     */
    static $wlist;
    /**
     * loads language from database
     * 
     * takes 2 languages - selected and default, loads them from database, saves default,
     * rewrite words with existing language -> if translation doesn't exist uses default language
     * 
     * @param default language
     * @param selected language
     */
    static function init($a, $b){
        self::$def = $a;
        self::$code = $b;
        $resulta = Con::$con->query('SELECT name, text, languages.language AS lang FROM langTexts LEFT JOIN languages ON langTexts.language = languages.id WHERE languages.language LIKE "' . $a . '"') OR die(Con::$con->error);
        $resultb = Con::$con->query('SELECT name, text, languages.language AS lang FROM langTexts LEFT JOIN languages ON langTexts.language = languages.id WHERE languages.language LIKE "' . $b . '"') OR die(Con::$con->error);
        while($res = $resulta->fetch_array())    //import default language
            self::$wlist[$res['lang']][$res['name']] = $res['text'];
        while($res = $resultb->fetch_array())    //import selected language
            self::$wlist[$res['lang']][$res['name']] = $res['text']; 
    }
    /**
     * returns word for given variable name
     * 
     * @param word
     */
    static function t($word){
        if(isset(self::$wlist[self::$code][$word])){
            return self::$wlist[self::$code][$word];
        }elseif(isset(self::$wlist[self::$def][$word])){
            return self::$wlist[self::$def][$word];
        }else{
            return '#LANG_MISSING#'.$word.'#';
        }       
    } 
    /**
     * returns default and selected language
     * @return object['default']; object['chosen'];
     */
    static function getLangs(){
        $ret['default'] = self::$def;
        $ret['chosen'] = self::$code;
        return $ret;
    }
}
?>
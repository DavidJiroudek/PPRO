<?php
/**
 * english language file
 */

$dict['en']['regError'] = "Uživatel se shodným nickem již bohužel existuje, zvolte si jiný nick.";
$dict['en']['menuAdd'] = "New Song"; 
$dict['en']['menuSongs'] = "Songs";
$dict['en']['menuArtist'] = "Artists";
$dict['en']['menuPrint'] = "Books";
$dict['en']['menuLogin'] = "Login";
$dict['en']['menuRegister'] = "Registration";
$dict['en']['menuEditUser'] = "Settings";
$dict['en']['menuLogout'] = "Logout";
$dict['en']['menuAbout'] = "About";
$dict['en']['menu'] = "&lt;&lt; Menu";
$dict['en']['search'] = "Search";
$dict['en']['blivnik'] = "Kytarový Blivník";
$dict['en']['pageNotFound'] = "Page not found";
$dict['en']['pageNotFoundDesc'] = "Litujeme, ale stránku se nepodařilo zobrazit. Byla smazána, přesunuta, nebo je adresa zadána špatně.";
$dict['en']['contact'] = "Contact: ";
$dict['en']['registerForm'] = "Register form";
$dict['en']['name'] = "Name: ";
$dict['en']['surname'] = "Surname: ";
$dict['en']['nick'] = "Nick: ";
$dict['en']['email'] = "Email: ";
$dict['en']['againEmail'] = "Again Email: ";
$dict['en']['password'] = "Password: ";
$dict['en']['againPassword'] = "Again Password: ";
$dict['en']['agreeWith'] = "I agree with";
$dict['en']['liceneTerms'] = "licence terms";
$dict['en']['register'] = "Register";
$dict['en']['loginForm'] = "Login";
$dict['en']['login'] = "Log me in";
$dict['en']['lostPassword'] = "Pro obnovení ztraceného hesla klikněte zde.";
$dict['en']['blockedAcc'] = "Váš účet ještě nebyl aktivován, nebo byl zablokován.";
$dict['en']['registerSuccess'] = "Účet byl úspěšně založen. <br> Na Vaši adresu byl zaslán ověřovací email.";
$dict['en']['OK'] = "OK";
$dict['en']['NOK'] = "Wrong";
$dict['en']['artistList'] = "Artist list";
$dict['en']['songList'] = "Song list";
$dict['en']['published'] = "Approved";
$dict['en']['unpublished'] = "Not Approved";
$dict['en']['unpublishable'] = "Not Publishable";
$dict['en']['songApprove'] = "Approve song";
$dict['en']['songEdit'] = "Edit song";
$dict['en']['settings'] = "Settings";
$dict['en']['language'] = "Language: ";
$dict['en']['edit'] = "Edit";
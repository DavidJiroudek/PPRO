<?php
/**
 * czech language file
 */

$dict['cs']['regError'] = "Uživatel se shodným nickem již bohužel existuje, zvolte si jiný nick.";
$dict['cs']['menuAdd'] = "Nová Skladba"; 
$dict['cs']['menuSongs'] = "Skladby";
$dict['cs']['menuArtist'] = "Umělci";
$dict['cs']['menuPrint'] = "Knihy";
$dict['cs']['menuLogin'] = "Login";
$dict['cs']['menuRegister'] = "Registrace";
$dict['cs']['menuEditUser'] = "Nastavení";
$dict['cs']['menuLogout'] = "Odhlášení";
$dict['cs']['menuAbout'] = "O Blivníku";
$dict['cs']['menu'] = "&lt;&lt; Menu";
$dict['cs']['search'] = "Hledat";
$dict['cs']['blivnik'] = "Kytarový Blivník";
$dict['cs']['pageNotFound'] = "Stránka nebyla nalezena.";
$dict['cs']['pageNotFoundDesc'] = "Litujeme, ale stránku se nepodařilo zobrazit. Byla smazána, přesunuta, nebo je adresa zadána špatně.";
$dict['cs']['contact'] = "Kontakt: ";
$dict['cs']['registerForm'] = "Registrační formulář";
$dict['cs']['name'] = "Jméno: ";
$dict['cs']['surname'] = "Příjmení: ";
$dict['cs']['nick'] = "Nick: ";
$dict['cs']['email'] = "Email: ";
$dict['cs']['againEmail'] = "Znovu Email: ";
$dict['cs']['password'] = "Heslo: ";
$dict['cs']['againPassword'] = "Znovu Heslo: ";
$dict['cs']['agreeWith'] = "Souhlasím s ";
$dict['cs']['liceneTerms'] = "licenčními podmínkami";
$dict['cs']['register'] = "Registrovat se";
$dict['cs']['loginForm'] = "Přihlášení";
$dict['cs']['login'] = "Přihlásit se";
$dict['cs']['lostPassword'] = "Pro obnovení ztraceného hesla klikněte zde.";
$dict['cs']['blockedAcc'] = "Váš účet ještě nebyl aktivován, nebo byl zablokován.";
$dict['cs']['registerSuccess'] = "Účet byl úspěšně založen. <br> Na Vaši adresu byl zaslán ověřovací email.";
$dict['cs']['OK'] = "V pořádku";
$dict['cs']['NOK'] = "Špatně";
$dict['cs']['artistList'] = "Seznam Umělců";
$dict['cs']['songList'] = "Seznam Skladeb";
$dict['cs']['published'] = "Schválené";
$dict['cs']['unpublished'] = "Neschválené";
$dict['cs']['unpublishable'] = "Nepublikovatelné";
$dict['cs']['songApprove'] = "Schválit Skladbu";
$dict['cs']['songEdit'] = "Upravit Skladbu";
$dict['cs']['settings'] = "Nastavení";
$dict['cs']['language'] = "Jazyk: ";
$dict['cs']['edit'] = "Upravit";          
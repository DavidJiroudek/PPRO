<?php
/**
 * bootloader loads all core files
 * 
 * This file loads all classes, sessions, sets language and initialize sql connection.
 * Requied for all acces pages
 * @package core
 */

//root path
define('ROOT_PATH', dirname(__DIR__) . '/');

//define internal encoding
header('Content-Type: text/html; charset=UTF-8');

mb_internal_encoding('UTF-8'); 
mb_http_output('UTF-8'); 
mb_http_input('UTF-8'); 
mb_regex_encoding('UTF-8');
//language
require_once(ROOT_PATH ."core/lang/lang.php");
//config
require_once(ROOT_PATH . 'core/config.php');
//mysqli
require_once(ROOT_PATH . 'core/mysqli.php');
Con::init(Settings::get('mysqli', 'server'), Settings::get('mysqli', 'login'), Settings::get('mysqli', 'password'), Settings::get('mysqli', 'db'));
require_once(ROOT_PATH . 'core/core.php');


/**
 * dynamic class loader
 * 
 * @param class loaded class
 */
function myAutoloader($class) {
    $class = strtolower($class);
    require_once ROOT_PATH .'core/class/' . $class . '.class.php';
}
spl_autoload_register('myAutoloader');
//session loader
session_start();


//language loader
$lang;
if(isset($_SESSION['user'])){
    $lang = $_SESSION['user']->lang;
}else{
    $language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
    switch ($language){
        case "cs":
            $lang = "cs";
            break;
           
        default:
            // english is default setting
            $lang = "en";
            break;
    }
}
if(isset($_GET['language'])){
    switch ($_GET['language']){
        case "cs":
            $lang = "cs";
            break;
           
        default:
            // english is default setting
            $lang = "en";
            break;
    }
}
Lang::init('cs', $lang);
//action loader
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'loginForm':
            User::loginFromForm();
            break;
        case 'registerForm':
            User::register();
    }
}

?>
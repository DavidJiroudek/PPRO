<?php
/**
 * contains log class
 */
/**
 * class allowing and managing logging
 */
class Log{
    /**
     * log id
     */
    public $id;
    /**
     * user ID
     */
    public $userId;
    /**
     * logged action name
     */
    public $action;
    /**
     * edited content ID
     */
    public $editId;
    /**
     * log time
     */
    public $time;
    /**
     * remote ID
     */
    public $ip;
    /**
     * HTTP_X_FORWARDED_FOR or HTTP_CLIENT_IP
     */
    public $ipRedir;
    
    /**
     * creates log in the database
     * @param action is action object
     */
    static function logAction($action){ 
        $userId = 0;       
        if(isset($_SESSION['user'])){
            $userId = $_SESSION['user']->id;
        }
        $actionText = Con::$con->real_escape_string($action->text);
        $editId = Con::$con->real_escape_string($action->id);
        $details = Con::$con->real_escape_string($action->details);
        $ip = $_SERVER['REMOTE_ADDR'];
        $ipRedir='';
        if(isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != ''){
            $ipRedir = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if(isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != ''){
            $ipRedir = $_SERVER['HTTP_CLIENT_IP'];
        }
        $ipRedir =  Con::$con->real_escape_string($ipRedir);
        
        Con::$con->query('INSERT INTO IPLog (userId, action, details, editId,  time, ip, ipRedir) VALUES ("'.$userId.'", "'.$actionText.'", "'.$details.'", "'.$editId.'", CURRENT_TIMESTAMP, "'.$ip.'","'.$ipRedir.'")') OR die(Con::$con->error);
    }
    
    /**
     * \brief resolves hostname of IP and proxyIP of given log message
     * 
     * TODO
     * @param ID to find info of
     */ 
    function getHostname($logId){
        //gethostbyaddr($_SERVER['REMOTE_ADDR']);
    }
    
    /** 
     * \brief returns array of log objects
     * @param userID if empty all user info will be visible
     * @param number of messages shown
     * @param shown log page
     */
    function logHistory($neededUser, $number, $page){
        $userSql='';
        if(!empty($neededUser)){
            $neededUser = Con::$con->real_escape_string($neededUser);
            $userSql = "WHERE UserID = ".$neededUser;
        }
        $number = Con::$con->real_escape_string($number);
        $page = Con::$con->real_escape_string($page);
        $offset = $number*($page-1);
                   
        $result = Con::$con->query('SELECT * FROM IPLog '.$userSql.' LIMIT '.$page.', '.offset);
        $returnArray;
        while($r = Con::$con->fetch_object('Log')){
            array_push($returnArray, $r);
        } 
        return $returnArray;
    }
}
?>
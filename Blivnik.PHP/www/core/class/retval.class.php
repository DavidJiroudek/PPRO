<?php
/**
 * checks return values for existence
 * @package core
 */
 
/**
 * checks return values for existence, returns values
 * 
 * used as singleton every method finds variable in post, then in get, then returns empty
 * @package core
 */
class RetVal{
    /**
     * returns searched variable
     * @param searched variable name
     * @return variable OR empty string
     */
    static function r($var){
        if(isset($_POST[$var])){
            return $_POST[$var];
        }
        if(isset($_GET[$var])){
            return $_GET[$var];
        }
        return '';
    }
    
    /**
     * returns what action has been done
     * 
     * a as action
     */
    static function action(){
        if(isset($_POST['action'])){
            return $_POST['action'];
        }
        if(isset($_GET['action'])){
            return $_GET['action'];
        }
        return '';
    }
    
    /**
     * returns if value vas not accepted
     * 
     * true if variable was corrupted, w as wrong
     * @param searched variable name
     * 
     */
    static function wrong($var){
        if(isset($GLOBALS['wrong'][$var])){
            return true;
        }
        return false;
    }
    /**
     * sets flag for variable that is wrong
     * @param variable name
     */
    static function setWrong($var){
        $GLOBALS['wrong'][$var] = true;
    }
    /**
     * returns if value is set
     * @param searched variable name
     * @return true if set
     */
    static function ifSet($var){
        if(isset($_POST[$var])){
            return true;
        }
        if(isset($_GET[$var])){
            return true;
        }
        return false;
    }
}
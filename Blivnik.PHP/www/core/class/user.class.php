<?php
/**
 * file contains class manipulating with users
 */
/**
 * user class
 *  user class defines user methods
 *  @package core
 */
class user{
    /**
     * user id
     */
    public $id;
    /**
     * user name
     */
    public $name;
    /**
     * user nick
     */
    public $nick;
    /**
     * user password hash
     */
    public $password = '';
    /**
     * user permission
     */
    public $permission;
    /**
     * user current points
     */
    public $points;
    /**
     * user total points
     */
    public $pointsTotal;
    /**
     * user registration date
     */
    public $registered;
    /**
     * user language
     */
    public $lang;
    /**
     * user email
     */
    public $email;
    /**
     * password for variable check
     */
    static private $pass;
    /**
     * email for variable check
     */
    static private $contrEmail;
    
    
    /**
     * loads all data of user
     * @param id to load from db
     */
    static function load($id){
        if(!is_numeric($id) || $id == 0){
            return;    
        }
        $result = Con::$con->query('SELECT * FROM Users WHERE id = '.$id) OR die(Con::$con->error);
        if($result->num_rows != 1){
            return null; 
        }   
        
        return $result->fetch_object('User');
    }
    
    /**
     * logs user in, action name: user_login
     *    
     * @param user nick
     * @param user password
     * @return sets object to null if not logged
     */
    static function login($nick, $password){
        $nick = Con::$con->real_escape_string($nick);
        $password = self::hashPassword($nick, $password);
        $action = new action;
        $action->text = 'user_login';
        $action->details = $nick;
        $user = NULL;
        

        $result = Con::$con->query('SELECT * FROM Users WHERE nick LIKE "'.$nick.'" AND password LIKE "'.$password.'"');
        if($result->num_rows == 1){
              $user = $result->fetch_object('User');
              if($user->permission > 0){
                  $_SESSION['user'] = $user;
                  $action->id = $user->id;
                  Points::Add($user->id, $action->text);
                  if(isset($_POST['action']) && $_POST['action'] == 'loginForm'){
                      unset($_POST['action']);
                  }
              } else{
                  $_POST['loginFormActivatedWrong'] = "";
              }     
        } else{
            $_POST['loginFormNickWrong'] = "";
            $_POST['loginFormPasswordWrong'] = "";
            $action->id = 0;     
        } 
        Log::logAction($action);    
        return $user;    
    }
    
    /**
     * \brief login form form
     */
    static function loginFromForm(){
        if(isset($_POST['loginFormNick']) && isset($_POST['loginFormPassword'])){
            return self::login($_POST['loginFormNick'], $_POST['loginFormPassword']);
        }
        $_POST['loginFormNickWrong'] = "";
        $_POST['loginFormPasswordWrong'] = "";
        return NULL;
    }
    
    /**
     * \brief logouts user
     */
    static function logout(){
        unset($_SESSION['user']);
    }
    
    /**
     * validate registration data
     */
    static function validateRegister(){
        $wrongArray = Array();
        self::$pass = RetVal::r('registerFormPassword');
        self::$contrEmail =  RetVal::r('registerFormEmail');
        $tests = Array(Array('name' => 'registerFormName', 'funct' => 'User::checkName'),
                       Array('name' => 'registerFormSurname', 'funct' => 'User::checkName'),
                       Array('name' => 'registerFormEmail', 'funct' => 'User::checkEmail'),
                       Array('name' => 'registerFormPassword', 'funct' => 'User::checkPassword'),
                       Array('name' => 'registerFormNick', 'funct' => 'User::checkNick'),
                       Array('name' => 'registerFormAgainPassword', 'funct' => 'User::checkSamePassword'),
                       Array('name' => 'registerFormAgainEmail', 'funct' => 'User::checkSameEmail'),
                       Array('name' => 'registerFormLicence', 'funct' => 'User::checkLicence')
                      );
        foreach($tests as $test){
            if(!self::checkPost($test['name'], $test['funct'])){
                array_push($wrongArray, $test['name']);
            }
        }
        return $wrongArray;

    } 
    
    /**
     * validate change user settings
     */
    static function validateSettings(){
        self::$pass =  RetVal::r('settingsFormPassword');
        $wrongArray = Array();
        $tests = Array(Array('name' => 'settingsFormName', 'funct' => 'User::checkName'),
                       Array('name' => 'settingsFormSurname', 'funct' => 'User::checkName'),
                       Array('name' => 'settingsFormEmail', 'funct' => 'User::checkEmail'),
                       Array('name' => 'settingsFormPassword', 'funct' => 'User::checkPassword'),
                       Array('name' => 'settingsFormAgainPassword', 'funct' => 'User::checkSamePassword'),
                       Array('name' => 'settingsFormNick', 'funct' => 'User::checkNick'),
                       Array('name' => 'settingsFormLanguage', 'funct' => 'User::checkLanguage')
                      );
        foreach($tests as $test){
            if(!self::checkPost($test['name'], $test['funct'])){   
                array_push($wrongArray, $test['name']);
            }
        }
        return $wrongArray;

    } 
    
    /**
     * \brief register user, action name: user_change_settings
     * //gets data from POST
     */
    static function changeSettings(){
        $wrongArray = self::validateSettings();
        if(!empty($wrongArray)){
            return NULL;    
        }
        $nick = Con::$con->real_escape_string($_POST['settingsFormNick']);
        $password = self::hashPassword($nick, $_POST['settingsFormPassword']);
        $name = Con::$con->real_escape_string($_POST['settingsFormName']);
        $surname = Con::$con->real_escape_string($_POST['settingsFormSurname']);
        $email = Con::$con->real_escape_string($_POST['settingsFormEmail']);
        $lang = Con::$con->real_escape_string($_POST['settingsFormLanguage']);
        
        $action = new Action;
        $action->text = 'user_change_settings';
        Con::$con->query('UPDATE Users SET name = "' . $name . '",
                                           surname = "' . $surname . '",
                                           nick = "' . $nick . '",
                                           email = "' . $email . '",
                                           password = "' . $password . '",
                                           lang =  "' . $lang . '"
                                       WHERE id = ' . $_SESSION['user']->id) OR die(Con::$con->error);
        $action->id = $_SESSION['user']->id;
        $_SESSION['user'] = self::load($_SESSION['user']->id);//user reload
        Log::logAction($action);        
    }
    
    /**
     * \brief register user, action name: user_register
     * //gets data from POST
     */
    static function register(){
        $wrongArray = self::validateRegister();
        if(!empty($wrongArray)){
            return NULL;    
        }
        $nick = Con::$con->real_escape_string($_POST['registerFormNick']);
        $password = self::hashPassword($nick, $_POST['registerFormPassword']);
        $name = Con::$con->real_escape_string($_POST['registerFormName']);
        $surname = Con::$con->real_escape_string($_POST['registerFormSurname']);
        $email = Con::$con->real_escape_string($_POST['registerFormEmail']);
        $action = new Action;
        $action->text = 'user_register';
        $result = Con::$con->query('SELECT * FROM Users WHERE nick LIKE "'.$nick.'"') OR die(Con::$con->error);
        if($result->num_rows == 0){
            Con::$con->query('INSERT INTO Users (name, surname, nick, email, password, lang, registered) VALUES ("'.$name.'", "'.$surname.'", "'.$nick.'", "'.$email.'", "'.$password.'", "'.Lang::getLangs()['chosen'].'", CURRENT_TIMESTAMP)');
            $userId = Con::$con->insert_id;
            Email::registerMail($userId, $email, $name . ' ' . $surname, $nick);
            $_POST['action'] = 'registerFormSuccess';
            Points::Add($userId, $action->text);
            Log::logAction($action);
        }else{
            $action->id = $result->fetch_object('User')->id;
            $_POST['registerFormNick']['wrong'] = "";
            $action->details = 'Existing:' . $userId;
            Log::logAction($action);
            return NULL;
        }
        
    }
    
    /**
     * check if name is valid (from POST name)
     * @param chcecked item
     * @param function to check item
     */
    static function checkPost($item, $func){
        if(is_callable($func)){
            if(RetVal::ifSet($item) && call_user_func($func, (RetVal::r($item)))){
                return true;
            }
        }
        RetVal::setWrong($item);
        return false;
    }
    
    /**
     * check if name is valid
     * @param given name
     */
    static function checkName($name){
        return $name != '' ? true : false;
    }
    

    /**
     * check email for vlidity
     * @param given email
     */
    static function checkEmail($email){
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }
    
    /**
     * check if nick is unique and long enough
     * @param given nick
     */
    static function checkNick($nick){
        Con::$con->real_escape_string($nick);
        if(self::islogged()){
            if($nick == $_SESSION['user']->nick){
                return true;
            }    
        }
        if(mb_strlen($nick) >= Settings::get('user', 'nick', 'strlen')){
            $result = Con::$con->query('SELECT id FROM Users WHERE nick LIKE "' . $nick . '"') OR die(Con::$con->error);
            if($result->num_rows == 0){
                return true;
            }
        }
        return false;
    }
  
    /**
     * check if password is long enough
     * @param given password
     */
    static function checkPassword($password){
        if(mb_strlen($password) >= Settings::get('user', 'password', 'strlen')){
            return true;
        }
        return false;
    }
    
    /**
     * check licence
     * @param licence flag
     */
    static function checkLicence($licence){
        if($licence == 'yes'){
            return true;
        }
        return false;
    }  
    /**
     * \brief hashing and salting password
     * @param nick
     * @param password
     * @todo salt with random string
     */
    private static function hashPassword($nick, $password){
        return sha1($password.$nick);
    }
    
    /**
     * check if AgainPassword is the same
     * @param given password
     */
     static function checkSamePassword($SamePassword){
        if(self::$pass == $SamePassword){
            return true;
        }
        return false;
     }
     
    /**
     * check if AgainEmail is the same
     * @param same email
     */
     static function checkSameEmail($SameEmail){
        if(self::$contrEmail == $SameEmail){
            return true;
        }
        return false;
     }
    
    /**
     * check if language is valid
     * @param given language
     * @todo rewrite for database version
     */
     static function checkLanguage($lang){
        if($lang == 'cs' || $lang == 'en'){
            return true;
        }
        return false;
     }
    
    /**
     * check if user has permission for given action
     * @param action name
     */
    public function permission($action){
        $perm = Settings::get('action', $action, 'permission');
        if($perm != null && $this->permission >= $perm){
            return true;
        }
        return false;    
    }
    
    /**
     * if user is logged in
     */
     public static function isLogged(){
        if(isset($_SESSION['user'])){
            return true;
        }
        return false;
     }
     
     /**
      * get param returns dynamic values stored in DB
      * if nothing found returns null
      */
    public function getParam($name){
        $name = Con::$con->real_escape_string($name);
        $ret = Con::$con->query('SELECT value FROM userDynamicValue WHERE (userId = ' . $this->id . ' AND name LIKE "' . $name . '")') OR die(Con::$con->error);
        if($ret->num_rows){
            $data = $ret->fetch_array();
            return $data["value"];
        }
        return null;
    }
     /**
      * set param returns dynamic values stored in DB
      * params list
      * lastBookDate - old book last generation date
      */
    public function setParam($name, $value){
        $name = Con::$con->real_escape_string($name);
        $ret = Con::$con->query('SELECT value FROM userDynamicValue WHERE (userId = ' . $this->id . ' AND name LIKE "' . $name . '")') OR die(Con::$con->error);
        if($ret->num_rows){
            Con::$con->query('UPDATE userDynamicValue SET value = "' . $value . '" WHERE (userID = ' . $this->id . ' AND name LIKE "' . $name . '")') OR die(Con::$con->error);        
        }else{
            Con::$con->query('INSERT INTO userDynamicValue (userId, name, value) VALUES (' . $this->id . ', "' . $name . '", "' . $value . '")') OR die(Con::$con->error);
        }   
    }
}


?>
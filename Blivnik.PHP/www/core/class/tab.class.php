<?php
/**
 * file contains tab class
 */
/**
 * class parsing and showing tabulatures
 * @package old
 */
class Tab{
    /**
     * taublature name
     */
    public $name;
    /**
     * list of tabulature lines
     */
    public $rawLines = Array();
  
    function __construct($jmeno){
        $this->name = $jmeno;
    }
    /**
     * add line to tab definition
     * @param string line to add
     */
    function addLine($line){
         $this->rawLines[] = $line;
    }
    /**
     * write parsed tabs
     */
    function writeParsed(){
        $shadowLines = $this->rawLines;
        $returnString = "";
        while(strlen($shadowLines[0]) > Settings::get('pdf', 'tabDelimiter')){
            foreach($shadowLines as &$shadowLine){
                $i = $this->findSplit($shadowLine);
                $returnString .= substr($shadowLine, 0 , $i+1);
                $returnString .= "\n";
                $shadowLine = substr($shadowLine,$i);
            }
            $returnString .= "\n";
        }
        foreach($shadowLines as $shadowLine){
            $returnString .= $shadowLine."\n";;
        }
        return $returnString;
    }
    /**
     * resolve line split
     */
    function findSplit($line){
        for($i = Settings::get('pdf', 'tabDelimiter'); $i>=0; $i--){
            if($line[$i]=='|'){
                return $i;
            }
        }
        return 0;
    }
    
}
<?php
/**
 * ajax call api
 * 
 * This file enables AJAX actions for web, loads classes via bootloader
 * module functions are stored in ajax/*module* and are included manually
 * @TODO automatic class loading
 * @package core
 */
 
//bootloader
require_once('../core/bootloader.php');
require_once( ROOT_PATH . 'ajax/ajaxHead.php');

if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'validateRegForm':            
            echo headValidateRegister();
            break;
        case 'validateSettingsForm':            
            echo headValidateSettings();
            break;
    }
}
//print_r($_POST);
?>
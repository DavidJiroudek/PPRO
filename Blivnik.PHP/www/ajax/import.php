<?php
/**
 * this file provides import form another pages
 * @package old
 */
/**
 * import from another pages
 * 
 * allows to import songs from another website
 */
abstract class Import
{
    var $rawData;
    var $finalText = "";
    
    public function __construct($address){
        $this->rawData = file_get_contents($address);
        $this->rawData = iconv('windows-1250', "UTF-8", $this->rawData);
    }
    
    public function parseSong(){}
    
    public function parseText(){}
    
    public function parseChords(){}
    
    public function parseTabs(){}
    
    public function writeSong(){
        echo "[TEXT:]\n".$this->finalText;
    }
    public function addLines(){
        $this->finalText = "    ".$this->finalText;
        $this->finalText = str_replace("\n", "\n    ",$this->finalText);
    }
}

/**
 * imports from supermusic.sk
 * @package old
 */
class ImportSuperMusic extends Import{

    public function __construct($address){
        $this->rawData = file_get_contents("http://www.supermusic.cz/skupina.php?idpiesne=".$address);
        $this->rawData = iconv('windows-1250', "UTF-8", $this->rawData);            
    }
    
    public function parseSong(){
        $data = explode("class=piesen", $this->rawData);
        $this->rawData = $data[2];
        $data = explode("CRIPT>", $this->rawData);
        $this->rawData = $data[1];
        $data = explode("<script", $this->rawData);
        $this->rawData = $data[0];  
        $this->rawData = str_replace("\n", "",$this->rawData);
        $this->rawData = str_replace('"', "",$this->rawData);
    }
    
    public function parseText(){
        $data = explode("</sup>", $this->rawData);
        foreach($data as $chord){
            $textline =  explode("<sup>", $chord)[0];
            $this->finalText .= $textline;
        }
    }
    
    public function parseChords(){
        $rows = explode("<br>", $this->rawData);
        foreach($rows as $row){
            $data = explode("</sup>", $row);
            $textLine = "";
            $chordLine = "";
            foreach($data as $chord){
                $dataChord = explode("<sup>", $chord);
                $text = $dataChord[0]; 
                $textLine .= $text;
                if(isset($dataChord[1])){ //chord found
                    $chordLine = str_pad($chordLine, strlen(utf8_decode($textLine))-2, " ", STR_PAD_RIGHT);
                    $chordLine .= "[".explode("<", explode('>', $dataChord[1])[1])[0]."]";
                } 
            }
            if($chordLine != "")
                $this->finalText .= $chordLine."\n";
            $this->finalText .= $textLine."\n";  
            $chordLine = "";
            $textLine = "";
        } 
    }
}

/**
 * imports from velkyzpevnik.cz
 * @package old
 */
class ImportVelkyZpevnik extends Import{
    public function __construct($address){
        $this->rawData = file_get_contents("http://www.velkyzpevnik.cz/zpevnik/".$address);
        //$this->rawData = iconv('windows-1250', "UTF-8", $this->rawData);  page is in UTF-8
    }
    public function parseSong(){
        $data = explode("class=\"pisen\">", $this->rawData);
        $this->rawData = $data[1];
        $data = explode("</pre>", $this->rawData);
        $this->rawData = $data[0];
    }
    public function parseChords(){
        $this->rawData = " ".$this->rawData;
        $this->rawData = str_replace("\n", "\n ",$this->rawData);
        $this->rawData = preg_replace("/<\/span>\s{0,1}/","]", $this->rawData);
        $this->rawData = preg_replace("/\s<span[\s\S]*?\>/","[", $this->rawData);
        $this->finalText = preg_replace("/<[\\s\\S]{1,}?\>/","", $this->rawData);
    }
    public function addLines(){
        $this->finalText = "   ".$this->finalText;
        $this->finalText = str_replace("\n", "\n   ",$this->finalText);
    }
}

/**
 * imports from velky-zpevnik.cz
 * @package old
 */
class ImportVelkyZpevnik2 extends ImportVelkyZpevnik{
    public function __construct($address){
        $this->rawData = file_get_contents("http://www.velky-zpevnik.cz/".$address);
        //$this->rawData = iconv('windows-1250', "UTF-8", $this->rawData);  page is in UTF-8
    }
    public function parseSong(){
        $data = explode("<pre>", $this->rawData);
        $this->rawData = $data[1];
        $data = explode("</pre>", $this->rawData);
        $this->rawData = $data[0];
    }
    public function addLines(){
    //    $this->finalText = " ".$this->finalText;
    //    $this->finalText = str_replace("\n", "\n ",$this->finalText);
    }
}
/**
 * imports from pisnicky-akordy.cz
 * @package old
 */
class ImportPisnickyAkordy extends ImportVelkyZpevnik2{
    public function __construct($address){
        $this->rawData = file_get_contents("http://www.pisnicky-akordy.cz/".$address);
        //$this->rawData = iconv('windows-1250', "UTF-8", $this->rawData);  page is in UTF-8
    }
}

if(isset($_GET["importID"])){
    if(isset($_GET["importClass"])){
         if($_GET["importClass"] == "superMusic"){
             $test = new ImportSuperMusic($_GET["importID"]);
             $test->parseSong();
             $test->parseChords();
             $test->addLines();
             echo $test->writeSong();
          }
        if($_GET["importClass"] == "velkyZpevnik"){
            $test = new ImportVelkyZpevnik($_GET["importID"]);
            $test->parseSong();
            $test->parseChords();
            $test->addLines();
            echo $test->writeSong();
        }
        if($_GET["importClass"] == "velkyZpevnik2"){
            $test = new ImportVelkyZpevnik2($_GET["importID"]);
            $test->parseSong();
            $test->parseChords();
            $test->addLines();
            echo $test->writeSong();
        }
        if($_GET["importClass"] == "pisnickyAkordy"){
            $test = new ImportPisnickyAkordy($_GET["importID"]);
            $test->parseSong();
            $test->parseChords();
            $test->addLines();
            echo $test->writeSong();
        }
    }
}
?>
<?php
/**
 * head ajax api file
 */
/**
 * ajax api function for validationg register form
 * @package head
 */
function headValidateRegister(){  
    $okImage = OUTER_PATH . "img/core/ok.png";
    $nokImage = OUTER_PATH . "img/core/nok.png";  
    $wrongArray = User::validateRegister();
    if(in_array($_POST['registerFormValidatedElement'], $wrongArray)){
        echo '<img src="' . $nokImage . '" alt="' . Lang::t('NOK') . '" class="itemApprovedImage">';
    } else{
        echo '<img src="' . $okImage . '" alt="' . Lang::t('OK') . '" class="itemApprovedImage">';
    }
}

/**
 * ajax api function for validationg settings form
 * @package head
 */
function headValidateSettings(){  
    $okImage = OUTER_PATH . "img/core/ok.png";
    $nokImage = OUTER_PATH . "img/core/nok.png";  
    $wrongArray = User::validateSettings();
    if(in_array($_POST['settingsFormValidatedElement'], $wrongArray)){
        echo '<img src="' . $nokImage . '" alt="' . Lang::t('NOK') . '" class="itemApprovedImage">';
    } else{
        echo '<img src="' . $okImage . '" alt="' . Lang::t('OK') . '" class="itemApprovedImage">';
    }
}

function fetchImport(type){
    var ID = "box" + type;
    var setID = document.getElementById(ID).value;
    var xmlhttp;
        if (!xmlhttp){
            if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp=new XMLHttpRequest();
             }
            else{// code for IE6, IE5
             xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
        }
        else if (xmlhttp.readyState != 0){
              xmlhttp.abort();
        }
        
        xmlhttp.onreadystatechange=function() {
           if (xmlhttp.readyState==4 && xmlhttp.status==200){
                  document.getElementById("songEdit").value = xmlhttp.responseText;
                  hideHelp();
              
          }
        }
        xmlhttp.open("GET","../../ajax/import.php?importID=" + setID + "&importClass=" + type ,true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlhttp.send();
}

function loadImport(){
    var help = document.createElement('div');
       help.setAttribute("id", "help");
       help.setAttribute("class", "help");
       help.innerHTML =  "<h1> Import z ostatních webových stránek</h1>";
       help.innerHTML +=  "Jedná se o pomůcku k vytváření vlastních verzí akordů, import je pouze k předpřipravení písničky k opravě, ne pro přidání celé skladby.<br><br>";
       help.innerHTML += "Supermusic (zadej pouze ID skladby): <input type='textbox' id='boxsuperMusic'><button onclick=\"fetchImport('superMusic');\"> Importuj </button><br>";
       help.innerHTML += "Velkyzpevnik.cz (zadej kategorie/jmeno): <input type='textbox' id='boxvelkyZpevnik'><button onclick=\"fetchImport('velkyZpevnik');\"> Importuj </button><br>";
       help.innerHTML += "Velky-zpevnik.cz (zadej kategorie/jmeno): <input type='textbox' id='boxvelkyZpevnik2'><button onclick=\"fetchImport('velkyZpevnik2');\"> Importuj </button><br>";
       help.innerHTML += "Pisnicky-akordy.cz (zadej kategorie/jmeno): <input type='textbox' id='boxpisnickyAkordy'><button onclick=\"fetchImport('pisnickyAkordy');\"> Importuj </button><br>";
       help.innerHTML += "<br><br><button onclick=\"hideHelp();\"> Zrušit </button>";
       //help.addEventListener("click", hideHelp, false);
    document.body.appendChild(help);
}


function addIn(){
    var elem = document.getElementById('interpret');
    var newDiv = document.createElement('div');
    newDiv.innerHTML += "<input type='text' name='interpret[]' list=\"authors\">";
    elem.appendChild(newDiv);
    }

function hideHelp(){
    document.getElementById("help").remove();
}

function loadHelp(){
    var xmlhttp;
    if (!xmlhttp){
        if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
         }
        else{// code for IE6, IE5
         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    else if (xmlhttp.readyState != 0){
          xmlhttp.abort();
    }
    
    xmlhttp.onreadystatechange=function() {
       if (xmlhttp.readyState==4 && xmlhttp.status==200){
           var help = document.createElement('div');
              help.setAttribute("id", "help");
              help.setAttribute("class", "help");
              help.innerHTML = xmlhttp.responseText;
              help.addEventListener("click", hideHelp, false);
          document.body.appendChild(help);
          
      }
    }
    xmlhttp.open("GET","../js/old/help.txt",true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send();
}

//Finds y value of given object
function findPos(obj) {
    var curtop = 0;
    if (obj.offsetParent) {
        do {
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
    return [curtop];
    }
}
//returns pixels per mm of the screen
function pixelsPerMM(){
    var pixels = document.getElementById("paper").clientWidth;
    pixels = pixels / 210;   //210 mm is width of A4
    return pixels;
}

function parseText(text){
    returnString = '';
    text = text.split("[TEXT:]");
    text = text[1];
    regVerse = /(\s*(\(|\[)[\s\S]*?\n\s*?\n)/g;
    while (verses = regVerse.exec(text)){
        returnString += '<div class="verse">';
        verses[0] = verses[0].replace(/^(\s*\n)/, '');
        verses[0] = verses[0].replace(/(\[(?!:))/g, '&nbsp;<chord>');
        verses[0] = verses[0].replace(/([^:])\]/g, '$1</chord>&nbsp;');
        verses[0] = verses[0].replace(/(\([SRI\d]{1,3}\))/, '<verse>$1</verse>');
        verses[0] = verses[0].replace(/\n/g, '<br>\n')
        verses[0] = verses[0].replace(/ /g, '&nbsp;')
        returnString += verses[0];
        returnString += '</div>'
    }
    return returnString;
}

function parseChords(text){
    var returnString = '<div class="chords">';
    var regexp = /\[(\S{1,10})\]\(([\d,X]*)\)/g;
    while (chord = regexp.exec(text)){
        returnString += '<chord>' + chord[1] + '</chord> <fingers>'  + chord[2] + '</fingers><br>\n';
    }
    returnString += '</div>';
    return returnString;
}

var delimiter = 83;

function findSplit(line){
    for(var i = delimiter;i>=0;i--){
        if(line[i]=='|'){
            return i;
        }
    }
    return 0;
}
function maxLength(lines){
    var i = 0;
    for(key in lines){
        if(lines[key].length > i){
            i = lines[key].length;
        }
    }
    return i;
}

function generateTabHTML(tabs){
    var returnString = '';
    for(key in tabs){
        returnString += '<div class="tabs">';
        returnString += tabs[key].name + "<br>\n"
        var shadowLines = tabs[key].lines;
        while(maxLength(shadowLines) > delimiter){
            for(key in shadowLines){
                var i = findSplit(shadowLines[key]);
                returnString += shadowLines[key].substr( 0 , i+1);
                shadowLines[key] = shadowLines[key].substr(i);
                returnString += "<br>\n";
            }
                returnString += "<br>\n";
        }
        for(key in shadowLines){
            returnString += shadowLines[key] + "<br>\n";
        }
        returnString += '</div>';
    }
    return returnString;
}

function parseTabs(text){
    var regexpTabs = /\[(.{1,50}:)\][\s]*\n(([\s]*\S{0,3}\|[\S]*\|[\s]*\n){1,12})/g;
    var regexpLines = /([\s]*\S{0,3}\|[\S]*\|)[\s]*\n/g;
    var tabArray = new Array();
    var tabyRaw = '';
    while (tabyRaw = regexpTabs.exec(text)){
        var tab = {
            name:"",
            lines: new Array()
        };
        tab.name = tabyRaw[1];
        while (tabLines = regexpLines.exec(tabyRaw[0])){
             tab.lines[tab.lines.length] = tabLines[1];
        }
        tabArray[tabArray.length] = tab;
    }
    return generateTabHTML(tabArray);
}

function parseCapo(text){
    var capo = 0;
    var capoText = "";
    var regexp = /\[CAPO:(\d{1,2})\]/g;
    var capoReg = regexp.exec(text);
    if(capoReg != null){
            capo = capoReg[1];
        }
    if(capo != 0){
        capoText +=  'CAPO: ' + capo;
    }
    return capoText;
}

function euDate(){
    var date = new Date();
    return date.getDate()+". "+(date.getMonth()+1)+". "+date.getFullYear();
}

function parser(text, name, authorText, importance, date, posFlag){
    var stPage = document.createElement('div');
        stPage.setAttribute("id", "paper");
        stPage.setAttribute("class", "paper");
        stPage.innerHTML = '<table id="paperHeader"><tr><td class="left">' + name + '</td><td class="right">' + authorText + '</td></tr></table>';
        stPage.innerHTML += '<table id="paperHeaderSmall"><tr><td class="left">' + date + '</td><td class="center">' + parseCapo(text) + '</td><td class="right">Důležitost: ' + importance + '</td></tr></table>';
        stPage.innerHTML += parseChords(text);
        stPage.innerHTML += parseTabs(text);
        stPage.innerHTML += parseText(text);
    var dest = document.getElementById('dest');
    dest.innerHTML = '';
    dest.appendChild(stPage);
    var secPage = document.createElement('div');
        secPage.setAttribute("id", "paper2");
        secPage.setAttribute("class", "paper");
    var pxMM = pixelsPerMM();
    while((stPage.offsetHeight) >= (297*pxMM)){  //constant, height of A4 in mm
        //(stPage.lastChild).prependTo(secPage);
        secPage.insertBefore(stPage.lastChild, secPage.firstChild);
        //secPage.appendChild(stPage.lastChild);
    } 
    if (secPage.firstChild) {
        dest.appendChild(secPage);
    }
    if(posFlag){
        //window.scroll(0,findPos(document.getElementById("songEdit")));
    }
}
function showText(){
          var text = document.getElementById('origText').innerHTML;
          var name = document.getElementById('origName').innerHTML;
          var authorText = document.getElementById('origAuth').innerHTML ;
          var importance = document.getElementById('origImportance').innerHTML;
          var date = document.getElementById('origDate').innerHTML;
          parser(text, name, authorText, importance, date, false);
}

var callCount = 0;
/**
 * this function allows to draw page only once in 500ms
 * now disabled 
 */ 
function areaDelay(){

    var setCount = function(){
        callCount = 0;
    }
    if(callCount == 0){
          var text = document.getElementById('songEdit').value;
          var name = document.getElementById('name').value;
          var authorText = document.getElementById('authorText').value;
          var importance = document.getElementById('importance').value; 
          if(! text.match(/\n\n$/)){
              //document.getElementById('songEdit').value += '\n\n';
              text += '\n\n';
          }
          parser(text, name, authorText, importance, euDate(), true);
          //callCount++;      
    }
    //setTimeout(setCount, 500);
}

function submitForm(){
    var form = document.forms['pridejForm'];
    
    if(form.checkValidity()){
        form.submit();
    }
    else{
        alert("Nejsou splněny povinné údaje.");
    }
}

function swap(what){
    var search = document.getElementById(what).style;
    if(search.display == 'none' || search.display == ''){
        search.display = 'block';
    }else{
        search.display = 'none';
    }
}

function onFormChange(input){
    var post = {};
    var elems = getElements();
    post['action'] = 'validateRegForm'; 
    post['registerFormValidatedElement'] = input.name;
    for(var i=0; i < elems.length; i++){
        post[elems[i].name] = elems[i].value;
    } 
    callContent(document.getElementById(input.name + 'IMG'), post, "");
}

function onSettingsChange(input){
    var post = {};
    var elems = getSettingsElements();
    post['action'] = 'validateSettingsForm'; 
    post['settingsFormValidatedElement'] = input.name;
    for(var i=0; i < elems.length; i++){
        post[elems[i].name] = elems[i].value;
    } 
    callContent(document.getElementById(input.name + 'IMG'), post, "");
}
function getElements(){
    var elems = Array();
    elems.push(document.getElementById('registerFormName'));
    elems.push(document.getElementById('registerFormSurname'));
    elems.push(document.getElementById('registerFormNick'));
    elems.push(document.getElementById('registerFormEmail'));
    elems.push(document.getElementById('registerFormAgainEmail'));
    elems.push(document.getElementById('registerFormPassword'));
    elems.push(document.getElementById('registerFormAgainPassword'));
    return elems;
}
function getSettingsElements(){
    var elems = Array();
    elems.push(document.getElementById('settingsFormName'));
    elems.push(document.getElementById('settingsFormSurname'));
    elems.push(document.getElementById('settingsFormNick'));
    elems.push(document.getElementById('settingsFormEmail'));
    elems.push(document.getElementById('settingsFormPassword'));
    elems.push(document.getElementById('settingsFormAgainPassword'));
    return elems;
}

//element check load
document.addEventListener('DOMContentLoaded', function () {
    var elems = getElements();
    for(var i=0; i < elems.length; i++){
        elems[i].addEventListener('blur', function(e) {
            onFormChange(e.srcElement);
        }, false); 
    }  

});

document.addEventListener('DOMContentLoaded', function () {
    var elems = getSettingsElements();
    for(var i=0; i < elems.length; i++){
        elems[i].addEventListener('blur', function(e) {
            onSettingsChange(e.srcElement);
        }, false); 
    }  

});

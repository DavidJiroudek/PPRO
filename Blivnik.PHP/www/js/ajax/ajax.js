/**
 * ajax api js
 */
 
function parseData(data, get){
    get = get || false;
    returnString="";
    if(Object.keys(data).length >= 1){
        if(get == true) {returnString += '?';}
        Object.keys(data).forEach(function (entry){
                          returnString += encodeURIComponent(entry) + '=' + encodeURIComponent(data[entry]) + '&'        
                     })
    } 
    return returnString;   
} 

//calls Ajax api server side , changes content of target element
function callContent(target, postData, getData) {
    getData = getData || "";
    var parseGet = parseData(getData, true);
    var parsePost = parseData(postData);
    //Send the proper header information along with the request
    var http = new XMLHttpRequest();
    http.open("POST", ajaxApiPath + parseGet, true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.onreadystatechange = function() {//Call a function when the state changes.
    	if(http.readyState == 4 && http.status == 200) {
          target.innerHTML = http.responseText;
    	}
    }
    http.send(parsePost);
}


function callAtribute(postData, getData, target, atribute) {
    getData = getData || "";
    usage = usage || alert;
    var parseGet = parseData(getData, true);
    var parsePost = parseData(postData);
    //Send the proper header information along with the request
    var http = new XMLHttpRequest();
    http.open("POST", ajaxApiPath + parseGet, true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.onreadystatechange = function() {//Call a function when the state changes.
    	if(http.readyState == 4 && http.status == 200) {
          usage.apply(http.responseText);
    	}
    }
    http.send(parsePost);
}      
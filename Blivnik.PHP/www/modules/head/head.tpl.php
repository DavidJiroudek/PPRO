<?php
/** 
 *head template file
 */

/**
 * shows header
 * 
 * @param initialized header module
 */
function showHead(&$myHead){ 
if(RetVal::ifSet('action') && RetVal::r('detail') == 'activated'){
    RetVal::setWrong('loginAccountActivated');
}
if(RetVal::ifSet('action') && RetVal::r('detail') == 'notActivated'){
    RetVal::setWrong('loginAccountNotActivated');
}
?>
    <div class="topPanel">
        <div class="topLogo">
            <a href="<?=  OUTER_PATH ?>">
            <img src="<?=  OUTER_PATH ?>img/core/logoTop.png" class="topLogoImg" alt="<?php echo Lang::t('blivnik');?>">
            </a>
        </div>
        <div class="topPanelRight">
            <nav>
                <ul class="topUl">
                    <li class="menuItemFirst"><span class="menuItemFirstText"><?php echo Lang::t('menu');?></span>
                    <ul class="topMenuUl">
                        <?php
                        foreach($myHead as $ite){
                            echo '<li class="menuItem"><a href="'.$ite['href'].'">'.$ite['name'].'</a></li>', PHP_EOL;
                        }
                        ?>
                     </ul>
                    </li>
                </ul>
            </nav>
            <div class=searchButton>
                <img src="<?=  OUTER_PATH ?>img/core/lineh.png" class="searchButtonSpacer"  alt="<?php echo Lang::t('spacer');?>">
                <img src="<?=  OUTER_PATH ?>img/core/magnif.png" class="searchButtonImg"  alt="<?php echo Lang::t('search');?>" onclick="swap('search');">  
            </div>
        </div>
    </div>
    <div class="search" id="search">
        <form target="_self" action="<?=  OUTER_PATH ?>search/">
            <div class="searchForm">
                <input type="text" class="searchBox" name="q">
                <input type="submit" value="<?php echo Lang::t('search');?>" class="searchSubmit" />
            </div>
        </form>
    </div>
    <div class="loginForm basicForm" id="loginForm"  <?php echo RetVal::action()== 'loginForm' ? 'style="display: block;"' : ''?>>
        <form method="post">
            <div class="basicFormClose">
                <a href="javascript: swap('loginForm');">✖</a>
            </div>
            <div class="registerFormTitle">
                <h1 class="central"><?php echo Lang::t('loginForm');?></h1>
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormNick"><?php echo Lang::t('nick');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="hidden" name="action" value="loginForm">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('loginFormNick') ? 'red' : '';?>" name="loginFormNick" id="loginFormNick" value="<?php echo RetVal::r('loginFormNick');?>" required>
                </div>       
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormPassword"><?php echo Lang::t('password');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="password" class="itemInput  <?php echo  RetVal::wrong('loginFormPassword') ? 'red' : '';?>" name="loginFormPassword" required>
                </div>            
            </div>
             <div class="registerFormItem">
                <div class="registerFormLicence">
                    <input type="submit" class="registerFormSubmit" value="<?php echo Lang::t('login');?>">
                </div>
            </div>
            <?php
            if(RetVal::wrong('loginFormActivated')){
            ?>
                <div class="registerFormItem">
                    <div class="registerFormLicence">
                        <h2><?php echo Lang::t('blockedAcc');?></h2>          
                    </div>
                </div>
                <?php
            }
            if(RetVal::wrong('loginAccountActivated')){
            ?>
                <div class="registerFormItem">
                    <div class="registerFormLicence">
                        <h2><?php echo Lang::t('activatedAcc');?></h2>          
                    </div>
                </div>
                <?php
            }
            if(RetVal::wrong('loginAccountNotActivated')){
            ?>
                <div class="registerFormItem">
                    <div class="registerFormLicence">
                        <h2><?php echo Lang::t('notActivatedAcc');?></h2>          
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="registerFormItem">
                <div class="registerFormLicence">
                    <h2><a href="<?=  OUTER_PATH ?>lost/"><?php echo Lang::t('lostPassword');?></a></h2>          
                </div>
            </div>
        </form>
    </div>
    
    <div class="registerFormSuccess basicForm" id="registerFormSuccess"  <?php echo RetVal::action()== 'registerFormSuccess' ? 'style="display: block;"' : ''?>>
        <form method="post">
            <div class="basicFormClose">
                <a href="javascript: swap('registerFormSuccess');">✖</a>
            </div>
            <div class="registerFormItem">
                <div class="registerFormLicence">
                    <h2><a href="<?=  OUTER_PATH ?>lost/"><?php echo Lang::t('registerSuccess');?></a></h2>          
                </div>
            </div>
        </form>
    </div>     
    <div class="registerForm basicForm" id="registerForm"  <?php echo RetVal::action()== 'registerForm' ? 'style="display: block;"' : ''?>>
        <div class="basicFormClose">
        <a href="javascript: swap('registerForm');">✖</a>
        </div>
        <div class="registerFormTitle">
            <h1 class="central"><?php echo Lang::t('registerForm');?></h1>
        </div>
        <form method="POST">
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormName"><?php echo Lang::t('name');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="hidden" name="action" value="registerForm">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('registerFormName') ? 'red' : '';?>" name="registerFormName" id="registerFormName" value="<?php echo RetVal::r('registerFormName');?>" required>
                </div>
                <div class="itemApproved" id="registerFormNameIMG">
                </div>         
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormSurname"><?php echo Lang::t('surname');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('registerFormSurname') ? 'red' : '';?>" name="registerFormSurname" id="registerFormSurname" value="<?php echo RetVal::r('registerFormSurname');?>" required>
                </div>
                <div class="itemApproved" id="registerFormSurnameIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormNick"><?php echo Lang::t('nick');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('registerFormNick') ? 'red' : '';?>" name="registerFormNick" id="registerFormNick" value="<?php echo RetVal::r('registerFormNick');?>" required>
                </div>
                <div class="itemApproved" id="registerFormNickIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormEmail"><?php echo Lang::t('email');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('registerFormEmail') ? 'red' : '';?>" name="registerFormEmail" id="registerFormEmail" value="<?php echo RetVal::r('registerFormEmail');?>" required>
                </div>
                <div class="itemApproved" id="registerFormEmailIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormAgainEmail"><?php echo Lang::t('againEmail');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="text" class="itemInput  <?php echo  RetVal::wrong('registerFormAgainEmail') ? 'red' : '';?>" name="registerFormAgainEmail" id="registerFormAgainEmail" value="<?php echo RetVal::r('registerFormAgainEmail');?>" required>
                </div>
                <div class="itemApproved" id="registerFormAgainEmailIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormPassword"><?php echo Lang::t('password');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="password" class="itemInput <?php echo  RetVal::wrong('registerFormPassword') ? 'red' : '';?>" name="registerFormPassword" id="registerFormPassword" required>
                </div>
                <div class="itemApproved" id="registerFormPasswordIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="itemText">
                    <h2><label for="registerFormAgainPassword"><?php echo Lang::t('againPassword');?></label></h2>
                </div>
                <div class="itemInputDiv">
                    <input type="password" class="itemInput <?php echo  RetVal::wrong('registerFormAgainPassword') ? 'red' : '';?>" name="registerFormAgainPassword" id="registerFormAgainPassword" required>
                </div>
                <div class="itemApproved" id="registerFormAgainPasswordIMG">
                </div>            
            </div>
            <div class="registerFormItem">
                <div class="registerFormLicence">
                    <h2 class=" <?php echo  RetVal::wrong('registerFormLicence') ? 'red' : '';?>"><input type="checkbox" id="registerFormLicence" name="registerFormLicence" value="yes" class="registerFormCheckbox <?php echo  RetVal::wrong('registerFormLicence') ? 'red' : '';?>" <?php echo RetVal::ifSet('registerFormLicence') ? 'checked' : '';?>><label for="registerFormLicence" class="<?php echo  RetVal::wrong('registerFormLicence') ? 'red' : '';?>"><?php echo Lang::t('agreeWith');?></label> <a href="<?=  OUTER_PATH ?>licence/" rel="license"><?php echo Lang::t('liceneTerms');?></a></h2>
                </div>
            </div>
            <div class="registerFormItem">
                <div class="registerFormLicence">
                    <input type="submit" class="registerFormSubmit" value="<?php echo Lang::t('register');?>">
                </div>
            </div>
        </form>
    </div>
<?php
}
?>
<?php
/**
 * file including head module logic
 */
/**
 * head module creates top bar with logo, menu and search
 */
class Head{
    /**
     * menu items list
     */
     public $items;
    /**
     * \brief return module header data
     * @return meta and script string
     */
    function initHead(){
        return '<link rel="stylesheet" type="text/css" href="' . OUTER_PATH . 'css/head/head.css">
                <script type="text/javascript" src="' . OUTER_PATH . 'js/head/head.js"></script>';
    }
    
    /**
     * will initialize show template
     */
    function show(){
        require_once(ROOT_PATH . 'modules/head/head.tpl.php');
        showHead($this->items);
    }
    /**
     * head module constructor
     * 
     * sets menu items, controls permissions
     */
    function __construct(){
        $allItems = $this->menuList();
        $permission = 0;
        $points = 0;
        $this->items = Array();
        if(isset($_SESSION['User'])){
            $permission = $_SESSION['User']->permission;
            $points = $_SESSION['User']->pointsTotal;
        }
        foreach($allItems as $allItem){
            $itemPerm = 0;
            $itemPts = 0;
            if(isset($setting['menu'][$allItem['item']]['permission'])){
                $itemPerm = $setting['menu'][$allItem['item']]['permission'];
            }
            if(isset($setting['menu'][$allItem['item']]['points'])){
                $itemPts = $setting['menu'][$allItem['item']]['points'];
            }
            if($permission >= $itemPerm && $points >= $itemPts){
                array_push($this->items, $allItem);
            }
        }
    }
    /**
     * function creating array of menu items
     */
    private function menuList(){
        $menuArray = Array();
        array_push($menuArray, Array('item' => 'printBook', 'href' =>  OUTER_PATH . 'printBook/', 'name' => Lang::t('menuBook')));
        array_push($menuArray, Array('item' => 'songs', 'href' =>  OUTER_PATH . 'songs/', 'name' => Lang::t('menuSongs')));
        array_push($menuArray, Array('item' => 'artists', 'href' => OUTER_PATH . 'artists/', 'name' => Lang::t('menuArtist')));
        if(isset($_SESSION['user'])) {
            array_push($menuArray, Array('item' => 'add', 'href' => OUTER_PATH . 'add/', 'name' => Lang::t('menuAdd')));
            //array_push($menuArray, Array('item' => 'print', 'href' => OUTER_PATH . 'print/', 'name' => Lang::t('menuPrint')));
            array_push($menuArray, Array('item' => 'editUser', 'href' => OUTER_PATH . 'editUser/', 'name' => Lang::t('menuEditUser')));
            array_push($menuArray, Array('item' => 'logout', 'href' => OUTER_PATH . 'logout/', 'name' => Lang::t('menuLogout')));        
        }else{
            array_push($menuArray, Array('item' => 'login', 'href' => 'javascript: swap(\'loginForm\');', 'name' => Lang::t('menuLogin')));
            array_push($menuArray, Array('item' => 'register', 'href' => 'javascript: swap(\'registerForm\');', 'name' => Lang::t('menuRegister')));
        }
        array_push($menuArray, Array('item' => 'about', 'href' => OUTER_PATH . 'about/', 'name' => Lang::t('menuAbout')));
        return $menuArray;
    }
}
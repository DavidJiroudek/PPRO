<?php
/**
 * title module
 * @package title
 */

/**
 * static class managing page title
 * @package title
 */
class Title{
    /**
     * @var string page title
     */
    static $title;

    /**
     * sets page title
     * @param string title
     */
    static function setTitle($tit){
        self::$title = $tit;
    }
    
    /**
     * return module header data
     * @return meta and script string
     */
    function initHead(){
        echo'<LINK rel="shortcut icon" href="' . OUTER_PATH . 'img/title/KB.ico" >';
        echo '<title>';
        if(self::$title != ""){
            echo self::$title;
            echo ' | ';
        }
        echo Lang::t('blivnik'); 
        echo '</title>';
    }  
}
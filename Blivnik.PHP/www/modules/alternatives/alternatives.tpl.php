<?php
/** 
 * addverts template file
 * @package addBox
 */
 
/**
 * alternative songs
 */
function showAlternatives(&$alternatives){  
?>        
    <div class="alternatives">
<?php
        echo '<h2>' . Lang::t('alternatives') . '</h2>';
    foreach ($alternatives->songList as $song){
        echo '<a href="' . OUTER_PATH  . 'song/' . $song->id . '/">' . $song->name . ' </a> ';
        $names = explode (";", $song->authorName);
        $ids = explode (";", $song->authorId);
        $nr = Settings::get('alternatives', 'strlen', 'author');
        if($nr == NULL){
            $nr = 30;
        }
        if(strlen($song->authorName) > $nr && sizeof($names) > 1){
            $names = array_slice($names, 0, -1);
        }
        $i=0;
        foreach( $names as $key=>$name){
            if($i != 0){
                echo ", ";
            } 
            $rawName = explode(",",$name);
            echo '<a class="artistLink" href="' . OUTER_PATH  . 'artist/' . $ids[$key] . '/">' . $rawName[0] . '</a>';
            $i++;
        }
        echo '<br>';
    }
?>
  </div>
<?php
}
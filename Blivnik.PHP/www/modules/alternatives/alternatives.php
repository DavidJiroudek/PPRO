


<?php
/**
 * this file defines Alternatives module for song page and main page
 */
/**
 * watchLine graphical module
 * 
 * This module shows vertical line with songs with same interpret
 * @package watchLine
 */
class Alternatives{
    /**
     * shown id
     */
    public $id;

    public $songList;
    /**
     * \brief return module header data
     * @return meta and script string
     */
    function initHead(){
        return '<link rel="stylesheet" type="text/css" href="' . OUTER_PATH . 'css/alternatives/alternatives.css">';
    }  
    
    /** \brief shows alternatives on page
     */
    function show(){
        $nr = Settings::get('alternatives', 'count', 'amount');
        if($nr == NULL){
            $nr = 20;
        }
        $this->GetSongList($nr);
        require_once(ROOT_PATH . 'modules/alternatives/alternatives.tpl.php');
        showAlternatives($this);
    }
    
    function GetSongList($nr){
        $this->songList = Array();
        $sql = "SELECT oldSong.id AS id, oldSong.nazev, GROUP_CONCAT(DISTINCT oldAuthors.id SEPARATOR ';') AS authId, GROUP_CONCAT(DISTINCT jmeno SEPARATOR ';') as jmeno FROM oldSong
                LEFT JOIN oldAutSong ON oldSong.id = oldAutSong.song
                LEFT JOIN oldAuthors ON oldAutSong.autor = oldAuthors.id
                WHERE oldSong.id IN (
                SELECT DISTINCT oldSong.id FROM oldAutSong
                LEFT JOIN oldAuthors on oldAutSong.autor = oldAuthors.id
                LEFT JOIN oldSong on oldSong.id = oldAutSong.song
                WHERE oldAutSong.autor IN (
                SELECT distinct oldAutSong.autor FROM oldAutSong WHERE oldAutSong.song = " . $this->id . " )
                ) AND (oldAutSong.vztah LIKE 'autorText' OR oldAutSong.vztah LIKE 'autorMusic')
                GROUP BY oldSong.id
                ORDER BY oldSong.seen DESC
                LIMIT " . ($nr + 1);
        $result = Con::$con->query($sql) OR die(Con::$con->error);      
        while($line = $result->fetch_array()){
            if($line["id"] != $this->id){
              $detail = new Detail();
              $detail->id = $line["id"];
              $detail->name =  $this->ForceSongLength($line["nazev"]);
              $detail->authorId =  $line["authId"];
              $detail->authorName =  $line["jmeno"];
              array_push($this->songList, $detail);
            }
        } 
        if(sizeof($this->songList) < $nr){
            $sql = "SELECT oldSong.id AS id, oldSong.nazev, GROUP_CONCAT(DISTINCT oldAuthors.id SEPARATOR ';') AS authId, GROUP_CONCAT(DISTINCT jmeno SEPARATOR ';') as jmeno FROM oldSong
                    LEFT JOIN oldAutSong ON oldSong.id = oldAutSong.song
                    LEFT JOIN oldAuthors ON oldAutSong.autor = oldAuthors.id
                    WHERE (oldAutSong.vztah LIKE 'autorText' OR oldAutSong.vztah LIKE 'autorMusic' ) and oldSong.overena = 1
                    GROUP BY oldSong.id
                    ORDER BY RAND()
                    LIMIT " . ($nr - sizeof($this->songList));
            $result = Con::$con->query($sql) OR die(Con::$con->error);      
            while($line = $result->fetch_array()){
                if($line["id"] != $this->id){
                  $detail = new Detail();
                  $detail->id = $line["id"];
                  $detail->name =  $this->ForceSongLength($line["nazev"]);
                  $detail->authorId =  $line["authId"];
                  $detail->authorName =  $line["jmeno"];
                  array_push($this->songList, $detail);
                }
            } 
        } 
                      
    }
    
    function ForceSongLength($name){
        $nr = Settings::get('alternatives', 'strlen', 'amount');
        if($nr == NULL){
            $nr = 30;
        }
        do{
          if(strlen($name) >= $nr){
            $k = explode(" ", $name);
            $k = array_slice($k, 0, -1);
            $name = implode(" ", $k);
            $name .= "...";
          }
        }while(strlen($name) >= $nr);
        return $name;
    }
}

class Detail{
    public $id;
    public $name;
    public $authorId;
    public $authorName;
}

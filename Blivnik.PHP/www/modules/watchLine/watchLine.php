<?php
/**
 * this file defines WatchLine module and WatchLine detail providing statistics on main page
 */
/**
 * watchLine graphical module
 * 
 * This module shows blue horizontal line with additional info
 * @package watchLine
 */
class WatchLine{
    /**
     * shown name
     */
    public $name;
    /**
     * url to shown image, KB logo if not provided
     */
    public $photo;
    /** detail information
     * list of WatchLine Detail
     */
    public $details = Array();
    
    /**
     * \brief return module header data
     * @return meta and script string
     */
    function initHead(){
        return '<link rel="stylesheet" type="text/css" href="' . OUTER_PATH . 'css/watchLine/watchLine.css">';
               // <script type="text/javascript" src="' . OUTER_PATH . 'js/watchLine/watchLine.js"></script>';
    }  
    
    /** \brief shows watchline on page
     */
    function show(){
        $nr = Settings::get('watchLine', 'strlen', 'amount');
        if($nr == NULL){
            $nr = 20;
        }
        foreach($this->details as &$detail){
            foreach($detail->parts as &$part){
                if(mb_strlen($part['key'] . $part['value']) > $nr + 3){   //three dots
                    $part['value'] = mb_substr($part['value'], 0, ($nr - mb_strlen($part['key'])));
                    $part['value'] .= '...';
                }   
            }
        }
        require_once(ROOT_PATH . 'modules/watchLine/watchLine.tpl.php');
        showWatchLine($this);
    }
    
}
/**
 * watchLine show right part - details
 * 
 * $parts is array, allowed ['key']['value']['href']
 *
 * @package watchLine
 */
class WatchLineDetail{
  /**
   * shown detail name such as "Best songs"
   */
    public $name;
  /**
   * array, allowed ['key']['value']['href']
   */
    public $parts = Array();    
}
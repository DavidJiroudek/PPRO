<?php
/**
 * watchLine template file
 */
/** 
 * watchLine Graphic module
 * @param initialized watchLine module
 * @package watchLine
 */
function showWatchLine($watchline){
    $detailsShown = 7;
    if(!file_exists($watchline->photo)){
        $watchline->photo = '' . OUTER_PATH . 'img/watchLine/notFound.png';
    }
?>
    <div class="watchLine">
        <div class="watchLineInfo">
            <div class="watchLineName">
                <?php echo $watchline->name;?>
            </div>
            <div class="watchLinePhoto">
                <img src="<?php echo $watchline->photo;?>" alt="<?php echo $watchline->name;?>" class="watchLinePhotoImg">      
            </div>
        </div>
        <div class="watchLineDetail">
            <div class="watchLineNameSmall">
                <?php echo $watchline->name;?>
            </div>
            <?php 
            foreach($watchline->details as $detail){
            ?>
                <div class="box">
                    <div class="boxName">
                        <?php echo $detail->name;?>
                    </div>
                    <div class="boxContents">
                        <div class="boxContent">
                        <?php
                            $i = 0;
                            foreach($detail->parts as $part){
                                if($i == $detailsShown){
                                    $i = 0;
                                    echo '</div><div class="boxContent">';
                                }
                                echo '<div class="boxContentItem">';
                                if(isset($part['key']) && $part['key'] != '')
                                    echo '<span class="boxContentKey">'.$part['key'].': </span>';
                                else
                                    echo '&#8226;  ';
                                if(isset($part['href']) && $part['href'] != '')
                                    echo '<a href="'.$part['href'].'">';
                                echo '<span class="boxContentValue">'.$part['value'].'</span>';
                                if(isset($part['href'])  && $part['href'] != '')
                                    echo '</a>';
                                echo '</div>';
                                $i++;    
                            }
                        ?>
                        </div>
                    </div>    
                </div>
            <?php
            }
            ?>
        </div>
    </div>
<?php 
}
?>

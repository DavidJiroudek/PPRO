<?php
function createPDF($output, $pdf){
    $autor=$output["autor"];
    $skladba=$output["skladba"];
    $pridano=$output["pridano"];
    $dulezitost=$output["dulezitost"];
    $capo=$output["capo"];
    $akordy=$output["akordy"];
    $taby=$output["taby"];
    $tabySet=$output["tabySet"];
    $text=$output["text"];
    $textSet=$output["textSet"];
    $pismeno = substr($skladba, 0, 2);

    //vypsani zahlavi
    $pdf->SetFont('Arial','B',12);
    $pdf->Cell(40,10,iconv("UTF-8", "WINDOWS-1250", $autor),0, 0, 'L');
    $pdf->Cell(110,10,iconv("UTF-8", "WINDOWS-1250", $skladba),0, 0, 'C');
    $pdf->Cell(40,10,iconv("UTF-8", "WINDOWS-1250", $pismeno), 0,0, 'R');
    $pdf->Line(10, 18, 200, 18);
    $pdf->Ln();
    $pdf->SetFont('Arial','',8);
    $pdf->Cell(40,1,iconv("UTF-8", "WINDOWS-1250", "Přidáno: ".$pridano),0, 0, 'L');
    $pdf->Cell(110,1,iconv("UTF-8", "WINDOWS-1250", "Capo: ".$capo),0, 0, 'C');

    $pdf->Cell(40,1,iconv("UTF-8", "WINDOWS-1250", "Důležitost: ".$dulezitost),0, 0, 'R');
    $pdf->Ln(6);
    //vypsani akordu
    if($akordy["set"]){
        $pdf->SetFont('Arial','',12);
        foreach($akordy["line"] as $akord){
            $pdf->SetX(20);
            $pdf->Cell(100,0,iconv("UTF-8", "WINDOWS-1250", $akord),0,1, 'L', false);
            $pdf->Ln(5);
        }
    }
    //vypsani tabu
    if($tabySet){
        foreach($taby as $tab){
            if($tab["name"] != ""){
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(50,0,iconv("UTF-8", "WINDOWS-1250", $tab["name"]),0,1, 'L', false);
                $pdf->Ln(3);
            }
            $pdf->SetFont('Courier','',10);
            for($i = 0;$i<=5;$i++){
                if(isset($tab[$i])){
                    $pdf->Cell(70,0,iconv("UTF-8", "WINDOWS-1250", $tab[$i]),0,1, 'L', false);
                    $pdf->Ln(3);
                }
            }
            $pdf->Ln(4);
        }
    }
    //vypsani textu s akordy
    if($textSet){
        foreach($text as $line){
        $ln=3;
        if( (strpos($line, '[') !== FALSE)  && (strpos($line, ':') == FALSE)){
            $line = str_replace( '[' , ' ' , $line);
            $line = str_replace( ']' , ' ' , $line);
            $pdf->SetFont('Courier','B',11);
            $ln=3;
        }else{
            $pdf->SetFont('Courier','',11);
            $ln=5;
        }
        $line = rtrim($line);
        $pdf->Cell(70,0,iconv("UTF-8", "WINDOWS-1250", $line),0,1, 'L', false);
        $pdf->Ln($ln);
        }
    }
    return $pdf;
}
?>
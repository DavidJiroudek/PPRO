<?php

function stupnice(&$pdf){
    $pdf->AddPage();
    $pdf->SetFont('Courier','B',24);
    $pdf->Cell(190,10,iconv("UTF-8", "WINDOWS-1250", "Stupnice Durové"),0, 1, 'C');
    
    $pdf->SetFont('Courier','B',12);
    $pdf->SetY(30);
    $pdf->Cell(190,8,iconv("UTF-8", "WINDOWS-1250", "# (půltón mezi 3-4 a 7-8 stupněm)"),0, 1, 'L');
    $pdf->SetFont('Courier','B',10);
    $pdf->SetY(45);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "C Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "G Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "D Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "A Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "E Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "H Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Fis Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Cis Dur"),0, 1, 'L');

    $pdf->SetFont('Courier','',10);
    $pdf->SetY(45);
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- C, D, E, F, G, A, H, C"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- G, A, H, C, D, E, Fis, G"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- D, E, Fis, G, A, H, Cis, D"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- A, H, Cis, D, E, Fis, Gis, A"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- E, Fis, Gis, A, H, Cis, Dis, E"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- H, Cis, Dis, E, Fis, Gis, Ais, H"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Gis, Ais, H, Cis, Dis, Eis, Fis"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Cis, Dis, Eis, Fis, Gis, Ais, His, Cis"),0, 1, 'L');
    
    $pdf->SetFont('Courier','B',10);
    $pdf->SetY(45);
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "0 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "1 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "2 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "3 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "4 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "5 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "6 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "7 #"),0, 1, 'L');

    $pdf->SetFont('Courier','',10);
    $pdf->SetY(45);
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", ""),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis "),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis, Dis"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis, Dis, Ais"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis, Dis, Ais, Eis"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis, Dis, Ais, Eis, His"),0, 1, 'L');          
    
    
    
    
    
    
    
    
    
    
    
    
    
    $pdf->SetFont('Courier','B',12);
    $pdf->SetY(100);
    $pdf->Cell(190,8,iconv("UTF-8", "WINDOWS-1250", "b(půltón mezi 2-3 a 5-6 stupněm)"),0, 1, 'L');
    $pdf->SetFont('Courier','B',10);
    $pdf->SetY(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "C Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "F Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "B Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Es Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "As Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Des Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Ges Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Ces Dur"),0, 1, 'L');

    $pdf->SetFont('Courier','',10);
    $pdf->SetY(115);
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- C, D, E, F, G, A, H, C"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- F, G, A, B, C, D, E, F"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, C, D, Es, F, G ,A ,B"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Es, F, G, As, B, C, D, Es"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- As, B, C, Des, Es, F, G, As "),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Des, Es, F, Ges, As, B, C, Des"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Ges, As, B, Ces, Des, Es, F, Ges"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Ces, Des, Es, Fes, Ges, As, B, Ces"),0, 1, 'L');
    
    $pdf->SetFont('Courier','B',10);
    $pdf->SetY(115);
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "0 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "1 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "2 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "3 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "4 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "5 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "6 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "7 b"),0, 1, 'L');

    $pdf->SetFont('Courier','',10);
    $pdf->SetY(115);
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", ""),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B "),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As, Des"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As, Des, Ges"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As, Des, Ges, Ces"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As, Des, Ges, Ces, Fes"),0, 1, 'L');        
    
    
    
    
    
    
    
    
    $pdf->SetFont('Courier','B',12);
    $pdf->SetY(170);
    $pdf->Cell(190,8,iconv("UTF-8", "WINDOWS-1250", "Kvintakordy (1. 3. 5. stupeň stupnice)"),0, 1, 'L');  
    $pdf->Cell(190,8,iconv("UTF-8", "WINDOWS-1250","#                                  b"),0, 1, 'L');  
    
    $pdf->SetFont('Courier','B',10);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "C Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "G Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "D Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "A Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "E Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "H Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Fis Dur"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Cis Dur"),0, 1, 'L');
    
    $pdf->SetFont('Courier','',10);
    $pdf->SetY(186);
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- C, E, G"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- G, H, D"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- D, Fis, A"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- A, Cis, E"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- E, Gis, H"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- H, Dis, Fis"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Ais, Cis"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Cis, Eis, Gis"),0, 1, 'L');
    
     $pdf->SetY(186);
    $pdf->SetFont('Courier','B',10);
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "C Dur"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "F dur"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "B dur"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Es dur"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "As dur"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Des dur"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Ges dur"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Ces dur"),0, 1, 'L');
    
    $pdf->SetFont('Courier','',10);
    $pdf->SetY(186);
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- C, E, G"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- F, A, C"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, D, F"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Es, G, B"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- As, C, Es"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Des, F, As"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Ges, B, Des"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Ces, Es, Ges"),0, 1, 'L');
    
    
    
    
    
    
    $pdf->AddPage();
    $pdf->SetFont('Courier','B',24);
    $pdf->Cell(190,10,iconv("UTF-8", "WINDOWS-1250", "Stupnice Mollové"),0, 1, 'C');
    
    $pdf->SetFont('Courier','B',12);
    $pdf->SetY(30);
    $pdf->Cell(190,8,iconv("UTF-8", "WINDOWS-1250", "# (půltón mezi 3-4 a 7-8 stupněm)"),0, 1, 'L');
    $pdf->SetFont('Courier','B',10);
    $pdf->SetY(45);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "a moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "e moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "h moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "fis moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "cis moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "gis moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "dis moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "ais moll"),0, 1, 'L');

    $pdf->SetFont('Courier','',10);
    $pdf->SetY(45);
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- A, H, C, D, E, F, G, A"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- E, Fis, G, A, H, C, D, E"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- H, Cis, D, E, Fis, G, A, H"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Gis, A, H, Cis, D, E, Fis"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Cis, Dis, E, Fis, Gis, A, H, Cis"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Gis, Ais, H, Cis, Dis, E, Fis, Gis"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Dis, Eis, Fis, Gis, Ais, H, Cis, Dis"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Ais, His, Cis, Dis, Eis, Fis, Gis, Ais"),0, 1, 'L');
    
    $pdf->SetFont('Courier','B',10);
    $pdf->SetY(45);
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "0 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "1 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "2 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "3 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "4 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "5 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "6 #"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "7 #"),0, 1, 'L');

    $pdf->SetFont('Courier','',10);
    $pdf->SetY(45);
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", ""),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis "),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis, Dis"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis, Dis, Ais"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis, Dis, Ais, Eis"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, Cis, Gis, Dis, Ais, Eis, His"),0, 1, 'L');          
    

    
    
    $pdf->SetFont('Courier','B',12);
    $pdf->SetY(100);
    $pdf->Cell(190,8,iconv("UTF-8", "WINDOWS-1250", "b(půltón mezi 2-3 a 5-6 stupněm)"),0, 1, 'L');
    $pdf->SetFont('Courier','B',10);
    $pdf->SetY(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "a moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "d moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "g moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "c moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "f moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "b moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "es moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "as moll"),0, 1, 'L');

    $pdf->SetFont('Courier','',10);
    $pdf->SetY(115);
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- A, H, C, D, E, F, G, A"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- D, E, F, G, A, B, C, D"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- G, A, B, C, D, Es, F, G"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- C, D, Es, F, G, As, B, C"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- F, G, As, B, C, Des, Es, F"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, C, Des, Es, F, Ges, As, B"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Es, F, Ges, As, B, Ces, Des, Es"),0, 1, 'L');
    $pdf->SetX(27);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- As, B, Ces, Des, Es, Fes, Ges, As"),0, 1, 'L');
    
    $pdf->SetFont('Courier','B',10);
    $pdf->SetY(115);
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "0 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "1 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "2 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "3 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "4 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "5 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "6 b"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "7 b"),0, 1, 'L');

    $pdf->SetFont('Courier','',10);
    $pdf->SetY(115);
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", ""),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B "),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As, Des"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As, Des, Ges"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As, Des, Ges, Ces"),0, 1, 'L');
    $pdf->SetX(123);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Es, As, Des, Ges, Ces, Fes"),0, 1, 'L');        
    
    
    
    
    
    
    
    
    $pdf->SetFont('Courier','B',12);
    $pdf->SetY(170);
    $pdf->Cell(190,8,iconv("UTF-8", "WINDOWS-1250", "Kvintakordy (1. 3. 5. stupeň stupnice)"),0, 1, 'L');  
    $pdf->Cell(190,8,iconv("UTF-8", "WINDOWS-1250","#                                  b"),0, 1, 'L');  
    
    $pdf->SetFont('Courier','B',10);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "a moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "e moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "h moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "fis moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "cis moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "gis moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "dis moll"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "ais moll"),0, 1, 'L');
    
    $pdf->SetFont('Courier','',10);
    $pdf->SetY(186);
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- A, C, E"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- E, G, H"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- H, D, Fis"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Fis, A, Cis"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Cis, E, Gis"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Gis, H, Dis"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Dis, Fis, Ais"),0, 1, 'L');
    $pdf->SetX(30);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Ais, Cis, Eis"),0, 1, 'L');
    
     $pdf->SetY(186);
    $pdf->SetFont('Courier','B',10);
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "a moll"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "d moll"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "g moll"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "c moll"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "f moll"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "b moll"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "es moll"),0, 1, 'L');
    $pdf->SetX(95);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "as moll"),0, 1, 'L');
    
    $pdf->SetFont('Courier','',10);
    $pdf->SetY(186);
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- A, C, E"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- D, F, A"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- G, B, D"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- C, Es, G"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- F, As, C"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- B, Des, F"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- Es, Ges, B"),0, 1, 'L');
    $pdf->SetX(115);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "- As, Ces, Es"),0, 1, 'L');
    
    $pdf->SetY(240);
    $pdf->SetFont('Courier','B',12);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Druhy"),0, 1, 'L');
    
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "     Aiolská"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "     Harmonická"),0, 1, 'L');
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "     Meldická"),0, 1, 'L');
    
    $pdf->SetFont('Courier','',10);
    $pdf->SetY(246);
    $pdf->SetX(45);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Základní mollová stupnice, při hraní se nezvyšuje ani nesnižuje žadný tón."),0, 1, 'L');
    $pdf->SetX(50);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Při hraní se zvyšuje 7. tón stupnice v obou směrech."),0, 1, 'L');
    $pdf->SetX(45);
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250", "Při hraní ve vzestupném směru se zvyšují 6. a 7. stupně,"),0, 1, 'L'); 
    $pdf->SetX(45); 
    $pdf->Cell(190,6,iconv("UTF-8", "WINDOWS-1250","v opačném se zvýšení ruší. "),0, 1, 'L');    
                                                    
}
?>
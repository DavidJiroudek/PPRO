<?php
function songs(&$pdf, $date){
    $_SESSION["lastDate"]=date("Y-m-d");
    $sqlPart = "";
    $sqlPart .= " AND overena = 1";
    $sql = "SELECT *, UNIX_TIMESTAMP(pridano) AS utime, songs.ID as IDs FROM songs 
            LEFT JOIN `aut-song` ON songs.ID = `aut-song`.song 
            LEFT JOIN autors ON `aut-song`.autor=autors.ID
            WHERE (songs.ID >0 AND `aut-song`.vztah='autorText' ".$sqlPart.")        
            ORDER BY songs.nazev
            COLLATE utf8_czech_ci";
            
    $ret = Con::$con->query($sql) or die(Con::$con->error);
    while($data = mysqli_fetch_array($ret)){
        $skladba = new OldSong($data["ID"]);
        $skladba->parseLoad($data); 
        //get song page count
        $testPDF = new PDF();
        $testPDF->documentInit();
        $skladba->writePDFBookHeader($testPDF);
        if($testPDF->numPageNo() > 1 && $pdf->numPageNo() % 2 == 0){
            $pdf->AddPage();
        }
        $skladba->writePDFBookHeader($pdf);
    }
    
}
?>
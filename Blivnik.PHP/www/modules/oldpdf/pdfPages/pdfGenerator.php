<?php
//require('fpdf/fpdf.php');

class PDF extends FPDF {
    var $_toc=array();
    var $_toi=array();
    var $_tor=array();
    var $_numbering=false;
    var $_numberingFooter=false;
    var $_numPageNum=1;

    function documentInit(){
        $this->AddFont('Arial', '', 'arial.php');
        $this->AddFont('Arial', 'B', 'arialbd.php');
        $this->AddFont('Courier', '', 'cour.php');
        $this->AddFont('Courier', 'B', 'courbd.php');
    }
    function AddPage($orientation='', $format='') {
        parent::AddPage($orientation,$format);
        if($this->_numbering)
            $this->_numPageNum++;
    }

    function startPageNums() {
        $this->_numbering=true;
        $this->_numberingFooter=true;
    }

    function stopPageNums() {
        $this->_numbering=false;
    }

    function numPageNo() {
        return $this->_numPageNum;
    }

    function TOC_Entry($txt, $level=0) {
        $this->_toc[]=array('t'=>$txt,'l'=>$level,'p'=>$this->numPageNo());
    }
    function TOI_Entry($txt, $level=0) {
        $this->_toi[]=array('t'=>$txt,'l'=>$level,'p'=>$this->numPageNo());
    }
    function TOR_Entry($txt, $level=0) {
        $this->_tor[]=array('t'=>$txt,'l'=>$level,'p'=>$this->numPageNo());
    }

    function insertTOC( $location=1) {
    $this->_numberingFooter = false;
        //make toc at end
        //$this->stopPageNums();
        $this->AddPage();
        $tocstart=$this->page;

        $this->SetFont('Courier','B',20);
        $this->Cell(0,5,"Obsah",0,0,'C');
        $this->Ln(10);

        foreach($this->_toc as $t) {

            //Offset
            $level=$t['l'];
            if($level>0)
                $this->Cell($level*8);
            $weight='';
            if($level==0)
                $weight='B';
            $str=$t['t'];
            $this->SetFont('Courier','B',12);
            $strsize=$this->GetStringWidth($str);
            $this->Cell($strsize+2,$this->FontSize+2,$str);

            //Filling dots
            $this->SetFont('Courier','',12);
            $PageCellSize=$this->GetStringWidth($t['p'])+2;
            $w=$this->w-$this->lMargin-$this->rMargin-$PageCellSize-($level*8)-($strsize+2);
            $nb=$w/$this->GetStringWidth('.');
            $dots=str_repeat('.',$nb);
            $this->Cell($w,$this->FontSize+2,$dots,0,0,'R');

            //Page number
            $this->SetFont('Courier','B',12);
            $this->Cell($PageCellSize,$this->FontSize+2,$t['p'],0,1,'R');
        }

        //Grab it and move to selected location
        $n=$this->page;
        $n_toc = $n - $tocstart + 1;
        $last = array();

        //store toc pages
        for($i = $tocstart;$i <= $n;$i++)
            $last[]=$this->pages[$i];

        //move pages
        for($i=$tocstart-1;$i>=$location-1;$i--)
            $this->pages[$i+$n_toc]=$this->pages[$i];

        //Put toc pages at insert point
        for($i = 0;$i < $n_toc;$i++)
            $this->pages[$location + $i]=$last[$i];
    $this->_numberingFooter = false;
    }
    
    function insertTOA( $conn) {
        //sql query
            $sql = "SELECT distinct jmeno, group_concat( DISTINCT nazev ORDER BY nazev SEPARATOR ';') AS nazvy FROM `songs` LEFT JOIN `aut-song` ON `songs`.`ID` = `aut-song`.`song` LEFT JOIN `autors` ON `aut-song`.`autor` = `autors`.`ID` WHERE `songs`.`overena`=1  GROUP BY jmeno ORDER BY jmeno
            COLLATE utf8_czech_ci";
        //make toc at end
        //$this->stopPageNums();
        $this->AddPage();
        $tocstart=$this->page;

        $this->SetFont('Courier','B',20);
        $this->Cell(0,5,iconv("UTF-8", "WINDOWS-1250", "Rejstřík umělců"),0,0,'C');
        $this->Ln(10);

        $ret = mysqli_query($conn, $sql) or die(mysqli_error($conn));
        while($data = mysqli_fetch_array($ret)){
            $this->SetFont('Courier','B',12);
            $songs = explode(";", $data["nazvy"]);
            if($this->getY() + 6 + count($songs) > 275){
                $this->AddPage();
            }
            $this->Cell(110, 6,iconv("UTF-8", "WINDOWS-1250", $data["jmeno"]), 0, 1, "L");
            foreach($songs as $song){
                $convSong = iconv("UTF-8", "WINDOWS-1250", $song);
                foreach($this->_toc as $t) { 
                    if($t['t'] == $convSong){
                        $convSong = trim($convSong);
                        $convSong = "    ".$convSong;
                        $this->SetFont('Courier','B',10);
                        $strsize=$this->GetStringWidth($convSong);
                        $this->Cell($strsize+2,4,$convSong);
                        //Filling dots
                        $this->SetFont('Courier','',10);
                        $PageCellSize=$this->GetStringWidth($t['p'])+2;
                        $w=$this->w-$this->lMargin-$this->rMargin-$PageCellSize-($strsize+2);
                        $nb=$w/$this->GetStringWidth('.');
                        $dots=str_repeat('.',$nb);
                        $this->Cell($w,4,$dots,0,0,'R');
            
                        //Page number
                        $this->SetFont('Courier','B',10);
                        $this->Cell($PageCellSize,4,$t['p'],0,1,'R');
                    }
                }
            }
            
        }
    }
        function insertTOI( $location=1) {
        //make toc at end
        $this->AddPage();
        $tocstart=$this->page;

        $this->SetFont('Courier','B',20);
        $this->Cell(0,5,iconv("UTF-8", "WINDOWS-1250", "Rejstřík začátků skladby"),0,0,'C');
        $this->Ln(10);
        $this->_toi = $this->subval_sort($this->_toi, 't');
        foreach($this->_toi as $t) {

            //Offset
            $level=$t['l'];
            if($level>0)
                $this->Cell($level*8);
            $weight='';
            if($level==0)
                $weight='B';
            $str=$t['t'];
            $this->SetFont('Courier','B',12);
            $strsize=$this->GetStringWidth($str);
            $this->Cell($strsize+2,$this->FontSize+2,$str);

            //Filling dots
            $this->SetFont('Courier','',12);
            $PageCellSize=$this->GetStringWidth($t['p'])+2;
            $w=$this->w-$this->lMargin-$this->rMargin-$PageCellSize-($level*8)-($strsize+2);
            $nb=$w/$this->GetStringWidth('.');
            $dots=str_repeat('.',$nb);
            $this->Cell($w,$this->FontSize+2,$dots,0,0,'R');

            //Page number
            $this->SetFont('Courier','B',12);
            $this->Cell($PageCellSize,$this->FontSize+2,$t['p'],0,1,'R');
        }

        //Grab it and move to selected location
        $n=$this->page;
        $n_toc = $n - $tocstart + 1;
        $last = array();

        //store toc pages
        for($i = $tocstart;$i <= $n;$i++)
            $last[]=$this->pages[$i];

        //move pages
        for($i=$tocstart-1;$i>=$location-1;$i--)
            $this->pages[$i+$n_toc]=$this->pages[$i];

        //Put toc pages at insert point
        for($i = 0;$i < $n_toc;$i++)
            $this->pages[$location + $i]=$last[$i];
    }
    
            function insertTOR( $location=1) {
        //make toc at end
        $this->AddPage();
        $tocstart=$this->page;

        $this->SetFont('Courier','B',20);
        $this->Cell(0,5,iconv("UTF-8", "WINDOWS-1250", "Rejstřík refrénů"),0,0,'C');
        $this->Ln(10);
         $this->_tor = $this->subval_sort($this->_tor, 't');
        foreach($this->_tor as $t) {

            //Offset
            $level=$t['l'];
            if($level>0)
                $this->Cell($level*8);
            $weight='';
            if($level==0)
                $weight='B';
            $str=$t['t'];
            $this->SetFont('Courier','B',12);
            $strsize=$this->GetStringWidth($str);
            $this->Cell($strsize+2,$this->FontSize+2,$str);

            //Filling dots
            $this->SetFont('Courier','',12);
            $PageCellSize=$this->GetStringWidth($t['p'])+2;
            $w=$this->w-$this->lMargin-$this->rMargin-$PageCellSize-($level*8)-($strsize+2);
            $nb=$w/$this->GetStringWidth('.');
            $dots=str_repeat('.',$nb);
            $this->Cell($w,$this->FontSize+2,$dots,0,0,'R');

            //Page number
            $this->SetFont('Courier','B',12);
            $this->Cell($PageCellSize,$this->FontSize+2,$t['p'],0,1,'R');
        }

        //Grab it and move to selected location
        $n=$this->page;
        $n_toc = $n - $tocstart + 1;
        $last = array();

        //store toc pages
        for($i = $tocstart;$i <= $n;$i++)
            $last[]=$this->pages[$i];

        //move pages
        for($i=$tocstart-1;$i>=$location-1;$i--)
            $this->pages[$i+$n_toc]=$this->pages[$i];

        //Put toc pages at insert point
        for($i = 0;$i < $n_toc;$i++)
            $this->pages[$location + $i]=$last[$i];
    }
    
    function Footer() {
        if(!$this->_numberingFooter)
            return;
        //Go to 1.5 cm from bottom
        $this->SetY(-15);
        //Select Arial italic 8
        $this->SetFont('Courier','B',12);
        $this->Cell(0,10,'= '.$this->numPageNo().' =',0,0,'C');
        if(!$this->_numbering)
            $this->_numberingFooter=false;
    }

    function subval_sort($a,$subkey) {
        foreach($a as $k=>$v) {
		$b[$k] = strtolower($v[$subkey]);
	}
	asort($b);
	foreach($b as $key=>$val) {
		$c[] = $a[$key];
	}
	return $c;
}
}
?>
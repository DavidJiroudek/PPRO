<?php
function mainPage($pdf){
    $pdf->AddPage();
    //$pdf->Image('../IMG/PDF/frontPageBorder.gif' , 5,5, 200, 290);

    $pdf->Image('../IMG/naVode/mainEvolution.png' , 15,-15, 100);
    
    $pdf->Image('../IMG/naVode/mainMole.jpg' , 120,0, 80);
    $pdf->Image('../IMG/PDF/blivnik-logo2.png' , 15,60, 200);
    $pdf->Image('../IMG/naVode/mainLogo.png' , 80,140, 110);
    $pdf->Image('../IMG/naVode/mainShisha.png' , 0,95, 70);
    $pdf->Image('../IMG/naVode/mainCanoe.png' , 5,170, 120);
    $pdf->Image('../IMG/naVode/mainBeer.png' , 130,170, 60);
    $pdf->Image('../IMG/naVode/mainBottle.jpg' , 15,235, 55);
    $pdf->Image('../IMG/naVode/mainBoat.png' , 120,235, 80);
    $pdf->Image('../IMG/naVode/mainGanja.png' , 75,225, 50);

    $pdf->Image('../IMG/naVode/mainWeb.png' , 70,275, 60);
    //$pdf->Image('../IMG/PDF/motto.png' , 30,230, 150);
    //$pdf->Image('../IMG/PDF/KB.png' , 40,90, 130);
}
function endPage($pdf){
    $pdf->AddPage();
    $pdf->Image('../IMG/naVode/endDeath.jpg' , 35,90, 130);
    $pdf->SetFont('Courier','B',12);
    $pdf->SetY(30);
    $pdf->SetX(20);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "Když jsem se ve 14 rozhodoval co s volným časem, chlast pro mě byl"), 0, 1, "L");
    $pdf->SetX(15);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "jasná volba. Jasně, člověk se musí promazávat, ale když chlastáte"), 0, 1, "L");
    $pdf->SetX(15);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "poctivě, tak játra zkamení."), 0, 1, "L");
    $pdf->SetX(20);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "A když už se chlastá, co si nezazpívat? Falešně, hlasitě,"), 0, 1, "L");
    $pdf->SetX(15);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "sprostě. A čím sprostší text, tím větší jásot davu. Heureka, Blivník."), 0, 1, "L");
    $pdf->SetX(15);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "Ať si sluníčkáři zakapávaj uši voskem, ať si humanisti plkaj, co"), 0, 1, "L");
    $pdf->SetX(15);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "chtěj. Nastal čas si připít, zničit hlasivky a nakonec usnout neznámo"), 0, 1, "L");
    $pdf->SetX(15);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "kde. Tak vzhůru do bezvědomí!"), 0, 1, "L");
    $pdf->Image('../IMG/naVode/endMole.png' , 0,170, 110);
    $pdf->Image('../IMG/naVode/endRQ.png' , 135,190, 50);
    $pdf->Image('../IMG/naVode/endQRInside.png' , 143,208, 23);
    $pdf->SetY(242);
    $pdf->SetX(140);
    $pdf->Cell(50,8, iconv("UTF-8", "WINDOWS-1250", "Humblee © 2015"), 0, 1, "L");
    $pdf->SetY(260);
    $pdf->SetX(95);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "P.S. Děkuji Davidovi a Terce za to,"), 0, 1, "L");
    $pdf->SetX(108);
    $pdf->Cell(180,8, iconv("UTF-8", "WINDOWS-1250", "že dostali Blivník tam, kde je."), 0, 1, "L");
    
} 
?>
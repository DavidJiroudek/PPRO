<?php
function kvint(&$pdf){
    $pdf->AddPage();
    $pdf->SetFont('Courier','B',24);
    $pdf->Cell(190,10,iconv("UTF-8", "WINDOWS-1250", "Kvartový a kvintový kruh"),0, 0, 'C');
    $pdf->Image('img/naVode/kvint.png' , 20,90, 160);
    $pdf->SetY(45);
    $pdf->SetFont('Courier','B',12);
    $pdf->MultiCell(180,6,iconv("UTF-8", "WINDOWS-1250", "- při odvozování postoupnosti stupnic s křížky se pohybujeme v kvintovém kruhu"));
    $pdf->SetY(60);
    $pdf->MultiCell(180,6,iconv("UTF-8", "WINDOWS-1250", "- při odvozování postoupnosti stupnic s béčky se pohybujeme v kvartovém kruhu"));

}
?>
<?php
function songs(&$pdf){  
    if(User::isLogged()){
        $_SESSION["user"]->setParam("lastBookDate", date("Y-m-d"));
    }
    $sqlPart = "";
    if(isset($_POST["importance"])){
        $sqlPart = "AND dulezitost <= ".$_POST["importance"];
    }
    if(isset($_POST["myDate"])){
        if($_POST["myDate"] != ""){
            $timest = strtotime($_POST["myDate"])+ date('Z');
            $sqlPart .= " AND UNIX_TIMESTAMP(schvaleno) > ".$timest;
        }
    }
    if(isset($_POST["notPublishable"])){
        if($_POST["notPublishable"]){
            $sqlPart .= " AND publikovatelna = 1";
        }
    }
    if(isset($_POST["notApproved"])){
        if($_POST["notApproved"] != 0){
            $sqlPart .= " AND overena = 1";
        }
    }
    $sql = "SELECT *, UNIX_TIMESTAMP(pridano) AS utime, oldSong.id as id FROM oldSong 
            LEFT JOIN `oldAutSong` ON oldSong.id = `oldAutSong`.song 
            LEFT JOIN oldAuthors ON `oldAutSong`.autor=oldAuthors.id
            WHERE (`oldAutSong`.vztah='autorText' ".$sqlPart." )
            ORDER BY oldSong.nazev
            COLLATE utf8_czech_ci";
    $ret = Con::$con->query($sql) or die(Con::$con->error);
    while($data = $ret->fetch_array()){
        $skladba = new OldSong($data["id"]);
        $skladba->parseLoad($data);
        $skladba->writePDFHeader($pdf);
    }
}

function songsBook(&$pdf){  
    $sql = "SELECT *, UNIX_TIMESTAMP(pridano) AS utime, oldSong.id as id FROM oldSong 
            LEFT JOIN `oldAutSong` ON oldSong.id = `oldAutSong`.song 
            LEFT JOIN oldAuthors ON `oldAutSong`.autor=oldAuthors.id
            WHERE (`oldAutSong`.vztah='autorText' AND overena = 1)
            ORDER BY oldSong.nazev
            COLLATE utf8_czech_ci";
    $ret = Con::$con->query($sql) or die(Con::$con->error);
    while($data = $ret->fetch_array()){
        $skladba = new OldSong($data["id"]);
        $skladba->parseLoad($data);
        $testPDF = new PdfGenerator();
        $testPDF->documentInit();
        $skladba->writePDFBookHeader($testPDF);
        if($testPDF->numPageNo() > 1 && $pdf->numPageNo() % 2 == 0){
            $pdf->AddPage();
        }
        $skladba->writePDFBookHeader($pdf);
    }
}
?>
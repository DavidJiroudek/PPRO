<?php
http://dusan.pc-slany.cz/hudba/soubory/mrizka.gif
function mrizka($pdf){
    $pdf->AddPage();
    $pdf->SetFont('Courier','B',24);
    $pdf->Cell(190,10,iconv("UTF-8", "WINDOWS-1250", "Akordová mřížka (Euler Tonnetz)"),0, 0, 'C');
    $pdf->Image('img/oldPDF/mrizka.gif' , 35,20, 140);
    $pdf->SetY(133);
    $pdf->SetFont('Courier','B',12);
    $pdf->MultiCell(190,6,iconv("UTF-8", "WINDOWS-1250", "O co se jedná?"));
    $pdf->SetFont('Courier','',12);
    $pdf->MultiCell(190,6,iconv("UTF-8", "WINDOWS-1250", "    Mřížka, kterou objevil Leonhard Euler. Slouží (nejen) pro rychlé odvozování akordů."));
    $pdf->Ln(3);
    $pdf->SetFont('Courier','B',12);
    $pdf->MultiCell(190,6,iconv("UTF-8", "WINDOWS-1250", "Hledání akordů"));
    $pdf->SetFont('Courier','',12);
    $pdf->MultiCell(190,6,iconv("UTF-8", "WINDOWS-1250", "    Každý trojúhelník je normální akord. Durové špičkou nahoru, mollové špičkou dolů. Jeden tón z trojúhelníku akord pojemenovává. Každý akord má svou masku, jejím zobrazení na mřížce lze dohledat tóny. Každá maska obsahuje alespoň jeden kroužek, který označuje základní tón (tón, podle kterého se akord jmenuje). Akordy Dim a Sus mají zvláštní složení, protože každý jejich tón je zároveň pojmenovává. Tak například A dim je totéž, co C dim."));
    $pdf->Image('img/oldPDF/eulerChord.png' , 40,198, 40);
    $pdf->Image('img/oldPDF/masky.png' , 90,196, 100);
    $pdf->SetY(233);
    $pdf->SetFont('Courier','B',12);
    $pdf->MultiCell(190,6,iconv("UTF-8", "WINDOWS-1250", "Další využití"));
    $pdf->SetFont('Courier','',12);
    $pdf->MultiCell(190,6,iconv("UTF-8", "WINDOWS-1250", "Hledání akordu podle tónu - tón musí náležet k jedné z \"pasujících\" masek."));
    $pdf->MultiCell(190,6,iconv("UTF-8", "WINDOWS-1250", "Souzvuk - Dva sousední tóny v mřížce tvoří ladný souzvuk. Tóny, které spolu nesousedí, znějí dohromady disharmonicky. "));
    $pdf->MultiCell(190,6,iconv("UTF-8", "WINDOWS-1250", "Dále lze najít stupnice, kadence a pentatoniky."));
  
}
?>
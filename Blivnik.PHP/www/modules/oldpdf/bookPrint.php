<?php
session_start();
//set_time_limit(3000);
require('fpdf/fpdf.php');
include "pdfPages/pdfGenerator.php";
//include "conn.php";
include "../../core/bootloader.php";
include "pdfPages/mrizka.php";
include "pdfPages/bookPage.php";
include "pdfPages/mainPage.php";
include "pdfPages/bookSongs.php";
include "pdfPages/bookKvint.php";
include "pdfPages/bookStupnice.php";
//get list of authors

$pdf = new PDF();
$pdf->documentInit();
        mainPage($pdf);
        
        
        $pdf->startPageNums();
        $pdf->_numberingFooter = false;
        $pdf->AddPage();
        kvint($pdf);
        stupnice($pdf);
        mrizka($pdf);
        $pdf->AddPage();
        $pdf->AddPage();
        songs($pdf, $conn);
        $pdf->AddPage();

        $pdf->insertTOA($conn);
        $pdf->insertTOC(3);
        $pdf->insertTOI($pdf->numPageNo()+5);
        $pdf->AddPage();
        $pdf->AddPage();
        //endPage($pdf);
        $pdf->Output();

?>
<?php
/** 
 * addverts template file
 * @package addBox
 */
 
/**
 * shows adds
 * @param initialized addbox module
 * @package addBox
 */
function showAddBox(&$addBox){
?> 
    <div class="addBoxRight">
        <!-- vertical_right_big -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:150px;height:600px"
             data-ad-client="ca-pub-5231277322823741"
             data-ad-slot="5980447718"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <div class="addBoxRight_s">
        <!-- vertical_right_small -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:150px;height:400px"
             data-ad-client="ca-pub-5231277322823741"
             data-ad-slot="3026981318"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>   
    </div>
    <div class="addBoxRight addBoxLeft">
        <!-- vertical_big_left -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:150px;height:600px"
             data-ad-client="ca-pub-5231277322823741"
             data-ad-slot="9852244118"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <div class="addBoxRight_s addBoxLeft">
        <!-- vertical_left_small -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:150px;height:400px"
             data-ad-client="ca-pub-5231277322823741"
             data-ad-slot="8096309314"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
<?php

}

/** 
 * responsive block
 * @param initialized addbox module
 * @package addBox
 */
function showResponsiveBox(&$addBox){
?>
    <div class="responsiveAddBox">
        <!-- responsive_1 -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-5231277322823741"
             data-ad-slot="9249163713"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
<?php

}
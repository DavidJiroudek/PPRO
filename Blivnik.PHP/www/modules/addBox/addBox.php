<?php
/**
 * adds module
 */
/**
 * addBox class provides advert boxes, left right - static and central - responsive
 * package addBox
 */
class AddBox{
    /**
     * init head
     */
    function initHead(){
        return '<link rel="stylesheet" type="text/css" href="' . OUTER_PATH . 'css/addBox/addBox.css">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>';
    }
    
    /**
     * will initialize show template
     */
    function show(){
        require_once(ROOT_PATH . 'modules/addBox/addBox.tpl.php');
        showAddBox($this->items);
    }
    /**
     * shows responsive add block
     */
    function showResponsiveBox(){
        require_once(ROOT_PATH . 'modules/addBox/addBox.tpl.php');
        showResponsiveBox($this->items);
    }
    
}
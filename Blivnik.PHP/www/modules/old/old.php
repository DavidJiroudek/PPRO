<?php
/**
 * old class file
 */
/**
 * class processing old blivnik methods
 * @package old
 */
class Old{
    /**
     * init head
     */
    function initHead(){
        return '<link rel="stylesheet" type="text/css" href="' . OUTER_PATH . 'css/old/old.css">
                <script type="text/javascript" src="' . OUTER_PATH . 'js/old/old.js"></script>';
    }
    
    /**
     * returns page with authors list
     */
    function authList($lim){
        $content="";
        $content .= '<div class="basicContent">'; 
        $content .= '<h1>' . Lang::t('artistList') . '</h1>';
        $sql = "SELECT *  FROM oldAuthors 
                    ORDER BY jmeno
                    COLLATE utf8_czech_ci
                    LIMIT ".(($lim-1)*66).", 66";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        $content .= '<div class="artistsList">';
        while($data = $ret->fetch_array()){
            $content.= '<a href="' . OUTER_PATH  . 'artist/' . $data['id'] . '/">' . $data['jmeno'] . '</a><br>';
        }
        
        $content .= "</div>
                      <div class='selectPage'>";
        $sql = "SELECT Count(ID) AS count  FROM oldAuthors";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        $data = $ret->fetch_array();
        
        for($i = 1;$i<=ceil($data["count"]/66);$i++){
            $content.='<a href="' . OUTER_PATH  . 'artists/' . $i . '/">   '.$i.'    </a>';
        }
        $content .= "</div></div>";
        return $content;
    }
    /**
     * returns author detail
     */
    function authorDetail($id){ 
        $sql = "SELECT oldSong.id AS IDs, nazev, dulezitost, pridano, Count(oldSong.id) AS count, jmeno  FROM oldSong 
                    LEFT JOIN `oldAutSong` ON oldSong.id = `oldAutSong`.song 
                    LEFT JOIN oldAuthors ON `oldAutSong`.autor = oldAuthors.id
                    WHERE ((oldAuthors.id = '".$id."'))
                    GROUP BY  oldSong.id
                    ORDER BY nazev
                    COLLATE utf8_czech_ci";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        $content="";
        $content .= '<div class="basicContent">'; 
        $content .= '<h1>' . $this->authorName($id) . '</h1>';
        $content .= '<div class="artistsList">';
        while($data = mysqli_fetch_array($ret)){
            $content.= '<a href="' . OUTER_PATH  . 'song/' . $data['IDs'] . '/">' . $data['nazev'] . '</a><br>';
        }
        Con::$con->query("UPDATE oldAuthors SET seen = seen + 1 WHERE (id = '". $id ."')") or die(Con::$con->error);
        $content .= "</div></div>";
        return $content;
    }
    /**
     * returns name of author, redirect if not found
     */
    function authorName($id){
        $sql = "SELECT jmeno FROM oldAuthors WHERE ID = '".$id."'";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        $data = mysqli_fetch_array($ret);
        if($ret->num_rows != 1){
            header('Location:' . OUTER_PATH . '404/');
        } 
        return OldSong::switchName($data["jmeno"]);
        
    }
    
    /**
     * returns list Page of the songs
     */
    function songList($type, $limit, $folder){
        $type = Con::$con->real_escape_string($type);
        $limit = Con::$con->real_escape_string($limit);
        $folder = Con::$con->real_escape_string($folder);
        $content = "";
        $content .= '<div class="basicContent">'; 


        $content .= '<h1>' . Lang::t('songList') . '</h1>';
        $publ="";
        $content .= '<div class="songMenu">';
        switch($type){
            case 'unpublished':
                $publ="overena=0 AND folder = '" . $folder . "' AND";
                $content .= '<p><a href="' . OUTER_PATH  . 'songs/published/">' . Lang::t('published') . '</a></p><br>'
                          . '<p><a href="' . OUTER_PATH  . 'songs/unpublishable/">' . Lang::t('unpublishable') . '</a></p><br>'
                          . '<p>' . Lang::t('unpublished'). '</p><br>';
                break; 
            case 'unpublishable':
                $publ="overena=1 AND publikovatelna=0 AND";
                $content .= '<p><a href="' . OUTER_PATH  . 'songs/published/">' . Lang::t('published') . '</a></p><br>'
                          . '<p>' . Lang::t('unpublishable'). '</p><br>'
                          . '<p><a href="' . OUTER_PATH  . 'songs/unpublished/">' . Lang::t('unpublished') . '</a></p><br>';
                break;
            case 'published':
            default:
                $publ="overena=1 AND publikovatelna=1 AND";
                $type = 'published';
                $content .= '<p>' . Lang::t('published'). '</p><br>'
                          . '<p><a href="' . OUTER_PATH  . 'songs/unpublishable/">' . Lang::t('unpublishable') . '</p></a><br>'
                          . '<p><a href="' . OUTER_PATH  . 'songs/unpublished/">' . Lang::t('unpublished') . '</p></a><br>';
                break;
        }
        $content .=  '</div>';
        
        if(isset($_SESSION['user']) && $type == 'unpublished' && $_SESSION['user']->permission("song_hidden_folders")){
            $ret = Con::$con->query("SELECT DISTINCT folder FROM `oldSong` WHERE (overena = 0 && folder != '')") or die(Con::$con->error); 
            $content .= '<div class="songMenu">';
            if($folder == ''){
                $content .= '<p>' . Lang::t('visible'). '</p><br>';     
            }else{
                $content .= '<p><a href="' . OUTER_PATH  . 'songs/unpublished/">' . Lang::t('visible') . '</a></p><br>';
            }
            if($ret->num_rows > 0){ 
                while($data = $ret->fetch_array()){
                    if($data['folder'] == $folder){
                        $content .= '<p>' . Lang::t($data['folder']). '</p><br>';     
                    }else{
                        $content .= '<p><a href="' . OUTER_PATH  . 'songs/unpublished/' . $data['folder'] . '">' . Lang::t($data['folder']) . '</a></p><br>';
                    }
                                   
                }         
                $content .=  '</div>';
            } 
        }
        
        
        $sql = "SELECT oldSong.id AS IDs, nazev, GROUP_CONCAT(DISTINCT jmeno SEPARATOR ';') as jmeno,
                       GROUP_CONCAT(DISTINCT oldAuthors.id SEPARATOR ';') as autId,
                       dulezitost, overena, pridano, Count(oldSong.id) AS count
                FROM oldSong
                    LEFT JOIN oldAutSong ON oldSong.id = oldAutSong.song 
                    LEFT JOIN oldAuthors ON oldAutSong.autor=oldAuthors.id
                WHERE (".$publ." (vztah LIKE 'autorText' OR vztah LIKE 'autorMusic'))
                GROUP BY  oldSong.id
                ORDER BY nazev
                COLLATE utf8_czech_ci
                LIMIT ".(($limit-1)*66).", 66";
        if($folder != ""){
          $sql = "SELECT oldSong.id AS IDs, nazev,
                         dulezitost, overena, pridano, Count(oldSong.id) AS count
                  FROM oldSong
                  WHERE (".$publ." 1)
                  GROUP BY  oldSong.id
                  ORDER BY nazev
                  COLLATE utf8_czech_ci
                  LIMIT ".(($limit-1)*66).", 66";
        }
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        
        //generate content
        $content .= '<div class="artistsList">';
        while($data = $ret->fetch_array()){
            if(!isset($data["jmeno"])){
                $data["jmeno"] = Lang::t('fill');    
            }
            if(!isset($data["autId"])){
                $data["autId"] = 0;    
            }
            $jmena = explode (";", $data["jmeno"]);
            $data["jmeno"]="";
            $i=0;
            $autIds = explode (";", $data["autId"]);
            foreach( $jmena as $key=>$jmeno){
                if($i != 0){
                    $data["jmeno"].= ", ";
                }
                $rawJmeno = explode(",",$jmeno);
                $data["jmeno"].= '<a class="artistLink" href="' . OUTER_PATH  . 'artist/' . $autIds[$key] . '/">' . $rawJmeno[0] . '</a>';
                $i++;
            }
            $content.= '<a href="' . OUTER_PATH  . 'song/' . $data['IDs'] . '/">' . $data['nazev'] . ' </a> ' . $data["jmeno"] . '<br>';
        }
        
        $content .= "</div>
                        <div class='selectPage'>";
        $sql = "SELECT Count(oldSong.id) AS count  FROM oldSong WHERE(" . $publ . " 1=1)";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        $data = $ret->fetch_array();
        if($folder != ""){
            $type = $type . "/" . $folder;  
        }
        for($i = 1;$i<=ceil($data["count"]/66);$i++){
            $content.='<a href="' . OUTER_PATH  . 'songs/' . $type . '/' . $i . '/">   '.$i.'    </a>';
        }
        $content .= "</div></div>";
        return $content;
    }
    /**
     * song detail page
     */
    function songDetail($skladba){
        //var_dump($skladba->authorList);
        $content=  '<h1>' . $skladba->name . ' - Akordy</h1>'
                  . '<div id="hiddenText">'
                     . '<div id="origText">' . $skladba->text . '</div>'
                     . '<div id="origName">' . $skladba->name . '</div>'
                     . '<div id="origAuth">' . $skladba->author . '</div>'
                     . '<div id="origImportance">' . $skladba->importance . '</div>'
                     . '<div id="origDate">' . $skladba->date . '</div>'
                  . '</div>'; 
        $count = 1;
        $flagSession = false;
        $flagPerm = false;
        $flagDelete = false;
        if(User::isLogged() && $skladba->appr != 1 && $_SESSION['user']->permission("song_appr")){
            $count++;
            $flagSession = true;
        }
        if(User::isLogged() && $_SESSION['user']->permission("song_edit")){
            $count++;
            $flagPerm = true;
        }   
        if(User::isLogged() && $_SESSION['user']->permission("song_delete")){
            $count++;
            $flagDelete = true;
        }   
        switch($count){
            case 1:
                $count = "one";
                break;
            case 2: 
                $count = "two";
                break;
            case 3: 
                $count = "";
                break;                  
        }       
        $content .= '<div class="songMenu ' . $count . '">';   
        if($flagSession){      
              $content .= '<form id="song_appr" method="POST">';
              $content .= '<input type="hidden" name="action" value="song_appr">';
              $content .= '<a href="#" onclick="document.getElementById(\'song_appr\').submit();">' . Lang::t('songApprove') . '</a><br>';
              $content .= '</form>';
        }  
        if($flagPerm){
            $content .= '<p><a href="' . OUTER_PATH  . 'add/' . $skladba->ID . '/">' . Lang::t('songEdit') . '</a></p><br>';
        }  
        if($flagDelete){
            $content .= '<p><a href="' . OUTER_PATH  . 'delete/' . $skladba->ID . '/">' . Lang::t('songDelete') . '</a></p><br>';
        }  
        $content .= '<p><a href="' . OUTER_PATH  . 'print/' . $skladba->ID . '/">' . Lang::t('songPDF') . '</a><p><br>';
        $content .= '</div>';
        Con::$con->query("UPDATE oldSong SET seen = seen + 1 WHERE (id = '".$skladba->ID."')") or die(Con::$con->error);
        Con::$con->query("UPDATE oldAuthors RIGHT JOIN oldAutSong ON oldAuthors.id = oldAutSong.autor SET seen = seen + 1 WHERE (oldAutSong.song = '".$skladba->ID."')") or die(Con::$con->error);
        
        $content .= 
              '<div id="dest"></div>
                <script type="text/javascript">
                    showText();
                </script>';
        return $content;
    }
    
    /**
     * return name of song, redirect if not found
     */
    function songName($id){
        $sql = "SELECT nazev FROM oldSong WHERE ID = '".$id."'";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        $data = mysqli_fetch_array($ret);
        if($ret->num_rows != 1){
            header('Location:' . OUTER_PATH . '404/');
        } 
        return OldSong::switchName($data["nazev"]);
        
    }
     
    function addAuthor($id, $authType, $jmeno){
        Con::$con->query("INSERT IGNORE INTO oldAuthors (jmeno) VALUES ('".$jmeno."')") or die(Con::$con->error);
        $ret = Con::$con->query("SELECT ID FROM oldAuthors WHERE jmeno = '".$jmeno."'") or die(Con::$con->error);
        $data = $ret->fetch_array();
        $authID = $data["ID"];
        Con::$con->query("INSERT INTO `oldAutSong` (autor, vztah, song) VALUES ('".$authID."', '".$authType."', '".$id."')") or die(Con::$con->error);
    }
    
    function addRelations($id, $authorText, $authorMusic, $interprets){
        //add authors and interprets
        $this->addAuthor($id, "autorText", $authorText);
        $this->addAuthor($id, "autorMusic", $authorMusic);
        foreach($interprets as $interpret){
            if($interpret != ""){
                $this->addAuthor($id, "interpret", $interpret);
            }
        }
    }
    
    static function dropRelations($id){
        Con::$con->query("DELETE FROM `oldAutSong` WHERE song = '".$id."'") or die(Con::$con->error);
        Con::$con->query("DELETE oldAuthors FROM oldAuthors LEFT JOIN `oldAutSong` ON oldAuthors.id=`oldAutSong`.autor WHERE (vztah IS NULL)") or die(Con::$con->error);
    }
    
    function postCheck(){
        $flag = 0;
        if(isset($_POST["interpret"])){
            foreach($_POST["interpret"] as &$interpret){
                $interpret = Con::$con->real_escape_string($_POST["authorText"]);
            }
        }
        if(isset($_POST["name"])){
            $_POST["name"] = Con::$con->real_escape_string($_POST["name"]);
            $flag++;
        }
        if(isset($_POST["text"])){
            $flag++;
        }
        if(isset($_POST["publishable"])){
            $_POST["publishable"] = Con::$con->real_escape_string($_POST["publishable"]);
        }
        if(isset($_POST["importance"])){
            $_POST["importance"] = Con::$con->real_escape_string($_POST["importance"]);
            $flag++;
        }
        if(isset($_POST["authorText"])){
            $_POST["authorText"] = Con::$con->real_escape_string($_POST["authorText"]);
            $flag++;
        }
        if(isset($_POST["authorMusic"])){
            $_POST["authorMusic"] = Con::$con->real_escape_string($_POST["authorMusic"]);
            $flag++;
        }
        if(isset($_POST["editID"])){
            $_POST["editID"] = Con::$con->real_escape_string($_POST["editID"]);
            $flag++;
        }
        if(isset($_POST["noDate"])){
            $_POST["noDate"] = Con::$con->real_escape_string($_POST["noDate"]);
            $flag++;
        }
        return true;
    }
    
    /*
     *values check, if not sent, return by header, values stored in session
     *
     */  
    function addSong($edit){
        $publishable = 0;
        if(isset($_POST["publishable"]) && $_POST["publishable"] == 1){
            $publishable = 1;
        }
        //if song edit
        if($edit != 0){
            $datumString = ", schvaleno=CURRENT_TIMESTAMP";
            $overenaString = ", overena = 0 ";
            if(isset($_POST["noDate"])){
                $_POST["noDate"] = Con::$con->real_escape_string( $_POST["noDate"]);
                if($_POST["noDate"] == 1){
                    $result  = Con::$con->query("SELECT pridano, schvaleno FROM oldSong WHERE ID = '".$edit."'");
                    $datum = mysqli_fetch_array($result);
                    $datumString = ", pridano='".$datum["pridano"]."', schvaleno='".$datum["schvaleno"]."'";
                    $overenaString = "";
                }
            }
            Old::backupSong($_POST["editID"]);
            Con::$con->query("UPDATE oldSong SET nazev='".$_POST["name"]."', text='".urlencode($_POST["text"])."', dulezitost='".$_POST["importance"]."', publikovatelna='".$publishable."' ".$datumString." ".$overenaString." WHERE (id = '".$_POST["editID"]."')") or die(Con::$con->error);
            Old::dropRelations($_POST["editID"]);
            $this->addRelations($_POST["editID"], $_POST["authorText"], $_POST["authorMusic"], $_POST["interpret"]);
            return $_POST["editID"];
        }
        //add new song
        else{
            Con::$con->query("INSERT INTO oldSong ( nazev, text, pridal, dulezitost, publikovatelna, pridano, schvaleno, overena) VALUES ('".$_POST["name"]."', '".Con::$con->real_escape_string(urlencode($_POST["text"]))."', '".$_SESSION['user']->id."', '".$_POST["importance"]."', '".$publishable."', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 0)") or die(Con::$con->error);
            $ret = Con::$con->query("SELECT LAST_INSERT_ID() as ID FROM oldSong");
            $data = mysqli_fetch_array($ret);
            $id = $data["ID"];
            $this->addRelations($id, $_POST["authorText"], $_POST["authorMusic"], $_POST["interpret"]);
            return $id;
        }
    }
    
    function addDetail($id, $error){
        //predef for edit
        $publishable = "";
        $nameValue = "";
        $text = "";
        $importance = 3;
        $authText = "";
        $authMusic = "";
        $edit="";
        $error = "";
        $editTime = "";
        $interpretFields = "<div class='interpretField'>Interpreti: <input type='text' name='interpret[]' list=\"authors\">
                            <input type='button' onClick='addIn();' value='+'></div>";
                            
        $nameValue = RetVal::r('name');
        $text= htmlspecialchars_decode(RetVal::r('text'));
        if(RetVal::r("publishable") == 1){
            $publishable = "checked";
        }   
        $importance = RetVal::r('importance');
        $authorText = RetVal::r('authorText');
        $authorMusic = RetVal::r('authorMusic');

        if(isset($_POST["interpret"]) != ''){
            $interpretFields = "";
            $i=true;
            foreach($_POST["interpret"] as $sesInter){
                if($i){
                    $interpretFields .= "<div class='interpretField'>Interpreti: <input type='text' name='interpret[]' value='".$sesInter."' list=\"authors\">
                                         <input type='button' onClick='addIn();' value='+'></div><br>";
                }
                else{
                    $interpretFields .= "<div><input type='text' name='interpret[]' value='".$sesInter."' list=\"authors\"></div>";
                }
                $i = false;
            }
            if($i==0){
                $interpretFields .= "<div class='interpretField'>Interpreti: <input type='text' name='interpret[]' value='' list=\"authors\">
                                     <input type='button' onClick='addIn();' value='+'></div><br>";
            }
        }  
        $edit="<input type='hidden' name='editID' value='".$id."'>";             
        //settings if edit
        if(isset($id) && $id != 0){
            
            $ret = Con::$con->query("SELECT * FROM oldSong WHERE id = '".$id."'") or die(Con::$con->error);
            $data = mysqli_fetch_array($ret);
            $nameValue = $data["nazev"];
            $text = urldecode($data["text"]);
            if($data["publikovatelna"]){
                $publishable = "checked";
            }
            $importance = $data["dulezitost"];
            //author of text
            $authTextSql = Con::$con->query("SELECT * FROM `oldAutSong` LEFT JOIN oldAuthors ON `oldAutSong`.autor=oldAuthors.ID WHERE (song='".$id."' AND vztah='autorText')") or die(Con::$con->error);
            $dataAuthText = mysqli_fetch_array($authTextSql);
            $authText = $dataAuthText["jmeno"];
            //author of music
            $authMusicSql = Con::$con->query("SELECT * FROM `oldAutSong` LEFT JOIN oldAuthors ON `oldAutSong`.autor=oldAuthors.ID WHERE (song='".$id."' AND vztah='autorMusic')") or die(Con::$con->error);
            $dataMusicText = mysqli_fetch_array($authMusicSql);
            $authMusic = $dataMusicText["jmeno"];
            //interprets
            $interpretFields = "";
            $interpetSql = Con::$con->query("SELECT * FROM `oldAutSong` LEFT JOIN oldAuthors ON `oldAutSong`.autor=oldAuthors.ID WHERE (song='".$id."' AND vztah='interpret')") or die(Con::$con->error);
            $i=0;
            while($dataInterpet = mysqli_fetch_array($interpetSql)){
                if($i == 0){
                    $interpretFields .= "<div class='interpretField'>Interpreti: <input type='text' name='interpret[]' value='".$dataInterpet["jmeno"]."' list=\"authors\">
                                         <input type='button' onClick='addIn();' value='+'></div><br>";
                }
                else{
                    $interpretFields .= "<div><input type='text' name='interpret[]' value='".$dataInterpet["jmeno"]."' list=\"authors\"></div>";
                }
                $i++;
            }
            if($i==0){
                $interpretFields .= "<div class='interpretField'>Interpreti: <input type='text' name='interpret[]' value='' list=\"authors\">
                                     <input type='button' onClick='addIn();' value='+'></div><br>";
            }
            $edit="<input type='hidden' name='editID' value='".$id."'>";
            $editTime="Oprava stojí za přetisk &nbsp;Ano<input type='range' class='yesNo' name='noDate' id='importance' min='0' max='1'  value=0>Ne<br>";
        }
        //end of edit setting

        $sql = "SELECT * FROM oldAuthors";
        $ret = Con::$con->query($sql) or die(Con::$con->error);
        $flagImport = (isset($_SESSION['user']) && $_SESSION['user']->permission('song_import'));  
        //end of err msg settings
        $content = '<div class="basicContent">
                        <h1>Nová Skladba</h1>
                        <form method="POST" name="pridejForm" id="pridejForm">
                        <datalist id="authors">';
        while($data = mysqli_fetch_array($ret) ){
            $content .= '<option value="'.$data["jmeno"].'">';
        }
        $content .= "   </datalist>
                        <div class='songMenu right'>
                            <a href=# onclick=\"submitForm();\">Odeslat</a><br>";
        if($flagImport){
            $content .= "   <a href=# onclick=\"loadImport();return false;\">Import</a><br>";
        }              
        $content .= "       <a href=# onclick=\"loadHelp();return false;\">Nápověda</a><br>
                        </div>
                        <div class='songMenu two'>
                            <div class='addMenuItem'>Jméno Skladby: </div><input type='text' name='name' id='name' onKeyUp='areaDelay();' onChange='areaDelay();' value='".$nameValue."' required autofocus><br>
                            <div class='addMenuItem'>Autor Textu: </div><input type='text' name='authorText' id='authorText' onKeyUp='areaDelay();' onChange='areaDelay();' value='".$authText."' required  list=\"authors\"><br>
                            <div class='addMenuItem'>Autor Hudby: </div><input type='text' name='authorMusic' id='authorMusic' onKeyUp='areaDelay();' onChange='areaDelay();' value='".$authMusic."' required  list=\"authors\"><br>
                           <div class='addMenuItem'>Duležitost: </div>1<input type='range' name='importance' id='importance' min='1' max='5' onKeyUp='areaDelay();' onChange='areaDelay();' value='".$importance."'>5<br>
                            <input type='checkbox' id='publishable' name='publishable' value=1 ".$publishable."/>
                            <label for='publishable'>Skladba je publikovatelná pred 10. hodinou</label><br>
                            " . $editTime . " <br>
                            ".$edit." <br>
                            
                            <input type='hidden' name='action' value='song_add'>
                        </div>
                        <div id='interpret'>".$interpretFields."</div> 
                        <div>
                            " . $error . " 
                        </div>";
                         

              $content .=   "
                      <div id='dest'></div>
                      <div class='spacerBig'></div>
                      <textarea id='songEdit', name='text' onKeyUp='areaDelay();' onChange='areaDelay();' required>".$text."</textarea><br>
                  </form>
              </div>
              
              ";
        return $content;
    }
    
    static function songDelete($id){
        $id = Con::$con->real_escape_string($id);
        if(User::isLogged() && $_SESSION['user']->permission("song_delete") && is_numeric($id)){
            Old::backupSong($id);
            Old::dropRelations($id);
            Con::$con->query("DELETE FROM `oldSong` WHERE ID = '" . $id . "' LIMIT 1") or die(Con::$con->error);
            return;
        }      
         
    }
    
    static function backupSong($id){
        Con::$con->query("INSERT INTO oldBackup (origID, nazev, text, publikovatelna, overena, pridal, dulezitost, date, seen) SELECT id, nazev, text, publikovatelna, overena, pridal, dulezitost, pridano, seen FROM oldSong WHERE (id = '".$id."')") or die(Con::$con->error);
    }
}
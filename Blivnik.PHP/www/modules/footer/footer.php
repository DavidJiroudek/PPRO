<?php
/**
 * footer module
 */
 
/**
 * footer class
 * 
 * includes footer css file and shows footer
 * @package footer
 */
class Footer{

    /**
     *return module header data
     * @return meta and script string
     */
    function initHead(){
        return '<link rel="stylesheet" type="text/css" href="' . OUTER_PATH . 'css/footer/footer.css">';
    }
    
    /**
     * will initialize show template
     */
    function show(){
        require_once(ROOT_PATH . 'modules/footer/footer.tpl.php');
        showFooter();
    }
}
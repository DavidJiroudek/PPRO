<?php
/**
 * footer template
 */
/**
 * shows footer
 */
function showFooter(){ 
?>
    <div class="footer">
        <div class="footerSign">
        By Humblee &copy; 2016
        </div>
        <div class="footerContact">
            <?php echo Lang::t('contact'); ?>
            <a href="mailto:admin@blivnik.cz">
            admin (at) blivnik.cz
            </a>
        </div>
    </div> 
<?php
}
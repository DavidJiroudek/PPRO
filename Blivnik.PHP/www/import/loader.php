<?php
require_once('../core/bootloader.php');
require_once('../ajax/import.php');
mb_internal_encoding("UTF-8");
//set_time_limit(3600);

$folder = "data";


class importLatexTP extends Import{

    public $post;
    public $interpret;
    
    /*
      "name"
      "text"
      "publishable"
      "importance"*/
      
    public function __construct($address){
        $this->rawData = file_get_contents($address);
        $this->rawData = preg_replace("/\\\\uv{(.*?)}/s", '"$1"', $this->rawData);
        $this->parseName();
        $this->parseSong();
        $this->parseChords();
        $this->post["text"] = "";
        $this->post["text"] .= "INTERPRETA DOPLNIT: " . $this->interpret . PHP_EOL;
        $this->post["text"] .= "[TEXT:]".PHP_EOL;
        $this->post["text"] .= $this->finalText;
        $this->post["publishable"] = 1;
        $this->post["importance"] = 3;
        Con::$con->query("INSERT INTO oldSong ( nazev, text, pridal, dulezitost, publikovatelna, pridano, overena, folder) VALUES
         ('".$this->post["name"]."',
          '".Con::$con->real_escape_string(urlencode($this->post["text"]))."',
           '1', '3', '1', CURRENT_TIMESTAMP, 0, 'tszpevnik')");
       
    }
    
    public function parseSong(){
        $this->rawData = preg_replace("/%.*?\\n/", '', $this->rawData);
        $this->rawData = str_replace("\\ifdefined\\TPBAND", '', $this->rawData);
        $this->rawData = preg_replace("/\\\\bigskip.*?\\n/", '', $this->rawData);
        $this->rawData = preg_replace("/(\\\\ks|\\\\kr).*?(\\\\zs|\\\\zr)/s", '$2'.PHP_EOL, $this->rawData);
        $this->rawData = preg_replace("/\\n[\\s]*\\n/", PHP_EOL, $this->rawData);
        $this->rawData = preg_replace("/\\n\\s*\\n\\s* /", '\\n', $this->rawData);
        
        $num = 1;
        $regex = '/\\\\zs[\\s]*?\\n/';
        while( preg_match($regex, $this->rawData)) {              
            $this->rawData = preg_replace($regex, PHP_EOL . '(' . $num++ .') ', $this->rawData, 1);  
        }
        $this->rawData = preg_replace("/(\\n{1,})/", PHP_EOL, $this->rawData);
        
        
        $this->rawData = preg_replace("/=.*([\\d])\\./", '= ($1)', $this->rawData);
        $this->rawData = str_replace("\ks", '', $this->rawData);
        $this->rawData = str_replace("\kr", '', $this->rawData);
        $this->rawData = str_replace("\kp", '', $this->rawData);
        $this->rawData = str_replace("/:", '[:', $this->rawData);
        $this->rawData = str_replace(":/", ':]', $this->rawData);
        $this->rawData = preg_replace("/(\\n{1,})/", PHP_EOL, $this->rawData);
        $num = 1;
        $regex = '/\\\\zr[\s]*?\n/';
        $this->rawData = preg_replace($regex, PHP_EOL . '(R) ', $this->rawData);
        $this->rawData = preg_replace('/\\\\zr/', PHP_EOL . '(R) ', $this->rawData);
        $this->rawData = preg_replace('/\\(R\\)[\\s]*?\\n/',  '(R) ' . PHP_EOL.PHP_EOL, $this->rawData);
        $this->rawData = str_replace("\\", '', $this->rawData);
        $newData = "";
        
        //
        $lines = explode(PHP_EOL, $this->rawData);
        foreach($lines as $line){
            $line = trim($line);
            if(!preg_match('/(\\(([\\d])|(R\\)))/', $line)){
                $line  = "    " . $line;
            }  
            $newData .=  $line . PHP_EOL;  
        }
        $this->rawData = $newData;
    }
    
    
    public function parseChords(){
        $rows = explode(PHP_EOL, $this->rawData);
        foreach($rows as $row){
            $data = explode(">", $row);
            $textLine = "";
            $chordLine = "";
            foreach($data as $chord){
                $dataChord = explode("<", $chord);
                $text = $dataChord[0]; 
                $textLine .= $text;
                 
                if(isset($dataChord[1])){ //chord found
                    $chordLine = str_pad($chordLine, strlen(utf8_decode($textLine))-1, " ", STR_PAD_RIGHT);
                    $chordLine .= "[".$dataChord[1]."]";
                } 
            }
            if($chordLine != "")
                $this->finalText .= $chordLine."\n";
            $this->finalText .= $textLine."\n";  
            $chordLine = "";
            $textLine = "";
        }   
        print_r($this->finalText);   
    }
    public function parseName(){
        $parts = explode("\zp{", $this->rawData);
        if(isset($parts[1])){
            $this->rawData = $parts[1];
            $nameRaw = explode("}", $parts[1]);  
            $this->post["name"] = $nameRaw[0]; 
            if(isset($nameRaw[1])){  
                $this->interpret =  substr($nameRaw[1], 1);
            }
            if(isset($nameRaw[2])){  
                $this->rawData = $nameRaw[2];
            }
        }
    }
}


class FileLoader {
    /**
     * foll path is stored there after getting file array
     */  
    static $path;
    
    /**
     * returns file array
     */
    static function getFileArray($folder){
        FileLoader::$path = ROOT_PATH . "import/" . $folder . "/";
        return array_diff(scandir(FileLoader::$path), array('..', '.'));
    }
}


function perform(){
    global $folder;
    $fileArray = FileLoader::getFileArray($folder);
    foreach($fileArray as $file){
        $file = iconv("ISO-8859-1", "UTF-8", $file);
        $fPraser = new importLatexTP(FileLoader::$path . $file);
    }
}
perform();
?>
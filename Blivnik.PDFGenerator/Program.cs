namespace Blivnik.PDFGenerator
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("appsettings.json")
                    .AddEnvironmentVariables("BLIVNIK_");
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
                //.UseServiceProviderFactory(new AutofacServiceProviderFactory())
                //.UseSerilog((hostingContext, loggerConfiguration) =>
                //{
                //    loggerConfiguration
                //        .ReadFrom.Configuration(hostingContext.Configuration)
                //        .Enrich.FromLogContext()
                //        .Enrich.WithExceptionDetails(new DestructuringOptionsBuilder()
                //                     .WithDefaultDestructurers()
                //                     .WithRootName("Exception")
                //                     )
                //        .Enrich.WithProperty("ApplicationName", typeof(Program).Assembly.GetName().Name)
                //        .Enrich.WithProperty("Environment", hostingContext.HostingEnvironment)
                //        // Used to filter out potentially bad data due debugging.
                //        // Very useful when doing Seq dashboards and want to remove logs under debugging session.
                //        .Enrich.WithProperty("DebuggerAttached", Debugger.IsAttached);
                //});

    }
}

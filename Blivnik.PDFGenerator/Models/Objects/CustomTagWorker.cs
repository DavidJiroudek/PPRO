﻿using iText.Html2pdf.Attach;
using iText.Html2pdf.Attach.Impl.Tags;
using iText.StyledXmlParser.Node;
using iText.Html2pdf.Attach.Impl;


namespace Blivnik.Net.API.PDFModels
{
    public class CustomTagWorkerFactory : DefaultTagWorkerFactory
    {

        public CustomTagWorkerFactory()
        {
        }

        public override ITagWorker GetCustomTagWorker(IElementNode tag, ProcessorContext context)
        {
            switch (tag.Name())
            {
                case "verse":
                case "row":
                    return new CustomDivTagWorker(tag, context);
                case "rep":
                case "dynwide":
                case "rythm":
                case "ch":
                case "dyn":
                case "space":
                    return new CustomSpanTagWorker(tag, context);
                default:
                    return null;
            }
        }

        private class CustomSpanTagWorker : SpanTagWorker
        {

            public CustomSpanTagWorker(IElementNode element, ProcessorContext context) : base(element,
                context)
            {
            }

            public override bool ProcessContent(string content, ProcessorContext context)
            {

                return base.ProcessContent(content, context);
            }
        }

        private class CustomDivTagWorker : DivTagWorker
        {

            public CustomDivTagWorker(IElementNode element, ProcessorContext context) : base(element,
                context)
            {
            }

            public override bool ProcessContent(string content, ProcessorContext context)
            {
                return base.ProcessContent(content, context);
            }
        }
    }
}

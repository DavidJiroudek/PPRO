﻿namespace Blivnik.PDFGenerator.Models.Objects
{
    [Serializable]
    public class SongData
    {
        public SongData() { }

        public string? Date { get; set; }
        public string? SongName { get; set; }
        public int? Capo { get; set; }
        public string? Text { get; set; }
        public string? Author { get; set; }
        public IEnumerable<int>? Rythms { get; set; }


    }
}

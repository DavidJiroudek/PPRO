﻿using iText.Html2pdf.Attach;
using iText.Html2pdf.Attach.Impl;
using iText.Html2pdf.Attach.Impl.Tags;
using iText.Html2pdf.Css;
using iText.Html2pdf.Css.Apply;
using iText.Html2pdf.Css.Apply.Impl;
using iText.Html2pdf.Css.Apply.Util;
using iText.Layout;
using iText.StyledXmlParser.Node;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKFischer.Booking.Pdf.TagCssHelpers
{
    public class HtmlTagCss : DefaultCssApplierFactory
    {

        public override ICssApplier GetCustomCssApplier(IElementNode tag)
        {
            var tagName = tag.Name().ToLower();
            switch (tag.Name())
            {

                case "verse":
                    return new VerseCss();
                case "row":
                    return new RowCss();
                case "rep":
                    return new RepCss();
                case "dynwide":
                    return new DynWCss();
                case "rythm":
                    return new RythmCss();
                case "ch":
                    return new ChCss();
                case "dyn":
                    return new DynCss();
                case "space":
                    return new SpaceCss();
                default:
                    return null;

            }

        }

        private class VerseCss : BlockCssApplier
        {
            public override void Apply(ProcessorContext context, IStylesContainer stylesContainer, ITagWorker tagWorker)
            {
                IDictionary<string, string> cssProps = stylesContainer.GetStyles();
                cssProps[CssConstants.FONT_SIZE] = "12pt";
                cssProps.Add(CssConstants.MARGIN_BOTTOM, "30pt");
                stylesContainer.SetStyles(cssProps);
                base.Apply(context, stylesContainer, tagWorker);
            }
        }

        private class RowCss : BlockCssApplier
        {
            public override void Apply(ProcessorContext context, IStylesContainer stylesContainer, ITagWorker tagWorker)
            {
                IDictionary<string, string> cssProps = stylesContainer.GetStyles();
                cssProps.Add(CssConstants.MARGIN_BOTTOM, "14pt");
                cssProps.Add(CssConstants.WHITE_SPACE, "pre");
                stylesContainer.SetStyles(cssProps);
                base.Apply(context, stylesContainer, tagWorker);
            }
        }

        private class RepCss : SpanTagCssApplier
        {
            public override void Apply(ProcessorContext context, IStylesContainer stylesContainer, ITagWorker tagWorker)
            {
                IDictionary<string, string> cssProps = stylesContainer.GetStyles();
                cssProps.Add(CssConstants.PADDING_LEFT, "0.25ex");
                cssProps.Add(CssConstants.PADDING_RIGHT, "0.25ex");
                cssProps.Add(CssConstants.MARGIN_LEFT, "1.5ex");
                cssProps.Add(CssConstants.MARGIN_RIGHT, "1.5ex");
                stylesContainer.SetStyles(cssProps);
                base.Apply(context, stylesContainer, tagWorker);
            }
        }
        private class DynWCss : SpanTagCssApplier
        {
            public override void Apply(ProcessorContext context, IStylesContainer stylesContainer, ITagWorker tagWorker)
            {
                IDictionary<string, string> cssProps = stylesContainer.GetStyles();

                stylesContainer.SetStyles(cssProps);
                base.Apply(context, stylesContainer, tagWorker);
            }
        }
        private class RythmCss : SpanTagCssApplier
        {
            public override void Apply(ProcessorContext context, IStylesContainer stylesContainer, ITagWorker tagWorker)
            {
                IDictionary<string, string> cssProps = stylesContainer.GetStyles();

                stylesContainer.SetStyles(cssProps);
                base.Apply(context, stylesContainer, tagWorker);
            }
        }
        private class ChCss : SpanTagCssApplier
        {
            public override void Apply(ProcessorContext context, IStylesContainer stylesContainer, ITagWorker tagWorker)
            {
                IDictionary<string, string> cssProps = stylesContainer.GetStyles();

                stylesContainer.SetStyles(cssProps);
                base.Apply(context, stylesContainer, tagWorker);
            }
        }
        private class DynCss : SpanTagCssApplier
        {
            public override void Apply(ProcessorContext context, IStylesContainer stylesContainer, ITagWorker tagWorker)
            {
                IDictionary<string, string> cssProps = stylesContainer.GetStyles();


                stylesContainer.SetStyles(cssProps);
                base.Apply(context, stylesContainer, tagWorker);
            }
        }
        private class SpaceCss : SpanTagCssApplier
        {
            public override void Apply(ProcessorContext context, IStylesContainer stylesContainer, ITagWorker tagWorker)
            {
                IDictionary<string, string> cssProps = stylesContainer.GetStyles();


                stylesContainer.SetStyles(cssProps);
                base.Apply(context, stylesContainer, tagWorker);
            }
        }

    }
}

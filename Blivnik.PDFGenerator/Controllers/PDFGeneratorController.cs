﻿using Blivnik.PDFGenerator.Models.Objects;
using iText.Html2pdf;
using iText.IO.Font;
using iText.Kernel.Font;
using iText.Kernel.Geom;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Borders;
using iText.Layout.Element;
using iText.Layout.Properties;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using CKFischer.Booking.Pdf.TagCssHelpers;
using Blivnik.Net.API.PDFModels;

namespace Blivnik.Net.API.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/pdf")]
    [Route("micro/")]
    public class PDFController
    {

        private SolidBorder _solidBorder;
        private PdfFont _headerFont;
        private PdfFont _bodyFont;
        private PdfFont _footerFont;

        private const int _upperHeaderFontSize = 9;
        private const int _lowerHeaderFontSize = 7;
        private const int _bodyFontSize = 9;
        private const int _footerFontSize = 9;

        [AllowAnonymous]
        [HttpPost, Route("CreatePDF")]
        public IActionResult CreatePDF(BookFilter bookfilter)
        {
                var resourcesFolderPath = System.IO.Path.GetDirectoryName("Resources/trebuc.ttf");
                _headerFont = _bodyFont = _footerFont = PdfFontFactory.CreateFont(new FileInfo(System.IO.Path.Combine(resourcesFolderPath, @"trebuc.ttf")).FullName);
                _solidBorder = new SolidBorder(0);
                MemoryStream ms = new MemoryStream();
                PdfWriter writer = new PdfWriter(ms);
                PdfDocument pdf = new PdfDocument(writer);
                writer.SetCloseStream(false);
                Document document = new Document(pdf, PageSize.A4);
                var metadata = pdf.GetDocumentInfo();
                metadata.SetTitle("Blivník");
                //document.SetMargins(20, 20, 20, 20);

                foreach (var song in bookfilter.Songs)
                {
                    AppendHeader(document, song);
                    AppendBody(document, song);
                    AppendFooter(document, song);

                    document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                }
                pdf.RemovePage(pdf.GetNumberOfPages());
                document.Close();


                byte[] byteInfo = ms.ToArray();
                ms.Write(byteInfo, 0, byteInfo.Length);
                ms.Position = 0;

                FileStreamResult fileStreamResult = new FileStreamResult(ms, "application/pdf");
                fileStreamResult.FileDownloadName = "Output.pdf";
                return fileStreamResult;
        }

        private void AppendHeader(Document doc, SongData song)
        {
            var header0 = new Table(new float[] { 1, 1 });

            header0.AddCell(new Paragraph(song.SongName));
            header0.AddCell(new Paragraph(song.Author).SetTextAlignment(TextAlignment.RIGHT));
            header0.UseAllAvailableWidth();
            header0.SetFont(_headerFont);
            header0.SetFontSize(_upperHeaderFontSize);
            header0.SetFixedLayout();
            header0.SetBorder(Border.NO_BORDER);


            doc.Add(header0);
            var header1 = new Table(new float[] { 1, 1, 1 });

            header1.AddCell(new Paragraph(song.Date).SetTextAlignment(TextAlignment.LEFT));

            string rythmsStr = "";
            if (song.Rythms != null)
            {
                foreach (var rythm in song.Rythms)
                {
                    rythmsStr += (rythm.ToString() + " ");
                }
            }

            header1.AddCell(new Paragraph(rythmsStr)).SetTextAlignment(TextAlignment.CENTER);

            header1.AddCell(new Paragraph("Capo: " + song.Capo.ToString()).SetTextAlignment(TextAlignment.RIGHT));
            header1.UseAllAvailableWidth();
            header1.SetFont(_headerFont);
            header1.SetFontSize(_lowerHeaderFontSize);
            header1.SetFixedLayout();
            header1.SetBorder(Border.NO_BORDER);


            doc.Add(header1);

        }
        private void AppendBody(Document doc, SongData song)
        {
            var css = new ConverterProperties();
            css.SetCharset(PdfEncodings.UTF8);
            css.SetCssApplierFactory(new HtmlTagCss());
            css.SetTagWorkerFactory(new CustomTagWorkerFactory());


            //var htmlText = "<rofl><rofl><div>" + song.Text.Replace("\"", string.Empty) + "</div></rofl>";

            var htmlarraylist = HtmlConverter.ConvertToElements(song.Text, css);
            foreach (IBlockElement element in htmlarraylist)
            {

                var blockEle = element;
                doc.Add(blockEle);

                //bodyCell.Add(blockEle);
            }
            //body.AddCell(bodyCell);
            //doc.Add(body);

        }

        private void AppendFooter(Document doc, SongData song) { }
    }
}
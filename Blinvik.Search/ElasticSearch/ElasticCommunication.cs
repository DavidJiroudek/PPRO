﻿using Blinvik.Search.Configuration;
using Blinvik.Search.ElasticSearch.Models;
using Elasticsearch.Net;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Nest;
using System.Diagnostics;

namespace Blinvik.Search.ElasticSearch
{
    /// <summary>
    /// elasticsearch communication wrapper, creates separate indexes for different document types
    /// </summary>
    public class ElasticCommunication : ISearchCommunication
    {
        private ElasticClient _client;

        private ILogger<ElasticCommunication> _logger { get; set; }
        private IOptions<ElasticOptions> _options { get; set; }


        public ElasticCommunication(IOptions<ElasticOptions> options, ILogger<ElasticCommunication> logger)
        {
            _options = options;
            _logger = logger;
            //http connection for real connection, inmemory connection is possible for test
            var conn = new HttpConnection();
            var pool = new SingleNodeConnectionPool(new Uri(_options.Value.Url));

            var settings = new ConnectionSettings(pool, conn);

            settings.EnableDebugMode();
            settings.RequestTimeout(TimeSpan.FromMinutes(2));
            settings.MaximumRetries(2);
            settings.MaxDeadTimeout(TimeSpan.FromMinutes(2));
            settings.EnableHttpCompression(true);
            settings.EnableHttpPipelining(true);

            _client = new ElasticClient(settings);
        }

        public void CreateIndex<T>() where T : ElasticIndexable
        {
            var response = _client.Indices.Create(typeof(T).Name.ToLower(), c =>
                        c.Settings(s => s.NumberOfShards(_options.Value.NumberOfShards).NumberOfReplicas(_options.Value.NumberOfReplicas))
                         .Map<T>(x => x.AutoMap())
            );
            if (!response.IsValid)
            {
                var exception = new Exception(response.ServerError.Error.ToString());
                _logger.LogError(exception, "Error while creating Index for type {Type}", typeof(T).Name.ToLower());
                throw exception;
            }
            _logger.LogInformation("Index for type {Type} created", typeof(T).Name.ToLower());
        }

        public void DeleteIndex<T>() where T : ElasticIndexable
        {
            var response = _client.Indices.Delete(typeof(T).Name.ToLower());
            if (!response.IsValid && response.ServerError.Status != 404)
            {
                var exception = new Exception(response.ServerError.Error.ToString());
                _logger.LogError(exception, "Error while deleting Index for type {Type}", typeof(T).Name.ToLower());
                throw exception;
            }
            _logger.LogInformation("Index for type {Type} deleted", typeof(T).Name.ToLower());
        }

        public void DeleteItem<T>(IEnumerable<int> identifiers) where T : ElasticIndexable
        {
            var response = _client.DeleteByQuery<T>(s =>
                s.Query(q =>
                   q.Terms(t =>
                    t.Field(f => f.Index)
                    .Terms(identifiers)
                   )
                ).Index(typeof(T).Name.ToLower())
            );
            if (!response.IsValid)
            {
                var exception = new Exception(response.ServerError.Error.ToString());
                _logger.LogError(exception, "Error while deleting item");
                throw exception;
            }
            _logger.LogInformation("Index {IndexName} created", typeof(T).Name.ToLower());
        }

        /// <summary>
        /// bulk index is for reindexing in huge amounts such as after crash or while rebuilding whole index
        /// time is measured by system.diagnostics stopwatch in all environments because of healthcheck
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        public async Task IndexItemBulk<T>(IEnumerable<T> items) where T : ElasticIndexable
        {
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            var descriptor = new BulkDescriptor();
            descriptor.CreateMany<T>(items, (bd, q) => bd.Index(typeof(T).Name.ToLower()));
            var response = await _client.BulkAsync(descriptor);
            stopwatch.Stop();

            if (!response.IsValid)
            {
                var exception = new Exception(response.ServerError.Error.ToString());
                _logger.LogError(exception, "Error while bulk indexing items type {Type}, elapsed time {Time}ms", typeof(T).Name.ToLower(), stopwatch.ElapsedMilliseconds);
                throw exception;
            }
            _logger.LogInformation("Items of type {Type} bulk indexed, elapsed time {Time}ms", typeof(T).Name.ToLower(), stopwatch.ElapsedMilliseconds);
        }

        public async Task IndexItem<T>(T item) where T : ElasticIndexable
        {
            var response = await _client.IndexAsync<T>(item, i => i.Index(typeof(T).Name.ToLower()));
            if (!response.IsValid)
            {
                var exception = new Exception(response.ServerError.Error.ToString());
                _logger.LogError(exception, "Error while indexing item type {Type}, {Item}", typeof(T).Name.ToLower(), item.ToString());
                throw exception;
            }
            _logger.LogInformation("Item of type {Type} indexed", typeof(T).Name.ToLower());
        }

        public async Task<ElasticResponse<T>> Query<T>(ISearchRequest<T> request) where T : ElasticIndexable
        {

            var searchResponse = await _client.SearchAsync<T>(request);
            var response = new ElasticResponse<T>();
            if (!searchResponse.IsValid)
            {
                _logger.LogError("Item of type {Type} Search error ocured, error detail: {ErrorDetail}", typeof(T).Name.ToLower(), searchResponse.ServerError.ToString());
                return response;
            }
            response.Results = searchResponse.Hits.Select(i => (i.Source, i.Score, i.Highlight.Select(j => string.Join("...", j.Value)).ToArray())).ToList();
            _logger.LogInformation("Item of type {Type} searched. Result count {Count}, Process time {Time}", typeof(T).Name.ToLower(), searchResponse.Total, searchResponse.Took);
            return response;
        }


        internal static Highlight GetHighlight()
        {
            var highlight = new Highlight();
            highlight.Fields = new FluentDictionary<Field, IHighlightField>().Add("*", new HighlightField());
            highlight.PostTags = new[] { "<b>" };
            highlight.PreTags = new[] { "</b>" };
            highlight.FragmentSize = 250;
            return highlight;
        }

    }
}

﻿using Blinvik.Search.ElasticSearch.Models;
using Nest;

namespace Blinvik.Search.ElasticSearch
{
    public interface ISearchCommunication
    {
        void CreateIndex<T>() where T : ElasticIndexable;
        void DeleteIndex<T>() where T : ElasticIndexable;
        void DeleteItem<T>(IEnumerable<int> identifiers) where T : ElasticIndexable;
        Task IndexItem<T>(T item) where T : ElasticIndexable;
        Task IndexItemBulk<T>(IEnumerable<T> items) where T : ElasticIndexable;
        Task<ElasticResponse<T>> Query<T>(ISearchRequest<T> request) where T : ElasticIndexable;
    }
}
﻿using Blinvik.Search.Models;
using Blinvik.Search.Models.Filters;
using Nest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blinvik.Search.ElasticSearch.Models
{
    [ElasticsearchType(RelationName = "Song")]
    public class ElasticSong : ElasticIndexable
    {
        public ElasticSong()
        {
        }

        [Keyword(Index = true)]
        public DateTime CreatedDate { get; set; }
        [Keyword(Index = true)]
        public DateTime ModifiedDate { get; set; }
        [Keyword(Index = true)]
        public int SongId { get; set; }


        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost =10, Fielddata = true )]
        public string SongName { get; set; }
        [Keyword(Index = true)]
        public string RawSongName { get { return SongName; } }
        [Keyword(Index = true)]
        public int Publicable { get; set; }
        [Keyword(Index = true)]
        public int Priority { get; set; }
        public bool Validated { get; set; }
        public int Creator { get; set; }
        [Keyword(Index = true)]
        public int Capo { get; set; }
        public int? Editor { get; set; }
        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost = 3, Fielddata = true)]
        public string Text { get; set; }
        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost = 7, Fielddata = true)]
        public string Chorus { get; set; }
        [Nested]
        public IEnumerable<ElasticAuthor> Authors { get; set; }
        public IEnumerable<int> Rythms { get; set; }
        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost = 7, Fielddata = true)]
        public string Link { get; set; }
        [Keyword(Index = true)]
        public string CultureCode { get; set; }
        [Keyword(Index = true)]
        public string ViewCount { get; set; }




        public static ISearchRequest<ElasticSong> PrepareFilterDetailSearch(SongFilter filter)
        {
            var request = new SearchRequest<ElasticSong>(nameof(ElasticSong).ToLower());
            if (filter.Capo != null) {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Term(ts => ts.Field(f => f.Capo).Value(filter.Capo.Value));
            }
            if (filter.CultureCodeISO2Letter != null) {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Term(ts => ts.Field(f => f.CultureCode).Value(filter.CultureCodeISO2Letter));
            }
            if (filter.CreatorId != null)
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Term(ts => ts.Field(f => f.Creator).Value(filter.CreatorId.Value));
            }
            if (filter.AuthorIds != null && filter.AuthorIds.Any())
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>()
                        .Nested(n => n.Name("authorIds")
                            .Path<IEnumerable<ElasticAuthor>>(f => f.Authors)
                            .Query(innerQ =>
                                innerQ.Terms(ts => ts.Field(f => f.Authors.First().Index).Terms(filter.AuthorIds)
                            )
                        )
                    );
            }
            if (filter.SongIds != null && filter.SongIds.Any())
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Terms(ts => ts.Field(f => f.SongId).Terms(filter.SongIds));
            }
            if (filter.AuthorMatch != null)
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Nested(n => n.Name("authorNames")
                                .Path<IEnumerable<ElasticAuthor>>(f => f.Authors)
                                .InnerHits(i => i.Explain(false))
                                .Query(innerQ =>
                                    innerQ.Bool(innerB => innerB.Should(
                                        innerSh => innerSh.Match(m => m.Field(innerF => innerF.Authors.First().Name).Fuzziness(Fuzziness.Auto).Strict(false).Query(filter.AuthorMatch)),
                                        innerSh => innerSh.Match(m => m.Field(innerF => innerF.Authors.First().AlternativeNames).Fuzziness(Fuzziness.Auto).Strict(false).Query(filter.AuthorMatch))
                                    )
                                )
                            )
                       );
            }
            //createdDate
            if (filter.CreatedDate != null && (filter.CreatedDateOperator == ComparerOperator.GreaterOrEqual || filter.CreatedDateOperator == ComparerOperator.Equals))
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().DateRange(dr => dr.Field(f => f.CreatedDate).GreaterThanOrEquals(DateMath.Anchored(filter.CreatedDate.Value).RoundTo(DateMathTimeUnit.Day)));
            }
            if (filter.CreatedDate != null && (filter.CreatedDateOperator == ComparerOperator.LessOrEqual || filter.CreatedDateOperator == ComparerOperator.Equals))
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().DateRange(dr => dr.Field(f => f.CreatedDate).LessThanOrEquals(DateMath.Anchored(filter.CreatedDate.Value).RoundTo(DateMathTimeUnit.Day)));
            }
            //modifiedDate
            if (filter.ModifiedDate != null && (filter.ModifiedDateOperator == ComparerOperator.GreaterOrEqual || filter.ModifiedDateOperator == ComparerOperator.Equals))
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().DateRange(dr => dr.Field(f => f.ModifiedDate).GreaterThanOrEquals(DateMath.Anchored(filter.ModifiedDate.Value).RoundTo(DateMathTimeUnit.Day)));
            }
            if (filter.ModifiedDate != null && (filter.ModifiedDateOperator == ComparerOperator.LessOrEqual || filter.ModifiedDateOperator == ComparerOperator.Equals))
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().DateRange(dr => dr.Field(f => f.ModifiedDate).LessThanOrEquals(DateMath.Anchored(filter.ModifiedDate.Value).RoundTo(DateMathTimeUnit.Day)));
            }
            //name - allow not exact matching
            if (filter.Name != null)
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Match(q => q.Field(f1 => f1.SongName).Fuzziness(Fuzziness.Auto).Strict(false).Query(filter.Name));
            }
            //priority
            if (filter.Priority != null && (filter.PriorityOperator == ComparerOperator.GreaterOrEqual || filter.PriorityOperator == ComparerOperator.Equals))
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Range(q => q.Field(f1 => f1.SongName).GreaterThanOrEquals(filter.Priority));
            }
            if (filter.Priority != null && (filter.PriorityOperator == ComparerOperator.LessOrEqual || filter.PriorityOperator == ComparerOperator.Equals))
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Range(q => q.Field(f1 => f1.SongName).LessThanOrEquals(filter.Priority));
            }
            //publicable
            if (filter.Publicable != null && filter.Publicable.Any())
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Terms(ts => ts.Field(f => f.Publicable).Terms(filter.Publicable));
            }
            //rythms
            if (filter.Rythms != null && filter.Rythms.Any())
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Terms(ts => ts.Field(f => f.Rythms).Terms(filter.Rythms));
            }
            //validated
            if (filter.Validated != null)
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Term(ts => ts.Field(f => f.Validated).Value(filter.Validated.Value));
            }
            if (filter.VerseMatch != null)
            {
                request.Query &= +new QueryContainerDescriptor<ElasticSong>().Match(q => q.Field(f2 => f2.Text).Fuzziness(Fuzziness.EditDistance(20)).Strict(false).Query(filter.VerseMatch));
            }

            //default sorting
            request.Sort = new List<ISort>() { new FieldSort { Field = "rawSongName", Order = SortOrder.Descending } };
            //specified sorting
            if (filter.SortBy != null && filter.SortBy.Any()) {
                request.Sort =  filter.SortBy.Select(i =>  new FieldSort { Field = i.Field, Order = i.Direction == ListSortDirection.Ascending ? SortOrder.Ascending : SortOrder.Descending }).ToList<ISort>();

            }

            var highlight = GetHighlightDescriptor();
            //we do not need text of the song
            request.Source = new Union<bool, ISourceFilter>(new SourceFilterDescriptor<ElasticSong>().Excludes(excluded => excluded.Field(i => i.Text).Field(i => i.Chorus)));
            request.Size = 10000;
            return request;
        }

        public static ISearchRequest<ElasticSong> PrepareFilterFulltextSearch(string keyword)
        {
            var request = new SearchRequest<ElasticSong>(nameof(ElasticSong).ToLower());
            request.Query = new QueryContainerDescriptor<ElasticSong>()
                .Bool(b =>
                    b.Should(
                        sh => sh.Match(q => q.Field(f1 => f1.SongName).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword)),
                        sh => sh.Match(q => q.Field(f2 => f2.Text).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword)),
                        sh => sh.Match(q => q.Field(f2 => f2.Link).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword)),
                        sh => sh.Match(q => q.Field(f3 => f3.Chorus).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword)),
                        sh => sh.Nested(n => n.Name("authorNames")
                                .Path<IEnumerable<ElasticAuthor>>(f => f.Authors)
                                .InnerHits(i => i.Explain(false))
                                .Query(innerQ =>
                                    innerQ.Bool(innerB => innerB.Should(
                                        innerSh => innerSh.Match(m => m.Field(innerF => innerF.Authors.First().Name).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword)),
                                        innerSh => innerSh.Match(m => m.Field(innerF => innerF.Authors.First().AlternativeNames).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword))
                                    )
                                )
                            )
                    )
                )
                .Must(m => m.Range(r => r.Field(f => f.Publicable).LessThanOrEquals((int)PublicableType.Unpublicable)))
                );
            request.Highlight = GetHighlightDescriptor();
            request.Size = 200;
            request.Sort = new List<ISort>() { new FieldSort { Field = "_score", Order = SortOrder.Descending } };
            request.Source = new Union<bool, ISourceFilter>(new SourceFilterDescriptor<ElasticSong>().Excludes(excluded => excluded.Field(i => i.Text).Field(i => i.Chorus)));
            request.MinScore = 1.001;
            return request;
        }

        internal static HighlightDescriptor<ElasticSong> GetHighlightDescriptor() => new HighlightDescriptor<ElasticSong>().PreTags(new[] { "<b>" }).PostTags(new[] { "</b>" })
                                                                                                                            .Fields(fs => fs.Field(f => f.Chorus),
                                                                                                                                    fs => fs.Field(f => f.SongName),
                                                                                                                                    fs => fs.Field(f => f.Text),
                                                                                                                                    fs => fs.Field(f => f.Link),
                                                                                                                                    fs => fs.Field(f => f.Authors.First().Name),
                                                                                                                                    fs => fs.Field(f => f.Authors.First().AlternativeNames));
    }
}

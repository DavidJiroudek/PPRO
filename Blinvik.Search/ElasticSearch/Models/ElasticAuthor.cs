﻿using Blinvik.Search.Models;
using Blinvik.Search.Models.Filters;
using Nest;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blinvik.Search.ElasticSearch.Models
{
    [ElasticsearchType(RelationName = "Author")]
    public class ElasticAuthor : ElasticIndexable
    {
        public ElasticAuthor() {
            RelatedAuthors = new ElasticAuthor[0];
        }
        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost = 9, Fielddata = true)]
        public string Name { get; set; }
        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost = 1, Fielddata = true)]
        public string Description { get; set; }
        [Keyword(Index = true)]
        public AuthorType AuthorTypeId { get; set; }
        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost = 1, Fielddata = true)]
        public string AuthorType { get; set; }
        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost = 5, Fielddata = true)]
        public string Link { get; set; }
        [Text(Analyzer = "simple", SearchAnalyzer = "simple", Index = true, IndexPhrases = true, Boost = 6, Fielddata = true)]
        public string[] AlternativeNames { get; set; }
        [Keyword(Index = true)]
        public int? MinimalPublicable { get; set; }
        [Keyword(Index = true)]
        public AuthorType[] AuthorTypesUsed { get; set; }
        [Nested]
        public ElasticAuthor[] RelatedAuthors { get; set; }

        public static ISearchRequest<ElasticAuthor> PrepareDetailSearch(AuthorFilter filter)
        {
            var request = new SearchRequest<ElasticAuthor>(nameof(ElasticAuthor).ToLower());
            if (filter.Ids != null && filter.Ids.Any())
            {
                request.Query &= +new QueryContainerDescriptor<ElasticAuthor>().Terms(ts => ts.Field(f => f.Index).Terms(filter.Ids));
            }
            if (filter.MinimalPublicable != null)
            {
                request.Query &= +new QueryContainerDescriptor<ElasticAuthor>().Range(q => q.Field(f1 => f1.MinimalPublicable).LessThanOrEquals(filter.MinimalPublicable));
            }
            if (filter.Name != null)
            {
                request.Query &= +new QueryContainerDescriptor<ElasticAuthor>().Match(q => q.Field(f1 => f1.Name).Fuzziness(Fuzziness.Auto).Strict(false).Query(filter.Name));
            }
            if (filter.AuthorTypes != null && filter.AuthorTypes.Any())
            {
                request.Query &= +new QueryContainerDescriptor<ElasticAuthor>().Match(q => q.Field(f1 => f1.Name).Fuzziness(Fuzziness.Auto).Strict(false).Query(filter.Name));
            }
            request.Highlight = GetHighlightDescriptor();
            //default sorting
            request.Sort = new List<ISort>() { new FieldSort { Field = "name", Order = SortOrder.Descending } };
            //specified sorting
            if (filter.SortBy != null && filter.SortBy.Any())
            {
                request.Sort = filter.SortBy.Select(i => new FieldSort { Field = i.Field, Order = i.Direction == ListSortDirection.Ascending ? SortOrder.Ascending : SortOrder.Descending }).ToList<ISort>();

            }
            request.Size = 10000;
            return request;
        }


        public static ISearchRequest<ElasticAuthor> PrepareFilterFulltextSearch(string keyword)
        {
            var request = new SearchRequest<ElasticAuthor>(nameof(ElasticAuthor).ToLower());
            request.Query = new QueryContainerDescriptor<ElasticAuthor>()
                .Bool(b =>
                    b.Should(
                        sh => sh.Match(q => q.Field(f1 => f1.Name).Fuzziness(Fuzziness.Auto).Strict(false).Boost(10).Query(keyword)),
                        sh => sh.Match(q => q.Field(f2 => f2.AlternativeNames).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword)),
                        sh => sh.Match(q => q.Field(f2 => f2.Description).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword)),
                        sh => sh.Match(q => q.Field(f3 => f3.Index).Fuzziness(Fuzziness.Auto).Strict(false).Query(keyword))
                )
                .Must(m => m.Range(r => r.Field(f => f.MinimalPublicable).LessThanOrEquals((int)PublicableType.Unpublicable)))
                );

            request.Highlight = GetHighlightDescriptor();
            request.Size = 200;
            request.Sort = new List<ISort>() { new FieldSort { Field = "_score", Order = SortOrder.Descending } };
            request.MinScore = 1.001;
            return request;
        }
        internal static HighlightDescriptor<ElasticAuthor> GetHighlightDescriptor() => new HighlightDescriptor<ElasticAuthor>().PreTags(new[] { "<b>" }).PostTags(new[] { "</b>" })
                                                                                                                    .Fields(fs => fs.Field(f => f.Name),
                                                                                                                            fs => fs.Field(f => f.AlternativeNames),
                                                                                                                            fs => fs.Field(f => f.Description),
                                                                                                                            fs => fs.Field(f => f.Link),
                                                                                                                            fs => fs.Field(f => f.RelatedAuthors.First().Name));
    }
}

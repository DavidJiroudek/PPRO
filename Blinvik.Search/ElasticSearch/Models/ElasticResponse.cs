﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blinvik.Search.ElasticSearch.Models
{
    public class ElasticResponse<T>
    {
        public ElasticResponse()
        {
            Results = new List<(T, double?, string[])>();
        }
        public List<(T Item, double? Score, string[] Highlight)> Results { get; set; }
    }
}

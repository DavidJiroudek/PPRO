﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blinvik.Search.ElasticSearch.Models
{
    public  class ElasticIndexable
    {
        public ElasticIndexable() { 
        
        }
        [Keyword(Index = true)]
        public int Index { get; set; }


    }
}

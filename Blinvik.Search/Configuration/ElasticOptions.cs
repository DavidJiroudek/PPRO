﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blinvik.Search.Configuration
{
    public class ElasticOptions
    {
        public int NumberOfReplicas { get; set; }
        public int NumberOfShards { get; set; }
        public string Url { get; set; }
    }
}

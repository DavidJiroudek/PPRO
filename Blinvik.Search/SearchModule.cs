﻿using Autofac;
using Blinvik.Search.Configuration;
using Blinvik.Search.ElasticSearch;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Blinvik.Search
{
    public class SearchModule : Autofac.Module
    {
        private IConfiguration _config;

        public SearchModule()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var cb = new ConfigurationBuilder()
             .AddJsonFile("searchSettings.json")
             .AddJsonFile($"searchSettings.{env}.json", optional: true)
             .AddJsonFile("searchSettings.Test.json", true)
             .AddEnvironmentVariables("BLIVNIK_");
            _config = cb.Build();

        }

        protected override void Load(ContainerBuilder builder)
        {
            var searchOptions = new SearchOptions();
            _config.GetSection("SearchOptions").Bind(searchOptions);
            builder.Register<ElasticCommunication>(c => new ElasticCommunication(Options.Create(searchOptions.ElasticOptions), c.Resolve<ILogger<ElasticCommunication>>())).As<ISearchCommunication>().SingleInstance();


        }
    }
}
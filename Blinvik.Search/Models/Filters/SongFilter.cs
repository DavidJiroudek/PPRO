﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blinvik.Search.Models.Filters
{
    public class SongFilter
    {
        public int? CreatorId { get; set; }
        public string? Name { get; set; }
        public string? VerseMatch { get; set; }
        public int? Capo { get; set; }
        public string? AuthorMatch { get; set; }
        public int[] AuthorIds { get; set; }
        public int[] SongIds { get; set; }
        public int? Priority { get; set; }
        public ComparerOperator? PriorityOperator { get; set; }
        public PublicableType[]? Publicable { get; set; }
        public bool? Validated { get; set; }
        public int[] Rythms { get; set; }

        public DateTime? CreatedDate { get; set; }
        public ComparerOperator? CreatedDateOperator { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public ComparerOperator? ModifiedDateOperator { get; set; }

        public string? CultureCodeISO2Letter { get; set; }

        public (string Field, ListSortDirection Direction)[] SortBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blinvik.Search.Models.Filters
{
    public class AuthorFilter
    {
        public string? Name { get; set; }
        public string? Detail { get; set; }
        public int? MinimalPublicable { get; set; }
        public AuthorType[]? AuthorTypes { get; set; }
        public int[]? Ids { get; set; }
        public (string Field, ListSortDirection Direction)[] SortBy { get; set; }
    }
}

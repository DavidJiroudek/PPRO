﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blinvik.Search.Models.Filters
{
    [Serializable]
    public enum ComparerOperator
    {
        LessOrEqual,
        GreaterOrEqual,
        Equals
    }
}

﻿namespace Blinvik.Search.Models
{
    public enum PublicableType
    {
        Publicable,
        Unpublicable,
        Invisible,
        Forbidden
    }
}

﻿namespace Blinvik.Search.Models
{
    public enum AuthorType
    {
        MusicAuthor,
        TextAuthor,
        Interpret
    }
}

USE [Blivnik]
GO
/****** Object:  StoredProcedure [dbo].[GetAllSongs]    Script Date: 08/08/2022 11:30:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GetAllSongs]

AS

DECLARE @ids table(Id int)

insert into @ids
SELECT Id FROM dbo.Version VER
WHERE VER.IsObsolete != 1

SELECT 
 VER.[Id]
,[SongId]
,[Date]
,[Name]
,[Publicable]
,[Priority]
,[Validated]
,[Creator]
,[Editor]
,[Value]
FROM dbo.[Version] VER
INNER JOIN @ids IDS ON IDS.Id = VER.Id





SELECT
IDS.Id,
Author_Id,
AUTH_TYPE.[Type],
AUTH.Name
FROM dbo.Version_Author_MN AUTH_VER
INNER JOIN @ids IDS ON IDS.Id = AUTH_VER.Version_Id
LEFT JOIN dbo.AuthorType AUTH_TYPE ON AUTH_VER.Type = AUTH_TYPE.Id
LEFT JOIN dbo.Author AUTH ON AUTH_VER.Author_Id = AUTH.Id



SELECT 
IDS.Id,
Rythm_Id
FROM dbo.Version_Rythm_MN MN
INNER JOIN @ids IDS ON IDS.Id = MN.Version_Id
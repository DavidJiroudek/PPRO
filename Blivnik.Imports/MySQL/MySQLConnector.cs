﻿using Blivnik.Imports.Configuration;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using MySqlX.XDevAPI;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Imports.MySQL
{
    internal class MySQLConnector
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        private MySqlConnection Connection { get; set; }

        public MySQLConnector(IOptions<MySQLConfiguration> options) { 

        
        }

        public void GetAuthors() {
            if (this.IsConnect())
            {
                //suppose col0 and col1 are defined as VARCHAR in the DB
                string query = "SELECT col0,col1 FROM YourTable";
                var cmd = new MySqlCommand(query, this.Connection);
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    string someStringFromColumnZero = reader.GetString(0);
                    string someStringFromColumnOne = reader.GetString(1);
                    Console.WriteLine(someStringFromColumnZero + "," + someStringFromColumnOne);
                }
                this.Close();
            }

        }





        private bool IsConnect()
        {
            if (Connection == null)
            {
                if (String.IsNullOrEmpty(Database))
                    return false;
                string connstring = string.Format("Server={0}; database={1}; UID={2}; password={3}", Server, Database, UserName, Password);
                Connection = new MySqlConnection(connstring);
                Connection.Open();
            }

            return true;
        }

        private void Close()
        {
            Connection.Close();
        }

    }
}

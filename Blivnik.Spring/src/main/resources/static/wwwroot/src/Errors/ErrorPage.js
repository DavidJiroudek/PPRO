import React from 'react';
const ErrorPage = (error) =>{
    switch(error.number){
        case 401:
            return(<div>Nemáte dostatečná práva pro tuto akci</div>)
        case 404:
            return(<div>Stránka nebyla nalezena</div>)
        default:
        return(<div>Něco se nepovedlo!</div>)
    }

}
export default ErrorPage;
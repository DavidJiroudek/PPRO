import './App.css';
import React from 'react';
import NavMenu from './Routing/NavMenu';

const App = ()=> {

//  if (location.protocol !== 'https:') {
//    location.replace(`https:${location.href.substring(location.protocol.length)}`);
//}
  return (
    <div className="App">
      <NavMenu/>
    </div>
  );
}
export default App

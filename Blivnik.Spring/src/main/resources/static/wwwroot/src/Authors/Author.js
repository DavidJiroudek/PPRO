import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import numberPages from '../Editor/NumberPages';

const Author = () => {
    const { authorId } = useParams();

    const [songList, setSongList] = useState([]);
    const [maxPage, setMaxPage] = useState(1);
    const [page, setPage] = useState(1);
    const loginData = useSelector((state) => state.loginState.value)
    useEffect(() => {

        const header = loginData !== null ? { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token } : { 'Content-Type': 'application/json' }
        fetch('/api/author/getAuthorsSongList', {
            method: 'POST',
            headers: header,
            body: JSON.stringify({ publicable: [0, 1, 2, 3], page, resultCount: 10, authorId: authorId, validated: true, sortBy: 0 })
        }).then(response => response.json())
            .then((result) => {
                setSongList(result);
                setMaxPage(result[0] !== undefined ? result[0].MaxPage : 1);
            });
    }, []);


    return (
        <div id="songListContainer">
            <div className="songList"><p>Přidané skladby</p>{songList.map(i => i = <Link className="songLink" key={i.songId} to={`/song/${i.songId}`} >{i.name}</Link>)}</div>
            <div id='pageNumbers'>
                {numberPages(maxPage, setPage, page)}
            </div>
        </div>
    );
}

export default Author
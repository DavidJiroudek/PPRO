import jwtDecode from 'jwt-decode';
import React, { useEffect } from 'react';
import {useState } from 'react';
// import FacebookLogin from 'react-facebook-login';
//import GoogleLogin from 'react-google-login';
import { useDispatch } from 'react-redux';
import { setLoginData } from '../Redux/reducers/loginState';
import './Auth.css';
import { _AppType, _OAuthProvider } from '../Enums';

//tab Login
//handles authentication and registration
const Auth = ({ popupSet }) => {
    const [isRegister, setIsRegister] = useState(false);
    //const [isRegister, setIsRegister] = useState(false);
    const dispatch = useDispatch();

    useEffect(()=>{
        /* global google */
        google.accounts.id.initialize({
            client_id: "118452082258-uc420e1oksqifgao1ck5vuk3vvj0f2kn.apps.googleusercontent.com",
            callback: (e) => responseGoogle(e, dispatch, popupSet)
        })
        google.accounts.id.renderButton(
            document.getElementById("googleSignIn"),
            {theme: "outline", size: "large"}
        )
      },[])

    return (

        <div className="loginPopup">
            <div className="authMenu">
                <span className="exitButton" onClick={() => { popupSet(false) }}>✖</span>            
                    <div className="loginForm">
                        <h1>Přihlášení</h1>
                        {/* <FacebookLogin
                            appId="355443926753265"
                            autoLoad={false}
                            fields="name,email,picture"
                            callback={(e) => responseFacebook(e, dispatch, popupSet)}
                            icon="fa-facebook" 
                        /> */}
                    <div id='googleSignIn'></div>
                    </div>
            </div>
        </div>
    )
}


const responseFacebook = (res, dispatch, popupSet) => {
    fetch('/api/user/Login', {  
        method: 'POST', 
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify({oAuthProvider: _OAuthProvider.Facebook, appType: _AppType.Web, token: res.accessToken}) 
      
    }).then(response => response.json())
    .then(i => i = {...jwtDecode(i.token), token: i.token})
    .then(i => {console.log(i);
        dispatch(setLoginData({id:i.sub, name: i.name, email:i.email, picture: i.picture, permission: i.permission, token: i.token}))});

    popupSet(false);
  }
const responseGoogle = (res, dispatch, popupSet) => {
    fetch('/api/user/Login', {  

        method: 'POST', 
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify({oAuthProvider: _OAuthProvider.Google, appType: _AppType.Web, token: res.credential}) 
      
    }).then(response => (response.json()))
    .then(i => i = {...jwtDecode(i.token), token: i.token})
    .then(i => dispatch(setLoginData({id:i.sub, name: i.name, email:i.email, picture: i.picture, permission: i.permission, token: i.token})));

    popupSet(false);
}


export default Auth;
import tagState from "./reducers/tagState";
import tagCounter from "./reducers/tagCounter";
import editMenuState from "./reducers/editMenuState";
import activeChordsState from "./reducers/activeChordsState";
import chordFromsState from "./reducers/chordFormsState";
import loginState from "./reducers/loginState";
import { configureStore } from '@reduxjs/toolkit'

export default configureStore({
  reducer: {
    tagState,
    tagCounter,
    editMenuState,
    activeChordsState,
    chordFromsState,
    loginState

  }
})

import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  value: [],
}

export const chordFormsSlice = createSlice({
  name: 'chordFormsState',
  initialState,
  reducers: {
    addChordForm: (state, action) => {
      if(!state.value.some(e => e.form === action.payload.form)){   
        console.log(action.payload);
        state.value = [...state.value, action.payload];
      }
    },
    deleteChordForms: (state) => {
      state.value = [];
    },
    // removeTagById: (state, action) => {
    //   state.value = state.value.filter(obj => obj.id !== action.payload)
    // }

  },
})

// Action creators are generated for each case reducer function
export const { addChordForm, deleteChordForms} = chordFormsSlice.actions

export default chordFormsSlice.reducer
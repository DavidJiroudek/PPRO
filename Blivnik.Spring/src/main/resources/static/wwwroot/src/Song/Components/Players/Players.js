import React from 'react';
import { useEffect, useState } from "react";
import YouTube from "react-youtube"
import "./Players.css"
const Players = (props) => {

    const [youtubeVideoId, setYoutubeVideoId] = useState("");
    const [spotifySongId, setSpotifySongId] = useState({});
    const youtubeAPIKey = "AIzaSyDrWJn5_zQxH16x581SZ8LDyURJX9AekxI"
    const spotifyClientId = "6fda1f16420e4783b028633803662238"
    window.localStorage.setItem("blivnikLastSongId", props.songId);
    const spotifyTokenJSON = window.localStorage.getItem("blivnikSpotifyToken")
    const spotifyToken = spotifyTokenJSON !== null ? JSON.parse(spotifyTokenJSON) : null;
    useEffect(() => {
      fetch("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=" + props.songName + " " + props.authorName + "&key=" + youtubeAPIKey, {
          method: 'GET'
      }).then(response => response.json())
      .then((result) => {setYoutubeVideoId(result.items[0].id.videoId) });
  }, []);


  useEffect(() => {
    if(spotifyToken !== null){
      fetch("https://api.spotify.com/v1/search?q=" + props.songName + "&type=track&limit=1", {
        method: 'GET',
        headers: {'Authorization': 'Bearer ' + spotifyToken.token},
      }).then(response => response.json())
      .then((result) => {setSpotifySongId(result.tracks.items[0].id) });      
    }
}, []);



    const opts = {
        height: '200pt',
        width: '300pt',
        playerVars: {
          // https://developers.google.com/youtube/player_parameters
          autoplay: 0,
        },
      };
    return(
    <div className='players'>
        <YouTube className='youtubePlayer' videoId={youtubeVideoId} opts={opts} />
        {(spotifyToken !== null && spotifyToken.expiry > Date.now())?
        <iframe id="spotifyPlayer" style={{borderRadius:"12px"}} src={"https://open.spotify.com/embed/track/" + spotifySongId + "?utm_source=generator"} width="300pt" height="100pt" frameBorder="0" allowFullScreen="" autoPlay="0" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"></iframe>          
        :
        <a href={"https://accounts.spotify.com/authorize?client_id=" + spotifyClientId +"&redirect_uri=http://localhost:84/spotifyCallback&response_type=token"}><button><img className='spotifyButton' src='../wwwroot/listen-on-spotify-button.png' /></button></a>
        }
    </div>)

}


 export default Players

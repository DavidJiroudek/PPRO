import React from 'react';
import { useEffect } from "react"
import { useNavigate } from 'react-router-dom';

const SpotifyCallback = () =>{
    const navigate = useNavigate();

    const hash = window.location.hash
    const tokenObject = {token: hash.substring(1).split("&").find(elem => elem.startsWith("access_token")).split("=")[1], expiry: Date.now() + 3600}
    console.log(tokenObject);
    window.localStorage.setItem("blivnikSpotifyToken", JSON.stringify(tokenObject));

    useEffect(() => {
        const lastSongId = window.localStorage.getItem("blivnikLastSongId");
        if(lastSongId !== null){
        navigate("/song/" + lastSongId)
        }else{
            navigate("/")
        }
  },[]);


return(<div>redirecting...</div>)
    
}

export default SpotifyCallback
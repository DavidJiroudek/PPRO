﻿import React from 'react';
import { useState, useEffect } from "react";
import { useSelector } from 'react-redux';
import { Link, useParams } from "react-router-dom";
import "./Rating.css"


const Rating = () => {
        const [rating, setRating] = useState(0);
        const [change, setChange] = useState({});
        const loginData = useSelector((state) => state.loginState.value);
        const { songId } = useParams();
        const [hasVoted, setHastVoted] = useState(false);
        const [defaultRating, setDefaultRating] = useState(0);

        useEffect(() => {
            fetch('/api/song/GetRating', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ SongId: songId })
            }).then(response => response.json())
            .then((result) => {     result.rating !== null ? setRating(result.rating*20) : setRating(0) 
                                    result.rating !== null ? setDefaultRating(result.rating*20) : setDefaultRating(0) 
                                });
        }, [change]);


        useEffect(() => {
            if(loginData !== null){
                fetch('/api/song/HasVotedRating', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token },
                    body: JSON.stringify({ SongId: songId })
                }).then(response => response.json())
                .then((result) => { setHastVoted(result.hasVoted) });
            }
        }, []);


        if(!hasVoted){
            return (
                <div>
                    <div className="star-ratings-css">
                        <div style={{width: rating + "%"}} className="star-ratings-css top" id="a" >
                            <a onClick={() => (songId, loginData, 1, setChange, setHastVoted)} onMouseOver={() => setRating(20)} onMouseOut={() => setRating(defaultRating)}>★</a>
                            <a onClick={() => saveRating(songId, loginData, 2, setChange, setHastVoted)} onMouseOver={() => setRating(40)} onMouseOut={() => setRating(defaultRating)}>★</a>
                            <a onClick={() => saveRating(songId, loginData, 3, setChange, setHastVoted)} onMouseOver={() => setRating(60)} onMouseOut={() => setRating(defaultRating)}>★</a>
                            <a onClick={() => saveRating(songId, loginData, 4, setChange, setHastVoted)} onMouseOver={() => setRating(80)} onMouseOut={() => setRating(defaultRating)}>★</a>
                            <a onClick={() => saveRating(songId, loginData, 5, setChange, setHastVoted)} onMouseOver={() => setRating(100)} onMouseOut={() => setRating(defaultRating)}>★</a>
                        </div>
                        <div className="star-ratings-css bottom">
                            <a onClick={() => saveRating(songId, loginData, 1, setChange, setHastVoted)}>★</a>
                            <a onClick={() => saveRating(songId, loginData, 2, setChange, setHastVoted)} onMouseOver={() => setRating(40)} onMouseOut={() => setRating(defaultRating)}>★</a>
                            <a onClick={() => saveRating(songId, loginData, 3, setChange, setHastVoted)} onMouseOver={() => setRating(60)} onMouseOut={() => setRating(defaultRating)}>★</a>
                            <a onClick={() => saveRating(songId, loginData, 4, setChange, setHastVoted)} onMouseOver={() => setRating(80)} onMouseOut={() => setRating(defaultRating)}>★</a>
                            <a onClick={() => saveRating(songId, loginData, 5, setChange, setHastVoted)} onMouseOver={() => setRating(100)} onMouseOut={() => setRating(defaultRating)}>★</a>
                        </div>
                    </div>
                </div>
            )}
        else{
            return (
                <div>
                    <div className="star-ratings-css">
                        <div style={{width: rating + "%"}} className="star-ratings-css top" id="a">
                            <a>★</a>
                            <a>★</a>
                            <a>★</a>
                            <a>★</a>
                            <a>★</a>
                        </div>
                        <div className="star-ratings-css bottom">
                            <a>★</a>
                            <a>★</a>
                            <a>★</a>
                            <a>★</a>
                            <a>★</a>
                        </div>
                    </div>
                </div>
                
            )
        }
}
const saveRating = (songId, loginData, rating, setChange, setHastVoted) => {
    if(loginData !== null){
        fetch('/api/song/SaveRating', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token},
            body: JSON.stringify({ SongId: Number(songId), UserId: Number(loginData.id), Ratio: rating})
        });
        setHastVoted(true);
        setChange({});

    }else{
        alert("Pro hodnocení písně musíte být přihlášeni!");
    }

}
export default Rating
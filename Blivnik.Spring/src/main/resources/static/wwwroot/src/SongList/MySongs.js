import React from 'react';
import { useState, useEffect } from "react";
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import "./SongList.css"


const MySongs = () => {

    const [mySongs, setMySongs] = useState();
    const loginData = useSelector((state) => state.loginState.value)
    useEffect(() => {
      fetch('api/song/GetUserSongList', {  

        method: 'POST', 
        headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token}
      
      }).then(response => response.json())
      .then((result) => {setMySongs(result.map(i=>i = <Link className="songLink" to= {`/song/${i.songId}`} >{i.name}</Link>))});
},[]);


return(
    <div className="songList">{mySongs}</div>
    
    );
}
export default MySongs
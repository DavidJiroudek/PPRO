import React from 'react';
import { useEffect, useState } from "react"
import { useSelector } from 'react-redux';
import { _UserType } from "../Enums";
import "./Users.css";

const Users = ()=>{

    const [users, setUsers] = useState([]);
    const loginData = useSelector((state) => state.loginState.value)

    useEffect(() => {
        fetch('api/user/GetAllUsers', {  

            method: 'POST', 
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token}
   
        }).then(response => response.json())
        .then(response => setUsers(response))
        },[])
        

        const options =[<option value={_UserType.Visitor}>Visitor</option>,
                        <option value={_UserType.Creator}>Creator</option>,
                        <option value={_UserType.Moderator}>Moderator</option>,
                        <option value={_UserType.Admin}>Admin</option>]

return( <div>
            {users.map(i => <div key={"user " + i.id} className='usersContainer'>
                <span>{i.name}</span>
                <span>{i.email}</span>
                <select defaultValue={i.permission} onChange={(e)=>changePermission(i.id, e.target.value, loginData.token)
            }>{options}</select></div> )}
        </div>)
}

const changePermission = (id, permission, token)=>{
    const request = {Id: id, Permission: parseInt(permission)}
    console.log(request);

    fetch('api/user/ChangeUserPermission', {  

        method: 'POST', 
        headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token},
        body: JSON.stringify(request)

    })

}



export default Users
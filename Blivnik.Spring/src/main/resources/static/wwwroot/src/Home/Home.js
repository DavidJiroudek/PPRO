import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import "./Home.css"
const Home = () => {

    const [popularSongList, setPopularSongList] = useState([]);
    const [recentSongList, setRecentSongList] = useState([]);
    const loginData = useSelector((state) => state.loginState.value)

    useEffect(() =>{
        const header = loginData !== null ? { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token } : { 'Content-Type': 'application/json' }
        fetch('/api/song/GetSongList', {
        method: 'POST',
        headers: header,
        body: JSON.stringify({publicable: 0, page: 1, resultCount: 7, validated: true, sortBy: 0} )
        }).then(response => response.json())
        .then((result) => { 
                            setPopularSongList(result.items);
        });

        fetch('/api/song/GetSongList', {
        method: 'POST',
        headers: header,
        body: JSON.stringify({publicable: 0, page: 1, resultCount: 7, validated: true, sortBy: 0} )
        }).then(response => response.json())
        .then((result) => { 
            setRecentSongList(result.items);
        });


    }, [])


    return(
    <div id='homePage'>
        <div id="popularSongs">
        <p>Nejnavštěvovanější skladby</p>
            {popularSongList.map(i => i = <Link className="songLink" key={i.songId} to={`/song/${i.songId}`} >{i.name}</Link>)}
        </div>
        <div id="recentSongs">
        <p>Naposledy přidané skladby</p>
            {recentSongList.map(i => i = <Link className="songLink" key={i.songId} to={`/song/${i.songId}`} >{i.name}</Link>)}
        </div>
    </div>
    )

}
export default Home;
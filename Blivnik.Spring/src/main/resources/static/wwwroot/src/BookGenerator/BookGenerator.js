import React, { useState } from "react";
import { useSelector} from "react-redux";
import { _UserType } from "../Enums";
import './BookGenerator.css';

const BookGenerator = () =>{

    const loginData = useSelector((state) => state.loginState.value)
    const[priority, setPriority] = useState(1);
    const[date, setDate] = useState(new Date().toLocaleDateString('en-CA'));

    if(loginData !== null){  
    return(
        <form id="bookGenForm" onSubmit={(e) => handleSubmit(e, loginData.token)}>
        <div className="bookGenContainer">
            <div className="bookGenItem">
                <div>Minimální priorita: {priority}</div>
                <input type="range" id="priority" name="priority" min="1" max="5" defaultValue="1" onChange={(e) => setPriority(e.target.value)}></input>
            </div>
            <div className="bookGenItem">
                <div>Odkdy?</div>
                <input type="date" id="date" name="date" defaultValue={new Date().toLocaleDateString('en-CA')} onChange={(e) => setDate(e.target.value)}></input>
            </div>
            <div className="bookGenItem">
                <div>Minimální publikovatelnost: </div>
                <select className="blueButton" id="publicable" name="publicable">
                    <option value={0}>Publikovatelná</option>
                    <option value={1}>Nepublikovatelná</option>
                    {loginData.permission >= _UserType.Moderator ? <option value={2}>Neviditelná</option> : []}
                    {loginData.permission >= _UserType.Admin ? <option value={3}>Zakázaná</option> : []}
                </select>
            </div>
            <div>
                <button className="blueButton bookGenSubmitButton" form="bookGenForm" type="submit"><p>Vygenerovat knihu</p></button>
            </div>
        </div>
        </form>
    )
    }else{
        return(
        <div></div>
        )
    }

}
const handleSubmit = (event, token) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    let formObj = {};
    for (let [key, value] of formData.entries()) {

        formObj = {...formObj, [key]:value}

    }
    console.log(formObj);
    generateBook(formObj, token);

}

const generateBook = (bookParams, token) => {

    fetch('/api/pdf/GenerateBook', {  
      method: 'POST', 
      headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token},
      body: JSON.stringify(bookParams) 
    
    }).then(response => response.blob())
    .then((blob) => { 
        console.log(blob);
        const fileURL = window.URL.createObjectURL(blob);
        // Setting various property values
        let alink = document.createElement('a');
        alink.href = fileURL;
        alink.download = 'Blivník.pdf';
        alink.click();
  });
  }

export default BookGenerator
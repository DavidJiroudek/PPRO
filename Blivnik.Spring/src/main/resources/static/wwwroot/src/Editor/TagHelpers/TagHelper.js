import { addChordForm } from "../../Redux/reducers/chordFormsState";
import { increment } from "../../Redux/reducers/tagCounter";
import { changeTags } from "../../Redux/reducers/tagState";
import calculateRealtiveOffset from "./ComputingPositions/CalcuateRelativeOffset";
import calculatePosition from "./ComputingPositions/CalculatePosition";
import calculatePositionWeighted from "./ComputingPositions/CalculatePositionWeighted";
import {displayTag, rerenderTags} from "./DisplayTags";

//adds given tag to innerhtml
const addTag = (tagType, tagValue, counter, dispatch, params)=> {//params:  tagSufix, tagForm,
    if (document.getSelection) {
        let sel = document.getSelection();
    if (sel.type == "None") {
      alert("Označte text")
      return null;
    }  
        let parentEl = sel.getRangeAt(0).commonAncestorContainer.parentElement;
        const selectedElement = parentEl;
        while (parentEl.tagName != 'VERSE') {
           parentEl = parentEl.parentElement;

           if(parentEl.className === 'App'){
               alert("Špatné označení")
                return null;
           }
        }
        dispatch(increment());
        let textVal = parentEl.innerHTML;
        let startOffset = calculateRealtiveOffset(parentEl, true);
        let endOffset = calculateRealtiveOffset(parentEl, false);
        let calculOffset;
        let tag;
        let endTag;
        //tagType resolver
        switch (tagType) {
          case "ch":
            if(endOffset - startOffset === 0){
              alert("Označte pouze jeden znak pro vložení této značky");
              return null;
            }
            

            if(params.chordForm !== ""){
              dispatch(addChordForm({chordName: tagValue + params.chordSufix, form: params.chordForm})); 
              setTimeout(() => {dispatch(changeTags(rerenderTags()))}, 100);  
              }
            calculOffset = calculatePosition(startOffset, startOffset+1, textVal);
            tag = createStartTag(tagType, tagValue, counter, params);
            endTag = createEndTag(tagType);
            parentEl.innerHTML = textVal.substring(0, calculOffset.startPosition) + tag + textVal.substring(calculOffset.startPosition, calculOffset.endPosition) + endTag + textVal.substring(calculOffset.endPosition, textVal.length);
            break;
          case "dyn":
          case "capo": 
          case "rythm":
          case "space": 
            calculOffset = calculatePosition(startOffset, startOffset, textVal);
            tag = createStartTag(tagType, tagValue, counter, params);
            endTag = createEndTag(tagType);
            parentEl.innerHTML = textVal.substring(0, calculOffset.startPosition) + tag + textVal.substring(calculOffset.startPosition, calculOffset.endPosition) + endTag + textVal.substring(calculOffset.endPosition, textVal.length);
            break;


          case "delete": 
          calculOffset = calculatePositionWeighted(startOffset, endOffset, textVal, tagType);
          if(calculOffset === null){
            return null;
          }
          parentEl.innerHTML = textVal.substring(0, calculOffset.startPosition) + textVal.substring(calculOffset.endPosition, textVal.length);
          dispatch(changeTags(rerenderTags()));
          return "Deleted";

          case "insertText": 
          calculOffset = calculatePosition(startOffset, startOffset, textVal);
          parentEl.innerHTML = textVal.substring(0, calculOffset.startPosition) + tagValue + textVal.substring(calculOffset.endPosition, textVal.length);
          dispatch(changeTags(rerenderTags()));
          return "Added";

          default:
              calculOffset = calculatePositionWeighted(startOffset, endOffset, textVal, tagType);
              if(calculOffset === null){
                return null;
              }
              tag = createStartTag(tagType, tagValue, counter, params);
              endTag = createEndTag(tagType);
              parentEl.innerHTML = textVal.substring(0, calculOffset.startPosition) + tag + textVal.substring(calculOffset.startPosition, calculOffset.endPosition) + endTag + textVal.substring(calculOffset.endPosition, textVal.length);
              break;

        }

        displayTag(counter, dispatch);

    }
}




const createStartTag = (tagType, tagValue, counter, params) =>{
    return "<" + tagType + " value = "+ tagValue + " id = " + counter + createTextParams(params) + ">";
}

const createEndTag = (tagType) =>{
    return "</" + tagType +">";
}
  
const createTextParams = (paramsOrig) => {
  const params = paramsOrig !== undefined ? paramsOrig : {}
  let paramString = " "
  for (const [key, value] of Object.entries(params)) {
    paramString = paramString.concat(key + " = '" + value + "' ");
  }
  return paramString
}

//later to margin rows


// const moveRow = (selectedElement, dispatch) => {
//   let counter = 1;
//   let element =  selectedElement;
//   while (element.tagName != 'ROW') {
//   console.log(element.tagName);
//     if (element.tagName === "DYNWIDE") {
//       counter++;
//     }
//     element = element.parentElement;
//   }
//   console.log(element.id);
//   document.getElementById(element.id).style.marginTop = (14 * counter) + "pt";
//  // dispatch(changeTags(rerenderTags()));
// } 

export default addTag
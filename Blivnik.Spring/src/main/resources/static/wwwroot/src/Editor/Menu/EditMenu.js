import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addChord } from "../../Redux/reducers/activeChordsState";
import { changeEditMenu } from "../../Redux/reducers/editMenuState";
import { changeTags, removeTagById } from "../../Redux/reducers/tagState";
import "../css/EditMenu.css"
import { rerenderTags } from "../TagHelpers/DisplayTags";
import addTag from "../TagHelpers/TagHelper";

const EditMenu = ()=>{
    const [rangeValue, setRangeValue] = useState(0);
    const tagState = useSelector((state) => state.tagState.value) 
    const tagCounter = useSelector(state => state.tagCounter.value)
    const dispatch = useDispatch();
    const menuData = useSelector((state) => state.editMenuState.value) //menudata contains 1)type to take from switch 2)ID - id of element !OR! VALUE - data when using for different action then editing tags
    const element = menuData === null || menuData.id === null ? null : document.getElementById(menuData.id);
    const params = {tagState,
                    dispatch,
                    menuData,
                    element}
    if(menuData === null || element === null && menuData.value === null){ //!check if this is right!
        return(
            <div className="editMenu">

            </div>
            )
    }
    switch (menuData.type) {
        case "chord":
        case "rythm":
        case "dynWide":
        case "dyn":
            return(
                <div className="editMenu">
                    <div className="chordMenu">
                        <button className="whiteButton" onClick={() => deleteTagAndSign(params)}><p>Smazat</p></button>
                    </div>
                </div>
            )

        case "verse":
            return(
                <div className="editMenu">
                    <div className="verseMenu">
                    <button className="whiteButton" onClick={()=>{setVerseType("verse", "verse", params)}}><p>Sloka</p></button>
                    <button className="whiteButton" onClick={()=>{setVerseType("R", "ref", params)}}><p>Ref</p></button>
                    <button className="whiteButton" onClick={()=>{setVerseType("I", "int", params)}}><p>Int</p></button>
                    <button className="whiteButton" onClick={()=>{setVerseType("Rec", "rec", params)}}><p>Rec</p></button>
                    <input type="number"></input>
                    </div>
                </div>
            )
        case "alternativeChord":
            return(
            <div className="editMenu">
                <p>Přidejte příponu akordu a jeho zápis</p>
                <p>např.: C#mi 446654</p>
                <form onSubmit={(e) => {handleChordSubmit(e, dispatch, menuData.value);dispatch(changeEditMenu(null))}}>
                    <span>{menuData.value.tone + menuData.value.sign + "  "}</span>
                    <input type="text" name="name" placeholder ="Přípona akordu" defaultValue={menuData.value.sufix}></input>
                    <input type="text" name="value" placeholder ="Zápis akordu"></input>
                    <button className="whiteButton" type="submit"><p>Přidat</p></button>
                </form>
            </div>)
        case "rep":
            return(
            <div className="editMenu">
                <div className="chordMenu">
                    <button className="whiteButton" onClick={() => deleteTagAndSign(params)}><p>Smazat</p></button>
                </div>
            </div>
            )
        case "capo":
            return(
            <div className="editMenu">
                <div className="chordMenu">
                <div>Capo: {rangeValue}</div>
                <input type="range" min="0" max="10" defaultValue={element.getAttribute("value")}  onChange={(e) => setRangeValue(e.target.value)}></input>
                <button className="whiteButton" onClick={() => { element.setAttribute("value", rangeValue);
                                                                 dispatch(changeTags(rerenderTags()));
                                                                 dispatch(changeEditMenu(null))
                                                                 }}><p>Upravit</p></button>
                    <button className="whiteButton" onClick={() => deleteTagAndSign(params)}><p>Smazat</p></button>
                </div>
            </div>
            )
        case "space":
            return(
            <div className="editMenu">
                <div className="chordMenu">
                <input type="range" min="1" max="10" defaultValue={element.getAttribute("value")}  onChange={(e) => setRangeValue(e.target.value)}></input>
                <button className="whiteButton" onClick={() => { element.setAttribute("value", rangeValue);
                                                                 dispatch(changeTags(rerenderTags()));
                                                                 dispatch(changeEditMenu(null))
                                                                 }}><p>Upravit</p></button>
                <button className="whiteButton" onClick={() => {element.setAttribute("dash", element.getAttribute("dash") === "true" ? "false" : "true");
                                                                dispatch(changeTags(rerenderTags()));
                                                                dispatch(changeEditMenu(null))
                                                                }}>"—"</button>
                    <button className="whiteButton" onClick={() => deleteTagAndSign(params)}><p>Smazat</p></button>
                </div>
            </div>
            )
        case "insertText":
            return(
                <div className="editMenu">
                    <p>Přidejte název akordu a jeho zápis</p>
                        <input type="text" id="insertedText" name="value" placeholder ="Vložený text"></input>
                        <button className="whiteButton" type="submit" onClick={() => {  addTag("insertText", document.getElementById("insertedText").value, tagCounter, dispatch, {});
                                                                                        dispatch(changeTags(rerenderTags()))}}><p>Přidat</p></button>
                </div>)
        default:
            break
    }
}

const setVerseType = (verseValue, verseType, {tagState, dispatch, menuData, element}, number) =>{
    element.setAttribute("verseValue", verseValue)
    element.setAttribute("verseType", verseType)
    dispatch(changeEditMenu(null));
    recalculateVerses(dispatch);
}

const deleteTagAndSign = ({tagState, dispatch, menuData, element}) => {
    element.outerHTML = element.innerHTML
    dispatch(removeTagById(tagState.filter(i=>i.tagId === menuData.id)[0].id))
    dispatch(changeEditMenu(null));
}

//recalculates number values for each verse, ref and rec
const recalculateVerses = (dispatch) =>{
    const types = [{verseType:"verse", verseValue:""}, {verseType:"int", verseValue:"I"}, {verseType:"rec", verseValue:"Rec"}]
    const parts = document.getElementsByTagName("verse");
    for (const type of types) {
        let counter = 1;
        for (const iterator of parts) {
            if(iterator.getAttribute("verseType") === type.verseType){
                iterator.setAttribute("verseValue", type.verseValue + ((counter === 1 && type.verseType !== "verse") ? "" : counter))
                counter++;
            }
        }
    }

    dispatch(changeTags(rerenderTags()));
}

const handleChordSubmit = (event, dispatch, data) =>{
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const formObj = [];
    for (let [key, value] of formData.entries()) {
      console.log(key, value);
        formObj.push(value);
    }

    dispatch(addChord({tone: data.tone + data.sign, sufix:formObj[0],form: formObj[1]}))

}
export default EditMenu;
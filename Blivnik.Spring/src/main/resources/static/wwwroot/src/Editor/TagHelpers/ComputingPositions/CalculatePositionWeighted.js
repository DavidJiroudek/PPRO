
const tagsWeighted = ["dyn","ch", "rythm", "dynW", "rep", "delete", "row", "verse"]

//calculates position for adding tags into innerHtml
const calculatePositionWeighted = ( start, end, text, tag ) => {
    //calculation for end of the tag
    let isTag = false;
    let wantedPosition = 0;
    //let splited = text.split("");
    let i;
    for (i in text) {
        if (wantedPosition === end) {
            break;
        }
        if (text[i] === '<') {
            isTag = true;
            continue;
        }
        if (text[i] === '>') {
            isTag = false;
            continue;
        }
        if (isTag === false) {
            wantedPosition++;
        }
    }

    for (let k = 0; k < tagsWeighted.indexOf(tag); k++) {
        while(text.startsWith("</" + tagsWeighted[k], i)){
            while (text[++i] !== '>') { }
        }

    }

    let endPosition = i++;
    if (text.length === ++i) {
        endPosition = i;
    }



    wantedPosition = 0;
    let j 
    for (j in text) {
        if (wantedPosition === start) {
            break;
        }
        if (text[j] === '<') {
            isTag = true;
            continue;
        }
        if (text[j] === '>') {
            isTag = false;
            continue;
        }
        if (isTag === false) {
            wantedPosition++;
        }
    }
    //for starting tag to be inside
    for (let k = (tagsWeighted.length - 1); k > tagsWeighted.indexOf(tag); k--) {
        while(text.startsWith("<" + tagsWeighted[k], j)){
            while (text[++j] !== '>') { }
            j++;
        }
    }


    let startPosition = j;

    const containingString = text.substring(startPosition, endPosition) 
    if((containingString.split("<").length - 1) % 2 !== 0){
        alert("Prvky se nesmí prolínat");
        return null;
    }
    return { startPosition, endPosition };
}


export default calculatePositionWeighted

//<rep>  <dynW>  <ch>  <ch>  <dynW>  <rep>
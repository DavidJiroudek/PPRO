import { addTag, changeTags } from '../../Redux/reducers/tagState';
import tagsData from "../SignHelpers/TagsData";

//function to show sing of tag immediately after adding in editor 
export const displayTag = (counter, dispatch) =>{
    const element = document.getElementById(counter);
    const data = tagsData(element);
    dispatch(addTag(combineData(data)));
    if(element.tagName === "SPACE"){
      dispatch(changeTags(rerenderTags()))
    }
}

export const rerenderTags = () =>{
    const allElements = document.getElementById("editorContent").getElementsByTagName("*")
    const jsxData = [];
      for (let i of allElements) {
        const data = tagsData(i);
        if(data !== null){
          jsxData.push(combineData(data))
        }
      }
    return(jsxData);
    
}

const combineData=(data)=>{
    return({style:data.style, id:data.tagId +"Sign", className: data.className, key:data.tagId +"Sign",tagId:data.tagId , textValue:data.value, params:data.params})

}
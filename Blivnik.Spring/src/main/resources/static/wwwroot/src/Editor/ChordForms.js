import React from 'react';
import { useSelector } from "react-redux"

const ChordForms = () =>{
    const chordForms = useSelector(state => state.chordFromsState.value)//loop
    return(                
        <div className="chordForms">
            {chordForms.map(i => <div>{i.chordName + ": " + i.form}</div>)}
        </div>
    )

}
export default ChordForms
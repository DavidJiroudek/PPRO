//calculates relative offet of selection
const calculateRealtiveOffset = (parentElement, isStart, currentNode) =>{
  
    let currentSelection, currentRange,
        offset = 0,
        prevSibling,
        nodeContent;
        
    if (!currentNode && isStart === true){
      currentSelection = window.getSelection();
      currentRange = currentSelection.getRangeAt(0);
      currentNode = currentRange.startContainer;
      offset += currentRange.startOffset;
    }

    if (!currentNode && isStart === false){
        currentSelection = window.getSelection();
        currentRange = currentSelection.getRangeAt(0);
        currentNode = currentRange.endContainer;
        offset += currentRange.endOffset;
      }
      
    if (currentNode === parentElement){
      return offset;
    }
    
    if (!parentElement.contains(currentNode)){
      return -1;
    }
    
    while ( prevSibling = (prevSibling  || currentNode).previousSibling ){
      nodeContent = prevSibling.innerText || prevSibling.nodeValue || "";
      offset += nodeContent.length;
    }
    
    return offset + calculateRealtiveOffset( parentElement,isStart, currentNode.parentNode);
  
}



// const calculateRealtiveEndOffset = (parentElement, currentNode) =>{

//     let currentSelection, currentRange,
//         offset = 0,
//         prevSibling,
//         nodeContent;

//     if (!currentNode){
//         currentSelection = window.getSelection();
//         currentRange = currentSelection.getRangeAt(0);
//         currentNode = currentRange.endContainer;
//         offset += currentRange.endOffset;
//         }
        
//     if (currentNode === parentElement){
//         return offset;
//     }

//     if (!parentElement.contains(currentNode)){
//         return -1;
//     }

//     while ( prevSibling = (prevSibling  || currentNode).previousSibling ){
//         nodeContent = prevSibling.innerText || prevSibling.nodeValue || "";
//         offset += nodeContent.length;
//     }

//     return offset + calculateRealtiveEndOffset( parentElement, currentNode.parentNode);

// }






export default calculateRealtiveOffset
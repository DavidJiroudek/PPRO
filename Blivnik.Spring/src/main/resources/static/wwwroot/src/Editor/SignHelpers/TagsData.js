const tagsData = (element) => {
    switch (element.tagName) {
        case "CH":
            return {
                style:{
                    top: (element.offsetTop - 20),
                    left: element.offsetLeft,
                  },
                className: "chordSign",
                tagId: element.id,
                value: element.getAttribute("value"),
                params:{
                    sufix: element.getAttribute("chordSufix"),
                    form: element.getAttribute("chordForm")
                }
            }
        case "VERSE":
            return {
                style:{
                    top: (element.offsetTop - 2),
                    right: element.parentElement.offsetWidth - element.offsetLeft - 5, //really dont know is this is the right way to do that
                },
                className: "verseSign",
                tagId: element.id,
                value: element.getAttribute("verseValue") + ")",
                params:{}
            }
            case "DYN":
            return {
                style:{
                    top: (element.offsetTop - 18),
                    left: element.offsetLeft,
                  },
                className: "dynSign",
                tagId: element.id,
                value: element.getAttribute("value"),
                params:{}
            }
            case "REP":
                return {
                    style:{
                        top: (element.offsetTop - 5),
                        left: (element.offsetLeft - 10),
                        position: "absolute",
                        zindex: 6 ,
                        fontSize: "1.5em"
                      },
                    className: "repSign",
                    tagId: element.id,
                    value: "𝄆",
                    params:{
                        endStyle:{
                            top: (element.offsetTop - 5),
                            left: (element.offsetLeft + element.offsetWidth),
                            position: "absolute",
                            zindex: 6 ,
                            fontSize: "1.5em"
                            },
                        endValue:"𝄇"
                    }
                }
            case "RYTHM":
                return {
                    style:{
                        top: (element.offsetTop - 18),
                        left: element.offsetLeft,
                        fontSize: "0.8em"
                        },
                    className: "rythmSign",
                    tagId: element.id,
                    value: element.getAttribute("value"),
                    params:{}
                }

            case "CAPO":
                return {
                    style:{
                        top: (element.offsetTop - 18),
                        left: element.offsetLeft
                        },
                    className: "capoSign",
                    tagId: element.id,
                    value: element.getAttribute("value"),
                    params:{}
                }
            case "SPACE":
                element.style.padding = "0 " + (parseInt(element.getAttribute("value")) + 0.75) + "ex"
                return {
                    style:{
                        top: element.offsetTop,
                        left: element.offsetLeft,
                        padding: "0 " + element.getAttribute("value") + "ex",
                        },
                    className: "spaceSign",
                    tagId: element.id,
                    value: element.getAttribute("dash") === "true" ? "—" : " ",
                    params:{}
                }
            case "DYNWIDE":
                return {
                    style:{
                        top: (element.offsetTop - 18),
                        left: element.offsetLeft,
                        width: element.offsetWidth,
                        backgroundImage: element.getAttribute("value") === "𝆒" ? "url("+"/crescendo.png"+")" : "url("+"/decrescendo.png"+")",
                        backgroundSize: "contain",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "center"
                        },
                    className: "dynWideSign",
                    tagId: element.id,
                    value: " ",
                    params:{}
                }

        default:
            return null;
    }

}
export default tagsData;
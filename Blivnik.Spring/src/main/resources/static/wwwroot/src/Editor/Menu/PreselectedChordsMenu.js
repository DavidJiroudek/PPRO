import React from 'react';
import { useDispatch, useSelector } from "react-redux"
import addTag from "../TagHelpers/TagHelper";
import "../css/ActiveChordsMenu.css"

const ActiveChords = () =>{
    const chords = useSelector(state => state.activeChordsState.value)
    const counter = useSelector(state => state.tagCounter.value)
    const dispatch = useDispatch();
return(<div className='activeChords'>{chords.map(i => <span className="activeChordsElement" key={i.tone + i.sufix} id={i.tone + i.sufix} onClick={() => addTag("ch", i.tone, counter, dispatch, {chordSufix: i.sufix, chordForm: i.form})}>{i.tone + i.sufix}</span>)}</div>)
}

export default ActiveChords
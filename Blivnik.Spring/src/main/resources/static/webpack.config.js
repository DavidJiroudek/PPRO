﻿module.exports = {
    mode: "development",
    context: __dirname,
    entry: "./wwwroot/src/index.js",
    output: {
        path: __dirname + "/wwwroot/dist",
        filename: "bundle.js"
    },
    watch: true,
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ["@babel/preset-env", "@babel/preset-react"]
                }
            }
        },
        {
            test: /\.css$/i,
            use: ["style-loader", "css-loader"],
        },
        ]

    }
}
package PPRO.Blivnik_Spring.Controllers;

import PPRO.Blivnik_Spring.Handlers.SQLHandler;
import PPRO.Blivnik_Spring.Handlers.UserRoleHandler;
import PPRO.Blivnik_Spring.Model.Enums.UserType;
import PPRO.Blivnik_Spring.Model.Objects.requests.search.GetFilteredSongsRequest;
import PPRO.Blivnik_Spring.Model.Objects.responses.authors.GetAuthorListResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;
import java.util.*;

@Controller
@RequestMapping(value = "/api/search/")
public class SearchController {

    @PostMapping("GetWhispererData")
    public @ResponseBody
    ResponseEntity<?> getWhispererData(@RequestHeader String Authorization) {

        var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Creator);
        if (user == null) {
            return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
        }
        List<GetAuthorListResponse> empList = new ArrayList<>();
        try {
            String selectSql = "SELECT [Id], [Name] FROM dbo.[Author]";
            var resultSet = SQLHandler.getConnection().createStatement().executeQuery(selectSql);
            while (resultSet.next()) {
                var author = new GetAuthorListResponse();
                author.setAuthorId(resultSet.getInt("Id"));
                author.setName(resultSet.getString("Name"));
                empList.add(author);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(empList, HttpStatus.OK);
    }

//    @PostMapping("/GetFilteredSearchResult")
//    public ResponseEntity<?> getFilteredSearchResult(@RequestHeader String Authorization, @RequestBody GetFilteredSongsRequest filter) {
//        Map.Entry<Integer, UserType> user = new AbstractMap.SimpleEntry<>(null, UserType.Visitor);
//
//        if (Authorization != null) {
//            user = UserRoleHandler.GetUserIdAndRole(Authorization);
//        }
//
//        List<GetFulltextResponse<ElasticSong>> data;
//
//        try {
//            // Perform fulltext search
//            ElasticQueryResult<ElasticSong> result = elasticService.query(ElasticSong.prepareFilterDetailSearch(filter.toAuthorFilter()));
//            int maxPage = (result.getResults().size() % filter.getResultCount() != 0) ?
//                    (result.getResults().size() / filter.getResultCount()) + 1 :
//                    (result.getResults().size() / filter.getResultCount());
//
//            List<GetFulltextResponse<ElasticSong>> pagedResult = result.getResults()
//                    .stream()
//                    .skip(filter.getResultCount() * (filter.getPage() - 1))
//                    .limit(filter.getResultCount())
//                    .map(i -> new GetFulltextResponse<>(i, maxPage))
//                    .collect(Collectors.toList());
//
//            cacheService.setValueAbandonTask(key, pagedResult);
//
//            data = pagedResult.stream()
//                    .filter(i -> PermissionHandler.isPermited(user.getRight(), i.getItem().getValidated(), i.getItem().getPublicable()))
//                    .collect(Collectors.toList());
//
//            return new ResponseEntity<>(data, HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
}

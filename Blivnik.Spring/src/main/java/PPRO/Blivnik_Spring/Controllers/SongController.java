package PPRO.Blivnik_Spring.Controllers;

import PPRO.Blivnik_Spring.Calls.EditSongCall;
import PPRO.Blivnik_Spring.Calls.GetSongCall;
import PPRO.Blivnik_Spring.Calls.SaveSongCall;
import PPRO.Blivnik_Spring.Handlers.PermissionHandler;
import PPRO.Blivnik_Spring.Handlers.SQLHandler;
import PPRO.Blivnik_Spring.Handlers.UserRoleHandler;
import PPRO.Blivnik_Spring.Model.Enums.UserType;
import PPRO.Blivnik_Spring.Model.Objects.SongList;
import PPRO.Blivnik_Spring.Model.Objects.requests.songs.EditSongRequest;
import PPRO.Blivnik_Spring.Model.Objects.requests.songs.GetSongListRequest;
import PPRO.Blivnik_Spring.Model.Objects.requests.songs.SaveSongRequest;
import PPRO.Blivnik_Spring.Model.Objects.requests.songs.SongIdRequest;
import PPRO.Blivnik_Spring.Model.Objects.responses.authors.GetAuthorListResponse;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetSongForEditResponse;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetSongListResponse;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetSongResponce;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetUserSongListResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static PPRO.Blivnik_Spring.Handlers.PageFilterHandler.FilterByRequest;


@RestController
@RequestMapping(value = "/api/song/")
public class SongController {
    @PostMapping("SaveSong")
    public @ResponseBody
    ResponseEntity<?> saveSong(@RequestHeader String Authorization, @RequestBody SaveSongRequest request) {

        var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Creator);
        if (user == null) {
            return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
        }
        SaveSongCall.saveSong(request.ToSong(user.getKey()));
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    @PostMapping("GetSongList")
    public GetSongListResponse GetSongList(@RequestHeader(required = false) String Authorization, @RequestBody GetSongListRequest request) {
        Map.Entry<Integer, UserType> user = new AbstractMap.SimpleEntry<>(null, UserType.Visitor);
        if (Authorization != null) {
            user = UserRoleHandler.GetUserIdAndRole(Authorization);
        }
        int maxPageValue;
        var result = new GetSongListResponse();
        try {
            String selectSql = "SELECT [SongId], [Name], [Publicable], [Validated], [ViewCount] FROM dbo.[Version] WHERE IsObsolete != 1";
            var resultSet = SQLHandler.getConnection().createStatement().executeQuery(selectSql);
            while (resultSet.next()) {
                var song = new SongList();
                song.setSongId(resultSet.getInt("SongId"));
                song.setName(resultSet.getString("Name"));
                song.setPublicable(resultSet.getInt("Publicable"));
                song.setValidated(resultSet.getBoolean("Validated"));
                song.setViewCount(resultSet.getInt("ViewCount"));
                result.getItems().add(song);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        var filteredResult = FilterByRequest(result, request);
        var permission = user.getValue();

        filteredResult.setItems(result.getItems().stream().filter(i -> PermissionHandler.IsPermited(permission, i.getValidated(), i.getPublicable())).collect(Collectors.toList()));
        filteredResult.setMaxPage(result.getItems().size() / request.getResultCount() + 1);

        return result;

    }

    @PostMapping("GetUserSongList")
    public @ResponseBody
    ResponseEntity<?> GetUserSongList(@RequestHeader String Authorization) {
        var user = UserRoleHandler.GetUserIdAndRole(Authorization);
        try {
            var con = SQLHandler.getConnection().prepareStatement("SELECT [SongId], [Name] FROM dbo.[Version] WHERE IsObsolete != 1 AND Creator = ?");
            con.setInt(1, user.getKey());
            var resultSet = con.executeQuery();
            var result = new ArrayList<GetUserSongListResponse>();
            while (resultSet.next()) {
                var song = new GetUserSongListResponse();
                song.setSongId(resultSet.getInt("SongId"));
                song.setName(resultSet.getString("Name"));

                result.add(song);
            }
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (SQLException e) {
            e.printStackTrace();
            return new ResponseEntity<>("GetUserSongList thrown exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("GetSong")
    public @ResponseBody
    ResponseEntity<?> GetSong(@RequestHeader(required = false) String Authorization, @RequestBody SongIdRequest request) {
        Map.Entry<Integer, UserType> user = new AbstractMap.SimpleEntry<>(null, UserType.Visitor);
        var sql = new GetSongCall();
        var result = sql.GetSong(request.getSongId());
        if (result == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (user.getKey() != null && (!Objects.equals(result.getCreator(), user.getKey()) || !Objects.equals(result.getEditor(), user.getKey()))) {
            if (!PermissionHandler.IsPermited(user.getValue(), result.getValidated(), result.getPublicable())) {
                return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
            }
        }
        return new ResponseEntity<>(new GetSongResponce(result), HttpStatus.OK);
    }

    @PostMapping("GetSongForEdit")
    public @ResponseBody
    ResponseEntity<?> GetSongForEdit(@RequestHeader String Authorization, @RequestBody SongIdRequest request) {

        var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Moderator);
        if (user == null) {
            return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
        }
        var sql = new GetSongCall();
        var result = sql.GetSong(request.getSongId());

        if (result == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if (!PermissionHandler.IsPermited(user.getValue(), result.getValidated(), result.getPublicable())) {
            return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(new GetSongForEditResponse(result), HttpStatus.OK);

    }

    @PostMapping("DeleteSong")
    public @ResponseBody
    ResponseEntity<?> DeleteSong(@RequestHeader String Authorization, @RequestBody SongIdRequest request) {

        var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Admin);
        if (user == null) {
            return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
        }
        try {
            var con = SQLHandler.getConnection().prepareStatement("UPDATE dbo.Version SET IsObsolete = 1 WHERE SongId = ? AND IsObsolete != 1");

            con.setInt(1, request.getSongId());
            con.execute();
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (SQLException e) {
            e.printStackTrace();
            return new ResponseEntity<>("DeleteSong thrown exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("ValidateSong")
    public @ResponseBody
    ResponseEntity<?> ValidateSong(@RequestHeader String Authorization, @RequestBody SongIdRequest request) {

        var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Moderator);
        if (user == null) {
            return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
        }
        try {

            if (UserRoleHandler.GetUserIdAndRole(Authorization).getValue() == UserType.Admin) {

                var con = SQLHandler.getConnection().prepareStatement("UPDATE dbo.Version SET Validated = 1, [Date] = GETDATE() WHERE SongId = ? AND IsObsolete != 1");

                con.setInt(1, request.getSongId());
                con.execute();
                return new ResponseEntity<>(HttpStatus.OK);
            }

            var con = SQLHandler.getConnection().prepareStatement("IF((SELECT TOP 1 Publicable FROM dbo.Version WHERE SongId = 0 AND IsObsolete != 1) <= 2) BEGIN UPDATE dbo.Version SET Validated = 1, [Date] = GETDATE() WHERE SongId = ? AND IsObsolete != 1 END");

            con.setInt(1, request.getSongId());
            con.execute();
            return new ResponseEntity<>(HttpStatus.OK);


        } catch (SQLException e) {
            e.printStackTrace();
            return new ResponseEntity<>("ValidateSong thrown exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("EditSong")
    public @ResponseBody
    ResponseEntity<?> EditSong(@RequestHeader String Authorization, @RequestBody EditSongRequest request) {

        var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Creator);
        if (user == null) {
            return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
        }

        var sql = new EditSongCall();
        sql.editSong(request.ToSong(user.getKey()));
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @PostMapping("IncreseViewCount")
    public @ResponseBody
    ResponseEntity<?> IncreseViewCount(@RequestBody SongIdRequest request) {

        try {
            var con = SQLHandler.getConnection().prepareStatement("UPDATE dbo.Version SET ViewCount = ViewCount + 1 WHERE SongId = ? AND IsObsolete != 1");

            con.setInt(1, request.getSongId());
            con.execute();
            return new ResponseEntity<>(HttpStatus.OK);

        } catch (SQLException e) {
            e.printStackTrace();
            return new ResponseEntity<>("IncreseViewCount thrown exception", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
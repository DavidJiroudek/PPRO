package PPRO.Blivnik_Spring.Controllers;

import PPRO.Blivnik_Spring.Handlers.PermissionHandler;
import PPRO.Blivnik_Spring.Handlers.SQLHandler;
import PPRO.Blivnik_Spring.Handlers.UserRoleHandler;
import PPRO.Blivnik_Spring.Model.Enums.UserType;
import PPRO.Blivnik_Spring.Model.Objects.Author;
import PPRO.Blivnik_Spring.Model.Objects.requests.authors.GetAuthorListRequest;
import PPRO.Blivnik_Spring.Model.Objects.requests.authors.GetAuthorsSongListRequest;
import PPRO.Blivnik_Spring.Model.Objects.requests.songs.GetSongListRequest;
import PPRO.Blivnik_Spring.Model.Objects.responses.authors.GetAuthorListResponse;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetAuthorsSongListResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/api/author/")
public class AuthorController {
    @PostMapping("GetAuthorList")
    public @ResponseBody
    ResponseEntity<?> getAuthorList(@RequestBody GetAuthorListRequest request) {
        List<GetAuthorListResponse> empList = new ArrayList<>();
        try {
            String selectSql = "SELECT [Id], [Name] FROM dbo.[Author]";
            var resultSet = SQLHandler.getConnection().createStatement().executeQuery(selectSql);
            while (resultSet.next()) {
                var author = new GetAuthorListResponse();
                author.setAuthorId(resultSet.getInt("Id"));
                author.setName(resultSet.getString("Name"));
                empList.add(author);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(empList, HttpStatus.OK);
    }
    @PostMapping("getAuthorsSongList")
    public @ResponseBody
    ResponseEntity<?> getAuthorsSongList(@RequestHeader(required = false) String Authorization, @RequestBody GetAuthorsSongListRequest request) {
        Map.Entry<Integer, UserType> user = new AbstractMap.SimpleEntry<>(null, UserType.Visitor);
        if (Authorization != null) {
            user = UserRoleHandler.GetUserIdAndRole(Authorization);
        }
        var result = new ArrayList<GetAuthorsSongListResponse>();
        try {
            var con = SQLHandler.getConnection().prepareCall("{call dbo.GetAuthorsSongList(?)}");
            con.setInt(1, request.getAuthorId());
            var rs = con.executeQuery();
            while (rs.next()) {
                var song = new GetAuthorsSongListResponse();
                song.setSongId(rs.getInt("SongId"));
                song.setName(rs.getString("Name"));
                song.setPublicable(rs.getInt("Publicable"));
                song.setValidated(rs.getBoolean("Validated"));
                result.add(song);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        var permission = user.getValue();
        var filteredResult = result.stream().filter(i -> PermissionHandler.IsPermited(permission, i.getValidated(), i.getPublicable())).collect(Collectors.toList());
        return new ResponseEntity<>(filteredResult, HttpStatus.OK);
    }
}

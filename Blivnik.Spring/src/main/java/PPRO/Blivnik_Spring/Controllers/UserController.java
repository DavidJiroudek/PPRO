package PPRO.Blivnik_Spring.Controllers;

import PPRO.Blivnik_Spring.Handlers.PermissionHandler;
import PPRO.Blivnik_Spring.Handlers.SQLHandler;
import PPRO.Blivnik_Spring.Handlers.UserRoleHandler;
import PPRO.Blivnik_Spring.Model.Enums.OAuthProvider;
import PPRO.Blivnik_Spring.Model.Enums.UserType;
import PPRO.Blivnik_Spring.Model.Objects.UserData;
import PPRO.Blivnik_Spring.Model.Objects.requests.songs.GetSongListRequest;
import PPRO.Blivnik_Spring.Model.Objects.requests.users.LoginRequest;
import PPRO.Blivnik_Spring.Model.Objects.responces.users.LoginResponse;
import PPRO.Blivnik_Spring.Model.Objects.responses.LoginTokenResponse;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetSongListResponse;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetUserSongListResponse;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping(value = "/api/user/")
public class UserController {


    @PostMapping("Login")
    public @ResponseBody
    ResponseEntity<?> Login(HttpServletResponse response, @RequestBody LoginRequest request) {
        UserData userData;
        try {
            if (OAuthProvider.values()[request.getOauthProvider()] == OAuthProvider.Google) {
                URL url = new URL("https://oauth2.googleapis.com/tokeninfo?id_token=" + request.getToken());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();

                ObjectMapper mapper = new ObjectMapper();
                JsonNode fbData = mapper.readTree(content.toString());
                if (!fbData.get("azp").asText().equals("118452082258-uc420e1oksqifgao1ck5vuk3vvj0f2kn.apps.googleusercontent.com")) {
                    return new ResponseEntity<>("Token is not valid", HttpStatus.BAD_REQUEST);
                }

                userData = new UserData(
                        fbData.get("name").asText(),
                        fbData.get("email").asText(),
                        fbData.get("picture").asText(),
                        UserType.Visitor.getValue());

            } else {
                URL url = new URL("https://graph.facebook.com/debug_token?input_token=" + request.getToken() + "&access_token=355443926753265|720fa17a7536ef98696e6178d359d0f7");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();

                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonFbResponse = mapper.readTree(content.toString());
                if (!Boolean.parseBoolean(jsonFbResponse.get("data").get("is_valid").asText())) {
                    return new ResponseEntity<>("Token is not valid", HttpStatus.BAD_REQUEST);
                }

                url = new URL("https://graph.facebook.com/" + jsonFbResponse.get("data").get("user_Id").asText());
                con = (HttpURLConnection) url.openConnection();

                in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();
                var fbData = mapper.readTree(content.toString());

                userData = new UserData(
                        fbData.get("name").asText(),
                        fbData.get("email").asText(),
                        fbData.get("picture").get("data").get("url").asText(),
                        UserType.Visitor.getValue());
            }


            var login = new LoginResponse();
            try {
                var con = SQLHandler.getConnection().prepareCall("{call dbo.Login(?,?)}");
                con.setString(1, userData.getName());
                con.setString(2, userData.getEmail());
                var resultSet = con.executeQuery();
                resultSet.next();

                    login.setId(resultSet.getInt("Id"));
                    login.setPermission(resultSet.getInt("Permissions"));
            } catch (SQLException e) {
                e.printStackTrace();
                return new ResponseEntity<>("GetUserSongList thrown exception", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            userData.setPermission(login.getPermission());
            String key = "qq22ZXqYDc&9ilps7@";
            var algorithm = Algorithm.HMAC256(key.getBytes(StandardCharsets.UTF_8));

            Calendar calendar = Calendar.getInstance(); // gets a calendar using the default time zone and locale.
            calendar.add(Calendar.SECOND, 55 * 60);
            var token = JWT.create()
                    .withSubject(String.valueOf(login.getId()))
                    .withClaim("name", userData.getName())
                    .withClaim("email", userData.getEmail())
                    .withClaim("picture", userData.getPicture())
                    .withClaim("permission", String.valueOf(userData.getPermission()))
                    .withExpiresAt(calendar.getTime()).sign(algorithm);

            var cookie = new Cookie("X-AccessToken", token);
            cookie.setMaxAge(55 * 60);
            cookie.setHttpOnly(true);
            response.addCookie(cookie);
            return new ResponseEntity<>(new LoginTokenResponse(token), HttpStatus.OK);

        } catch (IOException e) {
            return new ResponseEntity<>("Token could not be build", HttpStatus.NOT_FOUND);
        }
    }
    @PostMapping("GetAllUsers")
    public ResponseEntity<?> GetSongList(@RequestHeader(required = false) String Authorization) {
        Map.Entry<Integer, UserType> user = new AbstractMap.SimpleEntry<>(null, UserType.Admin);
        if (user == null) {
            return new ResponseEntity<>("Nemáte dostatečná oprávnění pro tuto akci", HttpStatus.UNAUTHORIZED);
        }
        var result = new ArrayList<UserData>();
        try {
            String selectSql = "SELECT [Id], [Name], [Email], [Permissions] FROM dbo.[User]";
            var resultSet = SQLHandler.getConnection().createStatement().executeQuery(selectSql);
                while(resultSet.next()) {
                    var temp = new UserData();
                    temp.setId(resultSet.getInt("Id"));
                    temp.setName(resultSet.getString("Name"));
                    temp.setEmail(resultSet.getString("Email"));
                    temp.setPermission(resultSet.getInt("Permissions"));
                    result.add(temp);
                }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(result, HttpStatus.OK);

    }




    @GetMapping("IsSignedIn")
    public ResponseEntity<?> IsSignedIn(HttpServletRequest request)
    {
        if(request.getCookies() != null) {
            for (var cookie : request.getCookies()) {
                if (cookie.getName().equals("X-AccessToken")) {
                    return new ResponseEntity<>(new LoginTokenResponse(cookie.getValue()), HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>("Access token is not valid or expired", HttpStatus.BAD_REQUEST);
    }
    @GetMapping("LogOut")
    public ResponseEntity<?> LogOut(HttpServletResponse response, HttpServletRequest request)
    {
        if(request.getCookies() != null) {
            for (var cookie : request.getCookies()) {
                if (cookie.getName().equals("X-AccessToken")) {
                    cookie.setValue("");
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                    return new ResponseEntity<>("You were logged out", HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>("Unable to logout from session", HttpStatus.BAD_REQUEST);
    }
}


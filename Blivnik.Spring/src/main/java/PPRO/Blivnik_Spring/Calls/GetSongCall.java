package PPRO.Blivnik_Spring.Calls;

import PPRO.Blivnik_Spring.Handlers.SQLHandler;
import PPRO.Blivnik_Spring.Model.Objects.Song;
import PPRO.Blivnik_Spring.Model.Objects.responses.authors.GetAuthorListResponse;
import com.microsoft.sqlserver.jdbc.SQLServerDataTable;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GetSongCall {

    public Song GetSong(int songId) {

        try {
            var con = SQLHandler.getConnection().prepareCall("{call dbo.GetSong(?)}");
            con.setInt(1, songId);

            ResultSet rs = con.executeQuery();
            var song = new Song();
            while (rs.next()) {

                song.setSongId(rs.getInt("SongId"));
                song.setSongName(rs.getString("Name"));
                song.setDate(rs.getDate("Date"));
                song.setPriority(rs.getInt("Priority"));
                song.setPublicable(rs.getInt("Publicable"));
                song.setCapo(rs.getInt("Capo"));
                song.setUrl(rs.getString("YoutubeURL"));
                song.setValidated(rs.getBoolean("Validated"));
                song.setCreator(rs.getInt("Creator"));
                song.setEditor(rs.getInt("Editor"));
                song.setText(rs.getString("Value"));
            }
            List<Rythm> authors = new ArrayList<>();
            if(con.getMoreResults()) {
                rs = con.getResultSet();
                while (rs.next()) {
                    var author = new Rythm(rs.getInt("SongId"), rs.getString("Name"), rs.getInt("Type"));
                    authors.add(author);
                }
            }

            List<Author> rythms = new ArrayList<>();
            if(con.getMoreResults()) {
                rs = con.getResultSet();
                while (rs.next()) {
                    var rythm = new Author(rs.getInt("SongId"), rs.getInt("Rythm_Id"));
                    rythms.add(rythm);
                }
            }

            List<Map.Entry<String, Integer>> rofl = authors.stream().map(x -> new AbstractMap.SimpleEntry<String, Integer>(x.authorName, x.type)).collect(Collectors.toList());
            song.setAuthors(rofl);
            song.setRythms(rythms.stream().map(x->x.type).collect(Collectors.toList()));
            return song;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private class Rythm{
        private int songId;
        private String authorName;
        private int type;

        public Rythm(int songId, String authorName, int type) {
            this.songId = songId;
            this.authorName = authorName;
            this.type = type;
        }
    }

    private class Author{
        private int songId;
        private int type;

        public Author(int songId, int type) {
            this.songId = songId;
            this.type = type;
        }
    }
}
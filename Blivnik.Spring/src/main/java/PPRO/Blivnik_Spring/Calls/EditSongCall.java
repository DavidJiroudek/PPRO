package PPRO.Blivnik_Spring.Calls;

import PPRO.Blivnik_Spring.Handlers.SQLHandler;
import PPRO.Blivnik_Spring.Model.Objects.Song;
import com.microsoft.sqlserver.jdbc.SQLServerDataTable;

import java.sql.SQLException;
import java.sql.Types;

public class EditSongCall {

    public void editSong(Song data){
        try {
            var con = SQLHandler.getConnection().prepareCall("{call dbo.EditSong(?,?,?,?,?,?,?,?,?,?,?)}");
            SQLServerDataTable authorParam = new SQLServerDataTable();
            authorParam.setTvpName("dbo.AuthorsData_Table");
            authorParam.addColumnMetadata("Name" , Types.NVARCHAR);
            authorParam.addColumnMetadata("Type" , Types.INTEGER);
            for (var author : data.getAuthors()) {
                authorParam.addRow(author.getKey(), author.getValue());
            }
            SQLServerDataTable rythms = new SQLServerDataTable();
            authorParam.setTvpName("dbo.Int_Table");
            authorParam.addColumnMetadata("Id" , Types.INTEGER);
            for (var rythm : data.getRythms()) {
                authorParam.addRow(rythm);
            }

// Pass the data table as a table-valued parameter using a prepared statement.
            con.setInt(1, data.getSongId());
            con.setString(2, data.getSongName());
            con.setInt(3, data.getPublicable());
            con.setInt(4, data.getPriority());
            con.setString(5, data.getUrl());
            con.setInt(6, data.getCreator());
            con.setString(7, data.getText());
            con.setBoolean(8,data.getReprint());
            con.setInt(9, data.getCapo());
            con.setObject(10, authorParam);
            con.setObject(11, rythms);

            con.executeQuery();



        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

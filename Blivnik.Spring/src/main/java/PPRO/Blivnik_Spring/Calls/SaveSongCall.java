package PPRO.Blivnik_Spring.Calls;

import PPRO.Blivnik_Spring.Handlers.SQLHandler;
import PPRO.Blivnik_Spring.Model.Objects.Song;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetAuthorsSongListResponse;
import com.microsoft.sqlserver.jdbc.SQLServerDataTable;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

public class SaveSongCall {

    public static int saveSong(Song data){
        try {
            var con = SQLHandler.getConnection().prepareCall("{call dbo.SaveSong(?,?,?,?,?,?,?,?,?)}");
            SQLServerDataTable authorParam = new SQLServerDataTable();
            authorParam.setTvpName("dbo.AuthorsData_Table");
            authorParam.addColumnMetadata("Name" , Types.NVARCHAR);
            authorParam.addColumnMetadata("Type" , Types.INTEGER);
            for (var author : data.getAuthors()) {
                authorParam.addRow(author.getKey(), author.getValue());
            }
            SQLServerDataTable rythms = new SQLServerDataTable();
            authorParam.setTvpName("dbo.Int_Table");
            authorParam.addColumnMetadata("Id" , Types.INTEGER);
            for (var rythm : data.getRythms()) {
                authorParam.addRow(rythm);
            }

// Pass the data table as a table-valued parameter using a prepared statement.
            con.setString(1, data.getSongName());
            con.setInt(2, data.getPublicable());
            con.setInt(3, data.getPriority());
            con.setInt(4, data.getCapo());
            con.setString(5, data.getUrl());
            con.setInt(6, data.getCreator());
            con.setString(7, data.getText());
            con.setObject(8, authorParam);
            con.setObject(9, rythms);

            var rs = con.executeQuery();

            rs.next();
            return rs.getInt("SongId");


        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }
}

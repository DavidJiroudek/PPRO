package PPRO.Blivnik_Spring.Handlers;

import PPRO.Blivnik_Spring.Model.Enums.PublicableType;
import PPRO.Blivnik_Spring.Model.Enums.UserType;

public final class PermissionHandler
{

	public static boolean IsPermited(UserType role, boolean validated, int publicable)
	{

		switch (role)
		{
			case Visitor:
				if (validated && publicable <= PublicableType.Unpublicable.getValue())
				{
					return true;
				}
				return false;
			case Creator:
				if (validated && publicable <= PublicableType.Unpublicable.getValue())
				{
					return true;
				}
				return false;
			case Moderator:
				if (publicable <= PublicableType.Invisible.getValue())
				{
					return true;
				}
				return false;
			case Admin:
				return true;
			default:
				return false;
		}




	}
}

package PPRO.Blivnik_Spring.Handlers;


import PPRO.Blivnik_Spring.Model.Enums.UserType;
import com.auth0.jwt.JWT;

import java.util.AbstractMap;
import java.util.Base64;
import java.util.Map;

public final class UserRoleHandler
{

	public static Map.Entry<Integer, UserType> IsUserPermited(String token, UserType requiredPerm)
		{

			var handler = new JWT();
			var data = handler.decodeJwt(token.substring(7));
			if ((Integer.parseInt(data.getClaim("permission").asString())) >= requiredPerm.getValue())
			{
				var permission = Integer.parseInt(data.getClaim("permission").asString());
				var userId = Integer.parseInt(data.getClaim("sub").asString());
				return new AbstractMap.SimpleEntry<>(userId, UserType.values()[permission]);

			}

			return null;
		}

	public static Map.Entry<Integer, UserType> GetUserIdAndRole(String token)
		{

            var handler = new JWT();
			var substring = token.substring(7);
            var data = handler.decodeJwt(substring);

            var permission = Integer.parseInt(data.getClaim("permission").asString());
            var userId = Integer.parseInt(data.getClaim("sub").asString());

            return new AbstractMap.SimpleEntry<>(userId, UserType.values()[permission]);

		}

}

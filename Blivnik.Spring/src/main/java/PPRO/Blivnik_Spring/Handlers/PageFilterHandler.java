package PPRO.Blivnik_Spring.Handlers;

import PPRO.Blivnik_Spring.Model.Enums.SortType;
import PPRO.Blivnik_Spring.Model.Objects.SongList;
import PPRO.Blivnik_Spring.Model.Objects.requests.songs.GetSongListRequest;
import PPRO.Blivnik_Spring.Model.Objects.responses.songs.GetSongListResponse;

import java.util.*;
import java.util.stream.Collectors;

public final class PageFilterHandler
{
	public static GetSongListResponse FilterByRequest(GetSongListResponse data, GetSongListRequest request)
	{

		var result = data.getItems().stream().filter(i -> i.getValidated() == request.getValidated() && i.getPublicable() == request.getPublicable());
        data.setItems(getOrderMethod(result.collect(Collectors.toList()), SortType.forValue(request.getSortBy())).stream().skip(request.getResultCount() * (request.getPage() - 1)).limit(request.getResultCount()).collect(Collectors.toList()));
		return data;
	}
	private static List<SongList> getOrderMethod(List<SongList> data, SortType sortBy)
	{

		switch (sortBy)
		{
			case Alphabetical:
				data.sort(Comparator.comparing(SongList::getName));
                return data;
            case ReverseAplhabecical:
                data.sort(Comparator.comparing(SongList::getName).reversed());
                return data;
			case ByPopularity:
                data.sort(Comparator.comparing(SongList::getViewCount).reversed());
				return data;
			case Random:
                Collections.shuffle(data);
				return data;
			default:
				return null;
		}
	}
}

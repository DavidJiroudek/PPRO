package PPRO.Blivnik_Spring.Handlers;

import java.sql.*;


public class SQLHandler {

//    public static Statement getStatement(){
//        final String connectionUrl =
//                "jdbc:sqlserver://yourserver.database.windows.net:1433;"
//                        + "database=AdventureWorks;"
//                        + "user=yourusername@yourserver;"
//                        + "password=yourpassword;"
//                        + "encrypt=true;"
//                        + "trustServerCertificate=false;"
//                        + "loginTimeout=30;";
//        try {
//            Connection connection = DriverManager.getConnection(connectionUrl);
//            return connection.createStatement();
//
//        }catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//    }
    public static Connection getConnection(){
        String connectionUrl =
                "jdbc:sqlserver://localhost:21433;"
                        + "database=Blivnik;"
                        + "user=blivnik_mssql;"
                        + "password=tbuW8%K8Y%@LkT-e;"
                        + "encrypt=true;"
                        + "allowMultiQueries=true;"
                        + "trustServerCertificate=true;";


        try {
            return DriverManager.getConnection(connectionUrl);

        }catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
}

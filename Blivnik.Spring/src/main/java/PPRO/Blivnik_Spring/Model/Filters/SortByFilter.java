package PPRO.Blivnik_Spring.Model.Filters;

public class SortByFilter {
    public String field;
    public ListSortDirection direction ;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public ListSortDirection getDirection() {
        return direction;
    }

    public void setDirection(ListSortDirection direction) {
        this.direction = direction;
    }
}

package PPRO.Blivnik_Spring.Model.Filters;

public enum ComparerOperator {
        LessOrEqual,
        GreaterOrEqual,
        Equals
    }


package PPRO.Blivnik_Spring.Model.Filters;

import PPRO.Blivnik_Spring.Model.Enums.PublicableType;

import java.util.Date;
import java.util.Map;
import java.util.Optional;
import java.util.Comparator;

public class SongFilter {
    private Integer creatorId;
    private String name;
    private String verseMatch;
    private Integer capo;
    private String authorMatch;
    private int[] authorIds;
    private int[] songIds;
    private Integer priority;
    private ComparerOperator priorityOperator;
    private PublicableType[] publicable;
    private Boolean validated;
    private int[] rythms;
    private Date createdDate;
    private ComparerOperator createdDateOperator;
    private Date modifiedDate;
    private ComparerOperator modifiedDateOperator;
    private String cultureCodeISO2Letter;
    private Map.Entry<String, ListSortDirection> sortBy;

}


class SortFieldDirection {
    private String field;
    private ListSortDirection direction;

    // Constructor, getters, and setters
    // ...
}



package PPRO.Blivnik_Spring.Model.Filters;

import PPRO.Blivnik_Spring.Model.Enums.AuthorType;

import java.util.Map;

public class AuthorFilter {

        public String name;
        public String detail;
        public Integer minimalPublicable;
        public AuthorType[] authorTypes;
        public int[] ids;
        public Map.Entry<String , ListSortDirection >[] sortBy;

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getDetail() {
                return detail;
        }

        public void setDetail(String detail) {
                this.detail = detail;
        }

        public Integer getMinimalPublicable() {
                return minimalPublicable;
        }

        public void setMinimalPublicable(Integer minimalPublicable) {
                this.minimalPublicable = minimalPublicable;
        }

        public AuthorType[] getAuthorTypes() {
                return authorTypes;
        }

        public void setAuthorTypes(AuthorType[] authorTypes) {
                this.authorTypes = authorTypes;
        }

        public int[] getIds() {
                return ids;
        }

        public void setIds(int[] ids) {
                this.ids = ids;
        }

        public Map.Entry<String, ListSortDirection>[] getSortBy() {
                return sortBy;
        }

        public void setSortBy(Map.Entry<String, ListSortDirection>[] sortBy) {
                this.sortBy = sortBy;
        }
}

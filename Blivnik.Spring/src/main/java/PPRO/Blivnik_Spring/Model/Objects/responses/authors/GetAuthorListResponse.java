package PPRO.Blivnik_Spring.Model.Objects.responses.authors;


public class GetAuthorListResponse
{
	private int authorId;
	public final int getAuthorId()
	{
		return authorId;
	}
	public final void setAuthorId(int value)
	{
		authorId = value;
	}
	private String name;
	public final String getName()
	{
		return name;
	}
	public final void setName(String value)
	{
		name = value;
	}
}

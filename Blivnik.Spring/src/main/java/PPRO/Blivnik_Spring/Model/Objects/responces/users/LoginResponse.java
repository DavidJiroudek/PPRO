package PPRO.Blivnik_Spring.Model.Objects.responces.users;


public class LoginResponse
{
	private int Id;
	public final int getId()
	{
		return Id;
	}
	public final void setId(int value)
	{
		Id = value;
	}
	private int Permission;
	public final int getPermission()
	{
		return Permission;
	}
	public final void setPermission(int value)
	{
		Permission = value;
	}
	private String Token;
	public final String getToken()
	{
		return Token;
	}
	public final void setToken(String value)
	{
		Token = value;
	}
}

package PPRO.Blivnik_Spring.Model.Objects.requests.authors;

public class GetAuthorListRequest {
    private int sortBy;
    private int resultCount;
    private int publicable;
    private int page;

    public int getSortBy() {
        return sortBy;
    }

    public void setSortBy(int sortBy) {
        this.sortBy = sortBy;
    }

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public int getPublicable() {
        return publicable;
    }

    public void setPublicable(int publicable) {
        this.publicable = publicable;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}

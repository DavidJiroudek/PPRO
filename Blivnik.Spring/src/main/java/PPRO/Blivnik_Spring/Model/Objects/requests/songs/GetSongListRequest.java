package PPRO.Blivnik_Spring.Model.Objects.requests.songs;


public class GetSongListRequest
{
	private int sortBy;
	private int resultCount;
	private int publicable;
	private int page;
	private boolean validated;

	public int getSortBy() {
		return sortBy;
	}

	public void setSortBy(int sortBy) {
		this.sortBy = sortBy;
	}

	public int getResultCount() {
		return resultCount;
	}

	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}

	public int getPublicable() {
		return publicable;
	}

	public void setPublicable(int publicable) {
		this.publicable = publicable;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public boolean getValidated() {
		return validated;
	}

	public void setValidated(boolean validated) {
		this.validated = validated;
	}

	public GetSongListRequest(int sortBy, int resultCount, int publicable, int page, boolean validated) {
		this.sortBy = sortBy;
		this.resultCount = resultCount;
		this.publicable = publicable;
		this.page = page;
		this.validated = validated;
	}

	public GetSongListRequest() {
	}
}

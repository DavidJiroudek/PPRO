package PPRO.Blivnik_Spring.Model.Objects.responses;

public class LoginTokenResponse {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginTokenResponse(String token) {
        this.token = token;
    }
}

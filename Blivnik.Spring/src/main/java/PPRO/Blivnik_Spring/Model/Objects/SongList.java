package PPRO.Blivnik_Spring.Model.Objects;

public class SongList {
    private int SongId;
    public int getSongId()
    {
        return SongId;
    }
    public void setSongId(int value)
    {
        SongId = value;
    }
    private String Name;
    public String getName()
    {
        return Name;
    }
    public void setName(String value)
    {
        Name = value;
    }
    private int Publicable;
    public int getPublicable()
    {
        return Publicable;
    }
    public void setPublicable(int value)
    {
        Publicable = value;
    }
    private boolean Validated;
    public boolean getValidated()
    {
        return Validated;
    }
    public void setValidated(boolean value)
    {
        Validated = value;
    }
    private int ViewCount;
    public int getViewCount()
    {
        return ViewCount;
    }
    public void setViewCount(int value)
    {
        ViewCount = value;
    }

    public SongList(int songId, String name, int publicable, boolean validated, int viewCount) {
        SongId = songId;
        Name = name;
        Publicable = publicable;
        Validated = validated;
        ViewCount = viewCount;
    }

    public SongList() {
    }
}

//package PPRO.Blivnik_Spring.Model.Objects.responses.search;
//
//import Blinvik.Search.ElasticSearch.Models.*;
//import blivnik.net.api.models.objects.*;
//
//public class GetFulltextResponse<T>
//{
//	private T Item;
//	public final T getItem()
//	{
//		return Item;
//	}
//	public final void setItem(T value)
//	{
//		Item = value;
//	}
//	private Double Score = null;
//	public final Double getScore()
//	{
//		return Score;
//	}
//	public final void setScore(Double value)
//	{
//		Score = value;
//	}
//	private String[] Highlight;
//	public final String[] getHighlight()
//	{
//		return Highlight;
//	}
//	public final void setHighlight(String[] value)
//	{
//		Highlight = value;
//	}
//	private int MaxPage;
//	public final int getMaxPage()
//	{
//		return MaxPage;
//	}
//	public final void setMaxPage(int value)
//	{
//		MaxPage = value;
//	}
//
//
//	public GetFulltextResponse((T item, Double score, String[] highlight) data)
//	{
//		this(data, 0);
//	}
//
////C# TO JAVA CONVERTER TASK: Tuple variables are not converted by C# to Java Converter:
////C# TO JAVA CONVERTER NOTE: Java does not support optional parameters. Overloaded method(s) are created above:
////ORIGINAL LINE: public GetFulltextResponse((T item, System.Nullable<double> score, string[] highlight) data, int maxPage = 0)
//	public GetFulltextResponse((T item, Double score, String[] highlight) data, int maxPage)
//	{
//		setItem(data.item);
//		setScore(data.score);
//		setHighlight(data.highlight);
//		setMaxPage(maxPage);
//	}
//}

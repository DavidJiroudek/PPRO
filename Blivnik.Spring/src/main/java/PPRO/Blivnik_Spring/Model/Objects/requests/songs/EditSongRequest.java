package PPRO.Blivnik_Spring.Model.Objects.requests.songs;

import PPRO.Blivnik_Spring.Model.Objects.Author;
import PPRO.Blivnik_Spring.Model.Objects.Song;

import java.util.AbstractMap;
import java.util.List;
import java.util.stream.Collectors;

public class EditSongRequest
{
	private int SongId;
	public final int getSongId()
	{
		return SongId;
	}
	public final void setSongId(int value)
	{
		SongId = value;
	}
	private String SongName;
	public final String getSongName()
	{
		return SongName;
	}
	public final void setSongName(String value)
	{
		SongName = value;
	}
	private int Creator;
	public final int getCreator()
	{
		return Creator;
	}
	public final void setCreator(int value)
	{
		Creator = value;
	}
	private int Publicable;
	public final int getPublicable()
	{
		return Publicable;
	}
	public final void setPublicable(int value)
	{
		Publicable = value;
	}
	private int Capo;
	public final int getCapo()
	{
		return Capo;
	}
	public final void setCapo(int value)
	{
		Capo = value;
	}
	private String Url;
	public final String getUrl()
	{
		return Url;
	}
	public final void setUrl(String value)
	{
		Url = value;
	}
	private List<Author> Authors;
	public final List<Author> getAuthors()
	{
		return Authors;
	}
	public final void setAuthors(List<Author> value)
	{
		Authors = value;
	}
	private int Priority;
	public final int getPriority()
	{
		return Priority;
	}
	public final void setPriority(int value)
	{
		Priority = value;
	}
	private List<Integer> Rythms;
	public final List<Integer> getRythms()
	{
		return Rythms;
	}
	public final void setRythms(List<Integer> value)
	{
		Rythms = value;
	}
	private String Text;
	public final String getText()
	{
		return Text;
	}
	public final void setText(String value)
	{
		Text = value;
	}
	private boolean Reprint;
	public final boolean getReprint()
	{
		return Reprint;
	}
	public final void setReprint(boolean value)
	{
		Reprint = value;
	}

    public final Song ToSong(int userId)
    {
        return new Song(this.getSongId(), this.getSongName(), this.getPublicable(), this.getPriority(), this.getCapo(), this.getUrl(), this.getReprint(), this.getCreator(), userId,this.getText(), getAuthors().stream().map(i -> new AbstractMap.SimpleEntry<>(i.getName(), i.getType())).collect(Collectors.toList()), this.getRythms());
    }
}

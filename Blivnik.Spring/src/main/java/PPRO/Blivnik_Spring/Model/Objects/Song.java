package PPRO.Blivnik_Spring.Model.Objects;

import java.time.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Song
{
	private Integer SongId = null;
	public final Integer getSongId()
	{
		return SongId;
	}
	public final void setSongId(Integer value)
	{
		SongId = value;
	}
	private Date Date = null;
	public final Date getDate()
	{
		return Date;
	}
	public final void setDate(Date value)
	{
		Date = value;
	}
	private String SongName;
	public final String getSongName()
	{
		return SongName;
	}
	public final void setSongName(String value)
	{
		SongName = value;
	}
	private Integer Publicable = null;
	public final Integer getPublicable()
	{
		return Publicable;
	}
	public final void setPublicable(Integer value)
	{
		Publicable = value;
	}
	private Integer Priority = null;
	public final Integer getPriority()
	{
		return Priority;
	}
	public final void setPriority(Integer value)
	{
		Priority = value;
	}
	private Integer Capo = null;
	public final Integer getCapo()
	{
		return Capo;
	}
	public final void setCapo(Integer value)
	{
		Capo = value;
	}
	private Boolean Validated = null;
	public final Boolean getValidated()
	{
		return Validated;
	}
	public final void setValidated(Boolean value)
	{
		Validated = value;
	}
//C# TO JAVA CONVERTER WARNING: Nullable reference types have no equivalent in Java:
//ORIGINAL LINE: public string? Url {get;set;}
	private String Url;
	public final String getUrl()
	{
		return Url;
	}
	public final void setUrl(String value)
	{
		Url = value;
	}
	private Boolean Reprint = null;
	public final Boolean getReprint()
	{
		return Reprint;
	}
	public final void setReprint(Boolean value)
	{
		Reprint = value;
	}
	private Integer Creator = null;
	public final Integer getCreator()
	{
		return Creator;
	}
	public final void setCreator(Integer value)
	{
		Creator = value;
	}
	private Integer Editor = null;
	public final Integer getEditor()
	{
		return Editor;
	}
	public final void setEditor(Integer value)
	{
		Editor = value;
	}
//C# TO JAVA CONVERTER WARNING: Nullable reference types have no equivalent in Java:
//ORIGINAL LINE: public string? Text {get;set;}
	private String Text;
	public final String getText()
	{
		return Text;
	}
	public final void setText(String value)
	{
		Text = value;
	}
	public List<Map.Entry<String, Integer>> Authors;

	public List<Map.Entry<String, Integer>> getAuthors() {
		return Authors;
	}

	public void setAuthors(List<Map.Entry<String, Integer>> authors) {
		Authors = authors;
	}

	//C# TO JAVA CONVERTER WARNING: Nullable reference types have no equivalent in Java:
//ORIGINAL LINE: public IEnumerable<int>? Rythms {get;set;}
	private List<Integer> Rythms;

	public List<Integer> getRythms() {
		return Rythms;
	}
	public void setRythms(List<Integer> rythms) {
		Rythms = rythms;
	}

	public Song(String songName, Integer publicable, Integer priority, Integer capo, String url, String text, List<Map.Entry<String, Integer>> authors, List<Integer> rythms, Integer creator) {

		SongName = songName;
		Publicable = publicable;
		Priority = priority;
		Capo = capo;
		Url = url;
		Text = text;
		Authors = authors;
		Rythms = rythms;
		Creator = creator;

	}

	public Song(Integer songId, String songName, Integer publicable, Integer priority, Integer capo, String url, Boolean reprint, Integer creator, Integer editor, String text, List<Map.Entry<String, Integer>> authors, List<Integer> rythms) {
		SongId = songId;
		SongName = songName;
		Publicable = publicable;
		Priority = priority;
		Capo = capo;
		Url = url;
		Reprint = reprint;
		Creator = creator;
		Editor = editor;
		Text = text;
		Authors = authors;
		Rythms = rythms;
	}

	public Song(){}
}

package PPRO.Blivnik_Spring.Model.Objects;

public class Author
{
	public Author()
	{
	}

	public Author(String name, int type)
	{
		setName(name);
		setType(type);
	}

	private String name;
	public final String getName()
	{
		return name;
	}
	public final void setName(String value)
	{
		name = value;
	}
	private int type;
	public final int getType()
	{
		return type;
	}
	public final void setType(int value)
	{
		type = value;
	}

}

package PPRO.Blivnik_Spring.Model.Objects.requests.users;


public class ChangeUserPermissionRequest
{
	private int Id;
	public final int getId()
	{
		return Id;
	}
	public final void setId(int value)
	{
		Id = value;
	}
	private int Permission;
	public final int getPermission()
	{
		return Permission;
	}
	public final void setPermission(int value)
	{
		Permission = value;
	}

}

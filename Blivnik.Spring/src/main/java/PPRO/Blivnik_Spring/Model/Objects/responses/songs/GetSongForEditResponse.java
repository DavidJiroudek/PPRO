package PPRO.Blivnik_Spring.Model.Objects.responses.songs;

import PPRO.Blivnik_Spring.Model.Objects.Author;
import PPRO.Blivnik_Spring.Model.Objects.Song;

import java.util.List;
import java.util.stream.Collectors;

public class GetSongForEditResponse
{
	private int SongId;
	public final int getSongId()
	{
		return SongId;
	}
	public final void setSongId(int value)
	{
		SongId = value;
	}
	private String SongName;
	public final String getSongName()
	{
		return SongName;
	}
	public final void setSongName(String value)
	{
		SongName = value;
	}
	private int Creator;
	public final int getCreator()
	{
		return Creator;
	}
	public final void setCreator(int value)
	{
		Creator = value;
	}
	private int Publicable;
	public final int getPublicable()
	{
		return Publicable;
	}
	public final void setPublicable(int value)
	{
		Publicable = value;
	}
	private int Priority;
	public final int getPriority()
	{
		return Priority;
	}
	public final void setPriority(int value)
	{
		Priority = value;
	}
	private int Capo;
	public final int getCapo()
	{
		return Capo;
	}
	public final void setCapo(int value)
	{
		Capo = value;
	}
	private String Url;
	public final String getUrl()
	{
		return Url;
	}
	public final void setUrl(String value)
	{
		Url = value;
	}
	private String Text;
	public final String getText()
	{
		return Text;
	}
	public final void setText(String value)
	{
		Text = value;
	}
	private List<Author> Authors;
	public final List<Author> getAuthors()
	{
		return Authors;
	}
	public final void setAuthors(List<Author> value)
	{
		Authors = value;
	}
	private List<Integer> Rythms;
	public final List<Integer> getRythms()
	{
		return Rythms;
	}
	public final void setRythms(List<Integer> value)
	{
		Rythms = value;
	}

	public GetSongForEditResponse(Song data)
	{
        setSongId(data.getSongId());
        setSongName(data.getSongName());
        setCreator(data.getCreator());
        setPublicable(data.getPublicable());
        setPriority(data.getPriority());
        setCapo(data.getCapo());
        setUrl(data.getUrl());
        setText(data.getText());
        setAuthors(data.Authors.stream().map(i -> new Author(i.getKey(), i.getValue())).collect(Collectors.toList()));
        setRythms(data.getRythms());

	}
}

package PPRO.Blivnik_Spring.Model.Objects.requests.songs;

import PPRO.Blivnik_Spring.Model.Objects.Author;
import PPRO.Blivnik_Spring.Model.Objects.Song;

import java.util.AbstractMap;
import java.util.List;
import java.util.stream.Collectors;

public class SaveSongRequest
{
	private String songName;
	public final String getSongName()
	{
		return songName;
	}
	public final void setSongName(String value)
	{
		songName = value;
	}
	private int publicable;
	public final int getPublicable()
	{
		return publicable;
	}
	public final void setPublicable(int value)
	{
		publicable = value;
	}
	private int priority;
	public final int getPriority()
	{
		return priority;
	}
	public final void setPriority(int value)
	{
		priority = value;
	}
	private int capo;
	public final int getCapo()
	{
		return capo;
	}
	public final void setCapo(int value)
	{
		capo = value;
	}
	private String url;
	public final String getUrl()
	{
		return url;
	}
	public final void setUrl(String value)
	{
		url = value;
	}
	private List<Author> authors;
	public final List<Author> getAuthors()
	{
		return authors;
	}
	public final void setAuthors(List<Author> value)
	{
		authors = value;
	}

	private List<Integer> rythms;
	public final List<Integer> getRythms()
	{
		return rythms;
	}
	public final void setRythms(List<Integer> value)
	{
		rythms = value;
	}
	private String text;
	public final String getText()
	{
		return text;
	}
	public final void setText(String value)
	{
		text = value;
	}


	public final Song ToSong(int userId)
	{
	    return new Song(this.getSongName(), this.getPublicable(), this.getPriority(), this.getCapo(), this.getUrl(), this.getText(), getAuthors().stream().map(i -> new AbstractMap.SimpleEntry<>(i.getName(), i.getType())).collect(Collectors.toList()), this.getRythms(), userId);
	}
}

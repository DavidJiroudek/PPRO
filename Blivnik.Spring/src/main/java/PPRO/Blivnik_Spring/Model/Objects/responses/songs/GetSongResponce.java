package PPRO.Blivnik_Spring.Model.Objects.responses.songs;

import PPRO.Blivnik_Spring.Model.Objects.Author;
import PPRO.Blivnik_Spring.Model.Objects.Song;

import java.time.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class GetSongResponce
{
	private int SongId;
	public final int getSongId()
	{
		return SongId;
	}
	public final void setSongId(int value)
	{
		SongId = value;
	}
	private Date Date;
	public final Date getDate()
	{
		return Date;
	}
	public final void setDate(Date value)
	{
		Date = value;
	}
	private String SongName;
	public final String getSongName()
	{
		return SongName;
	}
	public final void setSongName(String value)
	{
		SongName = value;
	}
	private boolean Validated;
	public final boolean getValidated()
	{
		return Validated;
	}
	public final void setValidated(boolean value)
	{
		Validated = value;
	}
	private int Capo;
	public final int getCapo()
	{
		return Capo;
	}
	public final void setCapo(int value)
	{
		Capo = value;
	}
	private String Text;
	public final String getText()
	{
		return Text;
	}
	public final void setText(String value)
	{
		Text = value;
	}
	private List<Author> Authors;
	public final List<Author> getAuthors()
	{
		return Authors;
	}
	public final void setAuthors(List<Author> value)
	{
		Authors = value;
	}
	private List<Integer> Rythms;
	public final List<Integer> getRythms()
	{
		return Rythms;
	}
	public final void setRythms(List<Integer> value)
	{
		Rythms = value;
	}



	public GetSongResponce(Song data)
	{
		setSongId(data.getSongId());
		setDate(data.getDate());
		setSongName(data.getSongName());
		setValidated(data.getValidated());
		setCapo(data.getCapo().intValue());
		setText(data.getText());
		setAuthors(data.Authors.stream().map(i -> new Author(i.getKey(), i.getValue())).collect(Collectors.toList()));
		setRythms(data.getRythms());

	}

	public GetSongResponce()
	{
	}
}

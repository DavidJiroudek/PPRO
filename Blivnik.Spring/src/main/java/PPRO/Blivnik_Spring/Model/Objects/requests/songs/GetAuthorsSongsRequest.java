package PPRO.Blivnik_Spring.Model.Objects.requests.songs;


public class GetAuthorsSongsRequest
{
	private int Id;
	public final int getId()
	{
		return Id;
	}
	public final void setId(int value)
	{
		Id = value;
	}
	private int SortBy;
	public final int getSortBy()
	{
		return SortBy;
	}
	public final void setSortBy(int value)
	{
		SortBy = value;
	}
	private int ResultCount;
	public final int getResultCount()
	{
		return ResultCount;
	}
	public final void setResultCount(int value)
	{
		ResultCount = value;
	}
	private int Page;
	public final int getPage()
	{
		return Page;
	}
	public final void setPage(int value)
	{
		Page = value;
	}

}

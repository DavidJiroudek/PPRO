package PPRO.Blivnik_Spring.Model.Objects.requests.users;


import PPRO.Blivnik_Spring.Model.Enums.OAuthProvider;

public class LoginRequest
{
	private String appType;
	public final String getAppType()
	{
		return appType;
	}
	public final void setAppType(String value)
	{
		appType = value;
	}

	private int OauthProvider;

	public int getOauthProvider() {
		return OauthProvider;
	}

	public void setOauthProvider(int oauthProvider) {
		OauthProvider = oauthProvider;
	}

	private String token;
	public final String getToken()
	{
		return token;
	}
	public final void setToken(String value)
	{
		token = value;
	}


}

package PPRO.Blivnik_Spring.Model.Objects.requests.search;


public class GetFulltextRequest
{
//C# TO JAVA CONVERTER TASK: Java annotations will not correspond to .NET attributes:
//ORIGINAL LINE: [JsonPropertyName("fulltext")] public string Fulltext {get;set;}
	private String Fulltext;
	public final String getFulltext()
	{
		return Fulltext;
	}
	public final void setFulltext(String value)
	{
		Fulltext = value;
	}
}

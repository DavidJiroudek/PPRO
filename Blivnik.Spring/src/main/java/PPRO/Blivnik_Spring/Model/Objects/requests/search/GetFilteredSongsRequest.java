package PPRO.Blivnik_Spring.Model.Objects.requests.search;
import PPRO.Blivnik_Spring.Model.Enums.PublicableType;
import PPRO.Blivnik_Spring.Model.Filters.SongFilter;
import PPRO.Blivnik_Spring.Model.Filters.SortByFilter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;

public class GetFilteredSongsRequest {
    @JsonProperty("publicable")
    private PublicableType[] publicable;
    private Boolean validated;
    @JsonProperty("sortBy")
    private SortByFilter[] sortBy;
    @JsonProperty("authorIds")
    private int[] authorIds;
    @JsonProperty("resultCount")
    private int resultCount;
    @JsonProperty("page")
    private int page;

    public PublicableType[] getPublicable() {
        return publicable;
    }

    public void setPublicable(PublicableType[] publicable) {
        this.publicable = publicable;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public SortByFilter[] getSortBy() {
        return sortBy;
    }

    public void setSortBy(SortByFilter[] sortBy) {
        this.sortBy = sortBy;
    }

    public int[] getAuthorIds() {
        return authorIds;
    }

    public void setAuthorIds(int[] authorIds) {
        this.authorIds = authorIds;
    }

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

//    public SongFilter ToAuthorFilter() {
//        return new SongFilter() {
//        publicable = publicable,
//        validated,
//        authorIds,
//        Arrays.stream(sortBy).map(i -> new AbstractMap.SimpleEntry<>(i.getField(), i.getDirection())).toArray(Map.Entry[]::new);
//        }
//    }
//
//    public SongFilter toAuthorFilter(int userId) {
//        return new SongFilter(
//                publicable,
//                validated,
//                authorIds,
//                Arrays.stream(sortBy).map(i -> new AbstractMap.SimpleEntry<>(i.getField(), i.getDirection())).toArray(Map.Entry[]::new),
//                userId
//        );
//    }
}


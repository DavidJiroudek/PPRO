package PPRO.Blivnik_Spring.Model.Objects.responses.songs;


import PPRO.Blivnik_Spring.Model.Objects.SongList;

import java.util.ArrayList;
import java.util.List;

public class GetSongListResponse
{
	private List<SongList> items;
	private int maxPage;

	public List<SongList> getItems() {
		return items;
	}

	public void setItems(List<SongList> items) {
		this.items = items;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(int maxPage) {
		this.maxPage = maxPage;
	}

	public GetSongListResponse(List<SongList> items, int maxPage) {
		this.items = items;
		this.maxPage = maxPage;
	}
	public GetSongListResponse() {
		this.items = new ArrayList<>();
		this.maxPage = 1;
	}
}

package PPRO.Blivnik_Spring.Model.Objects.responses.songs;


public class GetAuthorsSongListResponse
{
	private int SongId;
	public final int getSongId()
	{
		return SongId;
	}
	public final void setSongId(int value)
	{
		SongId = value;
	}
	private String Name;
	public final String getName()
	{
		return Name;
	}
	public final void setName(String value)
	{
		Name = value;
	}
	private int Publicable;
	public final int getPublicable()
	{
		return Publicable;
	}
	public final void setPublicable(int value)
	{
		Publicable = value;
	}
	private boolean Validated;
	public final boolean getValidated()
	{
		return Validated;
	}
	public final void setValidated(boolean value)
	{
		Validated = value;
	}

}

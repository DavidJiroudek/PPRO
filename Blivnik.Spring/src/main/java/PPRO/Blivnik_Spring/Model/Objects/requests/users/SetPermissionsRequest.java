package PPRO.Blivnik_Spring.Model.Objects.requests.users;


public class SetPermissionsRequest
{
	private int Id;
	public final int getId()
	{
		return Id;
	}
	public final void setId(int value)
	{
		Id = value;
	}
	private String Permission;
	public final String getPermission()
	{
		return Permission;
	}
	public final void setPermission(String value)
	{
		Permission = value;
	}
}

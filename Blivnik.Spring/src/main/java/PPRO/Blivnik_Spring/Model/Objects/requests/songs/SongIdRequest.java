package PPRO.Blivnik_Spring.Model.Objects.requests.songs;


public class SongIdRequest
{
	private int songId;
	public final int getSongId()
	{
		return songId;
	}
	public final void setSongId(int value)
	{
		songId = value;
	}
}

package PPRO.Blivnik_Spring.Model.Objects.responses.songs;


public class GetUserSongListResponse
{
	private int SongId;
	public final int getSongId()
	{
		return SongId;
	}
	public final void setSongId(int value)
	{
		SongId = value;
	}
	private String Name;
	public final String getName()
	{
		return Name;
	}
	public final void setName(String value)
	{
		Name = value;
	}
}

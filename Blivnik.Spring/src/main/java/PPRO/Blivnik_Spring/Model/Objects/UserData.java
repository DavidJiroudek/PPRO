package PPRO.Blivnik_Spring.Model.Objects;


public class UserData
{
	private Integer Id = null;
	public final Integer getId()
	{
		return Id;
	}
	public final void setId(Integer value)
	{
		Id = value;
	}
	private String Name;
	public final String getName()
	{
		return Name;
	}
	public final void setName(String value)
	{
		Name = value;
	}
	private String Email;
	public final String getEmail()
	{
		return Email;
	}
	public final void setEmail(String value)
	{
		Email = value;
	}
	private String Picture;
	public final String getPicture()
	{
		return Picture;
	}
	public final void setPicture(String value)
	{
		Picture = value;
	}
	private int Permission;
	public final int getPermission()
	{
		return Permission;
	}
	public final void setPermission(int value)
	{
		Permission = value;
	}

	public UserData(int id, String name, String email, String picture, int permission)
	{
		setId(id);
		setName(name);
		setEmail(email);
		setPicture(picture);
		setPermission(permission);
	}
	public UserData(String name, String email, String picture, int permission)
	{
		setId(null);
		setName(name);
		setEmail(email);
		setPicture(picture);
		setPermission(permission);
	}

	public UserData()
	{
	}
}

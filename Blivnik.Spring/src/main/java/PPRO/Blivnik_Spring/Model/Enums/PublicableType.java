package PPRO.Blivnik_Spring.Model.Enums;

public enum PublicableType {
        Publicable,
        Unpublicable,
        Invisible,
        Forbidden;

    public static final int SIZE = Integer.SIZE;

    public int getValue()
    {
        return this.ordinal();
    }

    public static PublicableType forValue(int value)
    {
        return values()[value];
    }
}

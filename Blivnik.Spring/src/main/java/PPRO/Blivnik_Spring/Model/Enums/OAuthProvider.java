package PPRO.Blivnik_Spring.Model.Enums;

public enum OAuthProvider {
    Google,
    Facebook
}

package PPRO.Blivnik_Spring.Model.Enums;

public enum SortType
{
	Alphabetical,
	ReverseAplhabecical,
	ByPopularity,
	Random;

	public static final int SIZE = Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static SortType forValue(int value)
	{
		return values()[value];
	}
}

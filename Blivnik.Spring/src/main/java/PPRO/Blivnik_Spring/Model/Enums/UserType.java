package PPRO.Blivnik_Spring.Model.Enums;

public enum UserType
{
	Visitor,
	Creator,
	Moderator,
	Admin;

	public static final int SIZE = Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static UserType forValue(int value)
	{
		return values()[value];
	}
}

package PPRO.Blivnik_Spring.Model.Enums;

public enum AuthorType
{
	MusicAuthor,
	TextAuthor,
	Interpret;

	public static final int SIZE = Integer.SIZE;

	public int getValue()
	{
		return this.ordinal();
	}

	public static AuthorType forValue(int value)
	{
		return values()[value];
	}
}

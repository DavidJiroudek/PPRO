﻿using Autofac;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Blivnik.Caching.Test
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class CachingServiceTests : CachingTestBase
    {
        [Test]
        public void KeyedCacheGetter()
        {
            Assert.True(_cache != null && _cache.CacheConfiguration.CacheType == CacheType.TestCache);
        }

        [Test]
        public void DefaultCacheTest()
        {
            var cache = Container.Resolve<IKeyValueStore>();
            Assert.True(cache != null && cache.CacheConfiguration.CacheType == CacheType.SongCache);
        }

        [Test]
        public void GetSetValueTest()
        {
            var cacheName = "test0";
            var cacheValue = "blablabla";
            _cache.SetValue(cacheName, cacheValue);
            var result = _cache.GetValue<string>(cacheName);
            Assert.AreEqual(cacheValue, result);
        }

        [Test]
        public void ExpireTest()
        {
            var cacheName = "test1";
            var cacheValue = "blablabla";
            _cache.SetValue(cacheName, cacheValue);
            //after duration plus 100ms cache option has to be expired
            Thread.Sleep(_cache.CacheConfiguration.Duration * 1000 + 100);
            var result = _cache.GetValue<string>(cacheName);
            Assert.Null(result);
        }
        [Test]
        public void SetMultipleValuesTest()
        {
            var data = new List<Tuple<string, string>>();
            data.Add(new Tuple<string, string>("test2.0", "blablabla0"));
            data.Add(new Tuple<string, string>("test2.1", "blablabla1"));
            data.Add(new Tuple<string, string>("test2.2", "blablabla2"));
            _cache.SetMultipleValues(data);
            var result0 = _cache.GetValue<string>("test2.0");
            var result1 = _cache.GetValue<string>("test2.1");
            var result2 = _cache.GetValue<string>("test2.2");
            Assert.AreEqual("blablabla0", result0);
            Assert.AreEqual("blablabla1", result1);
            Assert.AreEqual("blablabla2", result2);

        }
        [Test]
        public void GetMultipleValuesTest()
        {
            var data = new List<Tuple<string, string>>();
            var keys = new List<string> { "test3.0", "test3.1", "test3.2" };
            data.Add(new Tuple<string, string>("test3.0", "blablabla0"));
            data.Add(new Tuple<string, string>("test3.1", "blablabla1"));
            data.Add(new Tuple<string, string>("test3.2", "blablabla2"));
            _cache.SetMultipleValues(data);
            var result = _cache.GetMultipleValues<string>(keys);
            Assert.AreEqual("blablabla0", result["test3.0"]);
            Assert.AreEqual("blablabla1", result["test3.1"]);
            Assert.AreEqual("blablabla2", result["test3.2"]);


        }
        [Test]
        public void GetMultipleValuesAsListTest()
        {
            var data = new List<Tuple<string, string>>();
            var keys = new List<string> { "test4.0", "test4.1", "test4.2" };
            data.Add(new Tuple<string, string>("test4.0", "blablabla0"));
            data.Add(new Tuple<string, string>("test4.1", "blablabla1"));
            data.Add(new Tuple<string, string>("test4.2", "blablabla2"));
            _cache.SetMultipleValues(data);
            var result = _cache.GetMultipleValuesAsList<string>(keys);
            Assert.AreEqual("blablabla0", result[0]);
            Assert.AreEqual("blablabla1", result[1]);
            Assert.AreEqual("blablabla2", result[2]);


        }
        [Test]
        public void DeleteValueTest()
        {
            var cacheName = "test5";
            var cacheValue = "blablabla";
            _cache.SetValue(cacheName, cacheValue);
            _cache.DeleteValue(cacheName);
            var result = _cache.GetValue<string>(cacheName);
            Assert.Null(result);


        }
        [Test]
        public void TryGetCachedDataTest()
        {
            var cacheName = "test65";
            var cacheValue = "saghathaetrh";
            _cache.SetValue(cacheName, cacheValue);
            var successResult = _cache.TryGetCachedData<string>(TryGetData, cacheName);
            Assert.AreEqual(cacheValue, successResult);
            var failResult = _cache.TryGetCachedData<string>(TryGetData, "randomstring");
            Assert.AreEqual(TryGetData(), failResult);


        }
        private string TryGetData()
        {
            return "Bagr sedi na strome a plete kakao";
        }


    }
}

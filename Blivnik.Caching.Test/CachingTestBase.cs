﻿using Autofac;
using NUnit.Framework;
using System;

namespace Blivnik.Caching.Test
{
    public class CachingTestBase
    {
        public IContainer Container;


        protected IKeyValueStore _cache;
        protected int _waitTime;
        [OneTimeSetUp]
        public void Setup() {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new CachingModule());
            Container = builder.Build();


            _cache = Container.ResolveKeyed<IKeyValueStore>(CacheType.TestCache);
            _waitTime = 100;

        }


        //public string DataGenerator()
    }
}

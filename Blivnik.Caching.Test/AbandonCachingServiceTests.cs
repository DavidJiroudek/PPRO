﻿using Autofac;
using System.Threading;
using NUnit.Framework;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;

namespace Blivnik.Caching.Test
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class AbandonCachingServiceTests : CachingTestBase
    {

        [Test]
        public void DeleteValueAbandonTaskTest() {
            var cacheName = "testAbandon0";
            var cacheValue = "blablabla";
            _cache.SetValue(cacheName, cacheValue);
            Thread.Sleep(_waitTime);
            _cache.DeleteValueAbandonTask(cacheName);
            Thread.Sleep(_waitTime);
            var result = _cache.GetValue<string>(cacheName);
            Assert.Null(result);
        }
        [Test]
        public void SetMultipleValuesAbandonTaskTest(){
            var data = new List<Tuple<string, string>>();
            data.Add(new Tuple<string, string>("testAbandon1.0", "blablabla0"));
            data.Add(new Tuple<string, string>("testAbandon1.1", "blablabla1"));
            data.Add(new Tuple<string, string>("testAbandon1.2", "blablabla2"));
            _cache.SetMultipleValuesAbandonTask(data);
            Thread.Sleep(_waitTime);
            var result0 = _cache.GetValue<string>("testAbandon1.0");
            var result1 = _cache.GetValue<string>("testAbandon1.1");
            var result2 = _cache.GetValue<string>("testAbandon1.2");
            Assert.AreEqual("blablabla0", result0);
            Assert.AreEqual("blablabla1", result1);
            Assert.AreEqual("blablabla2", result2);
        }
        [Test]
        public void SetValueAbandonTaskTest()
        {
            var cacheName = "testAbandon2";
            var cacheValue = "blablabla";
            _cache.SetValueAbandonTask(cacheName, cacheValue);
            Thread.Sleep(_waitTime);
            var result = _cache.GetValue<string>(cacheName);
            Assert.AreEqual(cacheValue, result);
        }

        [Test]
        public async Task GetHashedAbandonTaskTest()
        {
            var data = new List<Tuple<string, string>>();
            var key = "test7";
            data.Add(new Tuple<string, string>("test7.0", "blablabla0"));
            data.Add(new Tuple<string, string>("test7.1", "blablabla1"));
            data.Add(new Tuple<string, string>("test7.2", "blablabla2"));
            _cache.SetHashedAbandonTask(key, data);
            Thread.Sleep(_waitTime);
            var result = await _cache.GetHashedAsync<Tuple<string, string>>(key);
            Assert.NotNull(result);

        }
    }
}

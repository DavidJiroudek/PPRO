﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Blivnik.Caching.Test
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class AsyncCachingTests : CachingTestBase
    {
        [Test]
        public async Task GetSetValueAsyncTest()
        {
            var cacheName = "testAsync0";
            var cacheValue = "blablabla";
            await _cache.SetValueAsync(cacheName, cacheValue);
            var result = await _cache.GetValueAsync<string>(cacheName);
            Assert.AreEqual(cacheValue, result);

        }
        [Test]
        public async Task SetMultipleValuesAsyncTest()
        {
            var data = new List<Tuple<string, string>>();
            data.Add(new Tuple<string, string>("testAsync1.0", "blablabla0"));
            data.Add(new Tuple<string, string>("testAsync1.1", "blablabla1"));
            data.Add(new Tuple<string, string>("testAsync1.2", "blablabla2"));
            await _cache.SetMultipleValuesAsync(data);
            var result0 = _cache.GetValue<string>("testAsync1.0");
            var result1 = _cache.GetValue<string>("testAsync1.1");
            var result2 = _cache.GetValue<string>("testAsync1.2");
            Assert.AreEqual("blablabla0", result0);
            Assert.AreEqual("blablabla1", result1);
            Assert.AreEqual("blablabla2", result2);
        }
        [Test]
        public async Task GetMultipleValuesAsyncTest()
        {
            var data = new List<Tuple<string, string>>();
            var keys = new List<string> { "testAsync2.0", "testAsync2.1", "testAsync2.2" };
            data.Add(new Tuple<string, string>("testAsync2.0", "blablabla0"));
            data.Add(new Tuple<string, string>("testAsync2.1", "blablabla1"));
            data.Add(new Tuple<string, string>("testAsync2.2", "blablabla2"));
            await _cache.SetMultipleValuesAsync(data);
            var result = await _cache.GetMultipleValuesAsync<string>(keys);
            Assert.AreEqual("blablabla0", result["testAsync2.0"]);
            Assert.AreEqual("blablabla1", result["testAsync2.1"]);
            Assert.AreEqual("blablabla2", result["testAsync2.2"]);
        }
        [Test]
        public async Task GetMultipleValuesAsListAsyncTest()
        {
            var data = new List<Tuple<string, string>>();
            var keys = new List<string> { "testAsync3.0", "testAsync3.1", "testAsync3.2" };
            data.Add(new Tuple<string, string>("testAsync3.0", "blablabla0"));
            data.Add(new Tuple<string, string>("testAsync3.1", "blablabla1"));
            data.Add(new Tuple<string, string>("testAsync3.2", "blablabla2"));
            await _cache.SetMultipleValuesAsync(data);
            var result = await _cache.GetMultipleValuesAsListAsync<string>(keys);
            Assert.AreEqual("blablabla0", result[0]);
            Assert.AreEqual("blablabla1", result[1]);
            Assert.AreEqual("blablabla2", result[2]);
        }
        [Test]
        public async Task DeleteValueAsyncTest()
        {
            var cacheName = "testAsync4";
            var cacheValue = "blablabla";
            _cache.SetValue(cacheName, cacheValue);
            await _cache.DeleteValueAsync(cacheName);
            var result = _cache.GetValue<string>(cacheName);
            Assert.Null(result);
        }
        [Test]
        public async Task GetHashedTest()
        {
            var data = new List<Tuple<string, string>>();
            var key = "test7";
            data.Add(new Tuple<string, string>("test7.0", "blablabla0"));
            data.Add(new Tuple<string, string>("test7.1", "blablabla1"));
            data.Add(new Tuple<string, string>("test7.2", "blablabla2"));
            await _cache.SetHashedAsync(key, data);
            var result = await _cache.GetHashedAsync<Tuple<string, string>>(key);
            Assert.NotNull(result);

        }

        [Test]
        public async Task GetHashedDeduplicatingTest()
        {
            var data = new List<Tuple<string, string>>();
            var key = "test7";
            data.Add(new Tuple<string, string>("test7.0", "blablabla0"));
            data.Add(new Tuple<string, string>("test7.1", "blablabla1"));
            data.Add(new Tuple<string, string>("test7.2", "blablabla2"));
            await _cache.SetHashedAsync(key, data);
             key = "test8";
            data.Add(new Tuple<string, string>("test7.5", "blablabla0"));
            data.Add(new Tuple<string, string>("test7.1", "blablabla3"));
            data.Add(new Tuple<string, string>("test7.2", "blablabla2"));
            await _cache.SetHashedAsync(key, data);
            key = "test9";
            data.Add(new Tuple<string, string>("test7.5", "blablabla0"));
            data.Add(new Tuple<string, string>("test7.1", "blablabla3"));
            data.Add(new Tuple<string, string>("test7.2", "blablabla2"));
            data.Add(new Tuple<string, string>("test7.5", "blablabla0"));
            data.Add(new Tuple<string, string>("test7.1", "blablabla3"));
            data.Add(new Tuple<string, string>("test7.2", "blablabla2"));
            data.Add(new Tuple<string, string>("test7.5", "blablabla0"));
            data.Add(new Tuple<string, string>("test7.1", "blablabla3"));
            data.Add(new Tuple<string, string>("test7.2", "blablabla2"));
            data.Add(new Tuple<string, string>("test7.5", "blablabla0"));
            data.Add(new Tuple<string, string>("test7.1", "blablabla3"));
            data.Add(new Tuple<string, string>("test7.2", "blablabla2"));
            await _cache.SetHashedAsync(key, data);
            var result = await _cache.GetHashedAsync<Tuple<string, string>>(key);
            Assert.NotNull(result);

        }

    }
}

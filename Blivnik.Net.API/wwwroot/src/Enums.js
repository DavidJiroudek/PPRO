export const _UserType = {
    Visitor: 0,
    Creator: 1,
    Moderator: 2,
    Admin: 3
  }
export const _Publicable = {
    Publicable: 0,
    Unpublicable: 1,
    Invisible: 2,
    Forbidden: 3
  }
 export const _AuthorType = {
      MusicAuthor: 0,
      TextAuthor: 1,
      Interpret: 3
  }
  export const _AppType = {
    Web: 0,
    Android: 1
}
export const _OAuthProvider = {
  Google: 0,
  Facebook: 1
}

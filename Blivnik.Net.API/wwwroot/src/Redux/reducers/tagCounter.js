import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  value: 0,
}

export const tagCounterSlice = createSlice({
  name: 'tagCounter',
  initialState,
  reducers: {
    increment: (state) => {
        state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    setCounter: (state, action) => {
      state.value = action.payload;
    }
  },
})

// Action creators are generated for each case reducer function
export const { increment,decrement, setCounter} = tagCounterSlice.actions

export default tagCounterSlice.reducer
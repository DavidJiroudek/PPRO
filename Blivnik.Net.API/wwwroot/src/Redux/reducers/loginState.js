import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  value: null,
}

export const loginSlice = createSlice({
  name: 'loginState',
  initialState,
  reducers: {
    setLoginData: (state, action) => {
      state.value = action.payload;
    },

  },
})

// Action creators are generated for each case reducer function
export const { setLoginData} = loginSlice.actions

export default loginSlice.reducer
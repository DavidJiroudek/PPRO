import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useMediaQuery } from "react-responsive";
import { useNavigate, useParams } from "react-router-dom";
import { _UserType } from "../Enums";
import "./css/EditorInit.css";
import Editor from "./Editor";

let givenText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\nEtiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu.\nMaecenas ipsum velit, consectetuer eu lobortis ut, dictum at dui.\n\nPraesent id justo in neque elementum ultrices. Excepteur\nsint occaecat cupidatat non proident,\nsunt in culpa qui officia deserunt mollit anim id est laborum.\nQuisque tincidunt scelerisque libero.";
let finalResponce;


//renders first step of adding a new song
const EditorInit = (props) => {


    const [editEnabled, setEditEnabled] = useState(true);
    const [priority, setPriority] = useState(1);
    const [capo, setCapo] = useState(0);
    const [musicAuthor, setMusicAuthor] = useState([]);
    const [textAuthor, setTextAuthor] = useState([]);
    const [interpret, setInterpret] = useState([]);
    const [whisperAuthors, setWhisperAuthors] = useState([]);
    const loginData = useSelector((state) => state.loginState.value);
    const navigate = useNavigate();
    const {songId} = useParams();
    const isWideScreen = useMediaQuery({ query: `(min-width: 961px)` });
    useEffect(()=>{
    if(songId !== undefined){
    
        fetch('/api/song/GetSongForEdit', {  
            method: 'POST', 
            headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token},
            body: JSON.stringify({SongId: songId})
            
        }).then(response => {
            if(response.redirected){
              navigate(response.url.substring(response.url.lastIndexOf("/")))
            }
            return (response.json())})
        .then(songData => {
            document.getElementById("songName").value = songData.SongName;
            document.getElementById("url").value = songData.Url;
            document.getElementById("publicable").value = songData.Publicable;
            document.getElementById("priority").value = songData.Priority;
            document.getElementById("capo").value = songData.Capo;
            document.getElementById("textEditArea").style.visibility = "hidden";
            finalResponce = {songId, creator: songData.Creator, html: songData.Text}
            setPriority(songData.Priority);
            setCapo(songData.Capo);
            for (const author of songData.Authors) {
                    switch(author.type){
                        case 0:
                            setMusicAuthor([...musicAuthor, {id: musicAuthor.length, name: author.name}])
                        break;
                        case 1:
                            setTextAuthor([...textAuthor, {id: textAuthor.length, name: author.name}])
                        break;
                        case 2:
                            setInterpret([...interpret, {id: interpret.length, name: author.name}])
                        break;
                        default:
                        break;
                    }
            }
            //rythms       
        });
    }},[])

    useEffect(()=>{
            fetch('/api/search/GetWhispererData', {  
                method: 'POST', 
                headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token},
                body: "ROFLMAO"
                
            }).then(response => response.json())
            .then(response => setWhisperAuthors(response));
        }, [])



    if (editEnabled) {
        return (
            <div>
                <datalist id="authors" >
                    {whisperAuthors.map(i => <option key={"author" + i.AuthorId} value={i.Name} />)}
                </datalist>
                {isWideScreen ? <img id='openMenuBtn' className='openMenuBtnClass' onClick={() => openMenu(isWideScreen)} src="/logo_square.png"></img> : <button id="openMenuBtn" className='blueButton mobileOpenMenu' style={{display: "none"}} onClick={() => openMenu(isWideScreen)}><p>Možnosti písně</p></button>}
                    <div id="songPropsContainer">
                    <form id="songPropsForm" onSubmit={(e) => handleSubmit(e, setEditEnabled)}>
                        <div className="songProps">
                            <div className="songPropsItem">
                                <div>Název písně:</div>
                                <input id="songName" className="whiteInput" type="text" name="songName" placeholder="Název písně"></input>
                            </div>
                            <div className="songPropsItem">
                                <div>Autoři hudby:</div>
                                {musicAuthor.map(i =><div key={"musicAuthorContainer" + i.id}>
                                                        <input className="whiteInput" type="text" name="musicAuthor" placeholder="Autor hudby" key={"musicAuthor" + i.id} list="authors" ></input>
                                                        <button className="whiteButton" type="button" onClick={() => { setMusicAuthor(musicAuthor.filter(j => j.id !== i.id)) }} key={"musicAuthorDelBtn" + i.id} ><p>-</p></button>
                                                    </div>)}
                                <button className="whiteButton" type="button" onClick={() => { setMusicAuthor([...musicAuthor, {id: musicAuthor.length}]) }}><p>+</p></button>
                            </div>
                            <div className="songPropsItem">
                                <div>Autoři textu:</div>
                                {textAuthor.map(i => <div key={"textAuthorContainer" + i.id}>
                                                        <input className="whiteInput" type="text" name="textAuthor" defaultValue={i.name} placeholder="Autor textu" key={"textAuthor" + i.id} list="authors"></input>
                                                        <button className="whiteButton" type="button" onClick={() => { setTextAuthor(textAuthor.filter(j => j.id !== i.id)) }} key={"textAuthorDelBtn" + i.id}><p>-</p></button>
                                                    </div>)}
                                <button className="whiteButton" type="button" onClick={() => { setTextAuthor([...textAuthor, textAuthor.length]) }}><p>+</p></button>
                            </div>
                            <div className="songPropsItem">
                                <div>Interpreti:</div>
                                {interpret.map(i => <div key={"interpretContainer" + i.id}>
                                                        <input className="whiteInput" type="text" name="interpret" defaultValue={i.name} placeholder="Interpret" key={"interpret" + i.id} list="authors"></input>
                                                        <button className="whiteButton" type="button" onClick={() => { setInterpret(interpret.filter(j => j.id !== i.id)) }} key={"interpretDelBtn" + i.id}><p>-</p></button>
                                                    </div>)}
                                <button className="whiteButton" type="button" onClick={() => { setInterpret([...interpret, interpret.length]) }}><p>+</p></button>
                            </div>

                            <div className="songPropsItem">
                                <div>YouTube URL:</div>
                                <input id="url" className="whiteInput" type="text" name="url" placeholder="YouTube URL" ></input>
                            </div>
                            <div className="songPropsItem">
                                <div>Priorita: {priority}</div>
                                <input type="range" id="priority" name="priority" min="1" max="5" defaultValue="1" onChange={(e) => setPriority(e.target.value)}></input>
                            </div>
                            <div className="songPropsItem">
                                <div>Capo: {capo}</div>
                                <input type="range" id="capo" name="capo" min="0" max="10" defaultValue="0" onChange={(e) => setCapo(e.target.value)}></input>
                            </div>
                            <div className="songPropsItem">
                                <label htmlFor="publicable">Publikovatelnost: </label>
                                <select className="whiteButton" id="publicable" name="publicable">
                                    <option value={0}>Publikovatelná</option>
                                    <option value={1}>Nepublikovatelná</option>
                                    {loginData.permission >= _UserType.Moderator ? <option value={2}>Neviditelná</option> : []}
                                    {loginData.permission >= _UserType.Admin ? <option value={3}>Zakázaná</option> : []}
                                </select>
                            </div>
                            <div className="songPropsItem">
                                <label>Doporučený doprovod:</label>
                                <br/>
                                <label htmlFor="rythm4">Pochoďák - 4</label>
                                <input type="checkbox" id="rythm4" name="rythm4"></input>
                                <br/>
                                <label htmlFor="rythm6">Pionýrák - 6</label>
                                <input type="checkbox" id="rythm6" name="rythm6"></input>
                                <br/>
                                <label htmlFor="rythm7">Valčík - 7</label>
                                <input type="checkbox" id="rythm7" name="rythm7"></input> 
                                <br/>
                                <label htmlFor="rythm9">Trilolový - 9</label>
                                <input type="checkbox" id="rythm9" name="rythm9"></input> 
                                <br/>
                            </div> 
                        </div>
                        </form>
                        <div id='closeSongMenu' className='closeSongMenu' onClick={() => closeMenu()}></div>
                    </div>

                    <div className="songEditAreaContainer">
                        <textarea id="textEditArea"  className="songEditArea" defaultValue={givenText} ></textarea>
                    </div>
                    <div>
                    <button className="blueButton isEditBtn" form="songPropsForm" type="submit"><p>Přidat akordy</p></button>
                    </div>

            </div>
        );
    } else {
        return (

            <Editor {...finalResponce} />

        );
    }

}

const openMenu = (isWideScreen) =>{
    const menuElement = document.getElementById("songPropsContainer");
    menuElement.style.visibility = "visible"
    if(isWideScreen){
      menuElement.style.width = "20%";
  
    }else{
      menuElement.style.width = "80%";
    }
    document.getElementById("openMenuBtn").style.display = "none";
  }
  
  const closeMenu = () =>{
    const menuElement = document.getElementById("songPropsContainer");
    menuElement.style.width = "0";
    menuElement.style.visibility = "hidden"
    setTimeout(() => document.getElementById("openMenuBtn").style.display = "inline-flex", 500);
  }



const handleSubmit = (event, state) => {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    let formObj = {};
    const authors = [];
    const rythms = [];
    for (let [key, value] of formData.entries()) {
        switch (key) {
            case "musicAuthor":
                value !== "" ? authors.push({name: value, type: 0}) : {}
                break;
            case "textAuthor":
                value !== "" ? authors.push({name: value, type: 1}) : {}
            break;
            case "interpret":
                value !== "" ? authors.push({name: value, type: 2}) : {}
            break;
            default:
                if(key.startsWith("rythm")){
                    rythms.push(parseInt(key.slice(key.length - 1)))
                }else{
                formObj = {...formObj, [key]:value}
                }
                break;
        }
    }
    if(formObj.songName === ""){
        alert("Vyplňte název písně")
        return
    }
    if(authors.length === 0){
        alert("Doplňte alespoň jednoho autora")
        return
    }
    formObj = {...formObj, authors, rythms};
    processText(formObj, state);

}

//generates verses and rows from plain text
const processText = (formObj, state) => {
    var splited = document.getElementById("textEditArea").value.split('\n\n');
    var m = 0;
    var n = 0;

    const processedText = splited.map(i => i = <verse versetype="verse" versevalue={m++ + 1} className='verse' id={'verse' + m} key={'verse' + m} >{i.split('\n').map(j => j = <row className='row' id={'row' + n++} key={'row' + n} >{"   " + j + "   "}</row>)}</verse>);
    finalResponce = {...finalResponce, ...formObj, processedText}
    state(false);

}
export default EditorInit;
import React from 'react';
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addChord, changeChords } from "../../Redux/reducers/activeChordsState";
import { changeEditMenu } from "../../Redux/reducers/editMenuState";
import ActiveChords from "../Menu/PreselectedChordsMenu";
import "../css/MenuElements.css";
import addTag from "../TagHelpers/TagHelper";
const tones = "C;D;E;F;G;A;H"
const signs = "#;♭"
const basicSufixes = '7;mi;mi7;sus;dim;maj;maj7';
const cadence = ["C;F;G;G:7;A:mi;D:mi;E:mi","G;C;D;D:7;E:mi;A:mi;D:mi","D;G;A;A:7;H:mi;Emi;F#:mi", "A;D;E;E:7;F#:mi;A:mi;H:mi","E;A;H;H:7;C#:mi;E:mi;F#:mi", "F;B;C;C:7;D:mi;G:mi;A:mi"];
const dynamics = "𝆑𝆑𝆑;𝆑𝆑;𝆑;𝆐𝆑;𝆐𝆏;𝆏;𝆏𝆏;𝆏𝆏𝆏"
const dynamicsWide = "𝆒;𝆓"
const rythms = "rit.;accel."
//𝄆;𝄇
//renders elements o chord menu
const MenuElements = () => {
    const dispatch = useDispatch();
    const counter = useSelector(state => state.tagCounter.value)

    const [currentCadence, setCurrentCadence] = useState("Žádná kadence")
    const [formData, setFormData] = useState({   tone: "C",
                                                sign: "",
                                                sufix: "",
                                                form: ""});

    const parsedCadendes = parseCadenceData(cadence)


    return (<div className="chordMenu">
            <div>
                <span className="dropdown">
                    <button className="whiteButton"><p>{currentCadence}</p></button>
                    <div className="dropdown-content">
                        <button className='whiteButton' onClick={()=>{setCurrentCadence("Žádná kadence"); dispatch(changeChords([]))}} key={"Žádná kadence"}><p>Žádná kadence</p></button>
                        {parsedCadendes.map(i=> <button className='whiteButton' onClick={()=>{setCurrentCadence(i.textValue); dispatch(changeChords(i.chords))}} key={i.textValue}><p>{i.textValue}</p></button>)}
                    </div>
                </span>
            </div>
            <div className="dropdown">
                <button className="whiteButton"><p>{formData.tone}</p></button>
                <div className="dropdown-content">
                    {tones.split(';').map(i=> <button className='whiteButton' onClick={()=>setFormData({...formData,tone: i})} key={"tone" + i}><p>{i}</p></button>)}
                </div>
            </div>
            <div className="dropdown">
                <button className="whiteButton"><p>{formData.sign === "" ? "-": formData.sign}</p></button>
                <div className="dropdown-content">
                    <button className='whiteButton' onClick={()=>setFormData({...formData,tone: ""})} key={"sign"}><p>{"-"}</p></button>
                    {signs.split(';').map(i=> <button className='whiteButton' onClick={()=>setFormData({...formData,sign: i})} key={"sign" + i}><p>{i}</p></button>)}
                </div>
            </div>
            <div className="dropdown">
                <button className="whiteButton"><p>{formData.sufix === "" ? "-": formData.sufix}</p></button>
                <div className="dropdown-content">
                    <button className='whiteButton' onClick={()=>setFormData({...formData,tone: ""})} key={"Sufix"}><p>{"-"}</p></button>
                    {basicSufixes.split(';').map(i=> <button className='whiteButton' onClick={()=>setFormData({...formData,sufix: i})} key={"sufix" + i}><p>{i}</p></button>)}
                </div>
            </div>
                <button className='whiteButton' onClick={() => dispatch(addChord({tone: formData.tone + formData.sign, sufix: formData.sufix, form: ""}))}><p>Přidat</p></button>

            <span>
                <button className='whiteButton' onClick={() => dispatch(changeEditMenu({type:"alternativeChord", value: formData}))}><p>Přidat jiný akord</p></button>
            </span>
            <div className="menuItems">
                {dynamics.split(";").map(i=><span className="whiteButton" key={i} id={i} onClick={() => addTag("dyn", i, counter, dispatch, {})}><p>{i}</p></span>)}
            </div>
            <div className="menuItems">
                <span className="whiteButton" id="rep" onClick={() => addTag("rep", "rep", counter, dispatch, {})}><p>𝄆 𝄇</p></span>
                <span className="whiteButton" id="capo" onClick={() => addTag("capo", 0, counter, dispatch, {})}><p>Capo</p></span>
                <span className="whiteButton" id="space" onClick={() => addTag("space", 5, counter, dispatch, {dash: true})}><p>—</p></span>
                {rythms.split(";").map(i=><span className="whiteButton" key={i} id={i} onClick={() => addTag("rythm", i, counter, dispatch, {})}><p>{i}</p></span>)}
            </div>
            <div className="menuItems">
            {dynamicsWide.split(";").map(i=><span className="whiteButton" key={i} id={i} onClick={() => addTag("dynWide", i, counter, dispatch, {})}><p>{i}</p></span>)}
            </div>
            <div className="menuItems">
                <span className="whiteButton" id="delete" onClick={() => addTag("delete", "delete", counter, dispatch, {})}><p>Smazat text</p></span>
                <span className="whiteButton" id="addText" onClick={() => dispatch(changeEditMenu({type:"insertText", value: counter}))}><p>Přidat text</p></span>
            </div>
            </div>);
}




const parseCadenceData = (cadences) =>{
    const result = [];
    for (const i of cadences) {
        const first = i.split(';');
        const cad = {
            textValue: i.replaceAll(";", " ").replaceAll(":", ""),
            chords: []
        }
        for (const k of first) {
            const splited = k.split(':');
            cad.chords.push({  tone: splited[0],
                        sufix: splited[1] !== undefined? splited[1] : "",
                        form: ""
                        })
        }
        result.push(cad);
    }
    return result
}
// const createParams = (tagState, setTagState, setEditMenuState, counter, setCounter) =>{
//     return({
//         tagState,
//         setTagState,
//         setEditMenuState,
//         counter,
//         setCounter
// })
// }
export default MenuElements;
//agfraergraegrvregaergerag
//tagState, setTagState, setEditMenuState, counter, setCounter
import React from 'react';
import { useEffect } from "react";
import { useDispatch, useSelector} from "react-redux";
import { useMediaQuery } from 'react-responsive';
import { useNavigate } from 'react-router-dom';
import { setCounter } from '../Redux/reducers/tagCounter';
import { changeTags } from "../Redux/reducers/tagState";
import ActiveChords from './Menu/PreselectedChordsMenu';
import ChordArea from "./ChordArea";
import ChordForms from "./ChordForms";
import EditMenu from "./Menu/EditMenu";
import MenuElements from "./Menu/MenuElements";
import "./css/Editor.css";
import { rerenderTags } from "./TagHelpers/DisplayTags";
import ValidateXML from './TagHelpers/ValidateXML';



//renders second stage of adding a new song -> adding chords
const Editor = (props) =>{
  const dispatch = useDispatch();
  const isWideScreen = useMediaQuery({ query: `(min-width: 961px)` });
  //initial rendering of tags
  useEffect(() => {
      dispatch(setCounter( document.getElementById("editorContent").getElementsByTagName("*").length))
      dispatch(changeTags(rerenderTags()));
        
  },[]);

  //handeling of tag positions after resize
  useEffect(() => {
      const debouncedHandleResize = debounce(function handleResize() {
        dispatch(changeTags(rerenderTags()));
      },1000)
      window.addEventListener('resize', debouncedHandleResize);
      return _ => {
          window.removeEventListener('resize', debouncedHandleResize)}
  });
  const loginData = useSelector((state) => state.loginState.value);
  const navigate = useNavigate();
    return(
        <div id='editor'>
             {isWideScreen ? <img id='openMenuBtn' className='openMenuBtnClass' onClick={() => openMenu(isWideScreen)} src="/logo_square.png"></img> : <button id="openMenuBtn" className='blueButton mobileOpenMenu' style={{display: "none"}} onClick={() => openMenu(isWideScreen)}><p>Možnosti písně</p></button>}
            <div id='menuEditor' className="menuEditor">
              <div className='menuProps'>
                <MenuElements/>
                <EditMenu/>
                </div>
              <div id='closeSongMenu' className='closeSongMenu' onClick={() => closeMenu()}></div>
            </div>
            <ActiveChords/>
            <div className="editAreaEditor" id="editArea">
              <div className="songHeader" id = "songHeader">
                <div className="songHeaderMain">
                <span>{props.authors.sort((a, b)=> a.type - b.type)[0].name}</span>
                    <span>{props.songName}</span>
                </div>
                <div className="songHeaderSub">   
                <span>{new Date().toLocaleDateString("cs-CZ")}</span>
                    {props.capo !== 0 ? <span>Capo: {props.capo}</span> : ""}
                    {props.rythms.lenght >= 0 ? <span>Doprovody: {props.rythms.map(i => i + " ")}</span> : ""}
                </div>
                <ChordForms/>
                </div>
                <ChordArea/>
                {props.html !== undefined ?
                 <div id = "editorContent" dangerouslySetInnerHTML={{__html: props.html} }/>
                :
                <div id = "editorContent">
                    {props.processedText}
                </div>}
              </div>
            <div id='editorSendingButtonContainer'>
              {props.html !== undefined ?
                <>
                  <label htmlFor='reprint'>Úprava je závažná.</label>
                  <input type="checkbox" id='reprint'></input>
                  <button className="blueButton isEditBtn" onClick={() => editSong(props, loginData.token, navigate)}><p>Upravit</p></button>
                </>

                :
                <button className="blueButton isEditBtn" onClick={() => saveSong(props, loginData.token, navigate)}><p>Odeslat</p></button>
                }
            </div>
        </div>
    );

}

const openMenu = (isWideScreen) =>{
  const menuElement = document.getElementById("menuEditor");
  menuElement.style.visibility = "visible"
  if(isWideScreen){
    menuElement.style.width = "20%";

  }else{
    menuElement.style.width = "80%";
  }
  document.getElementById("openMenuBtn").style.display = "none";
}

const closeMenu = () =>{
  const menuElement = document.getElementById("menuEditor");
  menuElement.style.width = "0";
  menuElement.style.visibility = "hidden"
  setTimeout(() => document.getElementById("openMenuBtn").style.display = "inline-flex", 500);
}

 const saveSong = (props, token, navigate) => {
    if(!ValidateXML()){
      alert("Píseň není zadána validně")
      return null;
    }

    document.getElementById("chordArea").remove();
    const songData = {
      SongName: props.songName,
      Publicable: props.publicable,
      Capo: props.capo,
      Url: props.url,
      Authors: props.authors,
      Priority: props.priority,
      Rythms: props.rythms,
      Text: document.getElementById("editorContent").innerHTML
    }

  fetch('/api/song/SaveSong', {  

  method: 'POST', 
  headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token},
  body: JSON.stringify(songData) 

})
navigate("/");
}

const editSong = (props, token, navigate) => {
  if(!ValidateXML()){
    alert("Píseň není zadána validně")
    return null;
  }
  document.getElementById("chordArea").remove();
  const songData = {
    SongId: props.songId,
    SongName: props.songName,
    Publicable: props.publicable,
    Capo: props.capo,
    Url: props.url,
    Authors: props.authors,
    Priority: props.priority,
    Rythms: props.rythms,
    Creator: props.creator,
    Text: document.getElementById("editorContent").innerHTML,
    Reprint: document.getElementById("reprint").checked
  }

fetch('/api/song/EditSong', {  

method: 'POST', 
headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token},
body: JSON.stringify(songData) 

})
navigate("/");
}



function debounce(fn, ms) {
    let timer
    return _ => {
      clearTimeout(timer)
      timer = setTimeout(_ => {
        timer = null
        fn.apply(this, arguments)
      }, ms)
    };
  }

export default Editor;
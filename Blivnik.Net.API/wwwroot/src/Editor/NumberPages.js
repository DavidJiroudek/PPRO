import React from 'react';
const numberPages = (maxPage, setPage, page) => {
    const pageNumbers = [];
  for (let i = 1; i <= maxPage; i++) {
    pageNumbers.push(<span id={"page" + i} className="pageNumber" key={"page" + i} style={ page === i ? {fontWeight: "bold", textDecoration: "underline"} : {}} onClick={()=>{setPage(i)}}>{i}</span>)
    
  }
    return(pageNumbers)
  
  }

  export default numberPages
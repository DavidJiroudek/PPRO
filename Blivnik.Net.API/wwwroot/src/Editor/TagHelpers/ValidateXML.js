import React from 'react';

const ValidateXML = () => {
    const availableTags = ["DYN","CH", "RYTHM", "DYNWIDE", "REP", "ROW", "VERSE", "CAPO", "SPACE" ]
    const allElements = document.getElementById("editorContent").getElementsByTagName("*");

    for (let i of allElements) {
        if (!availableTags.includes(i.tagName)) {
            return false
        }
    }
    return true;

}

export default ValidateXML
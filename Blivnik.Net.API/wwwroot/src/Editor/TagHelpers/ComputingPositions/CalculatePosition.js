﻿//calculates position for adding tags into innerHtml
const calculatePosition = ( start, end, text ) => {
    //calculation for end of the tag
    let isTag = false;
    let wantedPosition = 0;
    //let splited = text.split("");
    let i;
    for (i in text) {
        if (wantedPosition === end) {
            break;
        }
        if (text[i] === '<') {
            isTag = true;
            continue;
        }
        if (text[i] === '>') {
            isTag = false;
            continue;
        }
        if (isTag === false) {
            wantedPosition++;
        }
    }

    let endPosition = i;
    if (text.length === ++i) {
        endPosition = i;
    }



    wantedPosition = 0;
    let j 
    for (j in text) {
        if (wantedPosition === start) {
            break;
        }
        if (text[j] === '<') {
            isTag = true;
            continue;
        }
        if (text[j] === '>') {
            isTag = false;
            continue;
        }
        if (isTag === false) {
            wantedPosition++;
        }
    }
    //for starting tag to be inside
    while (text[j] === '<') {
        while (text[++j] !== '>') { }
        j++;
    }


    let startPosition = j;
    return { startPosition, endPosition };
}

export default calculatePosition
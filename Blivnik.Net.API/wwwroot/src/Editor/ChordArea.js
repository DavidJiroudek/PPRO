import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { changeEditMenu } from "../Redux/reducers/editMenuState";
import "./css/ChordArea.css"; 

//<span style={i.style} className={i.className} id={i.id} onClick={typeSwitch(dispatch, i.className, i.tagId)} key={i.key} >{i.textValue}</span>
const ChordArea = () => {
const tags = useSelector((state) => state.tagState.value)
const dispatch = useDispatch();
// dispatch(deleteChordForms());
    return(<div className = "chordArea" id="chordArea">{tags.map(i=>typeSwitch(dispatch, i))}</div>);

}
const typeSwitch = (dispatch, i) =>{
    switch (i.className) {
        case "chordSign":  
            return <span style={i.style} className={i.className + " sign"} id={i.id} onClick={() => dispatch(changeEditMenu({type:"chord", id: i.tagId}))} key={i.key} >{i.textValue + i.params.sufix}</span>

        case "verseSign":       
            return <span style={i.style} className={i.className + " sign"} id={i.id} onClick={() => dispatch(changeEditMenu({type:"verse", id: i.tagId}))} key={i.key} >{i.textValue}</span>

        case "dynSign":       
            return <span style={i.style} className={i.className + " sign"} id={i.id} onClick={() => dispatch(changeEditMenu({type:"dyn", id: i.tagId}))} key={i.key} >{i.textValue}</span>

        case "repSign":       
            return (<>
                        <span style={i.style} className={i.className + " sign"} id={i.id + "S"} onClick={() => dispatch(changeEditMenu({type:"rep", id: i.tagId}))} key={i.key + "S"} >{i.textValue}</span>
                        <span style={i.params.endStyle} className={i.className + " sign"} id={i.id + "E"} onClick={() => dispatch(changeEditMenu({type:"rep", id: i.tagId}))} key={i.key + "E"} >{i.params.endValue}</span>
                    </>)
        case "rythmSign":
            return(<span style={i.style} className={i.className + " sign"} id={i.id} onClick={() => dispatch(changeEditMenu({type:"rythm", id: i.tagId}))} key={i.key} >{i.textValue}</span>)
        case "capoSign":
            return(<span style={i.style} className={i.className + " sign"} id={i.id} onClick={() => dispatch(changeEditMenu({type:"capo", id: i.tagId}))} key={i.key} >{"Capo: " + i.textValue}</span>)
        case "spaceSign":
            return(<span style={i.style} className={i.className + " sign"} id={i.id} onClick={() => dispatch(changeEditMenu({type:"space", id: i.tagId}))} key={i.key} >{i.textValue}</span>)
        case "dynWideSign":
            return(<span style={i.style} className={i.className + " sign"} id={i.id} onClick={() => dispatch(changeEditMenu({type:"dynWide", id: i.tagId}))} key={i.key} >{i.textValue}</span>)
            default:
            return "";
    }

}

export default ChordArea;
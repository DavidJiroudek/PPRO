import React from 'react';
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate, useParams } from "react-router-dom";
import ChordArea from '../Editor/ChordArea';
import ChordForms from '../Editor/ChordForms';
import "./Song.css"
import {rerenderTags} from "../Editor/TagHelpers/DisplayTags";
import { changeTags } from '../Redux/reducers/tagState';
import { useMediaQuery } from 'react-responsive';
import { _UserType } from '../Enums';
import Rating from './Components/Rating/Rating';
import Players from './Components/Players/Players';



//component for showing song with tags
const Song = () => {
    const {songId} = useParams();
    const[songData, setSongData] = useState(null);
    const loginData = useSelector((state) => state.loginState.value)
    const isWideScreen = useMediaQuery({ query: `(min-width: 961px)` });
    const dispatch = useDispatch();
    const navigate = useNavigate();
    //load html of song
    useEffect(() => {
      const header = loginData !== null ? { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token} : { 'Content-Type': 'application/json'}
      
      fetch('/api/song/GetSong', {  

        method: 'POST', 
        headers: header,
        body: JSON.stringify({SongId: songId})
      
      })
      .then(response => {
        if(response.redirected){
          navigate(response.url.substring(response.url.lastIndexOf("/")))
        }
        return (response.json())})
        .then(response => {
          setSongData(response)
        setTimeout(() => {dispatch(changeTags(rerenderTags()))}, 200)});

        fetch('/api/song/IncreseViewCount', {  

          method: 'POST', 
          headers: header,
          body: JSON.stringify({SongId: songId})
        })
    },[]);
    useEffect(() => {
        const debouncedHandleResize = debounce(function handleResize() {
          dispatch(changeTags(rerenderTags()));
        },1000)

        window.addEventListener('resize', debouncedHandleResize);

        return _ => {
            window.removeEventListener('resize', debouncedHandleResize)}
    });

if(songData != null){
    return(
        <div>
          <Rating/>
        {isWideScreen ? <img id='openMenuBtn' className='openMenuBtnClass' onClick={() => openMenu(isWideScreen)} src="/logo_square.png"></img> : <button id="openMenuBtn" className='blueButton mobileOpenMenu' onClick={() => openMenu(isWideScreen)}><p>Možnosti písně</p></button>}
          <div id="songMenu">
            <span id='songMenuProps'>
            {loginData !== null && loginData.permission >= _UserType.Moderator ? <Link className="whiteButton" to= {`/Editor/${songId}`}><p>Upravit</p></Link> : []}
            {loginData !== null && loginData.permission >= _UserType.Visitor ? <button className="whiteButton" onClick={() => generatePDF(songData.SongId, loginData.token, navigate)}><p>Generovat PDF</p></button> : []}
            {loginData !== null && songData.Validated === false && loginData.permission >= _UserType.Moderator ? <button id='validateButton' className='whiteButton' onClick={() =>validateSong(songData.SongId, loginData.token, navigate)}><p>Schválit</p></button> : []}
            {loginData !== null && loginData.permission >= _UserType.Admin ? <button className='whiteButton' onClick={() => deleteSong(songData.SongId, loginData.token, navigate)}><p>Odstranit</p></button> : []}
            </span>
            <div id='closeSongMenu' onClick={() => closeMenu()}></div>
          </div>
            <div className="songArea" id="editArea">
              <div className="songHeader" id = "songHeader">
                <div className="songHeaderMain">
                    <span>{songData.Authors.sort((a, b)=> a.type - b.type)[0].name}</span>
                    <span>{songData.SongName}</span>
                </div>
                <div className="songHeaderSub">
                    <span>{new Date(songData.Date).toLocaleDateString("cs-CZ")}</span>
                    {songData.Capo !== 0 ? <span>Capo: {songData.Capo}</span> : ""}
                    {songData.Rythms.lenght >= 0 ? <span>Doprovody: {songData.Rythms.map(i => i + " ")}</span> : ""}
                </div>
                <ChordForms/>
                </div>
                <ChordArea/>
                 <div id = "editorContent" dangerouslySetInnerHTML={{__html: songData.Text} }/>
              </div>
              <Players {...{songId: songData.SongId, songName: songData.SongName, authorName: songData.Authors.sort((a, b)=> a.type - b.type)[0].name}}/>

        </div>
        
        );
  }
}
const openMenu = (isWideScreen) =>{
  const menuElement = document.getElementById("songMenu");
  menuElement.style.visibility = "visible"
  if(isWideScreen){
    menuElement.style.width = "20%";

  }else{
    menuElement.style.width = "80%";
  }
  document.getElementById("openMenuBtn").style.display = "none";
}

const closeMenu = () =>{
  const menuElement = document.getElementById("songMenu");
  menuElement.style.width = "0";
  menuElement.style.visibility = "hidden"
  setTimeout(() => document.getElementById("openMenuBtn").style.display = "inline-flex", 500);
}


const validateSong = (songId, token, navigate) =>{
  fetch('/api/song/ValidateSong', {  
    method: 'POST', 
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token},
    body: JSON.stringify({SongId: songId}) 
  
  })
  document.getElementById("validateButton").style.display = "none";

}
const deleteSong = (songId, token, navigate) =>{
  if (confirm("Opravdu chcete tuto píseň smazat?")) {

    fetch('/api/song/DeleteSong', {  
      method: 'POST', 
      headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token},
      body: JSON.stringify({SongId: songId}) 
    
    })
    navigate("/");

  } else {
    // Do nothing!
    console.log('Delete was interuped by user.');
  }
}

const generatePDF = (songId, token, navigate) => {

  fetch('/api/pdf/GenerateSingleSongPDF', {  
    method: 'POST', 
    headers: { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token},
    body: JSON.stringify({SongId: songId}) 
  
  }).then(response => response.blob())
  .then((blob) => { const fileURL = window.URL.createObjectURL(blob);
                      // Setting various property values
                      let alink = document.createElement('a');
                      alink.href = fileURL;
                      alink.download = 'SamplePDF.pdf';
                      alink.click();
});
}


function debounce(fn, ms) {
    let timer
    return _ => {
      clearTimeout(timer)
      timer = setTimeout(_ => {
        timer = null
        fn.apply(this, arguments)
      }, ms)
    };
  }



export default Song;
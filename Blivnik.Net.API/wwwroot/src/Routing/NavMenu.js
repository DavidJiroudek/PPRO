import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import SongList from '../SongList/SongList';
import Home from '../Home/Home';
import Navbar from './NavbarIndex';
import EditorInit from '../Editor/EditorInit';
import Song from '../Song/Song';
import { useDispatch, useSelector } from 'react-redux';
import { setLoginData } from '../Redux/reducers/loginState';
import jwtDecode from 'jwt-decode';
import MySongs from '../SongList/MySongs';
import Users from '../Users/Users';
import ErrorPage from '../Errors/ErrorPage';
import AuthorList from '../Authors/AuthorList';
import Author from '../Authors/Author';
import { _UserType } from '../Enums';
import SpotifyCallback from '../Song/Components/Players/SpotifyCallback';
import BookGenerator from '../BookGenerator/BookGenerator';



//router for aplication
export default function NavMenu(){
  const dispatch = useDispatch();
  useEffect(()=>{
    fetch('/api/user/IsSignedIn', {  

      method: 'get', 
      headers: { 'Content-Type': 'application/json'}
    
    }).then(response => response.json())
    .then(i => i = {...jwtDecode(i), token: i})
    .then(i => dispatch(setLoginData({id:i.sub, name: i.name, email:i.email, picture: i.picture, permission: i.permission, token: i.token})));
    
  },[])

  const loginData = useSelector((state) => state.loginState.value)
  if(loginData !== null){
    return (
      <Router>
        <Navbar/>
        <Routes>
          <Route path='/' exact element={<Home/>} />
          <Route path='/songList' element={<SongList/>} />
          <Route path='/authorList' element={<AuthorList/>} />
          <Route path='/bookGenerator' element={<BookGenerator/>} />
          <Route path='/author/:authorId' element={<Author/>} />
          <Route path='/error401' element={<ErrorPage {...{number: 401}}/>} />
          <Route path='/error404' element={<ErrorPage {...{number: 404}}/>} />
          {loginData.permission >= _UserType.Visitor ? <Route path='/mySongs' element={<MySongs/>} /> : []} 
          {loginData.permission >= _UserType.Creator ? <Route path='/editor' element={<EditorInit/>} /> : []} 
          {loginData.permission >= _UserType.Creator ? <Route path='/editor/:songId' element={<EditorInit/>} /> : []} 
          {loginData.permission >= _UserType.Admin ? <Route path='/users' element={<Users/>} /> : []} 
          <Route path='/song/:songId' element={<Song/>} />
          <Route path='/spotifyCallback' element={<SpotifyCallback/>} />
        </Routes>
    </Router>
    );
  }else{
    return (
    <Router>
    <Navbar/>
    <Routes>
      <Route path='/' exact element={<Home/>} />
      <Route path='/songList' element={<SongList/>} />
      <Route path='/song/:songId' element={<Song/>} />
      <Route path='/authorList' element={<AuthorList/>} />
      <Route path='/bookGenerator' element={<BookGenerator/>} />
      <Route path='/author/:authorId' element={<Author/>} />
      <Route path='/error401' element={<ErrorPage {...{number: 401}}/>} />
      <Route path='/error404' element={<ErrorPage {...{number: 404}}/>} />
      <Route path='/spotifyCallback' element={<SpotifyCallback/>} />
    </Routes>
  </Router>
  );

  }
}

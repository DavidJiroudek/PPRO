import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import useDebounce from '../CustomHooks';
import { _UserType } from '../Enums';
import { setLoginData } from '../Redux/reducers/loginState';
import Auth from '../Auth/Auth';
import './NavMenu.css';
  
//upper menu with elements
const Navbar = () => {
  const [showLoginPopup, setShowLoginPopup] = useState(false);
  const [fullText, setFullText] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const dispatch = useDispatch();
  const loginData = useSelector((state) => state.loginState.value);



  const debouncedSearchTerm = useDebounce(fullText, 300);
  useEffect(()=>{
    if(debouncedSearchTerm.length >= 3){
      const header = loginData !== null ? { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token } : { 'Content-Type': 'application/json' }
      fetch('/api/search/GetFulltextSearchResult', {  
            method: 'POST', 
            headers: header,
            body: JSON.stringify({fullText})
            
        }).then(response => response.json())
        .then(result => {(result.length !== 0 ? setSearchResults(result) : setSearchResults([]))});
      }else{
        setSearchResults([])
      }
  }, [debouncedSearchTerm])



  if(loginData !== null){
  return (
    <>
        <nav className='navMenu'>
        <div className="navMenuItem dropdown_Nav">
          <img className="dropbtn_Nav" src={loginData.picture} onError={() => dispatch(setLoginData({...loginData, picture: "/logo_square.png"}))}></img>
          <div className="dropdown-content_Nav">
          {loginData.permission >= _UserType.Visitor ? <Link to='/MySongs'><div>Mé písně</div></Link> : []}
          {loginData.permission >= _UserType.Admin ? <Link to='/Users'><div>Uživatelé</div></Link> : []}
          <div onClick={() => logOut(dispatch)}>Odhlásit se</div>
          </div>
        </div>
        <Link className='navMenuItem navLink' to='/editor'>
            <span>Editor</span>
        </Link>
        <Link className='navMenuItem navLink' to='/songList'>
            <span>Písně</span>
        </Link>
        <Link className='navMenuItem navLink' to='/authorList'>
            <span>Autoři</span>
        </Link>
        <Link className='navMenuItem navLink' to='/bookGenerator'>
            <span>Kniha</span>
        </Link>
        <div id="searchDropdown" className='navMenuItem'>
          <input id='search' type="text" onChange={(e)=>setFullText(e.target.value)} placeholder="Hledejte výraz..."></input>
            <div id="searchResultsContainer">
              {searchResults.slice(0, 7).map(i => 
                <Link key={i.Item.SongName} className='searchResult' onClick={() => {setSearchResults([]) ; document.getElementById("search").value("")}} to={`/song/${i.Item.SongId}`}>
                  <div className='searchSongName' key={i.Item.SongName + "nameField"}>{i.Item.SongName}</div>
                  {i.Highlight.map( j => j =<div className='searchHighlight' key={j} dangerouslySetInnerHTML={{__html: j}}></div>)}
                </Link>
              )}
            </div>
        </div>
        <Link className='navMenuItem logo' to='/'>
            <img className='logo' src='/logoTop.png' />
        </Link>
        </nav>

    </>
  );
  }else{
    return (
      <>
          {showLoginPopup === false ? "" : <Auth popupSet = {setShowLoginPopup}/>} 
          <nav className='navMenu'>
          <div className='navMenuItem navLink' onClick={() => setShowLoginPopup(!showLoginPopup)}>Login</div>
          <Link className='navMenuItem navLink' to='/songList'>
            <span>Písně</span>
        </Link>
        <Link className='navMenuItem navLink' to='/authorList'>
            <span>Autoři</span>
        </Link>
          <div id="searchDropdown">
            <input id='search' type="text" onChange={(e)=>setFullText(e.target.value)} placeholder="Hledejte výraz..."></input>
            <div id="searchResultsContainer">
              {searchResults.slice(0, 7).map(i => 
                <Link key={i.Item.SongName} className='searchResult' onClick={() => {setSearchResults([]) ; document.getElementById("search").value("")}} to={`/song/${i.Item.SongId}`}>
                  <div className='searchSongName' key={i.Item.SongName + "nameField"}>{i.Item.SongName}</div>
                  {i.Highlight.map( j => j =<div className='searchHighlight' key={j} dangerouslySetInnerHTML={{__html: j}}></div>)}
                </Link>
              )}
            </div>
        </div>
          <Link className='logo' to='/'>
            <img className='logo' src='/logoTop.png' />
          </Link>
          </nav>
      </>
    );


  }
};
  

const logOut = (dispatch) => {

  fetch('/api/user/LogOut', {  

    method: 'GET', 
    headers: { 'Content-Type': 'application/json',}
  })
    dispatch(setLoginData(null))
}


export default Navbar;
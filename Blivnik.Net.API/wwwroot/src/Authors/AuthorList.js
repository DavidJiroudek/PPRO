import React from 'react';
import { useState, useEffect } from "react";
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import numberPages from '../Editor/NumberPages';
import "./AuthorList.css"


const AuthorList = () => {

    const [authorList, setAuthorList] = useState([]);
    const [sortBy, setSortBy] = useState(0);
    const [page, setPage] = useState(1);
    const [maxPage, setMaxPage] = useState(1);
    const loginData = useSelector((state) => state.loginState.value)
    useEffect(() => {

      let sorbObj;
      switch (sortBy) {
        case "0":
          sorbObj = {field: "name", direction: 0}
          break;
        case "1":
        sorbObj = {field: "name", direction: 1}
        break;
        default:
          sorbObj = {field: "name", direction: 0}
          break;
      }
    
        const header = loginData !== null ? { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token } : { 'Content-Type': 'application/json' }

        fetch('/api/search/GetFilteredAuthorSearchResult', {

            method: 'POST',
            headers: header,
            body: JSON.stringify({sortBy: [sorbObj], page, resultCount: 10})

        }).then(response => response.json())
            .then((result) => { setAuthorList(result);
                                setMaxPage(result[0] !== undefined ? result[0].MaxPage : 1);
                  });
    }, [sortBy, page]);


    return (
        <>
        <div id='authorFilters'>
        <label>Řadit podle:</label>
          <select className="blueButton" onChange={(e)=>setSortBy(e.target.value)} id="sortBy">
            <option value={0}>A-Z</option>
            <option value={1}>Z-A</option>
          </select>
        </div>
        <div className="authorList"><p>Autoři</p>{authorList.map(i => i = <Link className="authorLink" key={i.Item.Index} to={`/author/${i.Item.Index}`} >{i.Item.Name}</Link>)}</div>
        <div id='pageNumbers'>
        {numberPages(maxPage, setPage, page)}
        </div>
        </>
    );

}


export default AuthorList;
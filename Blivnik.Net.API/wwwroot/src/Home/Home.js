import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import "./Home.css"
const Home = () => {

    const [popularSongList, setPopularSongList] = useState([]);
    const [recentSongList, setRecentSongList] = useState([]);
    const loginData = useSelector((state) => state.loginState.value)

    useEffect(() =>{
        const header = loginData !== null ? { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token } : { 'Content-Type': 'application/json' }
        fetch('/api/search/GetFilteredSearchResult', {
        method: 'POST',
        headers: header,
        body: JSON.stringify({publicable: [0, 1], page: 1, resultCount: 7, validated: true, sortBy: [{field: "viewCount", direction: 0}]} )
        }).then(response => response.json())
        .then((result) => { 
                            setPopularSongList(result);
        });

        fetch('/api/search/GetFilteredSearchResult', {
        method: 'POST',
        headers: header,
        body: JSON.stringify({publicable: [0, 1], page: 1, resultCount: 7, validated: true, sortBy: [{field: "createdDate", direction: 0}]} )
        }).then(response => response.json())
        .then((result) => { 
            setRecentSongList(result);
        });


    }, [])


    return(
    <div id='homePage'>
        <div id="popularSongs">
        <p>Nejnavštěvovanější skladby</p>
            {popularSongList.map(i => i = <Link className="songLink" key={i.Item.SongId} to={`/song/${i.Item.SongId}`} >{i.Item.SongName}</Link>)}
        </div>
        <div id="recentSongs">
        <p>Naposledy přidané skladby</p>
            {recentSongList.map(i => i = <Link className="songLink" key={i.Item.SongId} to={`/song/${i.Item.SongId}`} >{i.Item.SongName}</Link>)}
        </div>
    </div>
    )

}
export default Home;
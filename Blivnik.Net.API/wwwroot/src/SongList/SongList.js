import React from 'react';
import { useState, useEffect } from "react";
import { useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import numberPages from '../Editor/NumberPages';
import { _UserType } from '../Enums';
import "./SongList.css"


//side for all current songs
const SongList = () => {
  const [songList, setSongList] = useState([]);
  const [invalidated, setInvalidated] = useState(false);
  const [publicable, setPublicable] = useState(0);
  const [page, setPage] = useState(1);
  const [maxPage, setMaxPage] = useState(1);
  const [sortBy, setSortBy] = useState("0");
  const loginData = useSelector((state) => state.loginState.value)

  useEffect(() => {
    fetchSongs(loginData, setSongList, publicable, invalidated, page, sortBy, setMaxPage)
  },[invalidated, page, sortBy, publicable]);

  return (
    <div id="songListContainer">
      <div id='songGroups'>
        <button className='blueButton songGroupsItem' onClick={() => {setPage(1);setPublicable(0)}}><p>Publikovatené</p></button>
        <button className='blueButton songGroupsItem' onClick={() => {setPage(1);setPublicable(1)}}><p>Nepublikovatelné</p></button>
        {loginData !== null && loginData.permission >= _UserType.Moderator ? <button className='blueButton songGroupsItem' onClick={() => { setPage(1);setPublicable(2)}}><p>Neviditelné</p></button> : []}
        {loginData !== null && loginData.permission >= _UserType.Admin ? <button className='blueButton songGroupsItem' onClick={() => { setPage(1);setPublicable(3) }}><p>Zakázané</p></button> : []}
      </div>
      <div id='songFilters'>
      <label>Řadit podle:</label>
        <select className="blueButton" onChange={(e)=>setSortBy(e.target.value)} id="sortBy">
          <option value={0}>A-Z</option>
          <option value={1}>Z-A</option>
          <option value={2}>Popularita</option>
        </select>
        {loginData !== null && loginData.permission >= _UserType.Creator ? <><label htmlFor='invalidatedCheckbox'>Zobrazit nevalidované</label><input id='invalidatedCheckbox' type="checkbox" onChange={(e) => {setPage(1);setInvalidated(e.target.checked)}}></input></> : []}
      </div>

      <div className="songList">{songList.map(i => i = <Link className="songLink" key={i.Item.SongId} to={`/song/${i.Item.SongId}`} >{i.Item.SongName}</Link>)}</div>
      <div id='pageNumbers'>
      {numberPages(maxPage, setPage, page)}
      </div>
    </div>
  );

}


const fetchSongs = (loginData, setSongList, publicable, invalidated, page, sortBy, setMaxPage) => {
  let sorbObj
  switch (sortBy) {
    case "0":
      sorbObj = {field: "songName", direction: 0}
      break;
    case "1":
    sorbObj = {field: "songName", direction: 1}
    break;
    case "2":
      sorbObj = {field: "viewCount", direction: 0}
      break;
    default:
      sorbObj = {field: "songName", direction: 0}
      break;
  }


  const header = loginData !== null ? { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + loginData.token } : { 'Content-Type': 'application/json' }
  fetch('/api/search/GetFilteredSearchResult', {
    method: 'POST',
    headers: header,
    body: JSON.stringify({publicable: [publicable], page, resultCount: 10, validated: !invalidated, sortBy: [sorbObj]} )
  }).then(response => response.json())
    .then((result) => { setSongList(result);
                        setMaxPage(result[0] !== undefined ? result[0].MaxPage : 1);
  });
}


export default SongList;
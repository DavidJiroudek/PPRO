using Autofac;
using Autofac.Extensions.DependencyInjection;
using Blivnik.Net.API.App_Codes;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Blivnik.Net.API.App_Codes.Maintenance;

namespace BlivnikNetAPI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostEnvironment Env { get; }
        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpContextAccessor();
            services.AddOptions();
            services.AddAutofac();
            services.AddControllers();
            services.AddDistributedMemoryCache();
            services.AddSession();
            services.AddHeaderPropagation(options =>
            {
                // forward the 'x-test-features' if present.
                options.Headers.Add("x-test-features");
                options.Headers.Add("X-B3-TraceId");
            });
            services.AddSwaggerGen(c =>
            {
                ///internal documentation propagation, usage is mainly to show enums meaning
                //c.DocumentFilter<SwaggerAddEnumDescriptions>();
                //var filePath = Path.Combine(System.AppContext.BaseDirectory, "Blivnik.Net.API.xml");
                //c.IncludeXmlComments(filePath);
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "BlivnikNetAPI", Version = "v1" });
            });
            services.AddControllers()
                .AddJsonOptions(opts =>
                {
                    opts.JsonSerializerOptions.PropertyNamingPolicy = null;
                    opts.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                });
            services.AddMvc();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(cfg =>
                {
                    cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;

                    cfg.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("qq22ZXqYDc&9ilps7@")),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
            services.AddCors(opts =>
            {
                opts.AddPolicy("AllowAll", builder =>
                {
                    builder.AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    //.AllowCredentials();
                });
            });
            services.AddControllersWithViews();
        }
        public void ConfigureContainer(ContainerBuilder builder)
        {
            // Register your own things directly with Autofac
            DependencyConfig.Initialize(builder, Env, Configuration);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

            }
            //custom logging
            app.UseMiddleware<RequestResponseLoggerMiddleware>();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "BlivnikNetAPI v1"));

            //app.UseHttpsRedirection();
            app.UseSession();
            app.UseHeaderPropagation();
            app.UseRouting();
            app.UseCors("AllowAll");


            app.UseAuthentication();
            app.UseAuthorization();


            app.Use(async (context, next) =>
            {
                await next();
                if (context.Response.StatusCode == 401)
                {
                    context.Response.Redirect("/error401");
                    return;

                }
                if (context.Response.StatusCode == 404)
                {
                    context.Response.Redirect("/error404");
                    return;

                }
            });


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //frontend routes
                endpoints.MapControllerRoute(
                    name: "editor",
                    pattern: "editor/{id?}",
                        new { controller = "Home", action = "Index" });

                endpoints.MapControllerRoute(
                    name: "songList",
                    pattern: "songList",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "spotifyCallback",
                    pattern: "spotifyCallback",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "song",
                    pattern: "song/{id?}",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "authorList",
                    pattern: "authorList",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "author",
                    pattern: "author/{id?}",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "users",
                    pattern: "users",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "mySongs",
                    pattern: "mySongs",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "bookGenerator",
                    pattern: "bookGenerator",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "error401",
                    pattern: "error401",
                        new { controller = "Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "error404",
                    pattern: "error404",
                        new { controller = "Home", action = "Index" });
                //BackendRoutes
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
                //Error routes
            });


            app.UseStaticFiles();
        }
    }
}

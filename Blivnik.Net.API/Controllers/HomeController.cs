﻿using Microsoft.AspNetCore.Mvc;

namespace Blivnik.Net.API.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(string url)
        {
            return View();
        }
    }
}

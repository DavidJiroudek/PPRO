﻿using Blivnik.Net.API.Models.Objects.Requests.Users;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http;
using Blivnik.Database.SQL;
using Autofac;
using System.Data.SqlClient;
using Blivnik.Net.API.Models.Objects.Responces.Users;
using Blivnik.Net.API.Models.Objects;
using Blivnik.Net.API.Models.Enums;
using Newtonsoft.Json;
using Blivnik.Net.API.Handlers;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace Blivnik.Net.API.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/user/")]
    public class UserController : Controller
    {
        private IDataReaderContextFactory _dataReader;
        private IComponentContext _context;
        public UserController(IDataReaderContextFactory dataReader, IComponentContext context)
        {
            _dataReader = dataReader;
            _context = context;
        }


        [HttpPost, Route("Login")]
        public async Task<IActionResult> Login(LoginRequest request)
        {
            UserData userData;
            if (request.OAuthProvider == OAuthProvider.Google)
            {

                GoogleJsonWebSignature.ValidationSettings validationSettings;

                validationSettings = new GoogleJsonWebSignature.ValidationSettings
                {
                    Audience = new string[] { "118452082258-uc420e1oksqifgao1ck5vuk3vvj0f2kn.apps.googleusercontent.com" }
                };


                GoogleJsonWebSignature.Payload rofl;
                try
                {
                    rofl = GoogleJsonWebSignature.ValidateAsync(request.Token, validationSettings).Result;
                }
                catch (Exception e)
                {
                    //add logging
                    return new BadRequestObjectResult("Wrong access_token");
                }
                userData = new UserData(rofl.Name, rofl.Email, rofl.Picture, (int)UserType.Visitor);

            }
            else
            {
                var client = new HttpClient();
                var fbResponse = await client.Send(new HttpRequestMessage(HttpMethod.Get, "https://graph.facebook.com/debug_token?input_token=" + request.Token + "&access_token=355443926753265|720fa17a7536ef98696e6178d359d0f7")).Content.ReadAsStringAsync();
                var jsonFbResponse = JObject.Parse(fbResponse);
                var isValid = bool.Parse(jsonFbResponse["data"]["is_valid"].ToString());
                if (!isValid)
                {
                    return new BadRequestObjectResult("Wrong access_token");
                }

                var fbResponseUserData = await client.Send(new HttpRequestMessage(HttpMethod.Get, "https://graph.facebook.com/" + jsonFbResponse["data"]["user_id"].ToString() + "?fields=name,email,picture&access_token=" + request.Token)).Content.ReadAsStringAsync();
                var fbData = JObject.Parse(fbResponseUserData);


                //userData = new UserData(jsonFbResponse.Name, jsonFbResponse.Email, rofl.Picture, UserType.Visitor, String.Empty);
                userData = new UserData(
                                        fbData["name"].ToString(),
                                        fbData["email"].ToString(),
                                        fbData["picture"]["data"]["url"].ToString(),
                                        (int)UserType.Visitor);
            }


            var userParam = userData.Name.CreateParameter("@userName");
            var emailParam = userData.Email.CreateParameter("@email");

            var reader = _dataReader.GetReaderContext("dbo.Login", System.Data.CommandType.StoredProcedure, userParam, emailParam);

            var result = reader.CreateCompositeObject<LoginResponse>(obj =>
            {
                return (
                    obj.CreateObject(data => new LoginResponse()
                    {
                        Id = data.GetInt32("Id"),
                        Permission = data.GetInt32("Permissions")
                    })
                );
            });

            userData.Permission = result.Permission;
            var claims = new[]
                  {
                    new Claim(JwtRegisteredClaimNames.Sub, result.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Name, userData.Name),
                    new Claim(JwtRegisteredClaimNames.Email, userData.Email),
                    new Claim("picture", userData.Picture),
                    new Claim("permission", userData.Permission.ToString())
                      };


            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("qq22ZXqYDc&9ilps7@"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(null,
              null,
              claims,
              expires: DateTime.Now.AddSeconds(55 * 60),
              signingCredentials: creds);



            var resultToken = new JwtSecurityTokenHandler().WriteToken(token);
            Response.Cookies.Append("X-AccessToken", resultToken, new CookieOptions { HttpOnly = true, SameSite = SameSiteMode.Strict, Expires = DateTime.Now.AddSeconds(55 * 60) });

            return new OkObjectResult(resultToken);


        }


        [Authorize]
        [HttpPost, Route("GetAllUsers")]
        public IActionResult GetAllUsers([FromHeader] string Authorization)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Admin);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }
            var reader = _dataReader.GetReaderContext("SELECT [Id], [Name], [Email], [Permissions] FROM dbo.[User]", System.Data.CommandType.Text);
            var result = reader.CreateCompositeObject<IEnumerable<UserData>>(obj =>
            {
                return (
                    obj.CreateCollection(data => new UserData()
                    {
                        Id = data.GetInt32("Id"),
                        Name = data.GetString("Name"),
                        Email = data.GetString("Email"),
                        Permission = data.GetInt32("Permissions")
                    })
                );
            });
            return new OkObjectResult(result);


        }
        [Authorize]
        [HttpPost, Route("ChangeUserPermission")]
        public IActionResult ChangeUserPermission([FromHeader] string Authorization, ChangeUserPermissionRequest request)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Admin);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }
            var userIdParam = request.Id.CreateParameter("@userId");
            var permissionParam = request.Permission.CreateParameter("@permission");
            var reader = _dataReader.GetReaderContext("UPDATE dbo.[User] SET [Permissions] = @permission WHERE Id = @userId ", System.Data.CommandType.Text, permissionParam, userIdParam);
            reader.Execute();
            return new OkResult();

        }


        [HttpGet, Route("IsSignedIn")]
        public IActionResult IsSignedIn()
        {
            if (Request.Cookies["X-AccessToken"] == null)
            {
                return new BadRequestObjectResult("Access token is not valid or expired");

            }

            var value = Request.Cookies["X-AccessToken"];
            return new OkObjectResult(value);


        }

        [HttpGet, Route("LogOut")]
        public IActionResult LogOut()
        {
            Response.Cookies.Delete("X-AccessToken");

            return new OkResult();


        }
    }
}

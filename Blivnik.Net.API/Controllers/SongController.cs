﻿using Autofac;
using Blinvik.Search.ElasticSearch;
using Blinvik.Search.ElasticSearch.Models;
using Blinvik.Search.Models;
using Blivnik.Caching;
using Blivnik.Database.Calls.Songs;
using Blivnik.Database.Models;
using Blivnik.Database.SQL;
using Blivnik.Net.API.Handlers;
using Blivnik.Net.API.Models.Enums;
using Blivnik.Net.API.Models.Objects;
using Blivnik.Net.API.Models.Objects.Requests.Songs;
using Blivnik.Net.API.Models.Objects.Responses.Songs;
using iText.Layout.Properties;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Blivnik.Net.API.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/song/")]
    public class SongController
    {
        private IDataReaderContextFactory _dataReader;
        private IComponentContext _context;
        private IKeyValueStore _cache;
        private ISearchCommunication _elastic;
        public SongController(IDataReaderContextFactory dataReader, IComponentContext context, ISearchCommunication elastic)
        {
            _dataReader = dataReader;
            _context = context;
            _cache = _context.ResolveKeyed<IKeyValueStore>(CacheType.SongCache);
            _elastic = elastic;

        }
        [Authorize]
        [HttpPost, Route("SaveSong")]
        public async Task<IActionResult> SaveSong([FromHeader] string Authorization, SaveSongRequest request)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Creator);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }
            var sql = new SaveSongCall(_dataReader);
            var songId = sql.SaveSong(request.ToSong(user.Value.userId));

            var elasticSql = new GetElasticSong(_dataReader);
            var elasticSong = elasticSql.GetSong(songId);

            await _elastic.IndexItem<ElasticSong>(elasticSong);
            return new OkResult();
        }



        [AllowAnonymous]
        [HttpPost, Route("GetSongList")]
        public IActionResult GetSongList([FromHeader] string Authorization, GetSongListRequest request)
        {


            (int? userId, UserType permission) user = (null, UserType.Visitor);
            if (Authorization != null)
            {
                user = UserRoleHandler.GetUserIdAndRole(Authorization);

            }
            IEnumerable<GetSongListResponse> response;
            int maxPage;

            var key = string.Format("SongLists:{0}:{1}:{2}:{3}:{4}", request.Publicable, request.Validated, request.ResultCount, request.SortBy, request.Page);
            var cacheValues = _cache.GetValue<IEnumerable<GetSongListResponse>>(key);
            if (cacheValues != null)
            {
                response = cacheValues.Where(i => PermissionHandler.IsPermited(user.permission, i.Validated, i.Publicable));
                maxPage = (response.Count() / request.ResultCount) + 1;
                return new OkObjectResult(new { response, maxPage });
            }

            var reader = _dataReader.GetReaderContext("SELECT [SongId], [Name], [Publicable], [Validated], [ViewCount] FROM dbo.[Version] WHERE IsObsolete != 1", System.Data.CommandType.Text);
            var result = reader.CreateCompositeObject<IEnumerable<GetSongListResponse>>(obj =>
            {
                return (
                    obj.CreateCollection(data => new GetSongListResponse()
                    {
                        SongId = data.GetInt32("SongId"),
                        Name = data.GetString("Name"),
                        Publicable = data.GetInt32("Publicable"),
                        Validated = data.GetBoolean("Validated"),
                        ViewCount = data.GetInt32("ViewCount")
                    })
                );
            });


            var filteredResult = result.FilterByRequest(request);
            _cache.SetValueAbandonTask(key, filteredResult);

            response = filteredResult.Where(i => PermissionHandler.IsPermited(user.permission, i.Validated, i.Publicable));

            maxPage = (response.Count() / request.ResultCount) + 1;
            return new OkObjectResult(new { response, maxPage });
        }



        [Authorize]
        [HttpPost, Route("GetUserSongList")]
        public IActionResult GetUserSongList([FromHeader] string Authorization)
        {

            var user = UserRoleHandler.GetUserIdAndRole(Authorization);


            var userIdParama = user.userId.CreateParameter("@userId");
            var reader = _dataReader.GetReaderContext("SELECT [SongId], [Name] FROM dbo.[Version] WHERE IsObsolete != 1 AND Creator = @userId", System.Data.CommandType.Text, userIdParama);
            var result = reader.CreateCompositeObject<IEnumerable<GetUserSongListResponse>>(obj =>
            {
                return (
                    obj.CreateCollection(data => new GetUserSongListResponse()
                    {
                        SongId = data.GetInt32("SongId"),
                        Name = data.GetString("Name")
                    })
                );
            });

            return new OkObjectResult(result);
        }



        [AllowAnonymous]
        [HttpPost, Route("GetSong")]
        public IActionResult GetSong([FromHeader] string Authorization, SongIdRequest request)
        {

            (int? userId, UserType permission) user = (null, UserType.Visitor);
            if (Authorization != null)
            {
                user = UserRoleHandler.GetUserIdAndRole(Authorization);
            }
            var result = GetSongData(request.SongId);


            if (result == null)
            {
                return new NotFoundResult();
            }

            if (user.userId != null && (result.Creator != user.userId || result.Editor != user.userId))
            {
                if (!PermissionHandler.IsPermited(user.permission, result.Validated.Value, result.Publicable.Value))
                {
                    return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
                }
            }
            return new OkObjectResult(new GetSongResponce(result));
        }


        [Authorize]
        [HttpPost, Route("GetSongForEdit")]
        public IActionResult GetSongForEdit([FromHeader] string Authorization, SongIdRequest request)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Moderator);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }


            var sql = new GetSongCall(_dataReader);
            var result = sql.GetSong(request.SongId);

            if (result == null)
            {
                return new NotFoundResult();
            }

            if (!PermissionHandler.IsPermited(user.Value.permission, result.Validated.Value, result.Publicable.Value))
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }



            return new OkObjectResult(new GetSongForEditResponse(result));
        }



        [Authorize]
        [HttpPost, Route("DeleteSong")]
        public async Task<IActionResult> DeleteSong([FromHeader] string Authorization, SongIdRequest request)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Admin);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }
            var elasticSql = new GetElasticSong(_dataReader);
            var elasticSong = elasticSql.GetSong(request.SongId);

            IEnumerable<int> indexes = new List<int> { elasticSong.Index };
            _elastic.DeleteItem<ElasticSong>(indexes);

            var songIdParam = request.SongId.CreateParameter("@songId");
            var reader = _dataReader.GetReaderContext("UPDATE dbo.Version SET IsObsolete = 1 WHERE SongId = @songId AND IsObsolete != 1", System.Data.CommandType.Text, songIdParam);
            reader.Execute();



            return new OkResult();
        }



        [Authorize]
        [HttpPost, Route("ValidateSong")]
        public async Task<IActionResult> ValidateSong([FromHeader] string Authorization, SongIdRequest request)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Moderator);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }

            //i think there is no need to validate whether song is 
            var songIdParam = request.SongId.CreateParameter("@songId");
            SqlDataReaderContextAsync reader;

            GetElasticSong elasticSql;
            ElasticSong elasticSong;
            IEnumerable<int> indexes;
            if (UserRoleHandler.GetUserIdAndRole(Authorization).permission == UserType.Admin)
            {
                reader = _dataReader.GetReaderContext("UPDATE dbo.Version SET Validated = 1, [Date] = GETDATE() WHERE SongId = @songId AND IsObsolete != 1", System.Data.CommandType.Text, songIdParam);
                reader.Execute();


                elasticSql = new GetElasticSong(_dataReader);
                elasticSong = elasticSql.GetSong(request.SongId);

                indexes = new List<int> { elasticSong.Index };
                _elastic.DeleteItem<ElasticSong>(indexes);


                await _elastic.IndexItem<ElasticSong>(elasticSong);
                return new OkResult();

            }


            reader = _dataReader.GetReaderContext("IF((SELECT TOP 1 Publicable FROM dbo.Version WHERE SongId = 0 AND IsObsolete != 1) <= 2) BEGIN UPDATE dbo.Version SET Validated = 1, [Date] = GETDATE() WHERE SongId = 0 AND IsObsolete != 1 END", System.Data.CommandType.Text, songIdParam);
            reader.Execute();

            elasticSql = new GetElasticSong(_dataReader);
            elasticSong = elasticSql.GetSong(request.SongId);

            indexes = new List<int> { elasticSong.Index };
            _elastic.DeleteItem<ElasticSong>(indexes);


            await _elastic.IndexItem<ElasticSong>(elasticSong);

            return new OkObjectResult(new { indexes, elasticSong });


        }



        [Authorize]
        [HttpPost, Route("ChangePublicable")]
        public async Task<IActionResult> ChangePublicable([FromHeader] string Authorization, (int songId, int publicable) request)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Moderator);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }

            //check if exist
            if (!Enum.IsDefined(typeof(PublicableType), request.publicable))
            {
                return new BadRequestResult();
            }

            var songIdParam = request.songId.CreateParameter("@songId");
            var publicableParam = request.publicable.CreateParameter("@publicable");
            if (UserRoleHandler.GetUserIdAndRole(Authorization).permission == UserType.Admin)
            {
                var reader = _dataReader.GetReaderContext("UPDATE dbo.Version SET Publicable = @publicable WHERE SongId = @songId AND IsObsolete != 1", System.Data.CommandType.Text, songIdParam, publicableParam);
                reader.Execute();

                var elasticSql = new GetElasticSong(_dataReader);
                var elasticSong = elasticSql.GetSong(request.songId);

                IEnumerable<int> indexes = new List<int> { elasticSong.Index };
                _elastic.DeleteItem<ElasticSong>(indexes);

                await _elastic.IndexItem<ElasticSong>(elasticSong);
                return new OkResult();

            }
            else if (request.publicable <= (int)PublicableType.Invisible)
            {
                var reader = _dataReader.GetReaderContext("IF((SELECT Publicable FROM dbo.Version WHERE SongId = @songId AND IsObsolete != 1) > 3) BEGIN UPDATE dbo.Version SET Publicable = @publicable WHERE SongId = @songId AND IsObsolete != 1 END", System.Data.CommandType.Text, songIdParam, publicableParam);
                reader.Execute();

                var elasticSql = new GetElasticSong(_dataReader);
                var elasticSong = elasticSql.GetSong(request.songId);

                IEnumerable<int> indexes = new List<int> { elasticSong.Index };
                _elastic.DeleteItem<ElasticSong>(indexes);

                await _elastic.IndexItem<ElasticSong>(elasticSong);
                return new OkResult();
            }

            return new BadRequestResult();
        }



        [Authorize]
        [HttpPost, Route("EditSong")]
        public async Task<IActionResult> EditSong([FromHeader] string Authorization, EditSongRequest request)
        {

            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Creator);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }
            var sql = new EditSongCall(_dataReader);
            sql.EditSong(request.ToSong(user.Value.userId));


            var elasticSql = new GetElasticSong(_dataReader);
            var elasticSong = elasticSql.GetSong(request.SongId);

            IEnumerable<int> indexes = new List<int> { elasticSong.Index };
            _elastic.DeleteItem<ElasticSong>(indexes);

            await _elastic.IndexItem<ElasticSong>(elasticSong);

            return new OkResult();
        }

        [AllowAnonymous]
        [HttpPost, Route("IncreseViewCount")]
        public IActionResult IncreseViewCount(SongIdRequest request)
        {
            var songIdParam = request.SongId.CreateParameter("@songId");
            var reader = _dataReader.GetReaderContext("UPDATE dbo.Version SET ViewCount = ViewCount + 1 WHERE SongId = @songId AND IsObsolete != 1", System.Data.CommandType.Text, songIdParam);
            reader.Execute();

            return new OkResult();
        }

        [Authorize]
        [HttpPost, Route("SaveRating")]
        public IActionResult SaveRating([FromHeader] string Authorization, SaveRatingRequest request)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Visitor);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }

            var userIdParam = request.UserId.CreateParameter("@userId");
            var songIdParam = request.SongId.CreateParameter("@songId");
            var ratioParam = request.Ratio.CreateParameter("@ratio");

            var reader = _dataReader.GetReaderContext("dbo.InsertRating", System.Data.CommandType.StoredProcedure, userIdParam, songIdParam, ratioParam);
            reader.Execute();

            return new OkResult();
        }

        [AllowAnonymous]
        [HttpPost, Route("GetRating")]
        public IActionResult GetRating(SongIdRequest request)
        {

            var songIdParam = request.SongId.CreateParameter("@songID");
            var reader = _dataReader.GetReaderContext("SELECT AVG(Cast([Ratio] as float)) as Average FROM dbo.[Rating] WHERE SongID = @songID", System.Data.CommandType.Text, songIdParam);


            var result = reader.CreateCompositeObject(obj =>
            {
                return
                    obj.CreateObject(data =>

                    data.GetNullableDouble("Average")
                    )
                ;
            });
            return new OkObjectResult(new {rating = result});
        }

        [Authorize]
        [HttpPost, Route("HasVotedRating")]
        public IActionResult HasVotedRating([FromHeader] string Authorization, SongIdRequest request)
        {

            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Visitor);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }


            var songIdParam = request.SongId.CreateParameter("@songID");
            var userIdParam = user.Value.userId.CreateParameter("@userId");
            var reader = _dataReader.GetReaderContext("SELECT COUNT(*) as hasVoted FROM dbo.[Rating] WHERE SongID = @songID AND UserId = @userId", System.Data.CommandType.Text, songIdParam, userIdParam);


            var result = reader.CreateCompositeObject(obj =>
            {
                return
                    obj.CreateObject(data =>

                    data.GetInt32("hasVoted")
                    )
                ;
            });
            return new OkObjectResult(new { hasVoted = Convert.ToBoolean(result) });
        }






        private Song GetSongData(int songId)
        {
            var key = string.Format("Song:{0}", songId);
            var cacheValues = _cache.GetValue<Song>(key);

            if (cacheValues != null)
            {
                return cacheValues;
            }
            else
            {
                var sql = new GetSongCall(_dataReader);
                return sql.GetSong(songId);
            }
        }

    }
}

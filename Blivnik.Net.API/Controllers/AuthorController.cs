﻿using Autofac;
using Blivnik.Database.SQL;
using Blivnik.Net.API.Handlers;
using Blivnik.Net.API.Models.Enums;
using Blivnik.Net.API.Models.Objects.Requests.Authors;
using Blivnik.Net.API.Models.Objects.Responses.Authors;
using Blivnik.Net.API.Models.Objects.Responses.Songs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Blivnik.Net.API.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/author/")]
    public class AuthorController
    {

        private IDataReaderContextFactory _dataReader;
        private IComponentContext _context;
        public AuthorController(IDataReaderContextFactory dataReader, IComponentContext context)
        {
            _dataReader = dataReader;
            _context = context;
        }
        [AllowAnonymous]
        [HttpPost, Route("GetAuthorList")]
        public IActionResult GetAuthorList()
        {
            var reader = _dataReader.GetReaderContext("SELECT [Id], [Name] FROM dbo.[Author]", System.Data.CommandType.Text);
            var result = reader.CreateCompositeObject<IEnumerable<GetAuthorListResponse>>(obj =>
            {
                return (
                    obj.CreateCollection(data => new GetAuthorListResponse()
                    {
                        AuthorId = data.GetInt32("Id"),
                        Name = data.GetString("Name")
                    })
                );
            });

            return new OkObjectResult(result);
        }


        [AllowAnonymous]
        [HttpPost, Route("GetAuthorsSongList")]
        public IActionResult GetAuthorsSongList([FromHeader] string Authorization, GetAuthorsSongListRequest request)
        {

            (int? userId, UserType permission) user = (null, UserType.Visitor);
            if (Authorization != null)
            {
                user = UserRoleHandler.GetUserIdAndRole(Authorization);
            }

            var authorIdParam = request.AuthorId.CreateParameter("@authorId");
            var reader = _dataReader.GetReaderContext("dbo.GetAuthorsSongList", System.Data.CommandType.StoredProcedure, authorIdParam);
            var result = reader.CreateCompositeObject<IEnumerable<GetAuthorsSongListResponse>>(obj =>
            {
                return (
                    obj.CreateCollection(data => new GetAuthorsSongListResponse()
                    {
                        SongId = data.GetInt32("SongId"),
                        Name = data.GetString("Name"),
                        Publicable = data.GetInt32("Publicable"),
                        Validated = data.GetBoolean("Validated")
                    })
                );
            });

            var filteredResult = result.Where(i => PermissionHandler.IsPermited(user.permission, i.Validated, i.Publicable));

            return new OkObjectResult(filteredResult);
        }
    }
}

﻿using Autofac;
using Blinvik.Search.ElasticSearch;
using Blinvik.Search.ElasticSearch.Models;
using Blinvik.Search.Models.Filters;
using Blivnik.Caching;
using Blivnik.Database.Calls.Songs;
using Blivnik.Database.SQL;
using Blivnik.Net.API.Handlers;
using Blivnik.Net.API.Models.Enums;
using Blivnik.Net.API.Models.Objects.Requests.Search;
using Blivnik.Net.API.Models.Objects.Responses.Authors;
using Blivnik.Net.API.Models.Objects.Responses.Search;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Blivnik.Net.API.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/json")]
    [Route("api/search/")]
    public class SearchController
    {

        private IDataReaderContextFactory _dataReader;
        private IComponentContext _context;
        private ISearchCommunication _elastic;
        private IKeyValueStore _cache;
        public SearchController(IDataReaderContextFactory dataReader, IComponentContext context, ISearchCommunication elastic)
        {
            _dataReader = dataReader;
            _context = context;
            _elastic = elastic;
            _cache = _context.ResolveKeyed<IKeyValueStore>(CacheType.SongCache);
        }

        [HttpGet, Route("TestDevelopmentStackCall")]
        [AllowAnonymous]
        public async Task<string> TestDevelopmentStackCall() {
            var retVal = new
            {
                Env = Environment.GetEnvironmentVariables()
            };
            //try DB
            Random rd = new Random();
            var reader = _dataReader.GetReaderContext("INSERT INTO dbo.testTable (random1) VALUES (" + rd.Next(100, 200) + ")", System.Data.CommandType.Text);
            reader.Execute();
            //try cache
            var cache = _context.ResolveKeyed<IKeyValueStore>(CacheType.SongCache);
            string cacheName = "test";
            cache.SetValue(cacheName, "randomValue");
            var value = cache.GetValue<string>(cacheName);
            return JsonConvert.SerializeObject(retVal);

        }



        [Authorize]
        [HttpPost, Route("GetWhispererData")]
        public async Task<IActionResult> GetWhispererData([FromHeader] string Authorization)
        {
            var user = UserRoleHandler.IsUserPermited(Authorization, UserType.Creator);
            if (user == null)
            {
                return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            }
            var reader = _dataReader.GetReaderContext("SELECT [Id], [Name] FROM dbo.[Author]", System.Data.CommandType.Text);
            var result = reader.CreateCompositeObject<IEnumerable<GetAuthorListResponse>>(obj =>
            {
                return (
                    obj.CreateCollection(data => new GetAuthorListResponse()
                    {
                        AuthorId = data.GetInt32("Id"),
                        Name = data.GetString("Name")
                    })
                );
            });

            return new OkObjectResult(result);

        }
        [AllowAnonymous]
        [HttpPost, Route("GetFulltextSearchResult")]
        public async Task<IActionResult> GetFulltextSearchResult([FromHeader] string Authorization, GetFulltextRequest request)
        {
            if (string.IsNullOrEmpty(request.Fulltext)) {
                return new OkObjectResult(null);
            }
            (int? userId, UserType permission) user = (null, UserType.Visitor);
            if (Authorization != null)
            {
                user = UserRoleHandler.GetUserIdAndRole(Authorization);

            }
            //perform fulltext search
            var result = await _elastic.Query(ElasticSong.PrepareFilterFulltextSearch(request.Fulltext));

            var data = result.Results.Where(i => PermissionHandler.IsPermited(user.permission, i.Item.Validated, i.Item.Publicable));

            return new OkObjectResult(data.Select(i => new GetFulltextResponse<ElasticSong>(i)));

        }

        [AllowAnonymous]
        [HttpPost, Route("GetFilteredSearchResult")]
        public async Task<IActionResult> GetFilteredSearchResult([FromHeader] string Authorization, GetFilteredSongsRequest filter)
        {
            
            (int? userId, UserType permission) user = (null, UserType.Visitor);
            if (Authorization != null)
            {
                user = UserRoleHandler.GetUserIdAndRole(Authorization);

            }


            IEnumerable<GetFulltextResponse<ElasticSong>> data;

            var key = string.Format("SongLists:{0}:{1}:{2}:{3}:{4}:{5}",
                            String.Join(",", filter.Publicable.Select(i => i.ToString())),
                            filter.Validated,
                            filter.ResultCount,
                            filter.Page,
                            String.Join(",", filter.SortBy.Select(i => i.Field.ToString() + i.Direction.ToString())),
                            filter.AuthorIds != null ? String.Join(",", filter.AuthorIds.Select(i => i.ToString())) : "");

            var cacheValues = _cache.GetValue <IEnumerable<GetFulltextResponse<ElasticSong>>> (key);
            if (cacheValues != null)
            {
                data = cacheValues.Where(i => PermissionHandler.IsPermited(user.permission, i.Item.Validated, i.Item.Publicable));
                return new OkObjectResult(data);
            }

            //perform fulltext search
            var result = await _elastic.Query(ElasticSong.PrepareFilterDetailSearch(filter.ToAuthorFilter()));
            var maxPage = (result.Results.Count() % filter.ResultCount) != 0 ? (result.Results.Count() / filter.ResultCount) + 1 : (result.Results.Count() / filter.ResultCount);

            var pagedResult = result.Results.Skip(filter.ResultCount * (filter.Page - 1)).Take(filter.ResultCount).Select(i => new GetFulltextResponse<ElasticSong>(i, maxPage)); ;
            _cache.SetValueAbandonTask(key, pagedResult);


            data = pagedResult.Where(i => PermissionHandler.IsPermited(user.permission, i.Item.Validated, i.Item.Publicable));

            return new OkObjectResult(data);

        }

        [AllowAnonymous]
        [HttpPost, Route("GetAuthorFulltextResult")]
        public async Task<IActionResult> GetAuthorFulltextResult(GetFulltextRequest request)
        {

            //perform fulltext search
            var result = await _elastic.Query(ElasticAuthor.PrepareFilterFulltextSearch(request.Fulltext));

            return new OkObjectResult(result.Results.Select(i => new GetFulltextResponse<ElasticAuthor>(i)));

        }
        [AllowAnonymous]
        [HttpPost, Route("GetFilteredAuthorSearchResult")]
        public async Task<IActionResult> GetFilteredAuthorSearchResult([FromHeader] string Authorization, GetFilteredAuthorsRequest filter)
        {

            (int? userId, UserType permission) user = (null, UserType.Visitor);
            if (Authorization != null)
            {
                user = UserRoleHandler.GetUserIdAndRole(Authorization);

            }


            IEnumerable<GetFulltextResponse<ElasticAuthor>> data;

            var authorFilter = filter.ToAuthorFilter();
            switch (user.permission)
            {
                case UserType.Visitor:
                    authorFilter.MinimalPublicable = 1;
                    break;
                case UserType.Creator:
                    authorFilter.MinimalPublicable = 1;
                    break;
                case UserType.Moderator:
                    authorFilter.MinimalPublicable = 2;
                    break;
                case UserType.Admin:
                    authorFilter.MinimalPublicable = 3;
                    break;
                default:
                    authorFilter.MinimalPublicable = 1;
                    break;
            }




            var key = string.Format("SongLists:{0}:{1}:{2}:{3}", authorFilter.MinimalPublicable, filter.ResultCount, filter.Page, String.Join(",", filter.SortBy.Select(i => i.Field.ToString() + i.Direction.ToString())));
            data = _cache.GetValue<IEnumerable<GetFulltextResponse<ElasticAuthor>>>(key);
            if (data != null)
            {
                return new OkObjectResult(data);
            }
            //perform fulltext search

            var result = await _elastic.Query(ElasticAuthor.PrepareDetailSearch(authorFilter));
            var maxPage = (result.Results.Count() % filter.ResultCount) != 0 ? (result.Results.Count() / filter.ResultCount) + 1 : (result.Results.Count() / filter.ResultCount);
            data = result.Results.Skip(filter.ResultCount * (filter.Page - 1)).Take(filter.ResultCount).Select(i => new GetFulltextResponse<ElasticAuthor>(i, maxPage));


            _cache.SetValueAbandonTask(key, data);

            return new OkObjectResult(data);

        }
    }
}

﻿using Autofac;
using Blinvik.Search.ElasticSearch;
using Blivnik.Caching;
using Blivnik.Database.Calls.Songs;
using Blivnik.Database.Models;
using Blivnik.Database.SQL;
using Blivnik.Net.API.Handlers;
using Blivnik.Net.API.Models.Enums;
using Blivnik.Net.API.Models.Objects.Requests.PDF;
using Blivnik.Net.API.Models.Objects.Requests.Songs;
using Blivnik.PDFGenerator.Models.Objects;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Unicode;
using System.Threading.Tasks;

namespace Blivnik.Net.API.Controllers
{
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Produces("application/pdf")]
    [Route("api/pdf/")]
    public class PDFController
    {
        private IDataReaderContextFactory _dataReader;
        private IComponentContext _context;
        private IKeyValueStore _cache;
        private ISearchCommunication _elastic;
        //song.Date.Value.ToString("dd. MM. yyyy")



        public PDFController(IDataReaderContextFactory dataReader, IComponentContext context, ISearchCommunication elastic)
        {
            _dataReader = dataReader;
            _context = context;
            _cache = _context.ResolveKeyed<IKeyValueStore>(CacheType.SongCache);
            _elastic = elastic;
        }
        [Authorize]
        [HttpPost, Route("GenerateSingleSongPDF")]
        public async Task<IActionResult> GenerateSongPDF([FromHeader] string Authorization, SongIdRequest request)
        {
            (int? userId, UserType permission) user = (null, UserType.Visitor);
            if (Authorization != null)
            {
                user = UserRoleHandler.GetUserIdAndRole(Authorization);
            }

            var result = GetSongData(request.SongId);


            if (result == null)
            {
                return new NotFoundResult();
            }

            if (user.userId != null && (result.Creator != user.userId || result.Editor != user.userId))
            {
                if (!PermissionHandler.IsPermited(user.permission, result.Validated.Value, result.Publicable.Value))
                {
                    return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
                }
            }

            HttpClient client = new HttpClient();
            string json = JsonConvert.SerializeObject(new PDFBookFilter(result));
            try
            {
                var response = await client.PostAsync("http://host.docker.internal:85/micro/CreatePDF", new StringContent(json, Encoding.UTF8, "application/json"));
                Stream responseValue = null;
                if (response.IsSuccessStatusCode)
                {
                    responseValue = await response.Content.ReadAsStreamAsync();
                    var pdfFile = new FileStreamResult(responseValue, "application/pdf");
                    pdfFile.FileDownloadName = "Output.pdf";
                    return pdfFile;
                }
                else
                {
                    var exception = await response.Content.ReadAsStringAsync();
                    return new BadRequestObjectResult(exception);
                }

            }
            catch(Exception ex)
            {
                var exMessage = ex.Message;
            }
            return new NotFoundResult();

        }


        [Authorize]
        [HttpPost, Route("GenerateBook")]
        public async Task<IActionResult> GenerateBook([FromHeader] string Authorization, BookFilterRequest request)
        {
            (int? userId, UserType permission) user = (null, UserType.Visitor);
            if (Authorization != null)
            {
                user = UserRoleHandler.GetUserIdAndRole(Authorization);
            }

            var sql = new GetSongsForBook(_dataReader);
            var result = sql.GetSongList(request.ToBookFilter());


            if (result == null)
            {
                return new NotFoundResult();
            }

            //if (user.userId != null && (result.Creator != user.userId || result.Editor != user.userId))
            //{
            //    if (!PermissionHandler.IsPermited(user.permission, result.Validated.Value, result.Publicable.Value))
            //    {
            //        return new UnauthorizedObjectResult("Nemáte dostatečná oprávnění pro tuto akci");
            //    }
            //}

            HttpClient client = new HttpClient();
            var filter = new PDFBookFilter(result);
            string json = JsonConvert.SerializeObject(filter);
            try
            {
                var response = await client.PostAsync("http://host.docker.internal:85/micro/CreatePDF", new StringContent(json, Encoding.UTF8, "application/json"));
                Stream responseValue = null;
                if (response.IsSuccessStatusCode)
                {
                    responseValue = await response.Content.ReadAsStreamAsync();
                    var pdfFile = new FileStreamResult(responseValue, "application/pdf");
                    pdfFile.FileDownloadName = "Output.pdf";
                    return pdfFile;
                }
                else
                {
                    var exception = await response.Content.ReadAsStringAsync();
                    return new BadRequestObjectResult(exception);
                }

            }
            catch (Exception ex)
            {
                var exMessage = ex.Message;
            }
            return new NotFoundResult();

        }

        private Song GetSongData(int songId)
        {
            var key = string.Format("Song:{0}", songId);
            var cacheValues = _cache.GetValue<Song>(key);

            if (cacheValues != null)
            {
                return cacheValues;
            }
            else
            {
                var sql = new GetSongCall(_dataReader);
                return sql.GetSong(songId);
            }
        }


    }



}

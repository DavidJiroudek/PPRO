﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Blivnik.Net.API.App_Codes
{
    public class RequestResponseLoggerMiddleware
    {
        private readonly RequestDelegate _next;
        ILogger<RequestResponseLoggerMiddleware> _log;

        public RequestResponseLoggerMiddleware(RequestDelegate next, ILogger<RequestResponseLoggerMiddleware> log)
        {
            _next = next;
            _log = log;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            string javaTrackId = "";
            if (httpContext?.Request?.Headers["X-B3-TraceId"].Any() ?? false)
            {
                javaTrackId = httpContext.Request.Headers["X-B3-TraceId"].First();
            }
            // Middleware is enabled only when the EnableRequestResponseLogging config value is set.
            _log.LogInformation("API {LogType}:\n" +
                        "Method: {Method} \n" +
                        "Path: {Path} \n" +
                        "QueryString: {Query} \n" +
                        "Headers: {@Headers} \n" +
                        "Schema: {Schema}\n" +
                        "Host: {Host} \n" +
                        "TraceId: {TraceId} \n" +
                        "X-B3-TraceId: {X-B3-TraceId} \n" +
                        "RequestBody: {@RequestBody}",
                        "Request",
                    httpContext.Request.Method,
                    httpContext.Request.Path,
                    httpContext.Request.QueryString,
                    httpContext.Request.Headers,
                    httpContext.Request.Scheme,
                    httpContext.Request.Host,
                    httpContext.TraceIdentifier,
                    javaTrackId,
                    await ReadBodyFromRequest(httpContext.Request));

            // Temporarily replace the HttpResponseStream, which is a write-only stream, with a MemoryStream to capture it's value in-flight.
            var originalResponseBody = httpContext.Response.Body;
            var newResponseBody = new MemoryStream();
            httpContext.Response.Body = newResponseBody;

            // Call the next middleware in the pipeline
            await _next(httpContext);

            var responseBodyText = string.Empty;
            if (httpContext.Response.ContentType != "application/pdf")
            {
                responseBodyText = await new StreamReader(httpContext.Response.Body).ReadToEndAsync();
            }
            stopwatch.Stop();
            if ((httpContext.Response.StatusCode == 200 || httpContext.Response.StatusCode == 401) && stopwatch.ElapsedMilliseconds < 60000)
            {
                _log.LogTrace("API {LogType}:\n" +
                        "Path: {Path} \n" +
                       "StatusCode: {StatusCode} \n" +
                       "ContentType: {ContentType} \n" +
                       "Headers: {@Headers} \n" +
                       "TraceId: {TraceId} \n" +
                       "X-B3-TraceId: {X-B3-TraceId} \n" +
                       "Duration: {Duration}ms \n" +
                       "RequestBody: {@RequestBody}",
                   "Response",
                   httpContext.Request.Path,
                   httpContext.Response.StatusCode,
                   httpContext.Response.ContentType,
                   httpContext.Response.Headers,
                   httpContext.TraceIdentifier,
                   javaTrackId,
                   stopwatch.ElapsedMilliseconds,
                   responseBodyText); ;
            }
            else
            {
                _log.LogError("API {LogType}:\n" +
                        "Path: {Path} \n" +
                       "StatusCode: {StatusCode} \n" +
                       "ContentType: {ContentType} \n" +
                       "Headers: {@Headers} \n" +
                       "TraceId: {TraceId} \n" +
                       "X-B3-TraceId: {X-B3-TraceId} \n" +
                       "Duration: {Duration}ms \n" +
                       "RequestBody: {@RequestBody}",
                   "Response",
                   httpContext.Request.Path,
                   httpContext.Response.StatusCode,
                   httpContext.Response.ContentType,
                   httpContext.Response.Headers,
                   httpContext.TraceIdentifier,
                   javaTrackId,
                   stopwatch.ElapsedMilliseconds,
                   responseBodyText);
            }
            newResponseBody.Seek(0, SeekOrigin.Begin);
            await newResponseBody.CopyToAsync(originalResponseBody);
        }

        private static string FormatHeaders(IHeaderDictionary headers) => string.Join(", ", headers.Select(kvp => $"{{{kvp.Key}: {string.Join(", ", kvp.Value)}}}"));

        private static async Task<string> ReadBodyFromRequest(HttpRequest request)
        {
            // Ensure the request's body can be read multiple times (for the next middlewares in the pipeline).
            request.EnableBuffering();

            using var streamReader = new StreamReader(request.Body, leaveOpen: true);
            var requestBody = await streamReader.ReadToEndAsync();

            // Reset the request's body stream position for next middleware in the pipeline.
            request.Body.Position = 0;
            return requestBody;
        }
    }
}

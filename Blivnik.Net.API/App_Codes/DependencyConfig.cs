﻿using Autofac;
using Blinvik.Search;
using Blinvik.Search.ElasticSearch;
using Blivnik.Caching;
using Blivnik.Database.SQL;
using Blivnik.Net.API.App_Codes.Maintenance;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System.Collections.Generic;

namespace Blivnik.Net.API.App_Codes
{
    public class DependencyConfig
    {
        public static void Initialize(ContainerBuilder cb, IHostEnvironment env, IConfiguration configuration) {
            cb.Register(c =>
            {
                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                c.Resolve<IConfiguration>().GetSection("ConnectionStrings").Bind(dictionary);
                IOptions<Dictionary<string, string>> connectionStrings = Options.Create(dictionary);
                return new DataReaderContextFactory(connectionStrings);
            }).As<IDataReaderContextFactory>().SingleInstance();

            cb.RegisterModule<SearchModule>();
            cb.RegisterModule<CachingModule>();

            cb.Register<MaintenanceService>(c => new MaintenanceService(c.Resolve<IDataReaderContextFactory>(), c.Resolve<ISearchCommunication>()))
               .As<IHostedService>()
               .SingleInstance();
        }
    }
}

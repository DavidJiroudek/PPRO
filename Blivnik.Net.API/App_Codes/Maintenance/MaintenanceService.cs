﻿using Autofac;
using Blinvik.Search.ElasticSearch;
using Blinvik.Search.ElasticSearch.Models;
using Blivnik.Database.Calls.Songs;
using Blivnik.Database.SQL;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Blivnik.Net.API.App_Codes.Maintenance
{
    public class MaintenanceService : BackgroundService
    {
        private IDataReaderContextFactory _dataReader;
        private ISearchCommunication _elastic;

        public MaintenanceService(IDataReaderContextFactory dataReader, ISearchCommunication elastic) {
            _dataReader = dataReader;
            _elastic = elastic;
            //run on startup
            OnTimerFiredAsync(new CancellationToken()).Wait();
        }
        private static int SecondsUntilMidnight()
        {
            return (int)(DateTime.Today.AddDays(1.0) - DateTime.Now).TotalSeconds;
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var countdown = SecondsUntilMidnight();

            while (!stoppingToken.IsCancellationRequested)
            {
                if (countdown-- <= 0)
                {
                    try
                    {
                        await OnTimerFiredAsync(stoppingToken);
                    }
                    catch (Exception ex)
                    {
                        // TODO: log exception
                    }
                    finally
                    {
                        countdown = SecondsUntilMidnight();
                    }
                }
                await Task.Delay(1000, stoppingToken);
            }
        }

        public async Task OnTimerFiredAsync(CancellationToken stoppingToken)
        {
            var songLoader = new GetSongListElastic(_dataReader);
            _elastic.DeleteIndex<ElasticSong>();
            _elastic.CreateIndex<ElasticSong>();
            await _elastic.IndexItemBulk<ElasticSong>(songLoader.GetSongList());

            var authorLoader = new GetAuthorListElastic(_dataReader);
            _elastic.DeleteIndex<ElasticAuthor>();
            _elastic.CreateIndex<ElasticAuthor>();
            await _elastic.IndexItemBulk<ElasticAuthor>(authorLoader.GetAuthorList());
        }
    }
}

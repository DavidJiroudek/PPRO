﻿using Blinvik.Search.Models;
using Blivnik.Net.API.Models.Enums;

namespace Blivnik.Net.API.Handlers
{
    public static class PermissionHandler
    {

        public static bool IsPermited(UserType role, bool validated, int publicable) {

            switch (role)
            {
                case UserType.Visitor:
                    if (validated && publicable <= (int)PublicableType.Unpublicable)
                    {
                        return true;
                    }
                    return false;
                case UserType.Creator:
                    if (validated && publicable <= (int)PublicableType.Unpublicable)
                    {
                        return true;
                    }
                    return false;
                case UserType.Moderator:
                    if (publicable <= (int)PublicableType.Invisible)
                    {
                        return true;
                    }
                    return false;
                case UserType.Admin:
                    return true;
                default:
                    return false;
            }




        }
    }
}

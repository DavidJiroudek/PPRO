﻿using Blivnik.Net.API.Models.Enums;
using Blivnik.Net.API.Models.Objects.Requests.Songs;
using Blivnik.Net.API.Models.Objects.Responses.Songs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Blivnik.Net.API.Handlers
{
    public static class PageFilterHandler
    {
        public static IEnumerable<GetSongListResponse> FilterByRequest(this IEnumerable<GetSongListResponse> data, GetSongListRequest request) {

            var result = data.Where(i => i.Validated == request.Validated && i.Publicable == request.Publicable).GetOrderMethod((SortType)request.SortBy).Skip(request.ResultCount*(request.Page - 1)).Take(request.ResultCount);


            return result;
        }
        private static IEnumerable<GetSongListResponse> GetOrderMethod(this IEnumerable<GetSongListResponse> data, SortType sortBy)
        {
            switch (sortBy)
            {
                case SortType.Alphabetical:
                    return data.OrderBy(i => i.Name);
                case SortType.ReverseAplhabecical:
                    return data.OrderByDescending(i => i.Name);
                case SortType.ByPopularity:
                    return data.OrderByDescending(i => i.ViewCount);
                case SortType.Random:
                    return data.OrderBy(_ => Guid.NewGuid());
                default:
                    return null;
            }
        }
    }
}

﻿using Blivnik.Net.API.Models.Enums;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;

namespace Blivnik.Net.API.Handlers
{
    public static class UserRoleHandler
    {
        public static (int userId, UserType permission)? IsUserPermited(string token, UserType requiredPerm) {

            var handler = new JwtSecurityTokenHandler();
            var data = handler.ReadJwtToken(token.Remove(0, 7));
            if (int.Parse(data.Claims.FirstOrDefault(i => i.Type == "permission").Value) >= (int)requiredPerm)
            {
                var permission = int.Parse(data.Claims.FirstOrDefault(i => i.Type == "permission").Value);
                var userId = int.Parse(data.Claims.FirstOrDefault(i => i.Type == "sub").Value);
                return (userId, (UserType)permission);
            }

            return null;
        }

        public static (int userId, UserType permission) GetUserIdAndRole(string token)
        {

            var handler = new JwtSecurityTokenHandler();
            var data = handler.ReadJwtToken(token.Remove(0, 7));

            var permission = int.Parse(data.Claims.FirstOrDefault(i => i.Type == "permission").Value);
            var userId = int.Parse(data.Claims.FirstOrDefault(i => i.Type == "sub").Value);

            return (userId, (UserType)permission);

        }

    }
}

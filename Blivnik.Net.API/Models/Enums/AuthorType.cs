﻿namespace Blivnik.Net.API.Models.Enums
{
    public enum AuthorType
    {
        MusicAuthor,
        TextAuthor,
        Interpret
    }
}

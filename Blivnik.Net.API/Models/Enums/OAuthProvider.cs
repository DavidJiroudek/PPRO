﻿namespace Blivnik.Net.API.Models.Enums
{
    public enum OAuthProvider
    {
        Google,
        Facebook
    }
}

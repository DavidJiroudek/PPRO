﻿namespace Blivnik.Net.API.Models.Enums
{
    public enum SortType
    {   
        Alphabetical,
        ReverseAplhabecical,
        ByPopularity,
        Random
    }
}

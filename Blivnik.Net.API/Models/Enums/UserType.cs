﻿namespace Blivnik.Net.API.Models.Enums
{
    public enum UserType
    {
        Visitor,
        Creator,
        Moderator,
        Admin
    }
}

﻿using System.Collections.Generic;

namespace Blivnik.Net.API.Models.Objects.Requests.Authors
{
    public class GetAuthorsSongListRequest
    {
        public int AuthorId { get; set; }

    }
}

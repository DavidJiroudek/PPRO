﻿using Blinvik.Search.Models;
using System.Globalization;
using System;
using Blivnik.Database.Models;

namespace Blivnik.Net.API.Models.Objects.Requests.PDF
{
    public class BookFilterRequest
    {
        public string Priority { get; set; }
        public string Date { get; set; }
        public string Publicable { get; set; }
        internal BookFilter ToBookFilter()
        {
            //needs to be redone this is unacceptable
            return new BookFilter()
            {
                Priority = Int32.Parse(this.Priority),
                Date = DateTime.Parse(this.Date, new CultureInfo("en-CA")),
                Publicable = (PublicableType)Int32.Parse(this.Publicable)
            };
        }
    }
}

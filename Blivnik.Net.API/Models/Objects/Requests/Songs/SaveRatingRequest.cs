namespace Blivnik.Net.API.Models.Objects.Requests.Songs
{
    public class SaveRatingRequest
    {
        public int SongId { get; set; }
        public int UserId { get; set; }
        public int Ratio { get; set; }
    }
}

﻿namespace Blivnik.Net.API.Models.Objects.Requests.Songs
{
    public class SongIdRequest
    {
        public int SongId { get; set; }
    }
}

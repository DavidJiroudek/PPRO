﻿using Blivnik.Database.Models;
using System.Collections.Generic;
using System.Linq;

namespace Blivnik.Net.API.Models.Objects.Requests.Songs
{
    public class EditSongRequest
    {
        public int SongId { get; set; }
        public string SongName { get; set; }
        public int Creator { get; set; }
        public int Publicable { get; set; }
        public int Capo { get; set; }
        public string Url { get; set; }
        public IEnumerable<Author> Authors { get; set; }
        public int Priority { get; set; }
        public IEnumerable<int> Rythms { get; set; }
        public string Text { get; set; }
        public bool Reprint { get; set; }


        internal Song ToSong(int userId)
        {
            return new Song()
            {
                SongId = this.SongId,
                SongName = this.SongName,
                Publicable = this.Publicable,
                Capo = this.Capo,
                Url = this.Url,
                Priority = this.Priority,
                Text = this.Text,
                Authors = Authors.Select(i => (i.Name, i.Type)),
                Rythms = this.Rythms,
                Creator = this.Creator,
                Editor = userId,
                Reprint = this.Reprint
            };
        }
    }
}


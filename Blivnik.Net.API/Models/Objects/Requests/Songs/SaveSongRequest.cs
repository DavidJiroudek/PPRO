﻿using Blivnik.Database.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace Blivnik.Net.API.Models.Objects.Requests.Songs
{
    public class SaveSongRequest
    {
        public string SongName { get; set; }
        public int Publicable { get; set; }
        public int Priority { get; set; }
        public int Capo { get; set; }
        public string Url { get; set; }
        public IEnumerable<Author> Authors { get; set; }

        public IEnumerable<int> Rythms { get; set; }
        public string Text { get; set; }


        internal Song ToSong(int userId) {
            return new Song() { SongName = this.SongName,
                                Publicable = this.Publicable,
                                Priority = this.Priority,
                                Capo = this.Capo,
                                Url = this.Url,
                                Text = this.Text,
                                Authors = Authors.Select(i => (i.Name, i.Type)),
                                Rythms = this.Rythms,
                                Creator = userId
            };
        }
    }

}

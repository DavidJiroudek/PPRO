﻿namespace Blivnik.Net.API.Models.Objects.Requests.Songs
{
    public class GetAuthorsSongsRequest {
        public int Id { get; set; }
        public int SortBy { get; set; }
        public int ResultCount { get; set; }
        public int Page { get; set; }

    }
}

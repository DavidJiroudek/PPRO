﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Blivnik.Net.API.Models.Objects.Requests.Songs
{
    public class GetSongListRequest
    {
        [JsonPropertyName("sortBy")]
        public int SortBy { get; set; }
        [JsonPropertyName("resultCount")]
        public int ResultCount { get; set; }
        [JsonPropertyName("publicable")]
        public int Publicable { get; set; }
        [JsonPropertyName("page")]
        public int Page { get; set; }
        [JsonPropertyName("validated")]
        public bool Validated { get; set; }
    }
}

﻿namespace Blivnik.Net.API.Models.Objects.Requests.Users
{
    public class SetPermissionsRequest
    {
        public int Id { get; set; }
        public string Permission { get; set; }
    }
}

﻿namespace Blivnik.Net.API.Models.Objects.Requests.Users
{
    public class RegisterRequest
    {
        public string Token { get; set; }
        public string UserName { get; set; }
    }
}

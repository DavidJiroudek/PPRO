﻿namespace Blivnik.Net.API.Models.Objects.Requests.Users
{
    public class ChangeUserPermissionRequest
    {
        public int Id { get; set; }
        public int Permission { get; set; }//UserType

    }
}

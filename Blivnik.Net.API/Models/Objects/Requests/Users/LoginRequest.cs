﻿using Blivnik.Net.API.Models.Enums;

namespace Blivnik.Net.API.Models.Objects.Requests.Users
{
    public class LoginRequest
    {
        public OAuthProvider OAuthProvider { get; set; }
        public AppType AppType { get; set; }
        public string Token { get; set; }


    }
}

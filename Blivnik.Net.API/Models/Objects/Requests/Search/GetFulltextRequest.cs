﻿using System.Text.Json.Serialization;

namespace Blivnik.Net.API.Models.Objects.Requests.Search
{
    public class GetFulltextRequest
    {
        [JsonPropertyName("fulltext")]
        public string Fulltext { get; set; }
    }
}

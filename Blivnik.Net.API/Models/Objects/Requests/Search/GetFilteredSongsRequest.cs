﻿using Blinvik.Search.Models;
using Blinvik.Search.Models.Filters;
using System.Linq;
using System.Text.Json.Serialization;

namespace Blivnik.Net.API.Models.Objects.Requests.Search
{
    public class GetFilteredSongsRequest
    {
        [JsonPropertyName("publicable")]
        public PublicableType[] Publicable { get; set; }
        public bool? Validated { get; set; }
        [JsonPropertyName("sortBy")]
        public SortByFilter[] SortBy { get; set; }
        [JsonPropertyName("authorIds")]
        public int[] AuthorIds { get; set; }
        [JsonPropertyName("resultCount")]
        public int ResultCount { get; set; }
        [JsonPropertyName("page")]
        public int Page { get; set; }



        public SongFilter ToAuthorFilter()
        {
            return new SongFilter()
            {
                Publicable = Publicable,
                Validated = Validated,
                AuthorIds = AuthorIds,
                SortBy = SortBy.Select(i => (i.Field, i.Direction)).ToArray()
            };
        }
        public SongFilter ToAuthorFilter(int userId)
        {
            return new SongFilter()
            {
                Publicable = Publicable,
                Validated = Validated,
                SortBy = SortBy.Select(i => (i.Field, i.Direction)).ToArray(),
                AuthorIds = AuthorIds,
                CreatorId = userId
            };
        }
    }
}

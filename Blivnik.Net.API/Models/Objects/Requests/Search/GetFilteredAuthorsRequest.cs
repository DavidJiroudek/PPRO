﻿using Blinvik.Search.Models;
using Blinvik.Search.Models.Filters;
using System.Linq;
using System.Text.Json.Serialization;

namespace Blivnik.Net.API.Models.Objects.Requests.Search
{
    public class GetFilteredAuthorsRequest
    {
        [JsonPropertyName("sortBy")]
        public SortByFilter[] SortBy { get; set; }
        [JsonPropertyName("resultCount")]
        public int ResultCount { get; set; }
        [JsonPropertyName("page")]
        public int Page { get; set; }



        public AuthorFilter ToAuthorFilter()
        {
            return new AuthorFilter()
            {
                SortBy = SortBy.Select(i => (i.Field, i.Direction)).ToArray()
            };
        }
    }

}

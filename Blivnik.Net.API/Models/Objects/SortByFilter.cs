﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace Blivnik.Net.API.Models.Objects
{
    public class SortByFilter
    {
        [JsonPropertyName("field")]
        public string Field { get; set; }
        [JsonPropertyName("direction")]
        public ListSortDirection Direction { get; set; }
    }
}

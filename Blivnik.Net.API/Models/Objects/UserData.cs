﻿using Blivnik.Net.API.Models.Enums;

namespace Blivnik.Net.API.Models.Objects
{
    public class UserData
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Picture { get; set; }
        public int Permission { get; set; }//UserType

        public UserData(int id, string name, string email, string picture, int permission)
        {
            Id = id;
            Name = name;
            Email = email;
            Picture = picture;
            Permission = permission;
        }
        public UserData( string name, string email, string picture, int permission)
        {
            Id = null;
            Name = name;
            Email = email;
            Picture = picture;
            Permission = permission;
        }

        public UserData()
        {
        }
    }
}

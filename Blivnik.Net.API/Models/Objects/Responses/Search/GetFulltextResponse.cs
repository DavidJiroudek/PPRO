﻿using Blinvik.Search.ElasticSearch.Models;

namespace Blivnik.Net.API.Models.Objects.Responses.Search
{
    public class GetFulltextResponse<T>
    {
        public T Item { get; set; }
        public double? Score { get; set; }
        public string[] Highlight { get; set; }
        public int MaxPage { get; set; }

        public GetFulltextResponse((T item, double? score, string[] highlight) data, int maxPage = 0)
        {
            Item = data.item;
            Score = data.score;
            Highlight = data.highlight;
            MaxPage = maxPage;
        }
    }
}

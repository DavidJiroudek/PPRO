﻿namespace Blivnik.Net.API.Models.Objects.Responses.Authors
{
    public class GetAuthorListResponse
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }
    }
}

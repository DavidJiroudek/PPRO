﻿namespace Blivnik.Net.API.Models.Objects.Responses.Authors
{
    public class GetAuthorsResponce
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}

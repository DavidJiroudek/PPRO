﻿namespace Blivnik.Net.API.Models.Objects.Responces.Users
{
    public class LoginResponse
    {
        public int Id { get; set; }
        public int Permission { get; set; }
        public string Token { get; set; }
    }
}

﻿using Blivnik.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blivnik.Net.API.Models.Objects.Responses.Songs
{
    public class GetSongResponce
    {
        public int SongId { get; set; }
        public DateTime Date { get; set; }
        public string SongName { get; set; }
        public bool Validated {get;set;}
        public int Capo { get; set; }
        public string Text { get; set; }
        public IEnumerable<Author> Authors { get; set; }
        public IEnumerable<int> Rythms { get; set; }



        public GetSongResponce (Song data) {
            SongId = data.SongId.Value;
            Date = data.Date.Value;
            SongName = data.SongName;
            Validated = data.Validated.Value;
            Capo = data.Capo.Value;
            Text = data.Text;
            Authors = data.Authors.Select(i=>new Author(i.name,i.type));
            Rythms = data.Rythms;

        }

        public GetSongResponce()
        {
        }
    }
}

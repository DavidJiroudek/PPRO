﻿namespace Blivnik.Net.API.Models.Objects.Responses.Songs
{
    public class GetUserSongListResponse
    {
        public int SongId { get; set; }
        public string Name { get; set; }
    }
}

﻿namespace Blivnik.Net.API.Models.Objects.Responses.Songs
{
    public class GetAuthorsSongsResponce
    {
        public int Id { get; set; }
        public int Name { get; set; }
    }
}

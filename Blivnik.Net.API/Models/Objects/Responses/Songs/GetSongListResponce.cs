﻿namespace Blivnik.Net.API.Models.Objects.Responses.Songs
{
    public class GetSongListResponse
    {
        public int SongId { get; set; }
        public string Name { get; set; }
        public int Publicable { get; set; }
        public bool Validated { get; set; }
        public int ViewCount { get; set; }
    }
}

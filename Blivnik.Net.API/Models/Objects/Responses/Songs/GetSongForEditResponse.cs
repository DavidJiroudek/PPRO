﻿using Blivnik.Database.Models;
using System.Collections.Generic;
using System.Linq;

namespace Blivnik.Net.API.Models.Objects.Responses.Songs
{
    public class GetSongForEditResponse
    {
        public int SongId { get; set; }
        public string SongName { get; set; }
        public int Creator { get; set; }
        public int Publicable { get; set; }
        public int Priority { get; set; }
        public int Capo { get; set; }
        public string Url { get; set; }
        public string Text { get; set; }
        public IEnumerable<Author> Authors { get; set; }
        public IEnumerable<int> Rythms { get; set; }

        public GetSongForEditResponse(Song data)
        {
            SongId = data.SongId.Value;
            SongName = data.SongName;
            Creator = data.Creator.Value;
            Publicable = data.Publicable.Value;
            Priority = data.Priority.Value;
            Capo = data.Capo.Value;
            Url = data.Url;
            Text = data.Text;
            Authors = data.Authors.Select(i => new Author(i.name, i.type));
            Rythms = data.Rythms;



        }
    }

}

﻿namespace Blivnik.Net.API.Models.Objects.Responses.Songs
{
    public class GetAuthorsSongListResponse
    {
        public int SongId { get; set; }
        public string Name { get; set; }
        public int Publicable { get; set; }
        public bool Validated { get; set; }

    }
}

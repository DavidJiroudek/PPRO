﻿using System.Text.Json.Serialization;

namespace Blivnik.Net.API.Models.Objects
{
    public class Author
    {
        public Author()
        {
        }

        public Author(string name, int type)
        {
            Name = name;
            Type = type;
        }

        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("type")]
        public int Type { get; set; }

    }

}

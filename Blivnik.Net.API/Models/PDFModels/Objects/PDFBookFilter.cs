﻿using Blivnik.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blivnik.PDFGenerator.Models.Objects
{
    [Serializable]
    public class PDFBookFilter
    {
        public IEnumerable<PDFSongData> Songs { get; set; }

        public PDFBookFilter()
        {
        }

        public PDFBookFilter(Song songs)
        {
            Songs = new List<PDFSongData> { new PDFSongData(songs) };
        }
        public PDFBookFilter(IEnumerable<Song> songs)
        {
            Songs = songs.Select(i => new PDFSongData(i));
        }
    }

}

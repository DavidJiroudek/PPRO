﻿using Blivnik.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blivnik.PDFGenerator.Models.Objects
{
    [Serializable]
    public class PDFSongData
    {
        public PDFSongData() { }

        public string? Date { get; set; }
        public string? SongName { get; set; }
        public int? Capo { get; set; }
        public string? Text { get; set; }
        public string? Author { get; set; }
        public IEnumerable<int>? Rythms { get; set; }


        public PDFSongData(Song song)
        {
            Date = song.Date.Value.ToString("dd. MM. yyyy");
            SongName = song.SongName;
            Capo = song.Capo;
            Text = song.Text;
            Author = song.Authors.FirstOrDefault().name;
            Rythms = song.Rythms;
        }
    }
}

FROM mcr.microsoft.com/dotnet/sdk:6.0  AS base
WORKDIR /app
COPY . .
RUN dotnet restore
RUN dotnet publish -o /app/published-app

FROM mcr.microsoft.com/dotnet/aspnet:6.0 as runtime
WORKDIR /app
COPY --from=base /app/published-app /app
ENTRYPOINT ["dotnet", "Blivnik.Net.API.dll"]